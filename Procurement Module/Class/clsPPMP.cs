﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement_Module.Class
{
    class clsPPMP
    {

        clsData c_data = new clsData();

        public DataSet GetExpenseItems() 
        {
            var sb = new System.Text.StringBuilder(141);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mooe.id,");
            sb.AppendLine(@"mooe.mooe_id,");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mooe.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mooe");
            sb.AppendLine(@"WHERE mooe.is_active = 1");
            sb.AppendLine(@"ORDER BY mooe.id");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
     
        public DataSet GetMOOE()
        {
            var sb = new System.Text.StringBuilder(69);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures;");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetDivisionList(String _UACS)
        {
            var sb = new System.Text.StringBuilder(170);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.DivisionAccro as _accro");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfsl.division_id");
            sb.AppendLine(@"WHERE mfsl.mooe_id  ='"+_UACS +"'");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetProcurementList()
        {
            var sb = new System.Text.StringBuilder(182);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price,");
            sb.AppendLine(@"  expenditure_id");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items;");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetLocalAllowance()
        {
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.id,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '108'");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetForeignAllowance()
        {
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.id,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '109'");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetForeignPlaneRate()
        {
            var sb = new System.Text.StringBuilder(209);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.item_name as _search_code,");
            sb.AppendLine(@"CONCAT(mel.library_type,' - ',mel.item_name) as ticket");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '2' ");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetLocalPlaneRate()
        {
            var sb = new System.Text.StringBuilder(209);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.item_name as _search_code,");
            sb.AppendLine(@"CONCAT(mel.library_type,' - ',mel.item_name) as ticket");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '1' ");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetExpenseQuantity(String _ID)
        {
            var sb = new System.Text.StringBuilder(319);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.procurement_id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.travel_allowance,");
            sb.AppendLine(@"mad.rate,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"WHERE mad.status = 'FINANCE APPROVED' AND mad.mooe_sub_expenditure_id = '"+ _ID +"'");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetDivisionList()
        {

            var sb = new System.Text.StringBuilder(85);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"div.Division_Id as div_id,");
            sb.AppendLine(@"div.Division_Desc as div_name");
            sb.AppendLine(@"FROM Division div");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDataPPMP(string PAP, string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(300);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ISNULL(Uacs,'') as Uacs ,");
            sb.AppendLine(@"  ISNULL(Description,'') as Description,");
            sb.AppendLine(@"  ISNULL(Quantity,'') as Quantity,");
            sb.AppendLine(@"  ISNULL(EstimateBudget,'0') as EstimatedBudget,");
            sb.AppendLine(@"  ISNULL(ModeOfProcurement,'') as ModeOfProcurement,");
            sb.AppendLine(@"  ISNULL(Jan,'') as Jan,");
            sb.AppendLine(@"  ISNULL(Feb,'') as Feb,");
            sb.AppendLine(@"  ISNULL(Mar,'') as Mar,");
            sb.AppendLine(@"  ISNULL(Apr,'') as Apr,");
            sb.AppendLine(@"  ISNULL(May,'') as May,");
            sb.AppendLine(@"  ISNULL(Jun,'') as Jun,");
            sb.AppendLine(@"  ISNULL(Jul,'') as Jul,");
            sb.AppendLine(@"  ISNULL(Aug,'') as Aug,");
            sb.AppendLine(@"  ISNULL(Sep,'') as Sep,");
            sb.AppendLine(@"  ISNULL(Octs,'') as Octs,");
            sb.AppendLine(@"  ISNULL(Nov,'') as Nov,");
            sb.AppendLine(@"  ISNULL(Dec,'') as Dec,");
            sb.AppendLine(@"  ISNULL(Total,'') as Total,");
            sb.AppendLine(@"  ISNULL(Division,'') as Division,");
            sb.AppendLine(@"  ISNULL(Yearssss,'') as Yearssss,");
            sb.AppendLine(@"  ISNULL(PAP,'') as PAP,");
            sb.AppendLine(@"  ISNULL(approved,'') as approved,");
            sb.AppendLine(@"  ISNULL(header,'') as header,");
            sb.AppendLine(@"  ISNULL(revision,'') as revision,");
            sb.AppendLine(@"  ISNULL(overall,'') as revision");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE PAP = '" + PAP + "' AND Yearssss =" + _year + " AND Division ='Thematic Map (KMD)';");

            _dsData = new DataSet();
            _dsData = c_data.ExecuteSQLQuery(sb.ToString()); 

            dtHeader.TableName = "Header";


            return _dsData;
        }

        public DataSet LoadFundSource(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(282);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mooe.name, ");
            sb.AppendLine(@"  mfsl.Fund_Source_Id,");
            sb.AppendLine(@"  mfsl.division_id,");
            sb.AppendLine(@"  mfsl.mooe_id,");
            sb.AppendLine(@"  mfsl.Amount,");
            sb.AppendLine(@"  mfsl.Year");
            sb.AppendLine(@"FROM   dbo.mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.division_id = " + _div_id + " AND mfsl.Year = " + _year + ";");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchData(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(873);

          
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  mpe.acountable_division_code,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  div.Division_Desc,");
            sb.AppendLine(@"  div.Division_Code,");
            sb.AppendLine(@"  mooe.uacs_code,");
            sb.AppendLine(@"  mooe.name,");
            sb.AppendLine(@"  mad.total as rate,");
            sb.AppendLine(@"  mad.quantity,mad.type_service,ISNULL(mad.id,0) as act_id,");
            sb.AppendLine(@"  mad.month,");
            sb.AppendLine(@"  mad.year,");
            sb.AppendLine(@"  ISNULL(mad.entry_date,'') as entry_date,");
            sb.AppendLine(@"  ISNULL(mad.start,'') as s_date,");
            sb.AppendLine(@"  ISNULL(mad.[end],'') as e_date");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id = madp.main_activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = ma.output_id	");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = " + _div_id + " AND mad.year = " + _year + " and mad.is_suspended =0  and madp.is_submitted =0 and mfs.service_type = 1 and madp.isapproved = 1 AND mad.total !=0");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"  mpe.acountable_division_code,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  div.Division_Desc,");
            sb.AppendLine(@"  div.Division_Code,");
            sb.AppendLine(@"  mooe.uacs_code,");
            sb.AppendLine(@"  mooe.name,");
            sb.AppendLine(@"  mad.total,");
            sb.AppendLine(@"  mad.quantity,");
            sb.AppendLine(@"  mad.type_service,");
            sb.AppendLine(@"  mad.id,");
            sb.AppendLine(@"  mad.month,");
            sb.AppendLine(@"  mad.year ,mad.entry_date,");
            sb.AppendLine(@"  mad.start,");
            sb.AppendLine(@"  mad.[end]");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public Boolean ExecuteData(String _sql)
        {

            Boolean _res = c_data.ExecuteQuery(_sql);

            return _res;
        }
    }
}
