﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement_Module.Class
{
    public class clsAPP
    {
        clsData c_data = new clsData();
        public String GetData(String _year)
        {


            var sb = new System.Text.StringBuilder(1679);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'M' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mpe.project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"'' as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'S' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mooe.name as project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"''  as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");


            return sb.ToString();
        }

        public DataSet GetPrograms(string _year)
        {

            var sb = new System.Text.StringBuilder(554);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpe.id,0) as id,");
            sb.AppendLine(@"ISNULL(div.Division_Code,'-') as pap,");
            sb.AppendLine(@"ISNULL(mpe.project_name,'-') as project_name,");
            sb.AppendLine(@"ISNULL(div.DivisionAccro,'-') as pmo");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");
            sb.AppendLine(@"GROUP BY mpe.id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"mpe.project_name,");
            sb.AppendLine(@"div.DivisionAccro");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public void SaveAPP(String _code,String ref_code,String mod_procure,String ad_post_rei ,
            String sub_open_bids,String notice_award,String contract,String source,String years,String remarks
            ) 
        {
            var sb = new System.Text.StringBuilder(365);
            sb.AppendLine(@"DELETE dbo.mnda_proc_act_data WHERE code ='" + _code + "';");
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  ref_code,");
            sb.AppendLine(@"  mod_procure,");
            sb.AppendLine(@"  ad_post_rei,");
            sb.AppendLine(@"  sub_open_bids,");
            sb.AppendLine(@"  notice_award,");
            sb.AppendLine(@"  contract_signing,");
            sb.AppendLine(@"  source_funds,");
            sb.AppendLine(@"  years,");
            sb.AppendLine(@"  remarks");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _code  +"',");
            sb.AppendLine(@"  '"+ ref_code  +"',");
            sb.AppendLine(@"  '" + mod_procure +"',");
            sb.AppendLine(@"  '" + ad_post_rei +"',");
            sb.AppendLine(@"  '" + sub_open_bids +"',");
            sb.AppendLine(@"  '" + notice_award +"',");
            sb.AppendLine(@"  '" + contract +"',");
            sb.AppendLine(@"  '" + source +"',");
            sb.AppendLine(@"  '" + years +"',");
            sb.AppendLine(@"  '" + remarks +"'");
            sb.AppendLine(@");");

            c_data.ExecuteQuery(sb.ToString());
        }

        public DataSet GetModeProcurement()
        {

            var sb = new System.Text.StringBuilder(75);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  mode_procurement");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mode_procurement_lib;");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        } public DataSet GetModePROACT()
        {

            var sb = new System.Text.StringBuilder(67);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_schedule_procact_library;");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDataAPP(string _year)
        {
            DataSet dsData = new DataSet();

            StringBuilder sb = new StringBuilder(402);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpe.id,'') as _ref,");
            sb.AppendLine(@"ISNULL(mfs.Fund_Name,'') as Fund_Name,");
            sb.AppendLine(@"ISNULL(mpe.acountable_division_code,'') as Division_Id,");
            sb.AppendLine(@"ISNULL(div.Division_Desc,'') as Division_Desc,");
            sb.AppendLine(@"ISNULL(div.Division_Code,'') as Division_Code,");
            sb.AppendLine(@"ISNULL(mooe.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mooe.mooe_id,'') as mooe_id, ");
            sb.AppendLine(@"ISNULL(mooe.name,'') as name,");
            sb.AppendLine(@"ISNULL(mad.total,0) as rate,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as quantity,ISNULL(mad.type_service,'') as type_service,ISNULL(mad.id,0) as act_id,");
            sb.AppendLine(@"ISNULL(mad.month,'') as month,");
            sb.AppendLine(@"ISNULL(mad.year,'') as year,");
            sb.AppendLine(@"ISNULL(mad.entry_date,'') as entry_date,");
            sb.AppendLine(@"ISNULL(mad.start,'') as s_date,");
            sb.AppendLine(@"ISNULL(mad.[end],'') as e_date");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE  mad.year = " + _year + " and madp.is_submitted =0 and mfs.service_type = 1");
            
            dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet FetchAlignmentDataAPP(string _year)
        {
            var sb = new System.Text.StringBuilder(297);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mar.months,");
            sb.AppendLine(@"  mfs.Fund_Name as fundsource,");
            sb.AppendLine(@"  mar.from_uacs,");
            sb.AppendLine(@"  mar.from_total,");
            sb.AppendLine(@"  mmoe.name as mooe,");
            sb.AppendLine(@"  mmo.name,");
            sb.AppendLine(@"  mar.to_uacs,");
            sb.AppendLine(@"  mar.total_alignment");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_alignment_record mar");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mmo on mmo.uacs_code = mar.to_uacs");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mmoe on mmoe.code = mmo.mooe_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mar.fundsource");
            sb.AppendLine(@"  WHERE  mar.division_year =" + _year + ";");


            DataSet dsData = new DataSet();
            dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }

        public DataSet FetchFundSource(String div,string _year)
        {
            var sb = new System.Text.StringBuilder(388);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfsl.Fund_Source_Id as fund_id,");
            sb.AppendLine(@"mo.code as mooe_code,");
            sb.AppendLine(@"mo.name as mooe_name");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mo on mo.code = mooe.mooe_id");
            sb.AppendLine(@"WHERE mfsl.division_id = " + div + " and mfsl.Year = "+ _year +"");
            sb.AppendLine(@"GROUP BY mfsl.Fund_Source_Id,mo.code , mo.name, mo.id ");
            sb.AppendLine(@"ORDER BY mo.id ASC");


            DataSet dsData = new DataSet();
            dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public DataSet FetchFundActivity(String _fundid)
        {
            var sb = new System.Text.StringBuilder(401);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mooe.uacs_code as uacs,");
            sb.AppendLine(@"mo.name as mooe_name,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mo on mo.code = mooe.mooe_id");
            sb.AppendLine(@"WHERE mad.fund_source_id = '"+ _fundid +"'");
            sb.AppendLine(@"GROUP BY mooe.uacs_code,mo.name,mooe.name,mooe.id");
            sb.AppendLine(@"ORDER BY mooe.id");
            sb.AppendLine(@";");


            DataSet dsData = new DataSet();
            dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public DataSet FetchAPPRecords()
        {
            var sb = new System.Text.StringBuilder(196);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  ref_code,");
            sb.AppendLine(@"  mod_procure,");
            sb.AppendLine(@"  ad_post_rei,");
            sb.AppendLine(@"  sub_open_bids,");
            sb.AppendLine(@"  notice_award,");
            sb.AppendLine(@"  contract_signing,");
            sb.AppendLine(@"  source_funds,");
            sb.AppendLine(@"  years,");
            sb.AppendLine(@"  remarks");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data;");


            DataSet dsData = new DataSet();
            dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }

//        public void SavePrintOut(List<APPData_List> _Data, String _Year)
//        {
//            String _sqlString = "";
//            _sqlString = "DELETE FROM dbo.mnda_report_data_app;";
//            foreach (var item in _Data)
//            {
//                if (item.ad_post_rei == null)
//                {
//                    item.ad_post_rei = "-";
//                }
//                if (item.co == null)
//                {
//                    item.co = "";
//                }
//                if (item.code == null)
//                {
//                    item.code = "";
//                }
//                if (item.contract_signing == null)
//                {
//                    item.contract_signing = "";
//                }
//                if (item.mode_procurement == null)
//                {
//                    item.mode_procurement = "";
//                }
//                if (item.mooe_code == null)
//                {
//                    item.mooe_code = "";
//                }
//                if (item.mooe_total == null)
//                {
//                    item.mooe_total = "0";
//                }
//                if (item.notice_award == null)
//                {
//                    item.notice_award = "";
//                }
//                if (item.pmo == null)
//                {
//                    item.pmo = "";
//                }
//                if (item.procurement_project == null)
//                {
//                    item.procurement_project = "";
//                }
//                else if (item.procurement_project.Contains("'"))
//                {
//                    item.procurement_project = item.procurement_project.Replace("'", "");
//                }
//                if (item.remarks == null)
//                {
//                    item.remarks = "";
//                }
//                if (item.source_funds == null)
//                {
//                    item.source_funds = "";
//                }
//                if (item.sub_open == null)
//                {
//                    item.sub_open = "";
//                }
//                if (item.total == null)
//                {
//                    item.total = "0";
//                }
//                _sqlString += @"INSERT INTO   dbo.mnda_report_data_app(  code,  procurement_project, pmo_end_user,  mode_of_procurement,  ads_post_rei,  sub_open_bids,
//                          notice_award,  contract_signing,  source_fund,  eb_total,  eb_mooe,  eb_co,  remarks,  pap_code, yearsss
//                        ) VALUES ('" + item.code + "', '" + item.procurement_project + "', '" + item.pmo + "', '" + item.mode_procurement + "', '" + item.ad_post_rei + "', '" + item.sub_open + "', '" + item.notice_award + "', '" +
//                        item.contract_signing + "', '" + item.source_funds + "', '" + item.total + "', '" + item.mooe_total + "', '" + item.co + "', '" + item.remarks + "', '-', '" + _Year + "');";
//            }



//        }
   
        public DataSet GetProActData(string _year)
        {

            var sb = new System.Text.StringBuilder(184);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" ISNULL(code,'-') as code,");
            sb.AppendLine(@" ISNULL(ref_code,'-') as ref_code,");
            sb.AppendLine(@" ISNULL(mod_procure,'-') as mod_procure,");
            sb.AppendLine(@" ISNULL(ad_post_rei,'-') as ad_post_rei,");
            sb.AppendLine(@" ISNULL(sub_open_bids,'-') as sub_open_bids,");
            sb.AppendLine(@" ISNULL(notice_award,'-') as notice_award,");
            sb.AppendLine(@" ISNULL(contract_signing,'-') as contract_signing,");
            sb.AppendLine(@" ISNULL(source_funds,'-') as source_funds,");
            sb.AppendLine(@" ISNULL(years,'-') as years");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data WHERE years = '" + _year + "';");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public String GetDivisionList()
        {

            var sb = new System.Text.StringBuilder(102);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"div.DivisionAccro,");
            sb.AppendLine(@"div.Division_Desc");
            sb.AppendLine(@"FROM Division div");

            return sb.ToString();
        }
        public DataSet FetchDivision()
        {
            var sb = new System.Text.StringBuilder(104);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  div.Division_Id,");
            sb.AppendLine(@"  div.DivisionAccro as code");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"WHERE div.UnitCode = '-'");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDivisionData() 
        {
            //var sb = new System.Text.StringBuilder(910);
            //sb.AppendLine(@"  SELECT   		");
            //sb.AppendLine(@"  						mnd.id as p_id,mnd.accountable_division as div_id,");
            //sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name                       ");
            //sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            //sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            //sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            //sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            //sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            //sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            //sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            //sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            //sb.AppendLine(@"                        GROUP BY  mnd.id,mnd.accountable_division,div.DivisionAccro,mp.name  ");
            var sb = new System.Text.StringBuilder(1020);
            sb.AppendLine(@"  SELECT   		");
            sb.AppendLine(@"  						mnd.id as p_id,mnd.accountable_division as div_id,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name                      ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        GROUP BY  mnd.id,mnd.accountable_division,div.DivisionAccro,mpe.project_name,mp.name  ");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet FetchFundSource()
        {
            var sb = new System.Text.StringBuilder(168);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfsl.division_id,mfsl.Fund_Source_Id as fund_id,");
            sb.AppendLine(@"mosub.name");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.uacs_code = mfsl.mooe_id");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
         public DataSet FetchActData()
        {
            var sb = new System.Text.StringBuilder(401);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.main_program_id,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"GROUP BY mpe.main_program_id,mooe.name");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet GetMOOE()
        {

            var sb = new System.Text.StringBuilder(59);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mo.code,");
            sb.AppendLine(@" mo.name");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mo");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public String GetMOOESub()
        {

            var sb = new System.Text.StringBuilder(133);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mosub.uacs_code,");
            sb.AppendLine(@"mosub.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mosub ");
            sb.AppendLine(@"WHERE mosub.is_active = 1 and not mosub.uacs_code ='-'");

            return sb.ToString();
        }
    }
    public class APP_PROGRAMS
    {
        public String ID { get; set; }
        public String PAPCODE { get; set; }
        public String Programs { get; set; }
        public String PMO { get; set; }

    }
    public class APP_MOOEList
    {
        public String Code { get; set; }
        public String Name { get; set; }

    }
    public class APPProcAct_Data
    {
        public String Code { get; set; }
        public String RefCode { get; set; }
        public String ModProcure { get; set; }
        public String AdPost { get; set; }
        public String SubOpen { get; set; }
        public String NoticeAward { get; set; }
        public String ContractSign { get; set; }
        public String Years { get; set; }
        public String SourceFunds { get; set; }
    }
    public class PPMPAlignment
    {
        public String div_id { get; set; }
        public String fundsource { get; set; }
        public String mooe { get; set; }
        public String name { get; set; }
        public String from_uacs { get; set; }
        public String from_total { get; set; }
        public String to_uacs { get; set; }
        public String total_alignment { get; set; }
        public String months { get; set; }
    }

    public class PPMPFormat
    {
        public String ACTID { get; set; }
        public String UACS { get; set; }
        public String Description { get; set; }
        public String Quantity_Size { get; set; }
        public String EstimatedBudget { get; set; }
        public String ModeOfProcurement { get; set; }
        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }
        public String _Total { get; set; }
        public String _Division { get; set; }
        public String _Year { get; set; }
        public String _Pap { get; set; }

    }
    public class APPData_List
    {
        public String code { get; set; }
        public String mooe_code { get; set; }

        public String procurement_project { get; set; }
        public String pmo { get; set; }
        public String mode_procurement { get; set; }
        public String ad_post_rei { get; set; }
        public String sub_open { get; set; }
        public String notice_award { get; set; }
        public String contract_signing { get; set; }
        public String source_funds { get; set; }
        public String total { get; set; }
        public String mooe_total { get; set; }
        public String co { get; set; }
        public String remarks { get; set; }
    }
    public class PPMPData
    {
        public String Months { get; set; }
        public String Fund_Name { get; set; }
        public String Division_Id { get; set; }
        public String Division_Desc { get; set; }
        public String Division_Code { get; set; }
        public String entry_date { get; set; }
        public String s_date { get; set; }
        public String e_date { get; set; }
        public String uacs_code { get; set; }
        public String name { get; set; }
        public String rate { get; set; }
        public String quantity { get; set; }
        public String type_service { get; set; }
        public String month { get; set; }
        public String year { get; set; }
        public String act_id { get; set; }
        public String mooe_id { get; set; }
        public String _ref { get; set; }
    }
}
