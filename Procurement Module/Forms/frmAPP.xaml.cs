﻿using Infragistics.Windows.DataPresenter;
using Procurement_Module.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmAPP.xaml
    /// </summary>
    public partial class frmAPP : Window
    {
        private clsAPP c_app = new clsAPP();

        public String Division { get; set; }
        public String PAP { get; set; }
        public frmAPP()
        {
            InitializeComponent();
            GenerateYear();
        }

        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
          
        }
        private List<PPMPData> _PPMPData = new List<PPMPData>();
        private List<PPMPFormat> _Main = new List<PPMPFormat>();
        private List<PPMPAlignment> _AlignmentData = new List<PPMPAlignment>();
        private void FetchPPMPData()
        {
            DataSet ds_data_ppmp = c_app.FetchDataAPP(cmbYear.SelectedItem.ToString());
            DataSet ds_dataapp = c_app.FetchAlignmentDataAPP(cmbYear.SelectedItem.ToString());
         
            _PPMPData.Clear();
            _AlignmentData.Clear();



            foreach (DataRow item in ds_data_ppmp.Tables[0].Rows)
            {
                PPMPData _xvar = new PPMPData();
                _xvar._ref = item["_ref"].ToString();
                _xvar.act_id = item["act_id"].ToString();
                _xvar.Division_Id = item["Division_Id"].ToString();
                _xvar.Division_Code = item["Division_Code"].ToString();
                _xvar.Division_Desc = item["Division_Desc"].ToString();
                _xvar.e_date = item["e_date"].ToString();
                _xvar.entry_date = item["entry_date"].ToString();
                _xvar.Fund_Name = item["Fund_Name"].ToString();
                _xvar.month = item["month"].ToString();
                _xvar.mooe_id = item["mooe_id"].ToString();
                _xvar.name = item["name"].ToString();
                _xvar.quantity = item["quantity"].ToString();
                _xvar.rate = item["rate"].ToString();
                _xvar.s_date = item["s_date"].ToString();
                _xvar.type_service = item["type_service"].ToString();
                _xvar.uacs_code = item["uacs_code"].ToString();
                _xvar.year = item["year"].ToString();

                _PPMPData.Add(_xvar);
            }

            foreach (DataRow item in ds_dataapp.Tables[0].Rows)
            {
                PPMPAlignment _xvar = new PPMPAlignment();

                _xvar.months =  item["months"].ToString();
                _xvar.fundsource =  item["fundsource"].ToString();
                _xvar.mooe =  item["mooe"].ToString();
                _xvar.name =  item["name"].ToString();
                _xvar.from_uacs = item["from_uacs"].ToString();
                _xvar.from_total =  item["from_total"].ToString();
                _xvar.to_uacs =  item["to_uacs"].ToString();
                _xvar.total_alignment = item["total_alignment"].ToString();

                _AlignmentData.Add(_xvar);

            }

            GenerateData();
        }
        private void GenerateData()
        {

            double _Totals = 0.00;
            var results_1 = from p in _PPMPData
                            group p by p.Fund_Name into g
                            select new
                            {
                                Id = g.Key,
                                DivId = g.Select(m => m.Division_Id)
                            };
            _Main.Clear();



            List<PPMPData> _DataFinal = new List<PPMPData>();
            List<PPMPData> _DataReport = new List<PPMPData>();
            foreach (var item in results_1)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.Id;
                x_var.Division_Id = item.DivId.ToList()[0].ToString();
                _DataFinal.Add(x_var);
            }



            foreach (var item in _AlignmentData)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.to_uacs;
                x_var.Division_Id = item.div_id;
                x_var.month = item.months;
                x_var.name = item.name;
                x_var.Fund_Name = item.fundsource;
                x_var.rate = item.total_alignment;
                x_var.year = cmbYear.SelectedItem.ToString();
                x_var.type_service = item.name.ToString();
                x_var.quantity = "1";
                _PPMPData.Add(x_var);
            }

            foreach (var item in _DataFinal)
            {

                PPMPFormat _Data = new PPMPFormat();
                
                _Data.ACTID = "";
                _Data.UACS = item.uacs_code.ToString();
                _Data.Description = "";
                _Data.ModeOfProcurement = "";
                _Data.Quantity_Size = "";
                _Data._Year = "";
                _Data._Division = item.Division_Id;
                _Data._Pap = this.PAP;
                _Data.Jan = "";
                _Data.Feb = "";
                _Data.Mar = "";
                _Data.Apr = "";
                _Data.May = "";
                _Data.Jun = "";
                _Data.Jul = "";
                _Data.Aug = "";
                _Data.Sep = "";
                _Data.Oct = "";
                _Data.Nov = "";
                _Data.Dec = "";
                _Data.EstimatedBudget = "";
                _Data._Total = "0.00";
                _Data._Total = _Totals.ToString("#,##0.00");
                _Main.Add(_Data);
              
                List<PPMPData> x_data = _PPMPData.Where(itempp => itempp.Fund_Name == _Data.UACS).ToList();

                var results2nd = from p in x_data
                                 group p by p.name into g
                                 select new
                                 {
                                     name = g.Key,
                                     ExpenseItems = g.Select(m => m.name)
                                 };

                foreach (var res2nd in results2nd)
                {
                    PPMPFormat _Data2nd = new PPMPFormat();
                    double _Estimate = 0.00;
                    List<PPMPData> x_data2nd = _PPMPData.Where(items => items.name == res2nd.name).ToList();
                  

                    _Data2nd.ACTID = "";
                    _Data2nd.UACS = x_data2nd[0].uacs_code;
                    _Data2nd.Description = x_data2nd[0].name;
                    _Data2nd.ModeOfProcurement = "";
                    _Data2nd.Quantity_Size = "";
                    _Data2nd._Year = x_data2nd[0].year.ToString();
                    _Data2nd._Division = this.Division;
                    _Data2nd._Pap = this.PAP;
                    _Data2nd.Jan = "";
                    _Data2nd.Feb = "";
                    _Data2nd.Mar = "";
                    _Data2nd.Apr = "";
                    _Data2nd.May = "";
                    _Data2nd.Jun = "";
                    _Data2nd.Jul = "";
                    _Data2nd.Aug = "";
                    _Data2nd.Sep = "";
                    _Data2nd.Oct = "";
                    _Data2nd.Nov = "";
                    _Data2nd.Dec = "";
                    _Data2nd.EstimatedBudget = "";
                    _Data2nd._Total = "0.00";
                    _Data2nd._Total = _Totals.ToString("#,##0.00");
                    _Main.Add(_Data2nd);


                    _Totals = 0.00;
                    List<PPMPAlignment> x_dataalign = _AlignmentData.Where(items => items.from_uacs == _Data2nd.UACS).ToList();
                    double totals = 0.00;

                    foreach (var itemal in x_dataalign)
                    {
                        totals += Convert.ToDouble(itemal.total_alignment);
                    }

                    foreach (var item_data in x_data2nd)
                    {
                        PPMPFormat _DataDetail = new PPMPFormat();

                       

                        _DataDetail.ACTID = item_data.act_id;
                        _DataDetail.UACS = "";
                        _DataDetail.Description = item_data.type_service;
                        _DataDetail.ModeOfProcurement = "";
                        _DataDetail.Quantity_Size = item_data.quantity;
                        _DataDetail._Year = item_data.year.ToString();
                        _DataDetail._Division = this.Division;
                        _DataDetail._Pap = this.PAP;



                        _Estimate += Convert.ToDouble(item_data.rate) - totals;
                        _Totals -= totals;
                        _DataDetail.EstimatedBudget = _Estimate.ToString("#,##0.00");
                        _DataDetail._Total = _Totals.ToString("#,##0.00");
                        _Main.Add(_DataDetail);

                       

                        List<PPMPAlignment> x_data3rd = _AlignmentData.Where(items => items.to_uacs == _Data2nd.UACS).ToList();


                    }

                }



                //grdData.ItemsSource = null;
                //grdData.ItemsSource = _Main;
                //grdData.Columns["_Year"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Division"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Pap"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Total"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["ACTID"].Visibility = System.Windows.Visibility.Collapsed;
                //ArrangeColumn();

                //FetchPPMPRevision();
            }
         FetchAPP();
            // LoadReportData(_Main);
        }
        private List<APP_MOOEList> _AppMooe = new List<APP_MOOEList>();
        private List<APPProcAct_Data> _AppProAct = new List<APPProcAct_Data>();
        private List<APP_PROGRAMS> _AppPrograms = new List<APP_PROGRAMS>();
        private List<APPData_List> _AppData = new List<APPData_List>();
        DataTable dtMain = new DataTable();
        private void FetchMOOE()
        {
            dtMain = new DataTable();

            DataTable dtDivisions = new DataTable();
            DataTable dtDivisionData = new DataTable();
            DataTable dtFundSource = new DataTable();
            DataTable dtActivityData = new DataTable();

            dtMain.Columns.Add("id_code");
            dtMain.Columns.Add("id");
            dtMain.Columns.Add("prog_id");
            dtMain.Columns.Add("code");
            dtMain.Columns.Add("mooe_code");
            dtMain.Columns.Add("procurement_project");
            dtMain.Columns.Add("pmo");
            dtMain.Columns.Add("mode_procurement");
            dtMain.Columns.Add("ad_post_rei");
            dtMain.Columns.Add("sub_open");
            dtMain.Columns.Add("notice_award");
            dtMain.Columns.Add("contract_signing");
            dtMain.Columns.Add("source_funds");
            dtMain.Columns.Add("total");
            dtMain.Columns.Add("mooe_total");
            dtMain.Columns.Add("co");
            dtMain.Columns.Add("remarks");


            dtDivisions = c_app.FetchDivision().Tables[0].Copy();
            dtDivisionData = c_app.FetchDivisionData().Tables[0].Copy();
            DataSet dsAPPRecords = c_app.FetchAPPRecords();
            cmbPMO.Items.Clear();
            cmbPMO.Items.Add("Show All"); 
            foreach (DataRow item in dtDivisions.Rows)
            {
                cmbPMO.Items.Add(item[1].ToString());    
            }

            dtFundSource = c_app.FetchFundSource().Tables[0].Copy();

            dtActivityData = c_app.FetchActData().Tables[0].Copy();
            prgBar.Maximum = dtDivisions.Rows.Count;
            Int32 _bar = 0;
            Int32 _count = 0;
          
            foreach (DataRow item in dtDivisions.Rows)
            {
                prgBar.Value = _bar;

                Int32 _secondcount = 0;
                if (item["Division_Id"].ToString() == "6")
                {

                }
               
                dtDivisionData.DefaultView.RowFilter = "div_id ='" + item["Division_Id"].ToString() + "'";
                
                String FirstCode = "";
                String DivisionId = item["Division_Id"].ToString();
                if (dtDivisionData.DefaultView.Count!=0)
                {

                    foreach (DataRow itemR in dtDivisionData.DefaultView.ToTable().Rows)
                    {
                        String Code = item["code"].ToString();
                        String _PMO = item["code"].ToString();
                        String _Program = itemR["program_name"].ToString();
                        Code = Code + _count.ToString();
                        DataRow dr = dtMain.NewRow();

                        dr["id"] = item["Division_Id"].ToString();
                        dr["code"] = Code;
                        dr["mooe_code"] = "";
                        dr["procurement_project"] = _Program;
                        dr["pmo"] = _PMO;
                        dr["mode_procurement"] = "";
                        dr["ad_post_rei"] = "";
                        dr["sub_open"] = "";
                        dr["notice_award"] = "";
                        dr["contract_signing"] = "";
                        dr["source_funds"] = "";
                        dr["total"] = "";
                        dr["mooe_total"] = "";
                        dr["co"] = "";
                        dr["remarks"] = "";

                        dtMain.DefaultView.RowFilter = "procurement_project ='" + itemR["program_name"] + "'";
                       
                        if (dtMain.DefaultView.ToTable().Rows.Count==0)
                        {

                          
                             
                              dtMain.Rows.Add(dr);
                              dtMain.AcceptChanges();
                              DataSet ds = new DataSet();
                           
                              if (item["Division_Id"].ToString() != "")
                              {
                                  ds = c_app.FetchFundSource(item["Division_Id"].ToString(), cmbYear.SelectedItem.ToString());
                              }

                              if (ds.Tables.Count != 0)
                              {
                                 
                                  String _fundSource = ds.Tables[0].Rows[0][0].ToString();
                                  DataSet dsFundActivity = new DataSet();
                                  dsFundActivity = c_app.FetchFundActivity(_fundSource);
                                

                                  foreach (DataRow itemFund in ds.Tables[0].Rows)
                                  {
                                   
                                      DoEvents();
                                      DataRow drR = dtMain.NewRow();
                                      _secondcount += 1;
                                      drR["id"] = "";
                                      drR["code"] = Code + "." + _secondcount;
                                      drR["mooe_code"] = itemFund[1].ToString();
                                      drR["procurement_project"] = "        " + itemFund[2].ToString();
                                      drR["pmo"] =_PMO;
                                  
                                          drR["mode_procurement"] = "";
                                          drR["ad_post_rei"] = "";
                                          drR["sub_open"] = "";
                                          drR["notice_award"] = "";
                                          drR["contract_signing"] = "";
                                          drR["source_funds"] = "";
                                          drR["total"] = "";
                                          drR["mooe_total"] = "";
                                          drR["co"] = "";
                                          drR["remarks"] = "";
                                

                                      dtMain.Rows.Add(drR);

                                    
                                 
                                      if (dsFundActivity.Tables.Count!=0)
                                      {
                                          string _fund = itemFund[2].ToString();
                                          _fund = _fund.Replace("'", "");
                                          dsFundActivity.Tables[0].DefaultView.RowFilter = "mooe_name = '" + _fund + "'";
                                          DataTable dtFunds = dsFundActivity.Tables[0].DefaultView.ToTable().Copy();
                                          dsFundActivity.Tables[0].DefaultView.RowFilter = "";
                                          Int32 _thirdcount = 0;

                                          DataTable dtData = dsAPPRecords.Tables[0].Copy();

                                         



                                          foreach (DataRow itemFundAct in dtFunds.Rows)
                                          {
                                                 DataRow drRf = dtMain.NewRow();
                                                 _thirdcount += 1;
                                                 String _Third = _Program.Replace(".", "");
                                                 _Third = _Third.Replace("'", "");
                                                 _Third = _Third.Replace(" ", "");

                                                 double _Total = Convert.ToDouble(itemFundAct[3].ToString());
                                                 dtData.DefaultView.RowFilter = "";
                                                 dtData.DefaultView.RowFilter = "code ='" + _fundSource + "-" + itemFundAct["uacs"].ToString()  + _Third + "'";


                                                 drRf["id_code"] = _fundSource + "-" + itemFundAct["uacs"].ToString() + _Third;
                                                  drRf["id"] = "";
                                                  drRf["code"] = Code+"."+_secondcount + "." + _thirdcount;
                                                  drRf["mooe_code"] = "";
                                                  drRf["procurement_project"] = "               " + itemFundAct[2].ToString();
                                                  drRf["pmo"] = _PMO;
                                                  if (dtData.DefaultView.ToTable().Rows.Count != 0)
                                                  {
                                                      DataRow _dr_rec =dtData.DefaultView.ToTable().Rows[0];

                                                     
                                                      
                                                      drRf["mode_procurement"] = _dr_rec["mod_procure"].ToString();
                                                      drRf["ad_post_rei"] = _dr_rec["ad_post_rei"].ToString();
                                                      drRf["sub_open"] = _dr_rec["sub_open_bids"].ToString();
                                                      drRf["notice_award"] = _dr_rec["notice_award"].ToString();
                                                      drRf["contract_signing"] = _dr_rec["contract_signing"].ToString();
                                                      drRf["source_funds"] = _dr_rec["source_funds"].ToString();
                                                  }
                                                  else
                                                  {
                                                      drRf["mode_procurement"] = "";
                                                      drRf["ad_post_rei"] = "";
                                                      drRf["sub_open"] = "";
                                                      drRf["notice_award"] = "";
                                                      drRf["contract_signing"] = "";
                                                      drRf["source_funds"] = "";
                                                  }
                                                  drRf["total"] = _Total.ToString("#,##0.00");
                                                  drRf["mooe_total"] = _Total.ToString("#,##0.00");
                                                  drRf["co"] = "";
                                                  drRf["remarks"] = "";

                                                  dtMain.Rows.Add(drRf);
                                                  dtMain.AcceptChanges();
                                          }
                                      }
                                  }
                              }
                        }
                      
                       
                    }

                
                }


                _count += 1;

                dtDivisionData.DefaultView.RowFilter = "";
                _bar += 1;
                prgBar.Value = _bar;
                DoEvents();
            }

            dtMain.DefaultView.RowFilter = "";
            grdData.DataSource = null;
            grdData.DataSource = dtMain.DefaultView;
            grdData.FieldLayouts[0].Fields["id_code"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["id"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["prog_id"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["mooe_code"].Visibility = Visibility.Collapsed;

            grdData.FieldLayouts[0].Fields["procurement_project"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["code"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["pmo"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["mode_procurement"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["ad_post_rei"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["sub_open"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["notice_award"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["contract_signing"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["source_funds"].PerformAutoSize();
        }

        private void SaveAPPData(DataTable _data) 
        {

        }

        private void DoEvents() 
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
               System.Windows.Threading.DispatcherPriority.Background,
               new System.Threading.ThreadStart(() => { }));
        }
     

        private void FetchAPP()
        {

            FetchMOOE();
        }

        private void FetchAlignmentData()
        {
            
      
        }
        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
           
        }

        private void btnShowData_Click(object sender, RoutedEventArgs e)
        {
            FetchPPMPData();
        }

        private void cmbPMO_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbPMO.SelectedItem == "Show All")
            {
                dtMain.DefaultView.RowFilter = "";
            }
            else
            {
                try
                {
                    dtMain.DefaultView.RowFilter = "pmo ='" + cmbPMO.SelectedItem.ToString() + "'";
                }
                catch (Exception)
                {
                    dtMain.DefaultView.RowFilter = "";
                }
                
            }

            grdData.FieldLayouts[0].Fields["procurement_project"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["code"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["pmo"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["mode_procurement"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["ad_post_rei"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["sub_open"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["notice_award"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["contract_signing"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["source_funds"].PerformAutoSize();
        }

        private void UpdateData(List<String> _data) 
        {
            if (_data.Count==0)
            {
                return;
            }
            String _filter = "";

            for (int i = 0; i < _data.Count-1; i++)
            {
                _filter += "id_code ='" + _data[i].ToString() +"' OR ";

            }

            _filter += "id_code ='" + _data[_data.Count - 1].ToString() + "'";
            dtMain.AcceptChanges();
            DataTable dtTemp = dtMain.Copy();

            dtTemp.DefaultView.RowFilter = _filter;

            dtTemp = dtTemp.DefaultView.ToTable().Copy();

            foreach (DataRow item in dtTemp.Rows)
            {
                String _code = item["id_code"].ToString();
                String ref_code = "";
                String mod_procure = item["mode_procurement"].ToString();
                String ad_post_rei = item["ad_post_rei"].ToString();
                String sub_open_bids = item["sub_open"].ToString();
                String notice_award = item["notice_award"].ToString();
                String contract = item["contract_signing"].ToString();
                String source = item["source_funds"].ToString();
                String years = "";
                String remarks = "";

                c_app.SaveAPP(_code, ref_code, mod_procure, ad_post_rei, sub_open_bids, notice_award, contract, source, years, remarks);
                DoEvents();
            }
          
        
        }

        private void grdData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string _mode = "";
            if (grdData.ActiveCell.Field.Name=="mode_procurement")
            {
                _mode = "Procurement";
            }
            else
            {
                _mode = "Proact";
            }


            frmModeProcurement fMode = new frmModeProcurement(_mode);
            List<string> _UpdateCode = new List<string>();
            if (fMode.ShowDialog().Value ==true)
            {
                DataTable _dtUpdate = dtMain.Clone();
                String _codeUpdate = "";
                foreach (Cell item in grdData.SelectedItems.ToList())
                {
                    item.Value = fMode.SelectedData;
                    DataRowView _row =(DataRowView)item.Record.DataItem;
                    DataRow _rw = _row.Row;

                    _codeUpdate = _rw["id_code"].ToString();
                    _UpdateCode.Add(_codeUpdate);
                }
            }

            UpdateData(_UpdateCode);
        }

        private void grdData_RecordUpdated(object sender, Infragistics.Windows.DataPresenter.Events.RecordUpdatedEventArgs e)
        {
           
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {
            DataSet ds = new DataSet();
            dtMain.TableName = "APP";
            ds.Tables.Add(dtMain.Copy());
            frmPrintPreview f_view = new frmPrintPreview(ds);
            f_view.ShowDialog();
        }

      

    }
}
