﻿using Infragistics.Windows.DataPresenter;
using Procurement_Module.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmModeProcurement.xaml
    /// </summary>
    public partial class frmModeProcurement : Window
    {
        private clsAPP _app = new clsAPP();
        private String Mode = "";
        public String SelectedData { get; set; }
        public frmModeProcurement(string _mode)
        {
            InitializeComponent();
            Mode = _mode;
        }

        private void LoadModeProcurementLib() 
        {
            DataSet dsData = _app.GetModeProcurement().Copy();

            grdData.DataSource = dsData.Tables[0].DefaultView;
            grdData.FieldLayouts[0].Fields["id"].Visibility = Visibility.Collapsed;
        }
        private void LoadModePROACTLib()
        {
            DataSet dsData = _app.GetModePROACT().Copy();

            grdData.DataSource = dsData.Tables[0].DefaultView;
            grdData.FieldLayouts[0].Fields["id"].Visibility = Visibility.Collapsed;
        }

        private void frmMode_Loaded(object sender, RoutedEventArgs e)
        {
            switch (Mode)
            {
                case "Procurement":
                    LoadModeProcurementLib();
                    break;
                case "Proact":
                    LoadModePROACTLib();
                    break;
                default:
                    break;
            }
        }

        private void grdData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
       

           this.DialogResult = true;
        }

        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            foreach (DataRecord item in grdData.SelectedItems.ToList())
            {

                this.SelectedData = item.Cells[1].Value.ToString();
                break;
            }
        
          
        }
    }
}
