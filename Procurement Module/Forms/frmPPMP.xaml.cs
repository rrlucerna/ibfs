﻿using Infragistics.Windows.DataPresenter;
using Procurement_Module.Class;
using Procurement_Module.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmPPMP.xaml
    /// </summary>
    public partial class frmPPMP : Window
    {

        private DataTable dtDivisions = new DataTable();
        private clsPPMP c_ppmp = new clsPPMP();
        public frmPPMP()
        {
            InitializeComponent();
            frmppmp.Loaded += frmppmp_Loaded;
        }

        void frmppmp_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDivision();  
        }

        private void LoadDivision() 
        {
            dtDivisions = c_ppmp.GetDivisionList().Tables[0].Copy();
            cmbDivisions.Items.Clear();
            foreach (DataRow item in dtDivisions.Rows)
            {
                cmbDivisions.Items.Add(item[1].ToString());
            }
           
        }
        private void LoadFundSource() 
        {
            dtDivisions.DefaultView.RowFilter = "div_name ='"+ cmbDivisions.SelectedItem.ToString() +"'";
            String DivId = dtDivisions.DefaultView[0][0].ToString();
            dtDivisions.DefaultView.RowFilter = "";
            DataTable dtFundSource = c_ppmp.LoadFundSource(DivId, "2017").Tables[0].Copy();
            DataTable dtPPMPData =  c_ppmp.FetchData(DivId, "2017").Tables[0].Copy();

            if (chkCSE.IsChecked == true)
            {
                dtPPMPData.DefaultView.RowFilter = "uacs_code ='5020301000' or uacs_code='5020399000' or uacs_code='5023990001'";
                dtFundSource.DefaultView.RowFilter = "mooe_id ='5020301000' or mooe_id='5020399000' or mooe_id = '5023990001'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();

                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
            }
            else
            {
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020301000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020399000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5023990001'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();

                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020301000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020399000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5023990001'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
            }
            //dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();

            GenerateData(dtFundSource, dtPPMPData);
        }
        private DataTable dtFinal = new DataTable();
        private DataTable dtReport = new DataTable();
        private void GenerateData(DataTable _FundSource, DataTable _PPMPData)
        {

            double _Totals = 0.00;
            double _OverAll = 0.00;

             dtFinal = new DataTable();

             dtFinal.Columns.Add("ACTID");
             dtFinal.Columns.Add("UACS");
             dtFinal.Columns.Add("Description");
             dtFinal.Columns.Add("Quantity_Size");
             dtFinal.Columns.Add("EstimatedBudget");
             dtFinal.Columns.Add("ModeOfProcurement");
             
             dtFinal.Columns.Add("_Pap");
             dtFinal.Columns.Add("Jan");
             dtFinal.Columns.Add("Feb");
             dtFinal.Columns.Add("Mar");
             dtFinal.Columns.Add("Apr");
             dtFinal.Columns.Add("May");
             dtFinal.Columns.Add("Jun");
             dtFinal.Columns.Add("Jul");
             dtFinal.Columns.Add("Aug");
             dtFinal.Columns.Add("Sep");
             dtFinal.Columns.Add("Oct");
             dtFinal.Columns.Add("Nov");
             dtFinal.Columns.Add("Dec");
             
             dtFinal.Columns.Add("_Total");
             dtFinal.Columns.Add("_Year");
             dtFinal.Columns.Add("_Division");
             dtFinal.TableName = "PPMP";
            String _PAPCode ="";
             foreach (DataRow item in _FundSource.Rows)
             {
                 double _Allocation = Convert.ToDouble(item["Amount"].ToString());
                 DataRow dr = dtFinal.NewRow();

                 _PPMPData.DefaultView.RowFilter = "uacs_code ='" + item["mooe_id"].ToString() + "'";
                 try
                 {
                     _PAPCode = _PPMPData.DefaultView.ToTable().Rows[0]["Division_Code"].ToString();
                 }
                 catch (Exception)
                 {

                 }
              
                dr["ACTID"] = "" ;
                dr["UACS"] = item["mooe_id"].ToString() ;
                dr["Description"] = item["name"].ToString();
                dr["ModeOfProcurement"] ="" ;
                dr["Quantity_Size"] = "";
                dr["_Year"] = "2017";
                dr["_Division"] =cmbDivisions.SelectedItem.ToString();
                dr["_Pap"] =_PAPCode;
                dr["Jan"] ="";
                dr["Feb"] ="";
                dr["Mar"] ="" ;
                dr["Apr"] ="";
                dr["May"] ="";
                dr["Jun"] ="";
                dr["Jul"] ="";
                dr["Aug"] ="";
                dr["Sep"] ="";
                dr["Oct"] ="";
                dr["Nov"] ="";
                dr["Dec"] ="";
                dr["EstimatedBudget"] ="";
                dr["_Total"] = "0.00";

                dtFinal.Rows.Add(dr);
                _Totals = 0.00;
                double totals = 0.00;
                double total_for_contigency = 0.00;

             
                if (_PPMPData.DefaultView.ToTable().Rows.Count!=0)
                {
                    

                        DataTable x_data = _PPMPData.DefaultView.ToTable().Copy();
                        foreach (DataRow itemRows in x_data.Rows)
                        {
                            
                            _PAPCode = itemRows["Division_Code"].ToString();
                            dr = dtFinal.NewRow();

                            dr["ACTID"] = "";
                            dr["UACS"] = "";
                            dr["Description"] = itemRows["type_service"].ToString();
                            dr["ModeOfProcurement"] = "";
                            dr["Quantity_Size"] = itemRows["quantity"].ToString();
                            dr["_Year"] = "2017";
                            dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                            dr["_Pap"] = _PAPCode;


                            switch (itemRows["month"].ToString())
                            {
                                case "Jan":
                                    dr["Jan"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Feb":
                                    dr["Feb"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Mar":
                                    dr["Mar"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Apr":
                                    dr["Apr"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "May":
                                    dr["May"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Jun":
                                    dr["Jun"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Jul":
                                    dr["Jul"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;

                                case "Aug":
                                    dr["Aug"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Sep":
                                    dr["Sep"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Oct":
                                    dr["Oct"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Nov":
                                    dr["Nov"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;
                                case "Dec":
                                    dr["Dec"] = "X";
                                    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                    break;

                            }



                            dr["EstimatedBudget"] = Convert.ToDouble(itemRows["rate"].ToString()).ToString("#,##0.00");
                            dr["_Total"] =0.ToString("#,##0.00");
                            total_for_contigency += Convert.ToDouble(itemRows["rate"].ToString());

                            dtFinal.Rows.Add(dr);
                        }

                       

                        dr = dtFinal.NewRow();

                        dr["ACTID"] = "";
                        dr["UACS"] = "";
                        dr["Description"] = "Contingency";
                        dr["ModeOfProcurement"] = "";
                        dr["Quantity_Size"] = "";
                        dr["_Year"] = "2017";
                        dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                        dr["_Pap"] = _PAPCode;
                        dr["Jan"] = "";
                        dr["Feb"] = "";
                        dr["Mar"] = "";
                        dr["Apr"] = "";
                        dr["May"] = "";
                        dr["Jun"] = "";
                        dr["Jul"] = "";
                        dr["Aug"] = "";
                        dr["Sep"] = "";
                        dr["Oct"] = "";
                        dr["Nov"] = "";
                        dr["Dec"] = "";
                        dr["EstimatedBudget"] = (_Allocation - total_for_contigency).ToString("#,##0.00");
                        dr["_Total"] = "0.00";

                        dtFinal.Rows.Add(dr);
                        dr = dtFinal.NewRow();

                        dr["ACTID"] = "";
                        dr["UACS"] = "";
                        dr["Description"] = "Total :" + (total_for_contigency + (_Allocation - total_for_contigency)).ToString("#,##0.00"); ;
                        dr["ModeOfProcurement"] = "";
                        dr["Quantity_Size"] = "";
                        dr["_Year"] = "2017";
                        dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                        dr["_Pap"] = _PAPCode;
                        dr["Jan"] = "";
                        dr["Feb"] = "";
                        dr["Mar"] = "";
                        dr["Apr"] = "";
                        dr["May"] = "";
                        dr["Jun"] = "";
                        dr["Jul"] = "";
                        dr["Aug"] = "";
                        dr["Sep"] = "";
                        dr["Oct"] = "";
                        dr["Nov"] = "";
                        dr["Dec"] = "";
                        dr["EstimatedBudget"] = "";
                        dr["_Total"] = "0.00";

                        dtFinal.Rows.Add(dr);




                }
                else
                {
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Contingency";
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = "2017";
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = (_Allocation).ToString("#,##0.00");
                    dr["_Total"] = (_Allocation).ToString("#,##0.00"); 

                    dtFinal.Rows.Add(dr);
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Total :" + (_Allocation).ToString("#,##0.00"); ;
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = "2017";
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = "";
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);
                }

              

             }
        

            
             grdData.DataSource = null;
             grdData.DataSource = dtFinal.DefaultView;

             grdData.FieldLayouts[0].Fields["ACTID"].Visibility = Visibility.Collapsed;
             grdData.FieldLayouts[0].Fields["_Year"].Visibility = Visibility.Collapsed;
             grdData.FieldLayouts[0].Fields["_Pap"].Visibility = Visibility.Collapsed;
             grdData.FieldLayouts[0].Fields["_Division"].Visibility = Visibility.Collapsed;
             grdData.FieldLayouts[0].Fields["_Total"].Visibility = Visibility.Collapsed;
        
            
             grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
             grdData.FieldLayouts[0].Fields["Description"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
             grdData.FieldLayouts[0].Fields["Quantity_Size"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
             grdData.FieldLayouts[0].Fields["EstimatedBudget"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
             grdData.FieldLayouts[0].Fields["ModeOfProcurement"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

             grdData.FieldLayouts[0].Fields["UACS"].Label = "UACS";
             grdData.FieldLayouts[0].Fields["Description"].Label = "Description";
             grdData.FieldLayouts[0].Fields["Quantity_Size"].Label = "Quantity";
             grdData.FieldLayouts[0].Fields["EstimatedBudget"].Label = "Estimated Budget";
             grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Label = "Mode Procurement";
                
             grdData.FieldLayouts[0].Fields["UACS"].PerformAutoSize();
             grdData.FieldLayouts[0].Fields["Description"].PerformAutoSize();
             grdData.FieldLayouts[0].Fields["Quantity_Size"].PerformAutoSize();
             grdData.FieldLayouts[0].Fields["EstimatedBudget"].PerformAutoSize();
             grdData.FieldLayouts[0].Fields["ModeOfProcurement"].PerformAutoSize();

        }
        private void LoadReportPPMP()
        {
           

            dtReport = new DataTable();

            dtReport.Columns.Add("Uacs");
            dtReport.Columns.Add("Description");
            dtReport.Columns.Add("Quantity");
            dtReport.Columns.Add("EstimateBudget");
            dtReport.Columns.Add("ModeOfProcurement");
            dtReport.Columns.Add("Jan");
            dtReport.Columns.Add("Feb");
            dtReport.Columns.Add("Mar");
            dtReport.Columns.Add("Apr");
            dtReport.Columns.Add("May");
            dtReport.Columns.Add("Jun");
            dtReport.Columns.Add("Jul");
            dtReport.Columns.Add("Aug");
            dtReport.Columns.Add("Sep");
            dtReport.Columns.Add("Octs");
            dtReport.Columns.Add("Nov");
            dtReport.Columns.Add("Dec");
            dtReport.Columns.Add("Total");
            dtReport.Columns.Add("Division");
            dtReport.Columns.Add("Yearssss");
            dtReport.Columns.Add("PAP");
            dtReport.Columns.Add("approved");
            dtReport.Columns.Add("header");
            dtReport.Columns.Add("revision");
            dtReport.Columns.Add("overall");
            double Total = 0.00;
            double Contigency = 0.00;
            foreach (DataRow item in dtFinal.Rows)
            {
                
                string _debug = item["Description"].ToString();
                if (item["Description"].ToString()!= "Contingency")
                {
                    try
                    {
                        Total += Convert.ToDouble(item["EstimatedBudget"].ToString());
                    }
                    catch (Exception)
                    {

                    }
                
                }
                else
                {
                    try
                    {
                        Contigency += Convert.ToDouble(item["EstimatedBudget"].ToString());
                    }
                    catch (Exception)
                    {

                    }
                
                }
              
             
            }
            foreach (DataRow item in dtFinal.Rows)
            {
                DataRow dr = dtReport.NewRow();

                dr["Uacs"] = item["UACS"].ToString() ;
                dr["Description"] = item["Description"].ToString() ;
                dr["Quantity"] =item["Quantity_Size"].ToString()  ;
                dr["EstimateBudget"] =item["EstimatedBudget"].ToString()  ;
                dr["ModeOfProcurement"] = item["ModeOfProcurement"].ToString() ;
                dr["Jan"] = item["Jan"].ToString() ;
                dr["Feb"] =  item["Feb"].ToString()  ;
                dr["Mar"] = item["Mar"].ToString() ;
                dr["Apr"] = item["Apr"].ToString() ;
                dr["May"] = item["May"].ToString() ;
                dr["Jun"] = item["Jun"].ToString() ;
                dr["Jul"] = item["Jul"].ToString() ;
                dr["Aug"] = item["Aug"].ToString() ;
                dr["Sep"] = item["Sep"].ToString() ;
                dr["Octs"] = item["Oct"].ToString() ;
                dr["Nov"] = item["Nov"].ToString() ;
                dr["Dec"] = item["Dec"].ToString() ;
                dr["Total"] =(Total + Contigency).ToString() ;
                dr["Division"] = item["_Division"].ToString() ;
                dr["Yearssss"] = item["_Year"].ToString() ;
                dr["PAP"] =  item["_Pap"].ToString();
                dr["approved"] = "0" ;
                if (chkCSE.IsChecked== true)
                {
                    dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CSE)";
                }
                else
                {
                    dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP)";
                }
               
                dr["revision"] = "0" ;
                dr["overall"] = "0" ;

                dtReport.Rows.Add(dr);
            }
          
            
            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);


            frmGenericReport f_preview = new frmGenericReport(ds,"PPMP");


            f_preview.Show();
           

        }
        private void btnPPMPPrintout_Click(object sender, RoutedEventArgs e)
        {
            LoadFundSource();
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {
            LoadReportPPMP();


        }
    }
}
