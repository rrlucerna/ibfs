﻿using Procurement_Module.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmConsolidatedPPMP.xaml
    /// </summary>
    public partial class frmConsolidatedPPMP : Window
    {

        private clsPPMP c_ppmp = new clsPPMP();

        private DataTable dtMain = new DataTable();

        public frmConsolidatedPPMP()
        {
            InitializeComponent();
            this.Loaded += frmConsolidatedPPMP_Loaded;
        }

        void frmConsolidatedPPMP_Loaded(object sender, RoutedEventArgs e)
        {
           
        }


        private void LoadExpenditure() 
        {
            DataTable dtMOOE = c_ppmp.GetMOOE().Tables[0].Copy();
            DataTable dtExpenditure = c_ppmp.GetExpenseItems().Tables[0].Copy();
            DataTable dtProcurementList = c_ppmp.GetProcurementList().Tables[0].Copy();
            DataTable dtLocalAllowance = c_ppmp.GetLocalAllowance().Tables[0].Copy();
            DataTable dtLocalPlainRate = c_ppmp.GetLocalPlaneRate().Tables[0].Copy();
            DataTable dtForeignAllowance = c_ppmp.GetForeignAllowance().Tables[0].Copy();
            DataTable dtForeignPlainRate = c_ppmp.GetForeignPlaneRate().Tables[0].Copy();

            Boolean isCSE = false;

            if (chkCSE.IsChecked.Value)
            {
                dtExpenditure.DefaultView.RowFilter = "uacs_code = '5020301000' OR uacs_code ='5020399000' OR uacs_code='5023990001'";
           

                isCSE = true;
            }
            else
            {
                dtExpenditure.DefaultView.RowFilter = "NOT uacs_code = '5020301000'";
                dtExpenditure = dtExpenditure.DefaultView.ToTable().Copy();
                dtExpenditure.DefaultView.RowFilter =  "NOT uacs_code ='5020399000'";
                dtExpenditure = dtExpenditure.DefaultView.ToTable().Copy();
                dtExpenditure.DefaultView.RowFilter = "NOT uacs_code ='5023990001'";
                dtExpenditure = dtExpenditure.DefaultView.ToTable().Copy();
            }

            dtExpenditure = dtExpenditure.DefaultView.ToTable().Copy();
            dtMain = new DataTable();

            dtMain.Columns.Add("_id");
            dtMain.Columns.Add("_division");
            dtMain.Columns.Add("_description");
            dtMain.Columns.Add("_unitcost");
            dtMain.Columns.Add("_quantity");
            dtMain.Columns.Add("Jan");
            dtMain.Columns.Add("Feb");
            dtMain.Columns.Add("Mar");
            dtMain.Columns.Add("First_Qtr");
            dtMain.Columns.Add("Apr");
            dtMain.Columns.Add("May");
            dtMain.Columns.Add("Jun");
            dtMain.Columns.Add("Second_Qtr");
            dtMain.Columns.Add("Jul");
            dtMain.Columns.Add("Aug");
            dtMain.Columns.Add("Sep");
            dtMain.Columns.Add("Third_Qtr");
            dtMain.Columns.Add("Oct");
            dtMain.Columns.Add("Nov");
            dtMain.Columns.Add("Dec");
            dtMain.Columns.Add("Fourth_Qtr");
            dtMain.Columns.Add("Total");

            List<String> _MonthData = new List<string>();
            _MonthData.Add("Jan");
            _MonthData.Add("Feb");
            _MonthData.Add("Mar");
            _MonthData.Add("Apr");
            _MonthData.Add("May");
            _MonthData.Add("Jun");
            _MonthData.Add("Jul");
            _MonthData.Add("Aug");
            _MonthData.Add("Sep");
            _MonthData.Add("Oct");
            _MonthData.Add("Nov");
            _MonthData.Add("Dec");
            Int32 _prog = 0;
            prgBar.Maximum = dtMOOE.Rows.Count;
            grdData.DataSource = null;
            foreach (DataRow item in dtMOOE.Rows)
            {
                DataRow dr = dtMain.NewRow();


             

                 dtExpenditure.DefaultView.RowFilter = "mooe_id ='"+ item["code"].ToString() +"'";
                 DataTable dtExp = dtExpenditure.DefaultView.ToTable().Copy();

                 foreach (DataRow itemExp in dtExp.Rows)
                 {
                     DoEvents();
                     DataTable dtList = c_ppmp.GetDivisionList(itemExp["uacs_code"].ToString()).Tables[0].Copy();

                     DataTable dtCount = c_ppmp.GetExpenseQuantity(itemExp["id"].ToString()).Tables[0].Copy();
                     String _DivList = "";
                     foreach (DataRow itemdiv in dtList.Rows)
                     {
                         _DivList += itemdiv["_accro"].ToString() +" , ";   
                     }
                     Double _Jan = 0.00;
                     Double _Feb = 0.00;
                     Double _Mar = 0.00;
                     Double _Apr = 0.00;
                     Double _May = 0.00;
                     Double _Jun = 0.00;
                     Double _Jul = 0.00;
                     Double _Aug = 0.00;
                     Double _Sep = 0.00;
                     Double _Oct = 0.00;
                     Double _Nov = 0.00;
                     Double _Dec = 0.00;
                     Double _Quantity = 0.00;

                     if (isCSE==false)
                     {
                         foreach (String itemMonth in _MonthData)
                         {
                             dtCount.DefaultView.RowFilter = "month ='" + itemMonth + "'";
                             DataTable dtTotals = dtCount.DefaultView.ToTable();
                             dtCount.DefaultView.RowFilter = "";
                             _Quantity = 0;
                             foreach (DataRow itemTots in dtTotals.Rows)
                             {
                                 _Quantity += Convert.ToDouble(itemTots["quantity"].ToString());
                                 switch (itemTots["month"].ToString())
                                 {
                                     case "Jan":
                                         _Jan += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Feb":
                                         _Feb += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Mar":
                                         _Mar += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Apr":
                                         _Apr += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "May":
                                         _May += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Jun":
                                         _Jun += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Jul":
                                         _Jul += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Aug":
                                         _Aug += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Sep":
                                         _Sep += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Oct":
                                         _Oct += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Nov":
                                         _Nov += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                     case "Dec":
                                         _Dec += Convert.ToDouble(itemTots["total"].ToString());
                                         break;
                                 }
                             }

                         }
                     }
                     
                 

                     if (dtCount.Rows.Count !=0)
                     {
                         dr = dtMain.NewRow();
                         dr["_id"] = "-";
                         dr["_division"] = item["name"].ToString();
                         dr["_description"] = "";
                         dr["_quantity"] = "";
                         dr["Jan"] = "";
                         dr["Feb"] = "";
                         dr["Mar"] = "";
                         dr["First_Qtr"] = "";
                         dr["Apr"] = "";
                         dr["May"] = "";
                         dr["Jun"] = "";
                         dr["Second_Qtr"] = "";
                         dr["Jul"] = "";
                         dr["Aug"] = "";
                         dr["Sep"] = "";
                         dr["Third_Qtr"] = "";
                         dr["Oct"] = "";
                         dr["Nov"] = "";
                         dr["Dec"] = "";
                         dr["Fourth_Qtr"] = "";
                         dr["Total"] = "";
                         dtMain.Rows.Add(dr);

                         dr = dtMain.NewRow();
                         dr["_id"] = "-";
                         dr["_division"] = "Division";
                         dr["_description"] = _DivList;
                         dr["_quantity"] = "";
                         dr["Jan"] = "";
                         dr["Feb"] = "";
                         dr["Mar"] = "";
                         dr["First_Qtr"] = "";
                         dr["Apr"] = "";
                         dr["May"] = "";
                         dr["Jun"] = "";
                         dr["Second_Qtr"] = "";
                         dr["Jul"] = "";
                         dr["Aug"] = "";
                         dr["Sep"] = "";
                         dr["Third_Qtr"] = "";
                         dr["Oct"] = "";
                         dr["Nov"] = "";
                         dr["Dec"] = "";
                         dr["Fourth_Qtr"] = "";
                         dr["Total"] = "";
                         dtMain.Rows.Add(dr);

                         if ( itemExp["id"].ToString() =="1")
                         {
                             dr = dtMain.NewRow();
                             dr["_id"] = itemExp["id"].ToString();
                             dr["_division"] = "-";
                             dr["_description"] = "          " + itemExp["name"].ToString();
                             dr["_quantity"] ="";
                             dr["Jan"] = "";
                             dr["Feb"] = "";
                             dr["Mar"] = "";
                             dr["First_Qtr"] = "";
                             dr["Apr"] = "";
                             dr["May"] = "";
                             dr["Jun"] = "";
                             dr["Second_Qtr"] = "";
                             dr["Jul"] = "";
                             dr["Aug"] = "";
                             dr["Sep"] = "";
                             dr["Third_Qtr"] = "";
                             dr["Oct"] = "";
                             dr["Nov"] = "";
                             dr["Dec"] = "";
                             dr["Fourth_Qtr"] = "";
                             dr["Total"] = "";
                             dtMain.Rows.Add(dr);

                             foreach (DataRow itemPLA in dtLocalPlainRate.Rows)
                             {
                                 if (itemPLA["_search_code"].ToString() != "-")
                                 {
                                     dtCount.DefaultView.RowFilter = "destination ='"+itemPLA["_search_code"].ToString() +"'";
                                     DataTable dtTickets = dtCount.DefaultView.ToTable().Copy();

                                     if (dtTickets.Rows.Count !=0)
                                     {
                                          _Jan = 0.00;
                                          _Feb = 0.00;
                                          _Mar = 0.00;
                                          _Apr = 0.00;
                                          _May = 0.00;
                                          _Jun = 0.00;
                                          _Jul = 0.00;
                                          _Aug = 0.00;
                                          _Sep = 0.00;
                                          _Oct = 0.00;
                                          _Nov = 0.00;
                                          _Dec = 0.00;
                                          _Quantity = 0.00;
                                         foreach (DataRow itemTick in dtTickets.Rows)
                                         {
                                             _Quantity += Convert.ToDouble(itemTick["quantity"].ToString());
                                             switch (itemTick["month"].ToString())
                                             {
                                                 case "Jan":
                                                     _Jan += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Feb":
                                                     _Feb += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Mar":
                                                     _Mar += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Apr":
                                                     _Apr += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "May":
                                                     _May += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jun":
                                                     _Jun += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jul":
                                                     _Jul += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Aug":
                                                     _Aug += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Sep":
                                                     _Sep += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Oct":
                                                     _Oct += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Nov":
                                                     _Nov += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Dec":
                                                     _Dec += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                             }
                                         }
                                         
                                     }

                                     dr = dtMain.NewRow();
                                     dr["_id"] = "";
                                     dr["_division"] = "-";
                                     dr["_description"] = "                 " + itemPLA["ticket"].ToString();
                                     dr["_quantity"] = _Quantity;
                                     dr["Jan"] = _Jan.ToString("#,##0.00");
                                     dr["Feb"] = _Feb.ToString("#,##0.00");
                                     dr["Mar"] = _Mar.ToString("#,##0.00");
                                     dr["First_Qtr"] = (_Jan + _Feb + _Mar).ToString("#,##0.00");
                                     dr["Apr"] = _Apr.ToString("#,##0.00");
                                     dr["May"] = _May.ToString("#,##0.00");
                                     dr["Jun"] = _Jun.ToString("#,##0.00");
                                     dr["Second_Qtr"] = (_Apr + _May + _Jun).ToString("#,##0.00");
                                     dr["Jul"] = _Jul.ToString("#,##0.00");
                                     dr["Aug"] = _Aug.ToString("#,##0.00");
                                     dr["Sep"] = _Sep.ToString("#,##0.00");
                                     dr["Third_Qtr"] = (_Jul + _Aug + _Sep).ToString("#,##0.00");
                                     dr["Oct"] = _Oct.ToString("#,##0.00");
                                     dr["Nov"] = _Nov.ToString("#,##0.00");
                                     dr["Dec"] = _Dec.ToString("#,##0.00");
                                     dr["Fourth_Qtr"] = (_Oct + _Nov + _Dec).ToString("#,##0.00");
                                     dr["Total"] = (Convert.ToDouble(dr["First_Qtr"]) + Convert.ToDouble(dr["Second_Qtr"]) + Convert.ToDouble(dr["Third_Qtr"]) + Convert.ToDouble(dr["Fourth_Qtr"])).ToString("#,##0.00");
                                     dtMain.Rows.Add(dr);
                                 }

                             }

                             foreach (DataRow itemAll in dtLocalAllowance.Rows)
                             {
                                 if (itemAll["item_name"].ToString()!="-")
                                 {
                                     dtCount.DefaultView.RowFilter = "travel_allowance ='" + itemAll["rate"].ToString() + "'";
                                     DataTable dtAccom = dtCount.DefaultView.ToTable().Copy();
                                    
                                     if (dtAccom.Rows.Count != 0)
                                     {
                                         _Jan = 0.00;
                                         _Feb = 0.00;
                                         _Mar = 0.00;
                                         _Apr = 0.00;
                                         _May = 0.00;
                                         _Jun = 0.00;
                                         _Jul = 0.00;
                                         _Aug = 0.00;
                                         _Sep = 0.00;
                                         _Oct = 0.00;
                                         _Nov = 0.00;
                                         _Dec = 0.00;
                                         _Quantity = 0.00;
                                         foreach (DataRow itemTick in dtAccom.Rows)
                                         {
                                             _Quantity += Convert.ToDouble(itemTick["quantity"].ToString());
                                             switch (itemTick["month"].ToString())
                                             {
                                                 case "Jan":
                                                     _Jan += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Feb":
                                                     _Feb += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Mar":
                                                     _Mar += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Apr":
                                                     _Apr += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "May":
                                                     _May += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jun":
                                                     _Jun += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jul":
                                                     _Jul += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Aug":
                                                     _Aug += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Sep":
                                                     _Sep += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Oct":
                                                     _Oct += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Nov":
                                                     _Nov += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Dec":
                                                     _Dec += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                             }
                                         }

                                     }


                                     dr = dtMain.NewRow();
                                     dr["_id"] = "";
                                     dr["_division"] = "-";
                                     dr["_description"] = "                 " + itemAll["item_name"].ToString();
                                     dr["_quantity"] = _Quantity;
                                     dr["Jan"] = _Jan.ToString("#,##0.00");
                                     dr["Feb"] = _Feb.ToString("#,##0.00");
                                     dr["Mar"] = _Mar.ToString("#,##0.00");
                                     dr["First_Qtr"] = (_Jan + _Feb + _Mar).ToString("#,##0.00");
                                     dr["Apr"] = _Apr.ToString("#,##0.00");
                                     dr["May"] = _May.ToString("#,##0.00");
                                     dr["Jun"] = _Jun.ToString("#,##0.00");
                                     dr["Second_Qtr"] = (_Apr + _May + _Jun).ToString("#,##0.00");
                                     dr["Jul"] = _Jul.ToString("#,##0.00");
                                     dr["Aug"] = _Aug.ToString("#,##0.00");
                                     dr["Sep"] = _Sep.ToString("#,##0.00");
                                     dr["Third_Qtr"] = (_Jul + _Aug + _Sep).ToString("#,##0.00");
                                     dr["Oct"] = _Oct.ToString("#,##0.00");
                                     dr["Nov"] = _Nov.ToString("#,##0.00");
                                     dr["Dec"] = _Dec.ToString("#,##0.00");
                                     dr["Fourth_Qtr"] = (_Oct + _Nov + _Dec).ToString("#,##0.00");
                                     dr["Total"] = (Convert.ToDouble(dr["First_Qtr"]) + Convert.ToDouble(dr["Second_Qtr"]) + Convert.ToDouble(dr["Third_Qtr"]) + Convert.ToDouble(dr["Fourth_Qtr"])).ToString("#,##0.00");
                                     dtMain.Rows.Add(dr);
                                 }
                                
                             }
                         }
                         else if (itemExp["id"].ToString() =="2")
                         {
                              dr = dtMain.NewRow();
                             dr["_id"] = itemExp["id"].ToString();
                             dr["_division"] = "-";
                             dr["_description"] = "          " + itemExp["name"].ToString();
                             dr["_quantity"] ="";
                             dr["Jan"] = "";
                             dr["Feb"] = "";
                             dr["Mar"] = "";
                             dr["First_Qtr"] = "";
                             dr["Apr"] = "";
                             dr["May"] = "";
                             dr["Jun"] = "";
                             dr["Second_Qtr"] = "";
                             dr["Jul"] = "";
                             dr["Aug"] = "";
                             dr["Sep"] = "";
                             dr["Third_Qtr"] = "";
                             dr["Oct"] = "";
                             dr["Nov"] = "";
                             dr["Dec"] = "";
                             dr["Fourth_Qtr"] = "";
                             dr["Total"] = "";
                             dtMain.Rows.Add(dr);

                             foreach (DataRow itemPLA in dtForeignPlainRate.Rows)
                             {
                                 if (itemPLA["_search_code"].ToString() != "-")
                                 {
                                     dtCount.DefaultView.RowFilter = "destination ='"+itemPLA["_search_code"].ToString() +"'";
                                     DataTable dtTickets = dtCount.DefaultView.ToTable().Copy();

                                     if (dtTickets.Rows.Count !=0)
                                     {
                                          _Jan = 0.00;
                                          _Feb = 0.00;
                                          _Mar = 0.00;
                                          _Apr = 0.00;
                                          _May = 0.00;
                                          _Jun = 0.00;
                                          _Jul = 0.00;
                                          _Aug = 0.00;
                                          _Sep = 0.00;
                                          _Oct = 0.00;
                                          _Nov = 0.00;
                                          _Dec = 0.00;
                                          _Quantity = 0.00;
                                         foreach (DataRow itemTick in dtTickets.Rows)
                                         {
                                             _Quantity += Convert.ToDouble(itemTick["quantity"].ToString());
                                             switch (itemTick["month"].ToString())
                                             {
                                                 case "Jan":
                                                     _Jan += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Feb":
                                                     _Feb += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Mar":
                                                     _Mar += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Apr":
                                                     _Apr += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "May":
                                                     _May += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jun":
                                                     _Jun += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jul":
                                                     _Jul += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Aug":
                                                     _Aug += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Sep":
                                                     _Sep += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Oct":
                                                     _Oct += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Nov":
                                                     _Nov += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Dec":
                                                     _Dec += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                             }
                                         }
                                         
                                     }

                                     dr = dtMain.NewRow();
                                     dr["_id"] = "";
                                     dr["_division"] = "-";
                                     dr["_description"] = "                 " + itemPLA["ticket"].ToString();
                                     dr["_quantity"] = _Quantity;
                                     dr["Jan"] = _Jan.ToString("#,##0.00");
                                     dr["Feb"] = _Feb.ToString("#,##0.00");
                                     dr["Mar"] = _Mar.ToString("#,##0.00");
                                     dr["First_Qtr"] = (_Jan + _Feb + _Mar).ToString("#,##0.00");
                                     dr["Apr"] = _Apr.ToString("#,##0.00");
                                     dr["May"] = _May.ToString("#,##0.00");
                                     dr["Jun"] = _Jun.ToString("#,##0.00");
                                     dr["Second_Qtr"] = (_Apr + _May + _Jun).ToString("#,##0.00");
                                     dr["Jul"] = _Jul.ToString("#,##0.00");
                                     dr["Aug"] = _Aug.ToString("#,##0.00");
                                     dr["Sep"] = _Sep.ToString("#,##0.00");
                                     dr["Third_Qtr"] = (_Jul + _Aug + _Sep).ToString("#,##0.00");
                                     dr["Oct"] = _Oct.ToString("#,##0.00");
                                     dr["Nov"] = _Nov.ToString("#,##0.00");
                                     dr["Dec"] = _Dec.ToString("#,##0.00");
                                     dr["Fourth_Qtr"] = (_Oct + _Nov + _Dec).ToString("#,##0.00");
                                     dr["Total"] = (Convert.ToDouble(dr["First_Qtr"]) + Convert.ToDouble(dr["Second_Qtr"]) + Convert.ToDouble(dr["Third_Qtr"]) + Convert.ToDouble(dr["Fourth_Qtr"])).ToString("#,##0.00");
                                     dtMain.Rows.Add(dr);
                                 }

                             }

                             foreach (DataRow itemAll in dtForeignAllowance.Rows)
                             {
                                 if (itemAll["item_name"].ToString()!="-")
                                 {
                                     dtCount.DefaultView.RowFilter = "travel_allowance ='" + itemAll["rate"].ToString() + "'";
                                     DataTable dtAccom = dtCount.DefaultView.ToTable().Copy();
                                    
                                     if (dtAccom.Rows.Count != 0)
                                     {
                                         _Jan = 0.00;
                                         _Feb = 0.00;
                                         _Mar = 0.00;
                                         _Apr = 0.00;
                                         _May = 0.00;
                                         _Jun = 0.00;
                                         _Jul = 0.00;
                                         _Aug = 0.00;
                                         _Sep = 0.00;
                                         _Oct = 0.00;
                                         _Nov = 0.00;
                                         _Dec = 0.00;
                                         _Quantity = 0.00;
                                         foreach (DataRow itemTick in dtAccom.Rows)
                                         {
                                             _Quantity += Convert.ToDouble(itemTick["quantity"].ToString());
                                             switch (itemTick["month"].ToString())
                                             {
                                                 case "Jan":
                                                     _Jan += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Feb":
                                                     _Feb += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Mar":
                                                     _Mar += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Apr":
                                                     _Apr += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "May":
                                                     _May += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jun":
                                                     _Jun += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Jul":
                                                     _Jul += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Aug":
                                                     _Aug += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Sep":
                                                     _Sep += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Oct":
                                                     _Oct += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Nov":
                                                     _Nov += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                                 case "Dec":
                                                     _Dec += Convert.ToDouble(itemTick["rate"].ToString());
                                                     break;
                                             }
                                         }

                                     }


                                     dr = dtMain.NewRow();
                                     dr["_id"] = "";
                                     dr["_division"] = "-";
                                     dr["_description"] = "                 " + itemAll["item_name"].ToString();
                                     dr["_quantity"] = _Quantity;
                                     dr["Jan"] = _Jan.ToString("#,##0.00");
                                     dr["Feb"] = _Feb.ToString("#,##0.00");
                                     dr["Mar"] = _Mar.ToString("#,##0.00");
                                     dr["First_Qtr"] = (_Jan + _Feb + _Mar).ToString("#,##0.00");
                                     dr["Apr"] = _Apr.ToString("#,##0.00");
                                     dr["May"] = _May.ToString("#,##0.00");
                                     dr["Jun"] = _Jun.ToString("#,##0.00");
                                     dr["Second_Qtr"] = (_Apr + _May + _Jun).ToString("#,##0.00");
                                     dr["Jul"] = _Jul.ToString("#,##0.00");
                                     dr["Aug"] = _Aug.ToString("#,##0.00");
                                     dr["Sep"] = _Sep.ToString("#,##0.00");
                                     dr["Third_Qtr"] = (_Jul + _Aug + _Sep).ToString("#,##0.00");
                                     dr["Oct"] = _Oct.ToString("#,##0.00");
                                     dr["Nov"] = _Nov.ToString("#,##0.00");
                                     dr["Dec"] = _Dec.ToString("#,##0.00");
                                     dr["Fourth_Qtr"] = (_Oct + _Nov + _Dec).ToString("#,##0.00");
                                     dr["Total"] = (Convert.ToDouble(dr["First_Qtr"]) + Convert.ToDouble(dr["Second_Qtr"]) + Convert.ToDouble(dr["Third_Qtr"]) + Convert.ToDouble(dr["Fourth_Qtr"])).ToString("#,##0.00");
                                     dtMain.Rows.Add(dr);
                                 }
                                
                             }
                         }
                         else
                         {
                             dr = dtMain.NewRow();
                             dr["_id"] = itemExp["id"].ToString();
                             dr["_division"] = "-";
                             dr["_description"] = "          " + itemExp["name"].ToString();
                             dr["_unitcost"] = "";
                             dr["_quantity"] = dtCount.Rows.Count.ToString();
                             dr["Jan"] = _Jan.ToString("#,##0.00");
                             dr["Feb"] = _Feb.ToString("#,##0.00");
                             dr["Mar"] = _Mar.ToString("#,##0.00");
                             dr["First_Qtr"] = (_Jan + _Feb + _Mar).ToString("#,##0.00");
                             dr["Apr"] = _Apr.ToString("#,##0.00");
                             dr["May"] = _May.ToString("#,##0.00");
                             dr["Jun"] = _Jun.ToString("#,##0.00");
                             dr["Second_Qtr"] = (_Apr + _May + _Jun).ToString("#,##0.00");
                             dr["Jul"] = _Jul.ToString("#,##0.00");
                             dr["Aug"] = _Aug.ToString("#,##0.00");
                             dr["Sep"] = _Sep.ToString("#,##0.00");
                             dr["Third_Qtr"] = (_Jul + _Aug + _Sep).ToString("#,##0.00");
                             dr["Oct"] = _Oct.ToString("#,##0.00");
                             dr["Nov"] = _Nov.ToString("#,##0.00");
                             dr["Dec"] = _Dec.ToString("#,##0.00");
                             dr["Fourth_Qtr"] = (_Oct + _Nov + _Dec).ToString("#,##0.00");
                             dr["Total"] = (Convert.ToDouble(dr["First_Qtr"]) + Convert.ToDouble(dr["Second_Qtr"]) + Convert.ToDouble(dr["Third_Qtr"]) + Convert.ToDouble(dr["Fourth_Qtr"])).ToString("#,##0.00");
                             dtMain.Rows.Add(dr);
                         }
                  
                       

                         if (isCSE)
                         {
                            

                             foreach (DataRow itemProc in dtProcurementList.Rows)
                             {
                                 DoEvents();
                              Int32   _JanQty = 0;
                              Int32 _FebQty = 0;
                              Int32 _MarQty = 0;
                                 Int32 _AprQty = 0;
                                 Int32 _MayQty =0;
                                 Int32 _JunQty = 0;
                                 Int32 _JulQty =0;
                                 Int32 _AugQty = 0;
                                 Int32 _SepQty = 0;
                                 Int32 _OctQty = 0;
                                 Int32 _NovQty =0;
                                 Int32 _DecQty =0;

                                 dtCount.DefaultView.RowFilter = "procurement_id ='" + itemProc["id"].ToString() + "'";
                                  DataTable dtCSE = new DataTable();
                                  dtCSE = dtCount.DefaultView.ToTable().Copy();
                                  if (dtCSE.Rows.Count != 0)
                                 {
                                     Boolean hasData = false;
                                     foreach (String itemMonth in _MonthData)
                                     {
                                         dtCSE.DefaultView.RowFilter = "month ='" + itemMonth + "'";
                                         DataTable dtTotals = dtCSE.DefaultView.ToTable();
                                         if (dtTotals.Rows.Count !=0)
                                         {
                                             hasData = true;
                                         }
                                         dtCount.DefaultView.RowFilter = "";
                                         foreach (DataRow itemTots in dtTotals.Rows)
                                         {
                                             DoEvents();
                                            // _Quantity += Convert.ToDouble(itemTots["quantity"].ToString());
                                             switch (itemTots["month"].ToString())
                                             {
                                                 case "Jan":
                                                     _JanQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Feb":
                                                     _FebQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Mar":
                                                     _MarQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Apr":
                                                     _AprQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "May":
                                                     _MayQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Jun":
                                                     _JunQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Jul":
                                                     _JulQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Aug":
                                                     _AugQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Sep":
                                                     _SepQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Oct":
                                                     _OctQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Nov":
                                                     _NovQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                                 case "Dec":
                                                     _DecQty += Convert.ToInt32(itemTots["quantity"].ToString());
                                                     break;
                                             }
                                         }
                                        
                                        
                                     }
                                     if (hasData)
                                     {
                                         
                                             dr = dtMain.NewRow();
                                             dr["_id"] = itemProc["id"].ToString();
                                             dr["_division"] = "";
                                             dr["_description"] = "                              " + itemProc["item_specifications"].ToString();
                                             dr["_unitcost"] = itemProc[6].ToString();
                                             dr["_quantity"] = _Quantity.ToString(); 
                                             dr["Jan"] = _JanQty.ToString();
                                             dr["Feb"] = _FebQty.ToString();
                                             dr["Mar"] = _MarQty.ToString();
                                             dr["First_Qtr"] = (_JanQty + _FebQty + _MarQty).ToString();
                                             dr["Apr"] = _AprQty.ToString();
                                             dr["May"] = _MayQty.ToString();
                                             dr["Jun"] = _JunQty.ToString();
                                             dr["Second_Qtr"] = (_AprQty + _MayQty + _JunQty).ToString();
                                             dr["Jul"] = _JulQty.ToString();
                                             dr["Aug"] = _AugQty.ToString();
                                             dr["Sep"] = _SepQty.ToString();
                                             dr["Third_Qtr"] = (_JulQty + _AugQty + _SepQty).ToString();
                                             dr["Oct"] = _OctQty.ToString();
                                             dr["Nov"] = _NovQty.ToString();
                                             dr["Dec"] = _DecQty.ToString();
                                             dr["Fourth_Qtr"] = (_OctQty + _NovQty + _DecQty).ToString();
                                             dr["Total"] = (Convert.ToInt32(dr["First_Qtr"]) + Convert.ToInt32(dr["Second_Qtr"]) + Convert.ToInt32(dr["Third_Qtr"]) + Convert.ToInt32(dr["Fourth_Qtr"])).ToString();
                                             dtMain.Rows.Add(dr);
                                             _Quantity = 0;
                                             hasData = false;
                                       
                                     }
                                     
                                   

                                 }

                             }
                         }
                     }

                 }
                 _prog += 1;
                 prgBar.Value = _prog;
            }
            Double _Qty = 0.00;
            Double _JanTotal = 0.00;
            Double _FebTotal = 0.00;
            Double _MarTotal = 0.00;
            Double _AprTotal = 0.00;
            Double _MayTotal = 0.00;
            Double _JunTotal = 0.00;
            Double _JulTotal = 0.00;
            Double _AugTotal = 0.00;
            Double _SepTotal = 0.00;
            Double _OctTotal = 0.00;
            Double _NovTotal = 0.00;
            Double _DecTotal = 0.00;
            Double _1st =0.00;
            Double _2nd =0.00;
            Double _3rd =0.00;
            Double _4th =0.00;
            Double _Total =0.00;

            foreach (DataRow item in dtMain.Rows)
            {
                if (item["Jan"].ToString()!="")
                {
                    try
                    {
                        _Qty += Convert.ToDouble(item["_quantity"].ToString());
                    }
                    catch (Exception)
                    {
                        _Qty +=0;
                    }
                   
                    _JanTotal += Convert.ToDouble(item["Jan"].ToString());
                    _FebTotal += Convert.ToDouble(item["Feb"].ToString());
                    _MarTotal += Convert.ToDouble(item["Mar"].ToString());
                    _1st += Convert.ToDouble(item["First_Qtr"].ToString());
                    _AprTotal += Convert.ToDouble(item["Apr"].ToString());
                    _MayTotal += Convert.ToDouble(item["May"].ToString());
                    _JunTotal += Convert.ToDouble(item["Jun"].ToString());
                    _2nd += Convert.ToDouble(item["Second_Qtr"].ToString());
                    _JulTotal += Convert.ToDouble(item["Jul"].ToString());
                    _AugTotal += Convert.ToDouble(item["Aug"].ToString());
                    _SepTotal += Convert.ToDouble(item["Sep"].ToString());
                    _OctTotal += Convert.ToDouble(item["Oct"].ToString());
                    _NovTotal += Convert.ToDouble(item["Nov"].ToString());
                    _DecTotal += Convert.ToDouble(item["Dec"].ToString());
                    _4th += Convert.ToDouble(item["Fourth_Qtr"].ToString());
                    _Total += Convert.ToDouble(item["Total"].ToString());
                }
             
            }


            DataRow drN;

              drN = dtMain.NewRow();
              drN["_id"] = "-";
              drN["_division"] = "------------------------------------------------------";
              drN["_description"] = "------------------------------------------------------";
              drN["_quantity"] = "------------------";
              drN["Jan"] = "------------------";
              drN["Feb"] = "------------------";
              drN["Mar"] = "------------------";
              drN["First_Qtr"] = "------------------";
              drN["Apr"] = "------------------";
              drN["May"] = "------------------";
              drN["Jun"] = "------------------";
              drN["Second_Qtr"] = "------------------";
              drN["Jul"] = "------------------";
              drN["Aug"] = "------------------";
              drN["Sep"] = "------------------";
              drN["Third_Qtr"] = "------------------";
              drN["Oct"] = "------------------";
              drN["Nov"] = "------------------";
              drN["Dec"] = "------------------";
              drN["Fourth_Qtr"] = "------------------";
              drN["Total"] = "------------------";
              dtMain.Rows.Add(drN);
              drN = dtMain.NewRow();
            drN["_id"] ="-";
            drN["_division"] = "-";
            drN["_description"] = "          GRAND TOTAL";
            drN["_quantity"] = _Qty.ToString();
            drN["Jan"] = _JanTotal.ToString("#,##0.00");
            drN["Feb"] = _FebTotal.ToString("#,##0.00");
            drN["Mar"] = _MarTotal.ToString("#,##0.00");
            drN["First_Qtr"] = _1st.ToString("#,##0.00");
            drN["Apr"] = _AprTotal.ToString("#,##0.00");
            drN["May"] = _MayTotal.ToString("#,##0.00");
            drN["Jun"] = _JunTotal.ToString("#,##0.00");
            drN["Second_Qtr"] = _2nd.ToString("#,##0.00");
            drN["Jul"] = _JulTotal.ToString("#,##0.00");
            drN["Aug"] = _AugTotal.ToString("#,##0.00");
            drN["Sep"] = _SepTotal.ToString("#,##0.00");
            drN["Third_Qtr"] = _3rd.ToString("#,##0.00");
            drN["Oct"] = _OctTotal.ToString("#,##0.00");
            drN["Nov"] = _NovTotal.ToString("#,##0.00");
            drN["Dec"] = _DecTotal.ToString("#,##0.00");
            drN["Fourth_Qtr"] = _4th.ToString("#,##0.00");
            drN["Total"] =_Total.ToString("#,##0.00");
            dtMain.Rows.Add(drN);

            grdData.DataSource = dtMain.DefaultView;
            grdData.FieldLayouts[0].Fields["_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_division"].Width = new Infragistics.Windows.DataPresenter.FieldLength(150);
            grdData.FieldLayouts[0].Fields["_description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(250);
            grdData.FieldLayouts[0].Fields["_division"].Label = "Title";
            grdData.FieldLayouts[0].Fields["_description"].Label = "Description";
            grdData.FieldLayouts[0].Fields["_quantity"].Label = "Qty.";
            grdData.FieldLayouts[0].Fields["First_Qtr"].Label = "1st Quarter";
            grdData.FieldLayouts[0].Fields["Second_Qtr"].Label = "2nd Quarter";
            grdData.FieldLayouts[0].Fields["Third_Qtr"].Label = "3rd Quarter";
            grdData.FieldLayouts[0].Fields["Fourth_Qtr"].Label = "4th Quarter";
          
        }
        private void DoEvents()
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
               System.Windows.Threading.DispatcherPriority.Background,
               new System.Threading.ThreadStart(() => { }));
        }
     
        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            LoadExpenditure();
        }
        private void LoadReportPPMP()
        {


            dtReport = new DataTable();

            dtReport.Columns.Add("Division");
            dtReport.Columns.Add("Description");
            dtReport.Columns.Add("UnitCost");

            dtReport.Columns.Add("Quantity");
          
            dtReport.Columns.Add("Jan");
            dtReport.Columns.Add("Feb");
            dtReport.Columns.Add("Mar");
            dtReport.Columns.Add("1st");
            dtReport.Columns.Add("Apr");
            dtReport.Columns.Add("May");
            dtReport.Columns.Add("Jun");
            dtReport.Columns.Add("2nd");
            dtReport.Columns.Add("Jul");
            dtReport.Columns.Add("Aug");
            dtReport.Columns.Add("Sep");
            dtReport.Columns.Add("3rd");
            dtReport.Columns.Add("Octs");
            dtReport.Columns.Add("Nov");
            dtReport.Columns.Add("Dec");
            dtReport.Columns.Add("4th");
            dtReport.Columns.Add("Total");
          
        
           
            foreach (DataRow item in dtMain.Rows)
            {
                DataRow dr = dtReport.NewRow();

                dr["Division"] = item["_division"].ToString();
                dr["Description"] = item["_description"].ToString().Trim();
                dr["UnitCost"] = item["_unitcost"].ToString();
                dr["Quantity"] = item["_quantity"].ToString();
                dr["Jan"] = item["Jan"].ToString();
                dr["Feb"] = item["Feb"].ToString();
                dr["Mar"] = item["Mar"].ToString();
                dr["1st"] = item["First_Qtr"].ToString();
                dr["Apr"] = item["Apr"].ToString();
                dr["May"] = item["May"].ToString();
                dr["Jun"] = item["Jun"].ToString();
                dr["2nd"] = item["Second_Qtr"].ToString();
                dr["Jul"] = item["Jul"].ToString();
                dr["Aug"] = item["Aug"].ToString();
                dr["Sep"] = item["Sep"].ToString();
                dr["3rd"] = item["Third_Qtr"].ToString();
                dr["Octs"] = item["Oct"].ToString();
                dr["Nov"] = item["Nov"].ToString();
                dr["Dec"] = item["Dec"].ToString();
                dr["4th"] = item["Fourth_Qtr"].ToString();
                dr["Total"] = item["Total"].ToString(); ;
               

                dtReport.Rows.Add(dr);
            }
            for (int i = 0; i < dtMain.Columns.Count; i++)
            {
                foreach (DataRow item in dtMain.Rows)
                {
                    if (  item[i] ==null)
                    {
                        item[i] = "";
                    }
                }
            }


            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);


            frmGenericReport f_preview = new frmGenericReport(ds, "PPMP-CONSO");


            f_preview.Show();


        }
        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {
            LoadReportPPMP();
        }

        public DataTable dtReport { get; set; }
    }
}
