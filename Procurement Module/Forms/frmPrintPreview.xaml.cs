﻿using Procurement_Module.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmPrintPreview.xaml
    /// </summary>
    public partial class frmPrintPreview : Window
    {
        private DataSet dsMain = new DataSet();

        public frmPrintPreview(DataSet _data)
        {
            InitializeComponent();
            dsMain = _data;
            try
            {
                rptAPPReport _rptData = new rptAPPReport();

                String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";

                DataSet ds = dsMain.Copy();
               //   ds.WriteXmlSchema(_path + "div_app.xml");
                _rptData.SetDataSource(ds);

                _report.ViewerCore.Zoom(70);
                _report.ViewerCore.ReportSource = _rptData;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException.ToString());
            }
           
        }
    }
}
