﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Text;

namespace MinDAF.Web.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MinDAFSVC
    {
        [OperationContract]
        public void DoWork()
        {
            // Add your operation implementation here
            return;
        }
        [OperationContract]
        public Boolean VerifyUser(string _username, string _password)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;User Id=sa;Password=minda1234;";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter("SELECT COUNT(username) FROM mnda_users WHERE username ='" + _username + "' and password = '" + _password + "'", con);
            DataSet ds = new DataSet();
            Boolean isFound = false;

            da.Fill(ds);
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                if (item[0].ToString() == "1")
                {
                    isFound = true;
                    break;
                }
                else
                {
                    isFound = false;
                    break;
                }
            }
            return isFound;
        }

        [OperationContract]
        public OfficeData FetchOffice()
        {
            List<OfficeData> _ReturnData = new List<OfficeData>();

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM mnda_users", con);
            DataSet ds = new DataSet();


            da.Fill(ds);
            OfficeData _office = new OfficeData();
            foreach (DataRow item in ds.Tables[0].Rows)
            {

                _office.Id = item[0].ToString();
                _office.OfficeName = item[1].ToString();
                _ReturnData.Add(_office);
            }
            return _office;
        }

        [OperationContract]
        public String ExecuteImportDataSQL(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            //  string ConnectionString =@"Server=MINDAFINANCE\SQLEXPRESS;Database=dvDb;User Id=sa;Password=minda1234";
            //   string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            string ConnectionString = FetchConnectionString();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);

            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    _ReturnData = Encoding.UTF8.GetString(memoryStream.ToArray());

                }
            }


            return _ReturnData;
        }
        private String FetchConnectionString()
        {


            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            //  string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM dbo.mnda_settings", con);
            DataSet ds = new DataSet();


            da.Fill(ds);

            if (ds.Tables[0].Rows.Count != 0)
            {
                var sb = new System.Text.StringBuilder(94);
                sb.AppendLine(@"Server=" + ds.Tables[0].Rows[0]["host"].ToString() + ";Database=" + ds.Tables[0].Rows[0]["database_name"].ToString() + ";Uid=" + ds.Tables[0].Rows[0]["username"].ToString() + ";");
                sb.AppendLine(@"Pwd=" + ds.Tables[0].Rows[0]["password"].ToString() + ";");


                return sb.ToString();
            }
            return "";
        }


        [OperationContract]
        public String ExecuteSQL(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;"; 


            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);

            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    _ReturnData = Encoding.UTF8.GetString(memoryStream.ToArray());

                }
            }


            return _ReturnData;
        }
        [OperationContract]
        public DataSet ExecuteSQLReportQuery(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);

            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    _ReturnData = Encoding.UTF8.GetString(memoryStream.ToArray());

                }
            }


            return ds;
        }
        [OperationContract]
        public Boolean ExecuteQuery(object _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            String _sql = (string)_SQL;
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_sql, con);
         
            int result = comm.ExecuteNonQuery();
            con.Close();
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        
        [OperationContract]
        public Boolean ExecuteQueryLists(String _SQL)
        {
          
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm;

             string[] x = _SQL.Split('-');
      

             foreach (object item in x)
             {
                 String _sql = (String)item;
                 if (_sql!="")
                 {
                     comm = new SqlCommand(_sql, con);
                     comm.ExecuteNonQuery();
                 }
                
             }


            con.Close();

            return true;

        }
        [OperationContract]
        public String ExecuteQueryReturnID(string _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_SQL, con);

            int result = comm.ExecuteNonQuery();
            String _id = comm.Parameters["@id"].Value.ToString();
            con.Close();
            return _id;

        }

        [OperationContract]
        public Boolean ExecuteListQuery(object _data)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234;";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            int result = 0;
            //foreach (var item in _data)
            //{
            //    SqlCommand comm = new SqlCommand(item, con);

            //     result = comm.ExecuteNonQuery();

            //}
            con.Close();
            if (result == 1)
            {
                return true;
            }
            else
            {
                return true;
            }

        }


    }

    public class OfficeData
    {
        public String Id { get; set; }
        public String OfficeName { get; set; }
    }
}
