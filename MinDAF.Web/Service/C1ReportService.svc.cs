﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using C1.C1Report;
using System.IO;
namespace MinDAF.Web.Service
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class C1ReportService
    {
        
        /// <summary>
        /// Get a PDF stream containing the specified report.
        /// </summary>
        [OperationContract]
        public byte[] GetReport(string reportName)
        {
          
            // load report
            var c1r = new C1Report();
            var fn = GetReportDefinitionFile(reportName);
            c1r.Load(fn, reportName);

            // adjust connection string to point to our data source (c1nwind.mdb)
            c1r.DataSource.ConnectionString = GetConnectionString();
            foreach (Field f in c1r.Fields)
            {
                // adjust subreports too (one level deep, this is just a sample)
                if (f.Subreport != null)
                {
                    f.Subreport.DataSource.ConnectionString = GetConnectionString();
                }
            }

            // export to pdf
            using (var ms = new MemoryStream())
            {
                c1r.RenderToStream(ms, FileFormatEnum.PDF);
                return ms.ToArray();
            }
        }

        // report definition file is deployed with the app
        static string GetReportDefinitionFile(string _reportname)
        {
            switch (_reportname)
            {
                case"PPMP":
                    return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ppmp_report.xml");
                case "mao_urs":
                    return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "mao_usr.xml");
            }
            return "";
        }

        // connection string refers to C1 sample mdb file
        static string GetConnectionString()
        {

            //    string conn = @"Provider=SQLNCLI10.1;Integrated Security=SSPI;Persist Security Info=False;User ID="";Initial Catalog=minda_financials;Data Source=192.168.1.110\SQLEXPRESS;Initial File Name="";Server SPN=""";
            string conn = @"Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=minda_financials_dirty;Data Source=MINDAFINANCE\SQLEXPRESS";
           // string conn = @"Server=mindafinance\SQLEXPRESS;Database=dvDb;User Id=sa;Password=minda1234";
            return string.Format(conn);

        }
    }
}
