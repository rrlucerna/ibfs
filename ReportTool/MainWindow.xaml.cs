﻿using Infragistics.Windows.Reporting;
using ReportTool.Class;
using ReportTool.MinDAF;
using ReportTool.ReportDesigners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
     
        private clsReportFinance c_dues = new clsReportFinance();
        UserDetail c_user = new UserDetail();

        public MainWindow()
        {
           
                InitializeComponent();

            _report.Owner = Window.GetWindow(this);
         Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ShowUnhandledException(e);
        }

        void ShowUnhandledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError: {0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",

            e.Exception.Message + (e.Exception.InnerException != null ? "\n" + e.Exception.InnerException.Message : null));

            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void FetchReportsCache() 
        {
            DataSet ds = c_dues.FetchFDCache().Copy();
            String ReportType = "";
            String Years = "";
            String DivisionId = "";
            if (ds.Tables[0].Rows.Count !=0)
            {
                ReportType = ds.Tables[0].Rows[0]["report_command"].ToString();
                Years = ds.Tables[0].Rows[0]["requested_year"].ToString();
                DivisionId = ds.Tables[0].Rows[0]["division_id"].ToString();

            }

            switch (ReportType)
            {
                case "Division Distribution":
                    LoadReportDivisionDistribution();
                    break;
                case "APP REPORT":
                    LoadReportAPP(Years);
                    break;
            }
        }

        private void LoadReportTypes()        
        {
            cmbReportType.Items.Clear();
            if (c_user.DivisionId == "4" && c_user.UserLevel == "1")
            {
                cmbReportType.Items.Add("MOOE Summary");
                cmbReportType.Items.Add("PPMP Viewer");
                cmbReportType.Items.Add("Generate APP");
            }
            else if (c_user.DivisionId == "3" && c_user.UserLevel == "1")     
            {
                cmbReportType.Items.Add("Monthly Actual Obligation");
           //     cmbReportType.Items.Add("Monthly Actual Obligation Summary");
                cmbReportType.Items.Add("Division Distribution");
                cmbReportType.Items.Add("MOOE Summary");
                cmbReportType.Items.Add("PPMP Viewer");
                cmbReportType.Items.Add("PPMP Printout");
            }
           
        }

        private void LoadReportMembers()
        {
            cmbReportType.Items.Clear();
            cmbReportType.Items.Add("PPMP Printout");
     //       cmbReportType.Items.Add("APP Printout");
            cmbDivision.Items.Clear();

            cmbDivision.Items.Add(f_sec.c_user.DivisionName);
            cmbDivision.SelectedIndex = 0;
            cmbDivision.IsReadOnly = true;
     
        }
        


        private void LoadReportDivisionDistribution() 
        {
            try
            {
                rpt_NepReport _rptData = new rpt_NepReport();

                String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
               
                  DataSet ds = c_dues.ProcessDivisionDistribution().Copy();
                //   ds.WriteXmlSchema(_path + "div_dis.xml");
                _rptData.SetDataSource(ds);

                _report.ViewerCore.Zoom(70);
                _report.ViewerCore.ReportSource = _rptData;
               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException.ToString());
            }
           

        }
        private void LoadReportAPP(String Years)
        {
            rptAPP _rptData = new rptAPP();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            String DivisionPap = "";

            //var x_data = c_dues.DivisionData.Where(items => items.DivisionName == cmbDivision.SelectedItem.ToString()).ToList();

            //if (x_data.Count!=0)
            //{
            //    DivisionPap = x_data[0].DivisionCode;
            //}

            DataSet ds = c_dues.FetchDataAPP(Years).Copy();
          //    ds.WriteXmlSchema(_path + "div_app.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void LoadReportPPMP()
        {
            rptPPMP _rptData = new rptPPMP();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = c_dues.FetchDataPPMP(c_user.DivisionCode,cmbYear.SelectedItem.ToString(),c_user.DivisionName).Copy();
        //   ds.WriteXmlSchema(_path + "div_ppmp.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void LoadReportMAO()
        {
            rptMAO _rptData = new rptMAO();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = c_dues.FetchDataMAO().Copy();
           //  ds.WriteXmlSchema(_path + "div_mao.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }

        }

        private void btnX_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        frmSecurity f_sec = new frmSecurity();
        private void frmMain_Loaded(object sender, RoutedEventArgs e)
        {
           // FetchReportsCache();
            LoadReportTypes();
            GenerateYear();
            GenerateDivisions();
            f_sec = new frmSecurity();
            f_sec.LoginHandler += f_sec_LoginHandler;
            f_sec.Visibility = System.Windows.Visibility.Visible;
            f_sec.ShowDialog();
            
        }

        void f_sec_LoginHandler(object sender, EventArgs e)
        {
            c_user = f_sec.c_user;
            switch (f_sec.c_user.UserLevel)
            {
                case "1":
                    LoadReportTypes();
                    break;
                case "0":
                    LoadReportMembers();
                    break;
                case "2":
                    LoadReportMembers();
                    break;
            }
        }
        private void GenerateDivisions() 
        {
            cmbDivision.Items.Clear();
            c_dues.FetchDivisions();
            foreach (var item in c_dues.DivisionData)
            {
                cmbDivision.Items.Add(item.DivisionName);
            }

        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                

                switch (cmbReportType.SelectedItem.ToString())
                {
                    case "Division Distribution":
                        LoadReportDivisionDistribution();
                        break;
                    case "Monthly Actual Obligation":
                        LoadReportMAO();
                        break;
                    case "PPMP Printout":
                        LoadReportPPMP();
                        break;
                    case "Generate APP":
                        LoadReportAPP("");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException, "ERROR", MessageBoxButton.OK);
            }
           
        }

        private void cmbReportType_DropDownClosed(object sender, EventArgs e)
        {
            switch (cmbReportType.SelectedItem.ToString())
            {
                case "Division Distribution":
                    cmbDivision.IsEnabled = false;
                    break;
                case  "PPMP Printout":
                    if (c_user.DivisionId == "3" && c_user.UserLevel == "1")     
                    {
                        for (int i = 0; i < cmbDivision.Items.Count; i++)
                        {
                            if (cmbDivision.Items[i].ToString() == "Finance Division (FD)")
                            {
                                cmbDivision.SelectedIndex = i;
                                break;
                            }
                        }
                       
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
