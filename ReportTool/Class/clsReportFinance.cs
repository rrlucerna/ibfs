﻿using MinDAF;
using ReportTool.MinDAF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ReportTool.Class
{
    class clsReportFinance
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private MinDAFSVCClient svc_service = new MinDAFSVCClient();
        public List<DivisionList> DivisionData = new List<DivisionList>();
        public UserDetail UserInfo = new UserDetail();


        public Boolean ExecuteQuery(string _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=mindafinance;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=LOCALHOST\SQLEXPRESS;Database=mindafinance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_SQL, con);

            int result = comm.ExecuteNonQuery();
            con.Close();
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public DataSet ExecuteSQLQuery(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=LOCALHOST\SQLEXPRESS;Database=minda_finance;User Id=sa;Password=12341234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=mindafinance_dirty;User Id=sa;Password=minda1234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);




            return ds;
        }

        public DataSet FetchFDCache()
        {
            DataSet _dsData = new DataSet();
  
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  report_command,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  requested_year");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fd_report;");


            _dsData = ExecuteSQLQuery(sb.ToString());

            ExecuteQuery("DELETE FROM dbo.mnda_fd_report;");

            return _dsData;
        }


        public DataSet ProcessDivisionDistribution() 
        {
            DataSet _dsData = new DataSet();
            var sbD = new System.Text.StringBuilder(113);


            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(711);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code as code,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"msub.name as _expenditure,");
            sb.AppendLine(@"0.00 as _oc,");
            sb.AppendLine(@"0.00 as _oed,");
            sb.AppendLine(@"0.00 as _oed_iso,");
            sb.AppendLine(@"0.00 as _ad,");
            sb.AppendLine(@"0.00 as _ad_office_transfer,");
            sb.AppendLine(@"0.00 as _kmd_issp,");
            sb.AppendLine(@"0.00 as _fd,");
            sb.AppendLine(@"0.00 as _gms,");
            sb.AppendLine(@"0.00 as _prd,");
            sb.AppendLine(@"0.00 as _mdc,");
            sb.AppendLine(@"0.00 as _kmd,");
            sb.AppendLine(@"0.00 as _kmd_thematic_map,");
            sb.AppendLine(@"0.00 as _dpk,");
            sb.AppendLine(@"0.00 as _pfd,");
            sb.AppendLine(@"0.00 as _drpa,");
            sb.AppendLine(@"0.00 as _pdd,");
            sb.AppendLine(@"0.00 as _pdd_now,");
            sb.AppendLine(@"0.00 as _pdd_ppp,");
            sb.AppendLine(@"0.00 as _pdrg,");
            sb.AppendLine(@"0.00 as _amo_wm,");
            sb.AppendLine(@"0.00 as _amo_nm,");
            sb.AppendLine(@"0.00 as _amo_nem,");
            sb.AppendLine(@"0.00 as _amo_cm,");
            sb.AppendLine(@"0.00 as _amo,");
            sb.AppendLine(@"0.00 as _mirpp,");
            sb.AppendLine(@"0.00 as _ipd,");
            sb.AppendLine(@"0.00 as _ipd_cacao,");
            sb.AppendLine(@"0.00 as _purd,");
            sb.AppendLine(@"0.00 as _ipd_mpmc,");
            sb.AppendLine(@"0.00 as _ippr,");
            sb.AppendLine(@"0.00 as _ird,");
            sb.AppendLine(@"0.00 as _ird_eaga,");
            sb.AppendLine(@"0.00 as _ird_mct,");
            sb.AppendLine(@"0.00 as _mebeaga");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mooe");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.mooe_id = mooe.code");
            sb.AppendLine(@"WHERE msub.is_active = 1 ORDER BY mooe.id ASC");

            _dsData =ExecuteSQLQuery(sb.ToString());

            sbD.AppendLine(@"SELECT ");
            sbD.AppendLine(@"  division_id,");
            sbD.AppendLine(@"  mooe_id,");
            sbD.AppendLine(@"  Amount,");
            sbD.AppendLine(@"  Year");
            sbD.AppendLine(@"FROM ");
            sbD.AppendLine(@"  dbo.mnda_fund_source_line_item WHERE Year =2017;");

         

            DataSet _dsFundD = new DataSet();
            _dsFundD = ExecuteSQLQuery(sbD.ToString());

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =1";
            DataTable dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_OC.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_oc"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =2";
            DataTable dtData_OED = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_OED.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_OED.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_oed"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =16";
            DataTable dtData_OED_ISO = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_OED_ISO.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_OED_ISO.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_oed_iso"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =4";
            DataTable dtData_AD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ad"] = _total;
            }
            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =18";
            DataTable dtData_AD_OFFICE = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AD_OFFICE.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AD_OFFICE.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ad_office_transfer"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =19";
            DataTable dtData_KMD_ISSP = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_KMD_ISSP.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_KMD_ISSP.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_kmd_issp"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =3";
            DataTable dtData_FD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_FD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_FD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_fd"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_oc"].ToString()) + Convert.ToDouble(item["_oed"].ToString()) +
                Convert.ToDouble(item["_oed_iso"].ToString()) + Convert.ToDouble(item["_ad"].ToString()) +
                Convert.ToDouble(item["_ad_office_transfer"].ToString()) + Convert.ToDouble(item["_kmd_issp"].ToString()) +
                Convert.ToDouble(item["_fd"].ToString());

                item["_gms"] = _total;


            }
            //  '----------------------------------------'

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =5";
            DataTable dtData_PRD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PRD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PRD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_prd"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =21";
            DataTable dtData_MDC = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_MDC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_MDC.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_mdc"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =6";
            DataTable dtData_KMD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_KMD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_KMD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_kmd"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =22";
            DataTable dtData_Thematic = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_Thematic.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_Thematic.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_kmd_thematic_map"] = _total;
            }


            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_prd"].ToString()) + Convert.ToDouble(item["_mdc"].ToString()) +
                Convert.ToDouble(item["_kmd"].ToString()) + Convert.ToDouble(item["_kmd_thematic_map"].ToString());

                item["_dpk"] = _total;


            }

            //  '----------------------------------------'
            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =7";
            DataTable dtData_PFD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PFD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PFD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_pfd"] = _total;
            }


            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_pfd"].ToString());

                item["_drpa"] = _total;


            }

            //  '----------------------------------------'


            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =8";
            DataTable dtData_PDD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PDD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PDD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_pdd"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =23";
            DataTable dtData_PDD_NOW = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PDD_NOW.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PDD_NOW.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_pdd_now"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =24";
            DataTable dtData_PDD_PPP = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PDD_PPP.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PDD_PPP.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_pdd_ppp"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PDD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PDD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_pdrg"] = _total;
            }


            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_pdd"].ToString()) + Convert.ToDouble(item["_pdd_now"].ToString()) +
                Convert.ToDouble(item["_pdd_ppp"].ToString());

                item["_pdrg"] = _total;


            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =9";
            DataTable dtData_AMO_WM = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AMO_WM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AMO_WM.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_amo_wm"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_amo_wm"].ToString());

                item["_amo_wm"] = _total;


            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =10";
            DataTable dtData_AMO_NM = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AMO_NM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AMO_NM.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_amo_nm"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_amo_nm"].ToString());

                item["_amo_nm"] = _total;


            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =11";
            DataTable dtData_AMO_NEM = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AMO_NEM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AMO_NEM.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_amo_nem"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_amo_nem"].ToString());

                item["_amo_nem"] = _total;


            }
            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =12";
            DataTable dtData_AMO_CM = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_AMO_CM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_AMO_CM.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_amo_cm"] = _total;
            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_amo_cm"].ToString());

                item["_amo_cm"] = _total;


            }



            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_amo_wm"].ToString()) + Convert.ToDouble(item["_amo_nm"].ToString()) + Convert.ToDouble(item["_amo_nem"].ToString()) + Convert.ToDouble(item["_amo_cm"].ToString());

                item["_mirpp"] = _total;


            }



            //--------------------------------
            // sb.AppendLine(@"0.00 as _ipd,");
            ////  sb.AppendLine(@"0.00 as _ipd_cacao,");
            // sb.AppendLine(@"0.00 as _purd,");
            //  sb.AppendLine(@"0.00 as _ipd_mpmc,");
            //  sb.AppendLine(@"0.00 as _ippr,");

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =13";
            DataTable dtData_IPD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IPD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IPD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ipd"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =26";
            DataTable dtData_IPD_CACAO = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IPD_CACAO.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IPD_CACAO.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ipd_cacao"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =14";
            DataTable dtData_PURD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_PURD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_PURD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_purd"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =27";
            DataTable dtData_IPD_MPMC = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IPD_MPMC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IPD_MPMC.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ipd_mpmc"] = _total;
            }
            // sb.AppendLine(@"0.00 as _ipd,");
            ////  sb.AppendLine(@"0.00 as _ipd_cacao,");
            // sb.AppendLine(@"0.00 as _purd,");
            //  sb.AppendLine(@"0.00 as _ipd_mpmc,");
            //  sb.AppendLine(@"0.00 as _ippr,");

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_ipd"].ToString()) + Convert.ToDouble(item["_ipd_cacao"].ToString()) + Convert.ToDouble(item["_purd"].ToString()) + Convert.ToDouble(item["_ipd_mpmc"].ToString());

                item["_ippr"] = _total;


            }
            //--------------------------------
            // sb.AppendLine(@"0.00 as _ird,");
            // sb.AppendLine(@"0.00 as _ird_eaga,");
            // sb.AppendLine(@"0.00 as _ird_mct,");
           // sb.AppendLine(@"0.00 as _mebeaga");

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =15";
            DataTable dtData_IRD = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IRD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IRD.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ird"] = _total;
            }

            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =28";
            DataTable dtData_IRD_EAGA = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IRD_EAGA.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IRD_EAGA.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ird_eaga"] = _total;
            }
            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =29";
            DataTable dtData_IRD_MCT = _dsFundD.Tables[0].DefaultView.ToTable();

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                dtData_IRD_MCT.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                DataTable _items = dtData_IRD_MCT.DefaultView.ToTable();
                double _total = 0.00;
                foreach (DataRow itemd in _items.Rows)
                {
                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                }
                item["_ird_mct"] = _total;
            }


            foreach (DataRow item in _dsData.Tables[0].Rows)
            {

                double _total = 0.00;

                _total += Convert.ToDouble(item["_ird"].ToString()) + Convert.ToDouble(item["_ird_eaga"].ToString()) + Convert.ToDouble(item["_ird_mct"].ToString());

                item["_mebeaga"] = _total;


            }



            return _dsData;
        }

       
        public DataSet FetchDataPPMP(string PAP, string _year,String _div)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(300);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ISNULL(Uacs,'') as Uacs ,");
            sb.AppendLine(@"  ISNULL(Description,'') as Description,");
            sb.AppendLine(@"  ISNULL(Quantity,'') as Quantity,");
            sb.AppendLine(@"  ISNULL(EstimateBudget,'0') as EstimatedBudget,");
            sb.AppendLine(@"  ISNULL(ModeOfProcurement,'') as ModeOfProcurement,");
            sb.AppendLine(@"  ISNULL(Jan,'') as Jan,");
            sb.AppendLine(@"  ISNULL(Feb,'') as Feb,");
            sb.AppendLine(@"  ISNULL(Mar,'') as Mar,");
            sb.AppendLine(@"  ISNULL(Apr,'') as Apr,");
            sb.AppendLine(@"  ISNULL(May,'') as May,");
            sb.AppendLine(@"  ISNULL(Jun,'') as Jun,");
            sb.AppendLine(@"  ISNULL(Jul,'') as Jul,");
            sb.AppendLine(@"  ISNULL(Aug,'') as Aug,");
            sb.AppendLine(@"  ISNULL(Sep,'') as Sep,");
            sb.AppendLine(@"  ISNULL(Octs,'') as Octs,");
            sb.AppendLine(@"  ISNULL(Nov,'') as Nov,");
            sb.AppendLine(@"  ISNULL(Dec,'') as Dec,");
            sb.AppendLine(@"  ISNULL(Total,'') as Total,");
            sb.AppendLine(@"  ISNULL(Division,'') as Division,");
            sb.AppendLine(@"  ISNULL(Yearssss,'') as Yearssss,");
            sb.AppendLine(@"  ISNULL(PAP,'') as PAP,");
            sb.AppendLine(@"  ISNULL(approved,'') as approved,");
            sb.AppendLine(@"  ISNULL(header,'') as header,");
            sb.AppendLine(@"  ISNULL(revision,'') as revision,");
            sb.AppendLine(@"  ISNULL(overall,'') as revision");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE PAP = '" + PAP + "' AND Yearssss =" + _year + " AND Division ='"+ _div +"';");

            _dsData = new DataSet();
            _dsData = ExecuteSQLQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }
        public DataSet FetchDataMAO()
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(250);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ReportHeader,");
            sb.AppendLine(@"  UACS,");
            sb.AppendLine(@"  Allocation,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Balance,");
            sb.AppendLine(@"  Divisions,");
            sb.AppendLine(@"  FundSource,");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ORDER By id ASC;");

            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }

        public DataSet FetchDataAPP(string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(362);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" app.code,");
            sb.AppendLine(@" app.procurement_project,");
            sb.AppendLine(@" app.pmo_end_user,");
            sb.AppendLine(@" app.mode_of_procurement,");
            sb.AppendLine(@" app.ads_post_rei,");
            sb.AppendLine(@" app.sub_open_bids,");
            sb.AppendLine(@" app.notice_award,");
            sb.AppendLine(@" app.contract_signing,");
            sb.AppendLine(@" app.source_fund,");
            sb.AppendLine(@" app.eb_total,");
            sb.AppendLine(@" app.eb_mooe,");
            sb.AppendLine(@" app.eb_co,");
            sb.AppendLine(@" app.remarks,");
            sb.AppendLine(@" app.pap_code,");
            sb.AppendLine(@" app.yearsss");
            sb.AppendLine(@"FROM mnda_report_data_app app");
            sb.AppendLine(@"WHERE app.yearsss ='"+ _year +"';");


            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }
        public Boolean LoginUser(String Password) 
        {
            DataTable dtData = new DataTable();
            var sb = new System.Text.StringBuilder(228);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  div.Division_Id,");
            sb.AppendLine(@"  div.Division_Code,");
            sb.AppendLine(@"  div.Division_Desc,");
            sb.AppendLine(@"  ua.Username,");
            sb.AppendLine(@"  ua.is_admin as UserLevel");
            sb.AppendLine(@"FROM mnda_user_accounts ua");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = ua.Division_Id");
            sb.AppendLine(@" WHERE Password='"+ Password +"'");


            dtData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString()).Tables[0].Copy();
            if (dtData.Rows.Count!=0)
            {
                foreach (DataRow item in dtData.Rows)
                {
                    UserInfo = new UserDetail();

                    UserInfo.DivisionCode = item["Division_Code"].ToString();
                    UserInfo.DivisionId = item["Division_Id"].ToString();
                    UserInfo.DivisionName = item["Division_Desc"].ToString();
                    UserInfo.UserLevel = item["UserLevel"].ToString();
                    UserInfo.Username = item["Username"].ToString();
                    break;
                }
                return true;
            }else
	        {
                return false;
	        }
        }

        public DataTable FetchDivisions() 
        {
            DataTable dtData = new DataTable();
            var sb = new System.Text.StringBuilder(29);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"*");
            sb.AppendLine(@"FROM Division div");

            dtData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString()).Tables[0].Copy();
            DivisionData.Clear();

            foreach (DataRow item in dtData.Rows)
            {
                DivisionList _vData = new DivisionList();
                _vData.DivisionId = item["Division_Id"].ToString();
                _vData.DivisionCode = item["Division_Code"].ToString();
                _vData.DivisionName = item["Division_Desc"].ToString();
                
                DivisionData.Add(_vData);
            }
            return dtData;
        }
    }

    public class DivisionList 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        
    }

    public class UserDetail 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        public String Username { get; set; }
        public String UserLevel { get; set; }
    }
}
