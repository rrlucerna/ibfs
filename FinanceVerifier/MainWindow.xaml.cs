﻿using FinanceVerifier.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinanceVerifier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private clsVerify c_verif = new clsVerify();
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData() 
        {
          
            DataTable dtMain = new DataTable();

            DataTable dtExpenseItemMOOE = c_verif.FetchExpenseMOOE().Tables[0].Copy();
            DataTable dtExpenseItems = c_verif.FetchExpenseItems().Tables[0].Copy();

            dtMain.Columns.Add("id");
            dtMain.Columns.Add("code");
            dtMain.Columns.Add("expenditure");

        

                     

            foreach (DataRow item in dtExpenseItemMOOE.Rows)
            {
                DataRow dr = dtMain.NewRow();

                dr["id"] ="-";
                dr["code"] = item["code"].ToString();
                dr["expenditure"] = item["name"].ToString();

                dtMain.Rows.Add(dr);


                dtExpenseItems.DefaultView.RowFilter = "mooe_id ='" + item["code"].ToString() + "'";
                DataTable dtResult = dtExpenseItems.DefaultView.ToTable().Copy();

                foreach (DataRow itemChild in dtResult.Rows)
                {
                    dr = dtMain.NewRow();

                    dr["id"] = itemChild["id"].ToString();
                    dr["code"] = itemChild["uacs_code"].ToString();
                    dr["expenditure"] = "           " + itemChild["name"].ToString();
                     
                    dtMain.Rows.Add(dr);
                }
                dtExpenseItems.DefaultView.RowFilter = "";
            }

            DataTable dtPAP = c_verif.FetchPAP().Tables[0].Copy();
            DataTable dtDivision = c_verif.FetchDivisionList().Tables[0].Copy();


            
            foreach (DataRow item in dtPAP.Rows)
            {
                dtDivision.DefaultView.RowFilter = "DBM_Sub_Id ='" + item["DBM_Sub_Pap_id"].ToString() + "'";
                DataTable dtSub = dtDivision.DefaultView.ToTable().Copy();

                foreach (DataRow itemsub in dtSub.Rows)
                {
                    dtMain.Columns.Add(itemsub["accro"].ToString());
                }

                dtMain.Columns.Add("TOTAL " + item["Acrro"].ToString());

                dtDivision.DefaultView.RowFilter = "";
            }

            dtMain.Columns.Add("Total");

            DataTable dtActivities = c_verif.FetchActivityItems().Tables[0].Copy();
            
            for (int i = 3; i < dtMain.Columns.Count; i++)
            {

                dtActivities.DefaultView.RowFilter = "accro ='" + dtMain.Columns[i].Caption + "'";
                DataTable dtDivAct = dtActivities.DefaultView.ToTable().Copy();

                foreach (DataRow item in dtMain.Rows)
                {
                    dtDivAct.DefaultView.RowFilter = "mooe_id ='" + item["id"].ToString() + "'";
                    DataTable dtExpense = dtDivAct.DefaultView.ToTable().Copy();

                    if (dtExpense.Rows.Count !=0)
                    {
                        Double _Total = 0.00;
                        foreach (DataRow itemRows in dtExpense.Rows)
                        {
                            _Total += Convert.ToDouble(itemRows["total"].ToString());
                        }

                        item[i] = _Total.ToString("#,##0.00");
                    }
                }
               
            }
           
            foreach (DataRow item in dtMain.Rows)
            {
                double overLineTotal = 0.00;
                double forLineTotal = 0.00;

                for (int i = 3; i < dtMain.Columns.Count; i++)
                {
                    if (dtMain.Columns[i].Caption.Contains("Total"))
                    {
                        if (item["id"].ToString() != "-")
                        {
                            item[i] = forLineTotal.ToString("#,##0.00");
                            break;
                        }
                        else
                        {
                            break;
                        }
                      
                    }
                    else
                    {
                        if (dtMain.Columns[i].Caption.Contains("TOTAL"))
                        {
                            if (item["id"].ToString() != "-")
                            {
                                item[i] = overLineTotal.ToString("#,##0.00");
                                forLineTotal += overLineTotal;
                                overLineTotal = 0;
                            }

                        }
                        else
                        {
                            if (item["id"].ToString() != "-")
                            {
                                if (item[i].ToString() != "")
                                {
                                    overLineTotal += Convert.ToDouble(item[i].ToString());
                                }

                            }

                        }
                    }

                   
                }
            }
            DataRow drGrand = dtMain.NewRow();
            for (int i = 0; i < dtMain.Columns.Count; i++)
            {

                if (dtMain.Columns[i].ColumnName=="expenditure")
                {
                    drGrand[dtMain.Columns[i].ColumnName] = "Grand Total";
                }
                else
                {
                    drGrand[dtMain.Columns[i].ColumnName] = "";
                }
            

                
            }
            dtMain.Rows.Add(drGrand);
           
            for (int i =3; i < dtMain.Columns.Count; i++)
            {
                double _Totals = 0.00;
                foreach (DataRow item in dtMain.Rows)
                {
                    if (item["expenditure"].ToString() =="Grand Total")
                    {
                        item[i] = _Totals.ToString("#,##0.00");
                        break;
                    }
                    else
                    {
                        if (item["id"].ToString() != "-")
                        {
                            if (item[i].ToString() != "")
                            {
                                _Totals += Convert.ToDouble(item[i].ToString());
                            }

                        }
                       
                    }
                  
                }
            }

            

            grdData.DataSource = dtMain.DefaultView;
            grdData.FieldLayouts[0].Fields["expenditure"].PerformAutoSize();
            grdData.FieldLayouts[0].Fields["id"].Visibility = System.Windows.Visibility.Collapsed;

            grdData.FieldLayouts[0].Fields["expenditure"].Width = new Infragistics.Windows.DataPresenter.FieldLength(300);
            grdData.FieldLayouts[0].Fields["code"].Visibility = System.Windows.Visibility.Collapsed;
            
        }
    }
}
