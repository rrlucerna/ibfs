﻿using Finance_Portal.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Finance_Portal.Forms
{
    /// <summary>
    /// Interaction logic for frmPAPEntry.xaml
    /// </summary>
    public partial class frmPAPEntry : Window
    {
        public String Menu { get; set; }
        public String SubPAP { get; set; }

        public String DBM_Pap_Id { get; set; }

        public String DBM_Sub_Pap_id { get; set; }
        
        public String PAP { get; set; }
        public String DIVUNIT { get; set; }

        public event EventHandler Refresh;
        private clsPAP c_pap = new clsPAP();

        public frmPAPEntry()
        {
            InitializeComponent();

            this.Loaded += frmPAPEntry_Loaded;
        }

        void frmPAPEntry_Loaded(object sender, RoutedEventArgs e)
        {
            switch (Menu)
            {
                case "PAP":
                   // c_pap.SaveMainPAP(txtPap.Text, txtDescription.Text);
                    break;
                case "SUBPAP":
                  //  c_pap.SaveSubPAP(SubPAP, txtPap.Text, txtDescription.Text);
                    break;
                case "DIV":
                    txtPap.Text = DBM_Pap_Id;
                    txtPap.IsReadOnly = true;
                    txtDescription.Focus();
                  //  c_pap.SaveDivision(DBM_Pap_Id, txtPap.Text, txtDescription.Text, "-");
                    break;
                case "DIVUNIT":
                    txtPap.Text = DBM_Pap_Id;
                    txtPap.IsReadOnly = true;
                    txtDescription.Focus();
                    //  c_pap.SaveDivision(DBM_Pap_Id, txtPap.Text, txtDescription.Text, "-");
                    break;
                default:
                    break;
            }
        }

        private void SaveEntry() 
        {
            switch (Menu)
            {
                case "PAP":
                    c_pap.SaveMainPAP(txtPap.Text, txtDescription.Text);
                    break;
                case "SUBPAP":
                    c_pap.SaveSubPAP(SubPAP,txtPap.Text, txtDescription.Text);
                    break;
                case "DIV":
                    c_pap.SaveDivision(DBM_Sub_Pap_id, txtPap.Text, txtDescription.Text, "-");
                    break;
                case "DIVUNIT":
                    c_pap.SaveDivisionUnit(DBM_Sub_Pap_id, txtPap.Text, txtDescription.Text, "-",DIVUNIT);
                    break;
                default:
                    break;
            }
           
            if (Refresh !=null)
            {
                Refresh(this, new EventArgs());
            }
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveEntry();
        }
    }
}
