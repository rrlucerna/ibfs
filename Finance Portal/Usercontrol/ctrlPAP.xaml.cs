﻿using Finance_Portal.Class;
using Finance_Portal.Forms;
using Infragistics.Windows.DataPresenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Finance_Portal.Usercontrol
{
    /// <summary>
    /// Interaction logic for ctrlPAP.xaml
    /// </summary>
    public partial class ctrlPAP : UserControl
    {
        private clsPAP c_pap = new clsPAP();
        private DataTable dtOffice = new DataTable();
        public ctrlPAP()
        {
            InitializeComponent();

            this.Loaded += ctrlPAP_Loaded;
        }

        void ctrlPAP_Loaded(object sender, RoutedEventArgs e)
        {
            LoadPAP();
        }

        private void LoadPAP() 
        {
            dtOffice = c_pap.GetDivisionOffice().Tables[0].Copy();

            grdOffce.DataSource = dtOffice.DefaultView;

            grdOffce.FieldLayouts[0].Fields["DBM_Sub_Pap_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdOffce.FieldLayouts[0].Fields["DBM_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdOffce.FieldLayouts[0].Fields["DBM_Sub_Pap_Code"].Label = "PAP";
            grdOffce.FieldLayouts[0].Fields["DBM_Sub_Pap_Desc"].Label = "Name";
            // grdOffce.FieldLayouts[0].Fields["DBM_Sub_Pap_Code"].PerformAutoSize();
            grdOffce.FieldLayouts[0].Fields["DBM_Sub_Pap_Desc"].PerformAutoSize();
        }

        private void LoadSubPAP()         
        {
            grdDivision.DataSource = null;
            grdSubDiv.DataSource = null;
            DataRecord _selected = (DataRecord)grdOffce.ActiveRecord;

            DataSet ds = c_pap.GetSubPAP(_selected.Cells["DBM_Sub_Pap_id"].Value.ToString());

            grdSubPap.DataSource = null;
            grdSubPap.DataSource = ds.Tables[0].DefaultView;

            grdSubPap.FieldLayouts[0].Fields["Sub_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubPap.FieldLayouts[0].Fields["DBM_Sub_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubPap.FieldLayouts[0].Fields["PAP"].PerformAutoSize();
        }
        private void RemoveSubPAP()
        {
            if (MessageBox.Show("Confirm Delete selected item", "Delete Confirm", MessageBoxButton.YesNo)==MessageBoxResult.Yes)
            {
                DataRecord _selecteds = (DataRecord)grdSubPap.ActiveRecord;

                DataSet ds = c_pap.GetDivision(_selecteds.Cells["PAP"].Value.ToString());
                
                if (ds.Tables[0].Rows.Count!=0)
                {
                    MessageBox.Show("Cannot delete selected item because it has a division linked to it");
                }
                else
                {
                    DataRecord _selected = (DataRecord)grdSubPap.ActiveRecord;

                    if (c_pap.RemoveSubPAP(_selected.Cells["Sub_Id"].Value.ToString()))
                    {
                        MessageBox.Show("Selected Item has been delete", "Delete Succcess", MessageBoxButton.OK);
                    }

                    LoadSubPAP();
                }

              
            } ;
        }
        private void RemoveDivision()
        {
            if (MessageBox.Show("Confirm Delete selected item", "Delete Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                DataRecord _selecteds = (DataRecord)grdDivision.ActiveRecord;

                DataSet ds = c_pap.GetDivisionSub(_selecteds.Cells["Division_Id"].Value.ToString());

                if (ds.Tables[0].Rows.Count != 0)
                {
                    MessageBox.Show("Cannot delete selected item because it has a division linked to it");
                }
                else
                {
                    DataRecord _selected = (DataRecord)grdDivision.ActiveRecord;

                    if (c_pap.RemoveDivision(_selected.Cells["Division_Id"].Value.ToString()))
                    {
                        MessageBox.Show("Selected Item has been delete", "Delete Succcess", MessageBoxButton.OK);
                    }

                    LoadDivision();
                }


            };
        }
        private void RemoveDivisionUnit()
        {
            if (MessageBox.Show("Confirm Delete selected item", "Delete Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
               
                    DataRecord _selected = (DataRecord)grdSubDiv.ActiveRecord;

                    if (c_pap.RemoveDivision(_selected.Cells["Division_Id"].Value.ToString()))
                    {
                        MessageBox.Show("Selected Item has been delete", "Delete Succcess", MessageBoxButton.OK);
                    }

                    LoadDivisionSub();
              
            };
        }
        private void RemoveOffice()
        {
            if (MessageBox.Show("Confirm Delete selected item", "Delete Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                DataRecord _selecteds = (DataRecord)grdOffce.ActiveRecord;

                DataSet ds = c_pap.GetSubPAP(_selecteds.Cells["DBM_Sub_Pap_id"].Value.ToString());

                if (ds.Tables[0].Rows.Count != 0)
                {
                    MessageBox.Show("Cannot delete selected item because it has a division linked to it");
                }
                else
                {
                    DataRecord _selected = (DataRecord)grdOffce.ActiveRecord;

                    if (c_pap.RemoveOffice(_selected.Cells["DBM_Sub_Pap_id"].Value.ToString()))
                    {
                        MessageBox.Show("Selected Item has been delete", "Delete Succcess", MessageBoxButton.OK);
                    }

                    LoadPAP();
                }


              

            };
        }
        private void LoadDivision()
        {
            DataRecord _selected = (DataRecord)grdSubPap.ActiveRecord;

            DataSet ds = c_pap.GetDivision(_selected.Cells["PAP"].Value.ToString());

            grdDivision.DataSource = null;
            grdDivision.DataSource = ds.Tables[0].DefaultView;

            grdDivision.FieldLayouts[0].Fields["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdDivision.FieldLayouts[0].Fields["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdDivision.FieldLayouts[0].Fields["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;
            grdDivision.FieldLayouts[0].Fields["UnitCode"].Visibility = System.Windows.Visibility.Collapsed;
            grdDivision.FieldLayouts[0].Fields["Division_Code"].PerformAutoSize();
            grdDivision.FieldLayouts[0].Fields["Division_Code"].Label = "PAP";
            grdDivision.FieldLayouts[0].Fields["Division_Desc"].Label = "Division";
        }

        private void LoadDivisionSub()
        {
            DataRecord _selected = (DataRecord)grdDivision.ActiveRecord;

            DataSet ds = c_pap.GetDivisionSub(_selected.Cells["Division_Id"].Value.ToString());

            grdSubDiv.DataSource = null;
            grdSubDiv.DataSource = ds.Tables[0].DefaultView;

            grdSubDiv.FieldLayouts[0].Fields["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubDiv.FieldLayouts[0].Fields["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubDiv.FieldLayouts[0].Fields["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubDiv.FieldLayouts[0].Fields["UnitCode"].Visibility = System.Windows.Visibility.Collapsed;
            grdSubDiv.FieldLayouts[0].Fields["Division_Code"].PerformAutoSize();

            grdSubDiv.FieldLayouts[0].Fields["Division_Code"].Label = "PAP";
            grdSubDiv.FieldLayouts[0].Fields["Division_Desc"].Label ="Unit";
        }

        private void grdOffce_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
          
            LoadSubPAP();
        }

        private void grdSubPap_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            LoadDivision();
        }

        private void grdDivision_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            LoadDivisionSub();
        }
        frmPAPEntry f_entry = new frmPAPEntry();
        private void btnAddOffice_Click(object sender, RoutedEventArgs e)
        {
            f_entry = new frmPAPEntry();
            f_entry.Menu = "PAP";
            f_entry.Refresh += f_entry_Refresh;
            f_entry.Show();
        }

        void f_entry_Refresh(object sender, EventArgs e)
        {
            switch (f_entry.Menu)
            {
                case "PAP":
                    LoadPAP();
                    break;
                case "SUBPAP":
                    LoadSubPAP();
                    break;
                case "DIV":
                    LoadDivision();
                    break;
                case "DIVUNIT":
                    LoadDivisionSub();
                    break;
                default:
                    break;
            }
          
        }

        private void btnAddSubPap_Click(object sender, RoutedEventArgs e)
        {
            DataRecord _selected = (DataRecord)grdOffce.ActiveRecord;


            String _sub_pap = _selected.Cells["DBM_Sub_Pap_id"].Value.ToString();

            f_entry = new frmPAPEntry();
            f_entry.Menu = "SUBPAP";
            f_entry.SubPAP = _sub_pap;
            f_entry.Refresh += f_entry_Refresh;
            f_entry.Show();
        }

        private void btnDivision_Click(object sender, RoutedEventArgs e)
        {
            DataRecord _selected = (DataRecord)grdSubPap.ActiveRecord;
            DataRecord _selected_1 = (DataRecord)grdOffce.ActiveRecord;

            String DBM_Pap_Id = _selected.Cells["PAP"].Value.ToString();

            String DBM_Sub_Pap_id = _selected_1.Cells["DBM_Sub_Pap_id"].Value.ToString();

            f_entry = new frmPAPEntry();
            f_entry.Menu = "DIV";
            f_entry.DBM_Pap_Id = DBM_Pap_Id;
            f_entry.DBM_Sub_Pap_id = DBM_Sub_Pap_id;
            f_entry.Refresh += f_entry_Refresh;
            f_entry.Show();
        }

        private void btnRemoveSubPap_Click(object sender, RoutedEventArgs e)
        {
            RemoveSubPAP();
        }

        private void btnRemoveDivision_Click(object sender, RoutedEventArgs e)
        {
            RemoveDivision();
        }

        private void btnRemoveUnit_Click(object sender, RoutedEventArgs e)
        {
            RemoveDivisionUnit();
        }

        private void btnRemoveOffice_Click(object sender, RoutedEventArgs e)
        {
            RemoveOffice();
        }

        private void btnAddSubDiv_Click(object sender, RoutedEventArgs e)
        {
            DataRecord _selected = (DataRecord)grdSubPap.ActiveRecord;
            DataRecord _selected_1 = (DataRecord)grdOffce.ActiveRecord;
            DataRecord _selected_2 = (DataRecord)grdDivision.ActiveRecord;

            String DBM_Pap_Id = _selected.Cells["PAP"].Value.ToString();

            String DBM_Sub_Pap_id = _selected_1.Cells["DBM_Sub_Pap_id"].Value.ToString();
            String Div_id = _selected_2.Cells["Division_Id"].Value.ToString();

            f_entry = new frmPAPEntry();
            f_entry.Menu = "DIVUNIT";  
            f_entry.DBM_Pap_Id = DBM_Pap_Id;
            f_entry.DBM_Sub_Pap_id = DBM_Sub_Pap_id;
            f_entry.DIVUNIT = Div_id;
            f_entry.Refresh += f_entry_Refresh;
            f_entry.Show();
        }

        
    
        
    }
}
