﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance_Portal.Class
{
    public class clsPAP
    {
        clsData c_data = new clsData();
     

        public DataSet GetDivisionOffice()
        {

            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id,");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet GetSubPAP(String _id)
        {

            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Sub_Id,");
            sb.AppendLine(@"  DBM_Sub_Id,");
            sb.AppendLine(@"  PAP,");
            sb.AppendLine(@"  Description  ");
            sb.AppendLine(@"FROM   dbo.DBM_Sub_Office dso ");
            sb.AppendLine(@"WHERE dso.DBM_Sub_Id ='"+ _id +"';");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public Boolean RemoveSubPAP(String _id)
        {

            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"DELETE FROM dbo.DBM_Sub_Office WHERE Sub_Id ='"+_id +"' ");
    


          return c_data.ExecuteQuery(sb.ToString());

            
        }

        public Boolean RemoveOffice(String _id)
        {

            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"DELETE FROM dbo.DBM_Sub_Pap WHERE DBM_Sub_Pap_id ='" + _id + "' ");



            return c_data.ExecuteQuery(sb.ToString());


        }
        public Boolean RemoveDivision(String _id)
        {

            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"DELETE FROM dbo.Division WHERE Division_Id ='" + _id + "' ");



            return c_data.ExecuteQuery(sb.ToString());


        }

      public DataSet GetDivision(String _id)
        {

            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,");
            sb.AppendLine(@"  DivisionAccro,");
            sb.AppendLine(@"  UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE Division_Code ='"+ _id +"' AND is_sub_division = '0';");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
      public DataSet GetFundSource()
      {

          var sb = new System.Text.StringBuilder(124);
          sb.AppendLine(@"SELECT ");
          sb.AppendLine(@"  id,");
          sb.AppendLine(@"  respo_name,");
          sb.AppendLine(@"  code,");
          sb.AppendLine(@"  is_division,");
          sb.AppendLine(@"  is_used,");
          sb.AppendLine(@"  div_id,");
          sb.AppendLine(@"  fund_type");
          sb.AppendLine(@"FROM ");
          sb.AppendLine(@"  dbo.mnda_respo_library;");


          DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

          return dsData;
      }
       public DataSet GetFundSourceExpense(String fund_source)
          {

              var sb = new System.Text.StringBuilder(293);
              sb.AppendLine(@"SELECT");
              sb.AppendLine(@"mooe.name as expense_item,");
              sb.AppendLine(@"mooe.uacs_code as uacs");
              sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
              sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code= mfsl.mooe_id");
              sb.AppendLine(@"WHERE mfsl.Fund_Source_Id ='"+ fund_source +"'");
              sb.AppendLine(@"ORDER BY mooe.id ASC");


              DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

              return dsData;
          }
      public DataSet GetDivisionSub(String _id)
      {

          var sb = new System.Text.StringBuilder(132);
          sb.AppendLine(@"SELECT ");
          sb.AppendLine(@"  Division_Id,");
          sb.AppendLine(@"  DBM_Sub_Pap_Id,");
          sb.AppendLine(@"  Division_Code,");
          sb.AppendLine(@"  Division_Desc,");
          sb.AppendLine(@"  DivisionAccro,");
          sb.AppendLine(@"  UnitCode");
          sb.AppendLine(@"FROM ");
          sb.AppendLine(@"  dbo.Division WHERE UnitCode ='" + _id + "' AND is_sub_division = '1';");



          DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

          return dsData;
      }

      public Boolean SaveMainPAP(String _pap, String _description) 
      {
          var sb = new System.Text.StringBuilder(128);
          sb.AppendLine(@"INSERT INTO ");
          sb.AppendLine(@"  dbo.DBM_Sub_Pap");
          sb.AppendLine(@"(");
          sb.AppendLine(@"  DBM_Pap_Id,");
          sb.AppendLine(@"  DBM_Sub_Pap_Code,");
          sb.AppendLine(@"  DBM_Sub_Pap_Desc");
          sb.AppendLine(@") ");
          sb.AppendLine(@"VALUES (");
          sb.AppendLine(@"  '" + _pap + "',");
          sb.AppendLine(@"  '" + _pap + "',");
          sb.AppendLine(@"  '"+ _description +"'");
          sb.AppendLine(@");");

          try
          {
             return  c_data.ExecuteQuery(sb.ToString());
          }
          catch (Exception)
          {
             return false;
          }

      }
      public Boolean SaveSubPAP(String _sub_id,String _pap, String _description) 
      {
          var sb = new System.Text.StringBuilder(112);
          sb.AppendLine(@"INSERT INTO ");
          sb.AppendLine(@"  dbo.DBM_Sub_Office");
          sb.AppendLine(@"(");
          sb.AppendLine(@"");
          sb.AppendLine(@"  DBM_Sub_Id,");
          sb.AppendLine(@"  Description,");
          sb.AppendLine(@"  PAP");
          sb.AppendLine(@") ");
          sb.AppendLine(@"VALUES (");
          sb.AppendLine(@" '"+ _sub_id +"',");
          sb.AppendLine(@" '"+ _description +"',");
          sb.AppendLine(@" '"+ _pap +"'");
          sb.AppendLine(@");");


          try
          {
             return  c_data.ExecuteQuery(sb.ToString());
          }
          catch (Exception)
          {
             return false;
          }

      }

      public Boolean SaveDivision(String DBM_Sub_Pap_Id, String Division_Code, String Division_Desc, String DivisionAccro)
      {
          var sb = new System.Text.StringBuilder(195);
          sb.AppendLine(@"INSERT INTO ");
          sb.AppendLine(@"  dbo.Division");
          sb.AppendLine(@"(");
          sb.AppendLine(@"  DBM_Sub_Pap_Id,");
          sb.AppendLine(@"  Division_Code,");
          sb.AppendLine(@"  Division_Desc,");
          sb.AppendLine(@"  DivisionAccro,");
          sb.AppendLine(@"  UnitCode,");
          sb.AppendLine(@"  is_sub_division");
          sb.AppendLine(@") ");
          sb.AppendLine(@"VALUES (");
          sb.AppendLine(@"  '" + DBM_Sub_Pap_Id + "',");
          sb.AppendLine(@"  '" + Division_Code + "',");
          sb.AppendLine(@"  '" + Division_Desc + "',");
          sb.AppendLine(@"  '" + DivisionAccro + "',");
          sb.AppendLine(@"  '-',");
          sb.AppendLine(@"  0");
          sb.AppendLine(@");");


          try
          {
              return c_data.ExecuteQuery(sb.ToString());
          }
          catch (Exception)
          {
              return false;
          }

      }
      public Boolean SaveDivisionUnit(String DBM_Sub_Pap_Id, String Division_Code, String Division_Desc, String DivisionAccro,String UnitCode)
      {
          var sb = new System.Text.StringBuilder(195);
          sb.AppendLine(@"INSERT INTO ");
          sb.AppendLine(@"  dbo.Division");
          sb.AppendLine(@"(");
          sb.AppendLine(@"  DBM_Sub_Pap_Id,");
          sb.AppendLine(@"  Division_Code,");
          sb.AppendLine(@"  Division_Desc,");
          sb.AppendLine(@"  DivisionAccro,");
          sb.AppendLine(@"  UnitCode,");
          sb.AppendLine(@"  is_sub_division");
          sb.AppendLine(@") ");
          sb.AppendLine(@"VALUES (");
          sb.AppendLine(@"  '" + DBM_Sub_Pap_Id + "',");
          sb.AppendLine(@"  '" + Division_Code + "',");
          sb.AppendLine(@"  '" + Division_Desc + "',");
          sb.AppendLine(@"  '" + DivisionAccro + "',");
          sb.AppendLine(@"  '"+ UnitCode +"',");
          sb.AppendLine(@"  1");
          sb.AppendLine(@");");


          try
          {
              return c_data.ExecuteQuery(sb.ToString());
          }
          catch (Exception)
          {
              return false;
          }

      }
    }
}
