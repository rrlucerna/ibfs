﻿using Finance_Portal.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinanceConsolidatedPAPS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private clsPAP c_pap = new clsPAP();
        private DataTable dtMain = new DataTable();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadData() 
        {
            dtMain = new DataTable();

            dtMain.Columns.Add("f_source");
            dtMain.Columns.Add("id");
            dtMain.Columns.Add("code");
            dtMain.Columns.Add("Division");
            dtMain.Columns.Add("uacs");
            dtMain.Columns.Add("Jan");
            dtMain.Columns.Add("Feb");
            dtMain.Columns.Add("Mar");
            dtMain.Columns.Add("1_qtr");
            dtMain.Columns.Add("Apr");
            dtMain.Columns.Add("May");
            dtMain.Columns.Add("Jun");
            dtMain.Columns.Add("2_qtr");
            dtMain.Columns.Add("Jul");
            dtMain.Columns.Add("Aug");
            dtMain.Columns.Add("Sep");
            dtMain.Columns.Add("3_qtr");
            dtMain.Columns.Add("Oct");
            dtMain.Columns.Add("Nov");
            dtMain.Columns.Add("Dec");
            DataTable dtPAP = c_pap.GetDivisionOffice().Tables[0].Copy();
            DataTable dtFundSource = c_pap.GetFundSource().Tables[0].Copy();

            foreach (DataRow item in dtPAP.Rows)
            {
                DataRow dr = dtMain.NewRow();
                dr["f_source"] = "-";
                dr["id"] = item["DBM_Sub_Pap_id"].ToString();
                dr["code"] = item["DBM_Sub_Pap_Code"].ToString();
                dr["division"] = item["DBM_Sub_Pap_Desc"].ToString();
                dr["uacs"] ="";
                dtMain.Rows.Add(dr);
                DataTable dtSubPAP = c_pap.GetSubPAP(item["DBM_Sub_Pap_id"].ToString()).Tables[0].Copy();
                foreach (DataRow itemsub in dtSubPAP.Rows)
                {
                    dr = dtMain.NewRow();
                    dr["f_source"] = "-";
                    dr["id"] = itemsub["Sub_id"].ToString();
                    dr["code"] = itemsub["PAP"].ToString();
                    dr["division"] = "           " + itemsub["Description"].ToString();
                    dr["uacs"] = "";
                    dtMain.Rows.Add(dr);

                    DataTable dtDivision = c_pap.GetDivision(itemsub["PAP"].ToString()).Tables[0].Copy();
                    foreach (DataRow itemdiv in dtDivision.Rows)
                    {
                        dtFundSource.DefaultView.RowFilter = "div_id ='" + itemdiv["Division_Id"].ToString() + "'";
                        DataTable dt_resf = dtFundSource.DefaultView.ToTable();
                        String FSource = "";
                        if (dt_resf.Rows.Count!=0)
                        {
                            FSource = dt_resf.Rows[0]["code"].ToString();
                        }

                        dr = dtMain.NewRow();
                        dr["f_source"] = FSource;
                        dr["id"] = itemdiv["Division_Id"].ToString();
                        dr["code"] = itemdiv["Division_Code"].ToString();
                        dr["division"] = "                              " + itemdiv["Division_Desc"].ToString();
                        dr["uacs"] = "";
                        dtMain.Rows.Add(dr);
                        DataTable dtExpenseItems = c_pap.GetFundSourceExpense(FSource).Tables[0].Copy();

                        foreach (DataRow itemExp in dtExpenseItems.Rows)
                        {
                            dr = dtMain.NewRow();
                            dr["f_source"] = FSource;
                            dr["id"] = "-";
                            dr["code"] = "-";
                            dr["division"] = "                                      " + itemExp["expense_item"].ToString();
                            dr["uacs"] = itemExp["uacs"].ToString();
                            dtMain.Rows.Add(dr);
                        }

                        DataTable dtDivisionProj = c_pap.GetDivisionSub(itemdiv["Division_Id"].ToString()).Tables[0].Copy();
                           foreach (DataRow itemProj in dtDivisionProj.Rows)
	                        {
                                dtFundSource.DefaultView.RowFilter = "div_id ='" + itemdiv["Division_Id"].ToString() + "'";
                                 dt_resf = dtFundSource.DefaultView.ToTable();
                                 if (dt_resf.Rows.Count != 0)
                                 {
                                     FSource = dt_resf.Rows[0]["code"].ToString();
                                 }

		                       dr = dtMain.NewRow();
                               dr["f_source"] = FSource;
                               dr["id"] = itemProj["Division_Id"].ToString();
                                            dr["code"] = "";
                                            dr["division"] = "                                              " + itemProj["Division_Desc"].ToString();

                                            dtMain.Rows.Add(dr);
	                        }
                    }
                }
            }

            grdData.DataSource = dtMain.DefaultView;
            grdData.FieldLayouts[0].Fields["id"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["f_source"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["code"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);
            grdData.FieldLayouts[0].Fields["Division"].Width = new Infragistics.Windows.DataPresenter.FieldLength(300);
         //   grdData.FieldLayouts[0].Fields["code"].Visibility = System.Windows.Visibility.Collapsed;
        }

        private void LoadSubPap() 
        {

        }

        private void LoadDivision()
        {

        }

        private void LoadSpecialProjects() 
        {

        }

        private void frmMain_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

    }
}
