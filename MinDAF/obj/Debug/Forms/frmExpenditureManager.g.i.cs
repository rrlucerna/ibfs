﻿#pragma checksum "C:\Users\Minda\Desktop\MinDAF\MinDAF\Forms\frmExpenditureManager.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "04425A315FAB6F168E8C7876033489A2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace MinDAF.Forms {
    
    
    public partial class frmExpenditureManager : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.ChildWindow frm_b_expenm;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Button CancelButton;
        
        internal System.Windows.Controls.Button btnAdd;
        
        internal Infragistics.Controls.Grids.XamGrid grdData;
        
        internal System.Windows.Controls.TextBox txtCode;
        
        internal System.Windows.Controls.TextBox txtName;
        
        internal System.Windows.Controls.ComboBox cmbExpenditures;
        
        internal System.Windows.Controls.TextBox txtUACS;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/MinDAF;component/Forms/frmExpenditureManager.xaml", System.UriKind.Relative));
            this.frm_b_expenm = ((System.Windows.Controls.ChildWindow)(this.FindName("frm_b_expenm")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.CancelButton = ((System.Windows.Controls.Button)(this.FindName("CancelButton")));
            this.btnAdd = ((System.Windows.Controls.Button)(this.FindName("btnAdd")));
            this.grdData = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdData")));
            this.txtCode = ((System.Windows.Controls.TextBox)(this.FindName("txtCode")));
            this.txtName = ((System.Windows.Controls.TextBox)(this.FindName("txtName")));
            this.cmbExpenditures = ((System.Windows.Controls.ComboBox)(this.FindName("cmbExpenditures")));
            this.txtUACS = ((System.Windows.Controls.TextBox)(this.FindName("txtUACS")));
        }
    }
}

