﻿using MinDAF.Usercontrol.Budget_Allocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmProjectManager : ChildWindow
    {
        public String MainID { get; set; }
        public String Program { get; set; }
        public String DivisionId { get; set; }
        private  usrCreateProject xProj;
       
        public frmProjectManager()
        {
            InitializeComponent();

           
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            LoadProject();
        }

        private void LoadProject() 
        {
            xProj = new usrCreateProject();
            xProj.MainID = MainID;
            xProj.Program = Program;
            xProj.DivisionID = this.DivisionId;
            grdChild.Children.Add(xProj);
        }
    }
}

