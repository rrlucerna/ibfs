﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmProcActivityLibrary : ChildWindow
    {

        public APPProcAct_Data Data { get; set; }
        public String SelectMode { get; set; }
        public String Process { get; set; }

        public String FormType { get; set; }

        public event EventHandler SelectedData;

        public event EventHandler RefreshAPP;
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProcurementActivityLibrary c_procure = new clsProcurementActivityLibrary();
        private List<ProcurementActivityData> ListProcurementMode = new List<ProcurementActivityData>();


        public frmProcActivityLibrary()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }
        private void LoadData()
        {
            c_procure.Process = "LoadData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementActivityLibrary());
        }

        private void LoadDataProcurement()
        {
            c_procure.Process = "LoadData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementLibrary());
        }
        private void LoadDataFundSource()
        {
            c_procure.Process = "LoadData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadFundSource());
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_procure.Process)
            {
                case "LoadData":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementActivityData
                                     {
                                         Id = Convert.ToString(info.Element("Id").Value),
                                         Library = Convert.ToString(info.Element("Library").Value)
                                     };

                    ListProcurementMode.Clear();

                    foreach (var item in _dataLists)
                    {
                        ProcurementActivityData _varDetails = new ProcurementActivityData();


                        _varDetails.Id = item.Id;
                        _varDetails.Library = item.Library;


                        ListProcurementMode.Add(_varDetails);
                    }

                    this.Cursor = Cursors.Arrow;

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListProcurementMode;

                    grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
         
        }
      
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void SaveData() 
        {
            c_procure.Process = "SaveProc";
            c_procure.SaveProActData(Data.Code, Data.RefCode, Data.Years, Data.ModProcure, Data.AdPost, Data.SubOpen, Data.NoticeAward, Data.ContractSign,Data.SourceFunds);
            c_procure.SQLOperation += c_procure_SQLOperation;
        }

        void c_procure_SQLOperation(object sender, EventArgs e)
        {
            if (RefreshAPP!=null)
            {
                RefreshAPP(this, new EventArgs());
            }
        }

        private void frmproc__Loaded(object sender, RoutedEventArgs e)
        {
            switch (FormType)
            {
                case "Procurement Activity":
                    lblTitle.Content = "Procurement Activity Library";
                    LoadData();
                    break;
                case "Mode Procurement":
                    lblTitle.Content = "Mode of Procurment Library";
                    LoadDataProcurement();
                    break;
                case "Fund Source":
                    lblTitle.Content = "Main Fund Source Library";
                    LoadDataFundSource();
                    break;
              
            }
         
        }

      
        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            this.SelectMode = grdData.Rows[this.grdData.ActiveCell.Row.Index].Cells[1].Value.ToString();

            switch (FormType)
            {
                case "Procurement Activity":
                    switch (this.Process)
                    {
                        case "Ads/Post of IB/REI":
                            Data.AdPost = this.SelectMode;
                            break;
                        case "Sub/Open of Bids":
                            Data.SubOpen = this.SelectMode;
                            break;
                        case "Notice of Award":
                            Data.NoticeAward = this.SelectMode;
                            break;
                        case "Contract Signing":
                            Data.ContractSign = this.SelectMode;
                            break;
                    }
                   
                    break;
                case "Mode Procurement":
                    Data.ModProcure = this.SelectMode;
                    break;
                case "Fund Source":
                    Data.SourceFunds = this.SelectMode;
                    break;

            }

         

            SaveData();
            
            this.DialogResult = true;
        }
    }
}

