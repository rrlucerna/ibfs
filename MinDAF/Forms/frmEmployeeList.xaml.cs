﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmEmployeeList : ChildWindow
    {
        public event EventHandler AssignedUpdate;
        public String DivisionId { get; set; }
        public String AssignedId { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsEmployeeList c_employee = new clsEmployeeList();
        private List<EmployeeList> ListEmployee = new List<EmployeeList>();

        public frmEmployeeList()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_employee.Process)
            {
                case "FetchEmployeeList":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new EmployeeList
                                     {
                                         User_Id = Convert.ToString(info.Element("User_Id").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value)
                                         

                                     };

                    ListEmployee.Clear();

                    foreach (var item in _dataLists)
                    {
                        EmployeeList _varDetails = new EmployeeList();

                        _varDetails.User_Id = item.User_Id;
                        _varDetails.User_Fullname = item.User_Fullname;



                        ListEmployee.Add(_varDetails);
                    }

                    grdData.ItemsSource = ListEmployee;
                    grdData.Columns["User_Id"].Visibility = System.Windows.Visibility.Collapsed;

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void RefreshData()         
        {
            c_employee.Process = "FetchEmployeeList";
            svc_mindaf.ExecuteSQLAsync(c_employee.FetchEmployees(DivisionId));
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_employee_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            AssignedId = grdData.Rows[e.Cell.Row.Index].Cells["User_Id"].Value.ToString();
            if (AssignedUpdate!=null)
            {
                AssignedUpdate(this, new EventArgs());
            }
            this.DialogResult = false;
        }
    }
}

