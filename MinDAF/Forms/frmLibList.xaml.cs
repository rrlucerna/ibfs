﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmLibList : ChildWindow
    {
        public event EventHandler Selected;
        public List<ProfData> GridData{get;set;}
        public String SelectedData {get;set;}

        public frmLibList()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Selected!=null)
	            {
                SelectedData  = grdData.Rows[grdData.ActiveCell.Row.Index].Cells[0].Value.ToString();
		             Selected(this,new EventArgs());
	            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmLib_Loaded(object sender, RoutedEventArgs e)
        {
            grdData.ItemsSource = null;
            grdData.ItemsSource = GridData;
        }
    }
}

