﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmVideoHelpFile : ChildWindow
    {
        private Uri x_player;

        public frmVideoHelpFile()
        {
            InitializeComponent();
            x_player = new Uri("http://mindafinance/help/1.wmv");
            med_play.Source = x_player;
        }

        private void Element_MediaOpened(object sender, EventArgs e)
        {
            timelineSlider.Maximum = med_play.NaturalDuration.TimeSpan.TotalMilliseconds;
        }
        private void Element_MediaEnded(object sender, EventArgs e)
        {
            med_play.Stop();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void SeekToMediaPosition(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)timelineSlider.Value;


            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            med_play.Position = ts;
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            med_play.Play();
        }

        private void bnPause_Click(object sender, RoutedEventArgs e)
        {
            med_play.Pause();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            med_play.Stop();
        }

        private void SetSource(String _file) 
        {
            Uri x = new Uri(_file);
            med_play.Source = x;
        }

        private void frmvidhelp_Loaded(object sender, RoutedEventArgs e)
        {
            med_play.Play();
        }
    }
}

