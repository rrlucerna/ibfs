﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmProcurementManager : ChildWindow
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProcurement c_procurement = new clsProcurement();
        private List<ProcurementItemLists> ListProcurementItems = new List<ProcurementItemLists>();
        private List<ProcurementGeneralCategory> ListGeneralCategory = new List<ProcurementGeneralCategory>();
        private List<ProcurementSubCategory> ListSubCategory = new List<ProcurementSubCategory>();
        private List<ProcurementMindaInventory>ListMindaInventory = new List<ProcurementMindaInventory>();
        private List<ProcurementUnitOfMeasure> ListUnitOfMeasurement = new List<ProcurementUnitOfMeasure>();
        public frmProcurementManager()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_procurement.SQLOperation += c_procurement_SQLOperation;
        }

        void c_procurement_SQLOperation(object sender, EventArgs e)
        {
            switch (c_procurement.Process)
            {
               case "SaveData":
                    FetchProcurementLists();
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();

            switch (c_procurement.Process)
            {
                case "FetchProcurementLists":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementItemLists
                                     {
                                        general_category  = Convert.ToString(info.Element("general_category").Value),
                                         id = Convert.ToInt32(info.Element("id").Value),
                                         item_specifications = Convert.ToString(info.Element("item_specifications").Value),
                                        minda_inventory  = Convert.ToString(info.Element("minda_inventory").Value),
                                         price = Convert.ToDecimal(info.Element("price").Value),
                                         sub_category = Convert.ToString(info.Element("sub_category").Value),
                                        unit_of_measure  = Convert.ToString(info.Element("unit_of_measure").Value)
                                     };

                    ListProcurementItems.Clear();
             

                    foreach (var item in _dataLists)
                    {
                        ProcurementItemLists _varDetails = new ProcurementItemLists();


                        _varDetails.general_category = item.general_category;
                        _varDetails.id = item.id;
                        _varDetails.item_specifications = item.item_specifications;
                        _varDetails.minda_inventory = item.minda_inventory;
                        _varDetails.price = item.price;
                        _varDetails.sub_category = item.sub_category;
                        _varDetails.unit_of_measure= item.unit_of_measure;

                        ListProcurementItems.Add(_varDetails);
                        
                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListProcurementItems;

                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;

                    Column col_general_category = grdData.Columns.DataColumns["general_category"];
                    Column col_item_specifications = grdData.Columns.DataColumns["item_specifications"];
                    Column col_minda_inventory = grdData.Columns.DataColumns["minda_inventory"];
                    Column col_price = grdData.Columns.DataColumns["price"];
                    Column col_sub_category = grdData.Columns.DataColumns["sub_category"];
                    Column col_unit_of_measure = grdData.Columns.DataColumns["unit_of_measure"];

                    col_general_category.Width = new ColumnWidth(250, false);
                    col_item_specifications.Width = new ColumnWidth(450, false);
                    col_minda_inventory.Width = new ColumnWidth(70, false);
                    col_price.Width = new ColumnWidth(150, false);
                    col_sub_category.Width = new ColumnWidth(250, false);
                    col_unit_of_measure.Width = new ColumnWidth(70, false);

                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_item_specifications);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_general_category);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_sub_category);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_minda_inventory);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_unit_of_measure);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_price);

                    FetchGeneralCategory();
                    this.Cursor = Cursors.Arrow;

                    break;
                 case "FetchGeneralCategory":
                    XDocument oDocKeyResultsFetchGeneralCategory = XDocument.Parse(_results);
                    var _dataListsFetchGeneralCategory = from info in oDocKeyResultsFetchGeneralCategory.Descendants("Table")
                                     select new ProcurementGeneralCategory
                                     {
                                        general_category  = Convert.ToString(info.Element("general_category").Value)
                                     };

                    ListGeneralCategory.Clear();
                    cmbGeneralCategory.Items.Clear();

                    foreach (var item in _dataListsFetchGeneralCategory)
                    {
                        ProcurementGeneralCategory _varDetails = new ProcurementGeneralCategory();


                        _varDetails.general_category = item.general_category;

                        cmbGeneralCategory.Items.Add(item.general_category);
                        
                    }


                    FetchSubCategory();
                    this.Cursor = Cursors.Arrow;

                    break;
                 case "FetchSubCategory":
                    XDocument oDocKeyResultsFetchSubCategory = XDocument.Parse(_results);
                    var _dataListsFetchSubCategory = from info in oDocKeyResultsFetchSubCategory.Descendants("Table")
                                                         select new ProcurementSubCategory
                                                         {
                                                             sub_category = Convert.ToString(info.Element("sub_category").Value)
                                                         };

                    ListSubCategory.Clear();
                    cmbSubCategory.Items.Clear();

                    foreach (var item in _dataListsFetchSubCategory)
                    {
                        ProcurementSubCategory _varDetails = new ProcurementSubCategory();


                        _varDetails.sub_category = item.sub_category;

                        cmbSubCategory.Items.Add(item.sub_category);

                    }

                    FetchMindaInventory();
                    this.Cursor = Cursors.Arrow;

                    break;
                 case "FetchMindaInventory":
                    XDocument oDocKeyResultsFetchMindaInventory = XDocument.Parse(_results);
                    var _dataListsFetchMindaInventory = from info in oDocKeyResultsFetchMindaInventory.Descendants("Table")
                                                     select new ProcurementMindaInventory
                                                     {
                                                         minda_inventory = Convert.ToString(info.Element("minda_inventory").Value)
                                                     };

                    ListSubCategory.Clear();
                    cmbMindaInventory.Items.Clear();

                    foreach (var item in _dataListsFetchMindaInventory)
                    {
                        ProcurementMindaInventory _varDetails = new ProcurementMindaInventory();


                        _varDetails.minda_inventory = item.minda_inventory;

                        cmbMindaInventory.Items.Add(item.minda_inventory);

                    }

                    FetchUnitOfMeasure();

                    this.Cursor = Cursors.Arrow;

                    break;
                 case "FetchUnitOfMeasure":
                    XDocument oDocKeyResultsFetchUnitOfMeasure = XDocument.Parse(_results);
                    var _dataListsFetchUnitOfMeasure = from info in oDocKeyResultsFetchUnitOfMeasure.Descendants("Table")
                                                        select new ProcurementUnitOfMeasure
                                                        {
                                                            unit_of_measure = Convert.ToString(info.Element("unit_of_measure").Value)
                                                        };
                    cmbUnitOfMeasure.Items.Clear();
                    foreach (var item in _dataListsFetchUnitOfMeasure)
                    {
                        ProcurementUnitOfMeasure _varDetails = new ProcurementUnitOfMeasure();


                        _varDetails.unit_of_measure = item.unit_of_measure;

                        cmbUnitOfMeasure.Items.Add(item.unit_of_measure);

                    }

                    GenerateYear();

                    this.Cursor = Cursors.Arrow;

                    break;

            }

        }

        private void ClearData() 
        {
            txtItemSpecification.Text = "";
            cmbGeneralCategory.SelectedIndex = -1;
            cmbMindaInventory.SelectedIndex = -1;
            cmbSubCategory.SelectedIndex = -1;
            cmbUnitOfMeasure.SelectedIndex = -1;
            cmbYear.SelectedIndex = -1;
          
        }
        private void SaveData()
        {
            c_procurement.Process = "SaveData";
            c_procurement.SaveProjectRepresentation(txtItemSpecification.Text, cmbGeneralCategory.SelectedItem.ToString(), cmbSubCategory.SelectedItem.ToString(), cmbMindaInventory.SelectedItem.ToString(), cmbUnitOfMeasure.SelectedItem.ToString(), txtPrice.Value.ToString());
            ClearData();
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void FetchUnitOfMeasure()
        {
            c_procurement.Process = "FetchUnitOfMeasure";
            svc_mindaf.ExecuteSQLAsync(c_procurement.FetchUnitOfMeasure());
        }
        private void FetchMindaInventory()
        {
            c_procurement.Process = "FetchMindaInventory";
            svc_mindaf.ExecuteSQLAsync(c_procurement.FetchMindaInventory());
        }
        private void FetchSubCategory()
        {
            c_procurement.Process = "FetchSubCategory";
            svc_mindaf.ExecuteSQLAsync(c_procurement.FetchSubCategory());
        }
        private void FetchGeneralCategory()
        {
            c_procurement.Process = "FetchGeneralCategory";
            svc_mindaf.ExecuteSQLAsync(c_procurement.FetchGeneralCategory());
        }
        private void FetchProcurementLists() 
        {
            c_procurement.Process = "FetchProcurementLists";
            svc_mindaf.ExecuteSQLAsync(c_procurement.FetchProcurementItems());
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void GetProcurementList() 
        {

        }

        private void frm_procurement_Loaded(object sender, RoutedEventArgs e)
        {
            FetchProcurementLists();
        }

        private void grdData_CellExitedEditMode(object sender, CellExitedEditingEventArgs e)
        {
            String _Id =grdData.Rows[e.Cell.Row.Index].Cells["id"].Value.ToString();
            String _item_specifications=grdData.Rows[e.Cell.Row.Index].Cells["item_specifications"].Value.ToString(); 
            String _general_category=grdData.Rows[e.Cell.Row.Index].Cells["general_category"].Value.ToString();
            String _subcategory=grdData.Rows[e.Cell.Row.Index].Cells["sub_category"].Value.ToString(); 
            String _minda_inventory=grdData.Rows[e.Cell.Row.Index].Cells["minda_inventory"].Value.ToString(); 
            String _unit_of_measure=grdData.Rows[e.Cell.Row.Index].Cells["unit_of_measure"].Value.ToString();
            String _price = grdData.Rows[e.Cell.Row.Index].Cells["price"].Value.ToString();

            c_procurement.Process = "UpdateData";
            c_procurement.UpdatProcurementItem(_Id,_item_specifications,_general_category,_subcategory,_minda_inventory,_unit_of_measure,_price);
           

        }
    }
}

