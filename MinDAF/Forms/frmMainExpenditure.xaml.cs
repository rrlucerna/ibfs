﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmMainExpenditure : ChildWindow
    {
        private List<MainExpenditures> ListMainExpenditures = new List<MainExpenditures>();
        private List<MainExpenditures> ListMainExpendituresRecord = new List<MainExpenditures>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsExpenditureManagement c_expen = new clsExpenditureManagement();

        public frmMainExpenditure()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_expen.Process)
            {
            
                case "FetchMainExpenditure":
                    XDocument oDocKeyResultsFetchMainExpenditure = XDocument.Parse(_results);
                    var _dataListsFetchMainExpenditure = from info in oDocKeyResultsFetchMainExpenditure.Descendants("Table")
                                                         select new MainExpenditures
                                                         {

                                                             id = Convert.ToString(info.Element("id").Value),
                                                             sub_id = Convert.ToString(info.Element("sub_id").Value),
                                                             code = Convert.ToString(info.Element("code").Value),
                                                             name = Convert.ToString(info.Element("name").Value),
                                                             clone = Convert.ToString(info.Element("code").Value),
                                                             uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                             expenditure = Convert.ToString(info.Element("expenditure").Value)
                                                         };


                    ListMainExpenditures.Clear();
                    ListMainExpendituresRecord.Clear();

                    List<MainExpenditures> _list = new List<MainExpenditures>();

                    foreach (var item in _dataListsFetchMainExpenditure)
                    {
                        MainExpenditures _varProf = new MainExpenditures();


                        _varProf.id = item.id;
                        _varProf.sub_id = item.sub_id;
                        _varProf.code = item.code;
                        _varProf.name = item.name;
                        _varProf.uacs_code = item.uacs_code;
                        _varProf.expenditure = item.expenditure;
                     
                        _varProf.clone = item.clone;

                        _list.Add(_varProf);
                        ListMainExpendituresRecord.Add(_varProf);
                    }

                    var results = from p in _list
                                  group p by p.code into g
                                  select new
                                  {
                                      PAPCode = g.Key,
                                      ExpenseName = g.Select(m => m.name)
                                  };

                    foreach (var items in results)
                    {
                        MainExpenditures _data = new MainExpenditures();
                        foreach (var itemdata in items.ExpenseName.ToList())
                        {
                            _data.code = items.PAPCode;
                            _data.name = itemdata.ToString();
                            break;
                        }

                        var _listexp = _list.Where(iteml => iteml.code == items.PAPCode).ToList();
                        List<SubLibrary> _sublib = new List<SubLibrary>(); 
                        foreach (var itemexp in _listexp)
                        {
                            SubLibrary __sublib = new SubLibrary();
                            __sublib.Code = items.PAPCode;
                            __sublib.Id = itemexp.sub_id;
                            __sublib.Expenditure = itemexp.expenditure;
                            __sublib.UACS = itemexp.uacs_code;
                            __sublib.clone = itemexp.uacs_code;
                            _sublib.Add(__sublib);
                            
                        }
                        _data.ExpendituresItems = _sublib;
                        ListMainExpenditures.Add(_data);
                    }

                    grdData.ItemsSource = null;

                    grdData.ItemsSource = ListMainExpenditures;

                   
                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            foreach (var item_c in item.ChildBands[0].Rows)
                            {
                                if (item_c.Cells["Id"].Value!=null)
                                {
                                    if (item_c.Cells["Id"].Value.ToString() == _prev_code)
                                    {
                                        grdData.ScrollCellIntoView(item_c.Cells[0]);
                                        break;
                                    }
                                }
                                
                            }
                        }
                      
                    }
                    try
                    {
                        grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                        
      
                    }
                    try
                    {
                        grdData.Columns["sub_id"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {


                    }
                    try
                    {
                        grdData.Columns["clone"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                 
                    }
                    try
                    {
                        grdData.Columns["code"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["expenditure"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["uacs_code"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["name"].HeaderText = "UACS Library";
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                    
                
                   
                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.ChildBands[0].Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                            item.ChildBands[0].Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                            item.ChildBands[0].Columns["clone"].Visibility = System.Windows.Visibility.Collapsed;
                            item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                            item.IsExpanded = true;
                           
                           
                        }
                    }
                    try
                    {
                         grdData.ScrollCellIntoView(grdData.Rows[grdData.Rows.Count -1].Cells[0]);
                    }
                    catch (Exception)
                    {
                        
                      
                    }
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void SaveMainExpenditure()
        {
          
                if (ListMainExpenditures.Where(x => x.code ==txtCode.Text).ToList().Count != 0)
                {
                    MessageBox.Show("Item Entry has the same MOOE Code in the library," + Environment.NewLine + "Please Enter Another UACS CODE");
                }
                else
                {
                    c_expen.Process = "SaveMainExpenditure";
                    c_expen.SQLOperation += c_expen_SQLOperation;
                    c_expen.SaveMainExpenditure(txtCode.Text, txtName.Text);
                }
            
             
         
        }
        private void UpdateExpenditureMain( String _code, String _name)
        {
            c_expen.Process = "UpdateMainExpenditure";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.UpdateMainExpenditureMain( _code, _name);
        }
        private void UpdateExpenditureDetails( String _id, String _name, String _oldVal, String _mooeCode)
        {
            c_expen.Process = "UpdateMainExpenditure";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.UpdateMainExpenditure( _id, _name, _oldVal, _mooeCode);
        }
       
        void c_expen_SQLOperation(object sender, EventArgs e)
        {
            switch (c_expen.Process)
            {
                case "SaveMainExpenditure":
                    FetchMainExpenditure();
                    txtCode.Text = "";
                    txtName.Text = "";
                    btnAdd.Content = "Add Main Expenditure";
                    btnRemoveMainExp.Content = "Remove Main";
                    break;              
                case "UpdateMainExpenditure":
                    FetchMainExpenditure();
                    break;
                case "SaveDataExpenseItem":
                    FetchMainExpenditure();
                    ClearData();
                    tbExpenseItem.IsEnabled = false;
                    grdData.IsEnabled = true;
                    grdData.Focus();
                    break;
                case "RemoveExpenditure":
                    FetchMainExpenditure();
                    txtCode.Text = "";
                    txtName.Text = "";
                    btnRemoveMainExp.IsEnabled = true;
                    btnAdd.IsEnabled = true;
                    break;
                case "RemoveMainExpenditure":
                    FetchMainExpenditure();
                    break;
            }
        }
        private void FetchMainExpenditure()
        {
            c_expen.Process = "FetchMainExpenditure";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchMainExpenditureLists());

        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_b_expen_Loaded(object sender, RoutedEventArgs e)
        {
            FetchMainExpenditure();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            switch (btnAdd.Content.ToString())
            {
                case "Add Main Expenditure":
                    txtCode.Text = "";
                    txtName.Text = "";
                    txtCode.IsEnabled = true;
                    txtName.IsEnabled = true;
                    grdData.IsEnabled = false;
                    btnAdd.Content = "Save Main Expenditure";
                    btnRemoveMainExp.Content = "Cancel";
                    txtCode.Focus();
                    break;
                case "Save Main Expenditure":
                    SaveMainExpenditure();
                    txtCode.Text = "";
                    txtName.Text = "";
                   
                    txtCode.IsEnabled = false;
                    txtName.IsEnabled = false;
                    grdData.IsEnabled = true;
                    grdData.Focus();
                    break;
                default:
                    break;
            }
           
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
        }
        private void ClearData() 
        {
            txtExpenditureID.Text = "";
            txtExpenseName.Text = "";
            txtUACS.Text = "";

            
        }

        private void AddExpenditure()
        {
            bool is_Dupe = false;
            foreach (var item in ListMainExpenditures)
            {
                if (item.ExpendituresItems.Where(x => x.UACS ==txtUACS.Text).ToList().Count != 0)
                {
                    foreach (var item_ex in item.ExpendituresItems)
                    {
                        if (item_ex.UACS == txtUACS.Text)
                        {
                            is_Dupe = true;
                            break;
                        }
                    }
                }
                if (is_Dupe)
                {
                    break;
                }
            }
            if (is_Dupe)
            {
                MessageBox.Show("Item Entry has the same UACS Code in the library," + Environment.NewLine + "Please Enter Another UACS CODE");
            }
            else
            {
                c_expen.Process = "SaveDataExpenseItem";
                c_expen.SQLOperation += c_expen_SQLOperation;
                c_expen.SaveExpenditure(txtExpenditureID.Text, txtExpenseName.Text, txtUACS.Text);
            }
           
        }

        private void btnAddExpenseItem_Click(object sender, RoutedEventArgs e)
        {
            if (txtExpenditureID.Text == "")
            {
                MessageBox.Show("Please Select Main Expenditure");
            }
            else
            {
                tbExpenseItem.IsEnabled = true;
                txtName.IsEnabled = false;
                txtCode.IsEnabled = false;
                grdData.IsEnabled = false;
                btnAdd.IsEnabled = false;
                btnRemoveMainExp.IsEnabled = false;
                txtUACS.Focus();
            }
          
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            tbExpenseItem.IsEnabled = false;
            ClearData();
            txtName.IsEnabled = true;
            txtCode.IsEnabled = true;
            grdData.IsEnabled = true;
            btnAdd.IsEnabled = true;
            btnRemoveMainExp.IsEnabled = true;
            
            grdData.Focus();
        }

        private void grdData_ActiveCellChanged(object sender, EventArgs e)
        {
           
                try
                {
                    var _c = ListMainExpenditures.Where(x => x.name == grdData.ActiveCell.Value.ToString()).ToList();
                    if (_c.Count!=0)
                    {
                        txtExpenditureID.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString();
                        txtCode.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString();
                        txtName.Text = grdData.ActiveCell.Value.ToString();

                    }
                    else
                    {
                        txtExpenditureID.Text = "";
                    }
                       
                   
                }
                catch (Exception)
                {


                }
          
           
        }

        private String  _prev_code = "";
        private void RemoveMainExpenditure()
        {
            try
            {
                string _code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString();
                _prev_code = grdData.Rows[grdData.ActiveCell.Row.Index-1].Cells["code"].Value.ToString();

                c_expen.Process = "RemoveMainExpenditure";
                c_expen.SQLOperation += c_expen_SQLOperation;
                c_expen.RemoveMainExpenditure(_code);
                txtCode.Text = "";
                txtName.Text = "";

               
              //  _selected_uacs = "";
            }
            catch (Exception)
            {

            }

        }
        private void RemoveExpenditure() 
        {
            try
            {
             
                c_expen.Process = "RemoveExpenditure";
                c_expen.SQLOperation+=c_expen_SQLOperation;
                c_expen.RemoveExpenditure(_selected_id,_selected_uacs);
                _selected_uacs = "";
            }
            catch (Exception)
            {
                
            }
          
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
           
            AddExpenditure();
        }

        private void btnRemoveExpenditure_Click(object sender, RoutedEventArgs e)
        {
            RemoveExpenditure();
        }
        private String _selected_uacs = "";
        private String _selected_id = "";
        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            try
            {
                foreach (var item in ListMainExpenditures)
                {
                    if (item.ExpendituresItems.Where(x=>x.Expenditure==e.Cell.Value.ToString()).ToList().Count!=0)
                    {
                        foreach (var item_ex in item.ExpendituresItems)
	                    {
                            if (item_ex.Expenditure == e.Cell.Value.ToString())
                            {
                                _selected_uacs = item_ex.UACS;
                                _selected_id = item_ex.Id;
                                break;
                            }
                            else
                            {
                                _prev_code = item_ex.Id;
                            }
	                    }
                    }
                }
              
            }
            catch (Exception)
            {
                    _selected_uacs = "";
                        _selected_id = "";
            }
           
        }

        private void btnRemoveMainExp_Click(object sender, RoutedEventArgs e)
        {
            switch (btnRemoveMainExp.Content.ToString())
            {
                case "Remove Main":
                     RemoveMainExpenditure();
                    
                    break;
                case "Cancel":
                    txtCode.Text = "";
                    txtName.Text = "";
                    txtCode.IsEnabled = false;
                    txtName.IsEnabled = false;
                    grdData.IsEnabled = true;
                   
                    btnRemoveMainExp.Content = "Remove Main";
                    btnAdd.Content = "Add Main Expenditure";
                    grdData.Focus();
                    break;
            }
           
        }

        private void grdData_CellExitingEditMode(object sender, Infragistics.Controls.Grids.ExitEditingCellEventArgs e)
        {
            if (e.Cell.Value.ToString() == e.NewValue.ToString())
            {
             //   e.Cancel = true;
               
            }
            else
            {

                try
                {
                    if (e.Cell.Row.Cells["id"].Value == null)
                    {
                        UpdateExpenditureMain(grdData.Rows[e.Cell.Row.Index].Cells["code"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["name"].Value.ToString());
                    }

                }
                catch (Exception)
                {
                    String _Mooe = "";
                    String _Name = "";
                    Boolean _Invalid = false;
                    if (e.Cell.Column.HeaderText.ToString() == "UACS")
                    {
                        List<MainExpenditures> xdata = ListMainExpendituresRecord.Where(x => x.uacs_code == e.NewValue.ToString()).ToList();
                        if (xdata.Count() != 0)
                        {
                            MessageBox.Show("Item Entry has the same MOOE Code in the library," + Environment.NewLine + "Please Enter Another UACS CODE");
                            _Invalid = true;
                            e.Cancel = true;
                        }

                        if (  e.Cancel ==false)
                        {
                            if (grdData.Rows[e.Cell.Row.Index].HasChildren)
                            {
                                _Mooe = e.NewValue.ToString();

                            }
                            UpdateExpenditureDetails(e.Cell.Row.Cells["Id"].Value.ToString(), e.Cell.Row.Cells["Expenditure"].Value.ToString(), e.Cell.Row.Cells["clone"].Value.ToString(), _Mooe);

                        }

                    }
                    else if (e.Cell.Column.HeaderText.ToString() == "Expenditure")
                    {
                        if (grdData.Rows[e.Cell.Row.Index].HasChildren)
                        {
                            _Name = e.NewValue.ToString();
                            _Mooe = e.Cell.Row.Cells["clone"].Value.ToString();
                        }
                        UpdateExpenditureDetails(e.Cell.Row.Cells["Id"].Value.ToString(), _Name, e.Cell.Row.Cells["clone"].Value.ToString(), _Mooe);
                    }


                }
            }
        }
    }
}

