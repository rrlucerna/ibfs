﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmProcurementMode : ChildWindow
    {

        public String SelectMode { get; set; }
        public event EventHandler SelectedData;
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProcurementMode c_procure = new clsProcurementMode();
        private List<ProcurementData> ListProcurementMode = new List<ProcurementData>();

        public frmProcurementMode()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_procure.Process)
            {
                case "LoadProcurementData":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementData
                                     {
                                         Id = Convert.ToString(info.Element("Id").Value),
                                         Procurement = Convert.ToString(info.Element("Procurement").Value)
                                     };

                    ListProcurementMode.Clear();

                    foreach (var item in _dataLists)
                    {
                        ProcurementData _varDetails = new ProcurementData();


                        _varDetails.Id = item.Id;
                        _varDetails.Procurement = item.Procurement;


                        ListProcurementMode.Add(_varDetails);
                    }

                    this.Cursor = Cursors.Arrow;

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListProcurementMode;

                    grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }
        private void LoadProcurementData()
        {
            c_procure.Process = "LoadProcurementData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementLibrary());
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmProcDataList_Loaded(object sender, RoutedEventArgs e)
        {
            LoadProcurementData();
        }

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            this.SelectMode = grdData.Rows[this.grdData.ActiveCell.Row.Index].Cells[1].Value.ToString();
            if (SelectedData!=null)
            {
                SelectedData(this, new EventArgs());
            }
            this.DialogResult = true;
        }
    }
}

