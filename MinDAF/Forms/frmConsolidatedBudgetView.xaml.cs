﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmConsolidatedBudgetView : ChildWindow
    {
        public event EventHandler ReloadData;
        public event EventHandler CloseRealignment;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsConsolidatedBudgetView c_cons = new clsConsolidatedBudgetView();
        private List<Consolidated_DBMSUBPAP> _ListOffice = new List<Consolidated_DBMSUBPAP>();
        private List<Consolidated_Division> _ListDivision = new List<Consolidated_Division>();
        private List<Consodlidated_FundExpense> _ListFundSources = new List<Consodlidated_FundExpense>();
        private List<Consolidated_Expenses> _ListExpenses = new List<Consolidated_Expenses>();
        private List<Consolidated_SubOffice> _ListSubOffice = new List<Consolidated_SubOffice>();
        public frmConsolidatedBudgetView()
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_cons.Process)
            {
                case "LoadSUBPAP":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new Consolidated_DBMSUBPAP
                                     {
                                         SUBPAP_ID = Convert.ToString(info.Element("Id").Value),
                                         Description = Convert.ToString(info.Element("Description").Value)

                                     };

                    _ListOffice.Clear();


                    foreach (var item in _dataLists)
                    {
                        Consolidated_DBMSUBPAP _varDetails = new Consolidated_DBMSUBPAP();


                        _varDetails.SUBPAP_ID = item.SUBPAP_ID;
                        _varDetails.Description = item.Description;



                        _ListOffice.Add(_varDetails);

                    }

                    LoadDivision();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "LoadDivision":
                    XDocument oDocKeyResults_ListDivision = XDocument.Parse(_results);
                    var _dataLists_ListDivision = from info in oDocKeyResults_ListDivision.Descendants("Table")
                                                  select new Consolidated_Division
                                                  {
                                                      PAP = Convert.ToString(info.Element("Division_Code").Value),
                                                      SUBPAP_ID = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                      DIVISION_ID = Convert.ToString(info.Element("Division_Id").Value),
                                                      Description = Convert.ToString(info.Element("Description").Value)

                                                  };

                    _ListDivision.Clear();


                    foreach (var item in _dataLists_ListDivision)
                    {
                        Consolidated_Division _varDetails = new Consolidated_Division();

                        _varDetails.PAP = item.PAP;
                        _varDetails.SUBPAP_ID = item.SUBPAP_ID;
                        _varDetails.DIVISION_ID = item.DIVISION_ID;
                        _varDetails.Description = "         " + item.Description;


                        _ListDivision.Add(_varDetails);

                    }

                    LoadFundSource();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "LoadFundSource":
                    XDocument oDocKeyResultsLoadFundSource = XDocument.Parse(_results);
                    var _dataListsLoadFundSource = from info in oDocKeyResultsLoadFundSource.Descendants("Table")
                                                   select new Consodlidated_FundExpense
                                                   {
                                                       UACS = Convert.ToString(info.Element("uacs_code").Value),
                                                       EXPId = Convert.ToString(info.Element("Fund_Source_Id").Value),
                                                       Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value),
                                                       Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                       Expense = Convert.ToString(info.Element("Expense").Value),
                                                       Amount = Convert.ToString(info.Element("Amount").Value)

                                                   };

                    _ListFundSources.Clear();


                    foreach (var item in _dataListsLoadFundSource)
                    {
                        Consodlidated_FundExpense _varDetails = new Consodlidated_FundExpense();

                        _varDetails.UACS = item.UACS;
                        _varDetails.Fund_Source_Id = item.Fund_Source_Id;
                        _varDetails.Division_Id = item.Division_Id;
                        _varDetails.Expense = "         " + item.Expense;

                        _varDetails.Amount = item.Amount;
                        _ListFundSources.Add(_varDetails);

                    }

                    LoadExpenseItems();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "LoadExpenseItems":
                    XDocument oDocKeyResultsLoadExpenseItems = XDocument.Parse(_results);
                    var _dataListsLoadLoadExpenseItems = from info in oDocKeyResultsLoadExpenseItems.Descendants("Table")
                                                         select new Consolidated_Expenses
                                                         {
                                                             UACS = Convert.ToString(info.Element("uacs_code").Value),
                                                             Months = Convert.ToString(info.Element("month").Value),
                                                             div_id = Convert.ToString(info.Element("div_id").Value),
                                                             fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                             name = Convert.ToString(info.Element("name").Value),
                                                             total = Convert.ToString(info.Element("total").Value)

                                                         };

                    _ListExpenses.Clear();


                    foreach (var item in _dataListsLoadLoadExpenseItems)
                    {
                        Consolidated_Expenses _varDetails = new Consolidated_Expenses();

                        _varDetails.UACS = item.UACS;
                        _varDetails.Months = item.Months;
                        _varDetails.div_id = item.div_id;
                        _varDetails.fund_source_id = item.fund_source_id;
                        _varDetails.name = item.name;

                        _varDetails.total = item.total;
                        _ListExpenses.Add(_varDetails);

                    }

                    LoadSUBOffice();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "LoadSUBOffice":
                    XDocument oDocKeyResultsLoadSUBOffice = XDocument.Parse(_results);
                    var _dataListsLoadLoadSUBOffice = from info in oDocKeyResultsLoadSUBOffice.Descendants("Table")
                                                         select new Consolidated_SubOffice
                                                         {
                                                             DBM_Sub_Id = Convert.ToString(info.Element("DBM_Sub_Id").Value),
                                                             Description = Convert.ToString(info.Element("Description").Value),
                                                             PAP  = Convert.ToString(info.Element("PAP").Value),
                                                             Sub_Id  = Convert.ToString(info.Element("Sub_Id").Value)

                                                         };

                    _ListSubOffice.Clear();


                    foreach (var item in _dataListsLoadLoadSUBOffice)
                    {
                        Consolidated_SubOffice _varDetails = new Consolidated_SubOffice();

                        _varDetails.DBM_Sub_Id = item.DBM_Sub_Id;
                        _varDetails.Description = item.Description;
                        _varDetails.PAP = item.PAP;
                        _varDetails.Sub_Id = item.Sub_Id;
                        
                        _ListSubOffice.Add(_varDetails);

                    }

                    GenerateData();
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void LoadSUBPAP()
        {
            c_cons.Process = "LoadSUBPAP";
            svc_mindaf.ExecuteSQLAsync(c_cons.FetchDBMSubPAP());
        }
        private void LoadSUBOffice()
        {
            c_cons.Process = "LoadSUBOffice";
            svc_mindaf.ExecuteSQLAsync(c_cons.FetchSubOffice());
        }
        private void LoadDivision()
        {
            c_cons.Process = "LoadDivision";
            svc_mindaf.ExecuteSQLAsync(c_cons.FetchDivisionList());
        }

        private void LoadFundSource()
        {
            c_cons.Process = "LoadFundSource";
            svc_mindaf.ExecuteSQLAsync(c_cons.FetchFundSourceDivisionList());
        }

        private void LoadExpenseItems()
        {
            c_cons.Process = "LoadExpenseItems";
            svc_mindaf.ExecuteSQLAsync(c_cons.FetchDivisionExpenseItems("2017"));
        }
        private void GenerateData()
        {
            List<MainConsolidated> _Source = new List<MainConsolidated>();

            foreach (Consolidated_DBMSUBPAP item in _ListOffice)
            {
                MainConsolidated _OfficeData = new MainConsolidated();

                _OfficeData.Description = item.Description;
                _OfficeData.Allocation = "";
                _OfficeData.Jan = "";
                _OfficeData.Feb = "";
                _OfficeData.Mar = "";
                _OfficeData.Apr = "";
                _OfficeData.May = "";
                _OfficeData.Jun = "";
                _OfficeData.Jul = "";
                _OfficeData.Aug = "";
                _OfficeData.Sep = "";
                _OfficeData.Oct = "";
                _OfficeData.Nov = "";
                _OfficeData.Dec = "";

                _Source.Add(_OfficeData);

                List<Consolidated_SubOffice> _x_office = _ListSubOffice.Where(x => x.DBM_Sub_Id == item.SUBPAP_ID).ToList();

                foreach (var item_x_office in _x_office)
                {
                    
                    MainConsolidated _SubOfficeData = new MainConsolidated();

                    _SubOfficeData.Description = "                          " + item_x_office.Description;
                    _SubOfficeData.Allocation = "";
                    _SubOfficeData.Jan = "";
                    _SubOfficeData.Feb = "";
                    _SubOfficeData.Mar = "";
                    _SubOfficeData.Apr = "";
                    _SubOfficeData.May = "";
                    _SubOfficeData.Jun = "";
                    _SubOfficeData.Jul = "";
                    _SubOfficeData.Aug = "";
                    _SubOfficeData.Sep = "";
                    _SubOfficeData.Oct = "";
                    _SubOfficeData.Nov = "";
                    _SubOfficeData.Dec = "";

                    _Source.Add(_SubOfficeData);

                    List<Consolidated_Division> _x_division = _ListDivision.Where(x => x.PAP == item_x_office.PAP).ToList();

                    foreach (var item_x_division in _x_division)
                    {
                        MainConsolidated _DivisionData = new MainConsolidated();

                        _DivisionData.Description = "                                       " + item_x_division.Description;
                        _DivisionData.Allocation = "";
                        _DivisionData.Jan = "";
                        _DivisionData.Feb = "";
                        _DivisionData.Mar = "";
                        _DivisionData.Apr = "";
                        _DivisionData.May = "";
                        _DivisionData.Jun = "";
                        _DivisionData.Jul = "";
                        _DivisionData.Aug = "";
                        _DivisionData.Sep = "";
                        _DivisionData.Oct = "";
                        _DivisionData.Nov = "";
                        _DivisionData.Dec = "";

                        _Source.Add(_DivisionData);

                        List<Consodlidated_FundExpense> _x_fundsource = _ListFundSources.Where(x => x.Division_Id == item_x_division.DIVISION_ID).ToList();

                        double Totals = 0.00;
                        double LineTotal = 0.00;

                        double _Jan = 0.00;
                        double _Feb = 0.00;
                        double _Mar = 0.00;
                        double _Apr = 0.00;
                        double _May = 0.00;
                        double _Jun = 0.00;
                        double _Jul = 0.00;
                        double _Aug = 0.00;
                        double _Sep = 0.00;
                        double _Oct = 0.00;
                        double _Nov = 0.00;
                        double _Dec = 0.00;
                        foreach (var item_x_fundsource in _x_fundsource)
                        {
                            MainConsolidated _DivisionFunds = new MainConsolidated();
                            Totals += Convert.ToDouble(item_x_fundsource.Amount);
                            _DivisionFunds.Description = "                                                  " + item_x_fundsource.Expense;
                            _DivisionFunds.Allocation = item_x_fundsource.Amount;

                            List<Consolidated_Expenses> _expenses = _ListExpenses.Where(x_e => x_e.fund_source_id == item_x_fundsource.Fund_Source_Id).ToList();
                            List<Consolidated_Expenses> _localExpense = _expenses.Where(x_f => x_f.UACS == item_x_fundsource.UACS).ToList();
                            LineTotal = 0;

                            foreach (Consolidated_Expenses item_expenses in _localExpense)
                            {
                                LineTotal += Convert.ToDouble(item_expenses.total);
                                switch (item_expenses.Months)
                                {
                                    case "Jan":
                                        _DivisionFunds.Jan = (Convert.ToDouble(_DivisionFunds.Jan) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jan += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Feb":
                                        _DivisionFunds.Feb = (Convert.ToDouble(_DivisionFunds.Feb) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Feb += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Mar":
                                        _DivisionFunds.Mar = (Convert.ToDouble(_DivisionFunds.Mar) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Mar += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Apr":
                                        _DivisionFunds.Apr = (Convert.ToDouble(_DivisionFunds.Apr) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Apr += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "May":
                                        _DivisionFunds.May = (Convert.ToDouble(_DivisionFunds.May) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _May += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Jun":
                                        _DivisionFunds.Jun = (Convert.ToDouble(_DivisionFunds.Jun) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jun += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Jul":
                                        _DivisionFunds.Jul = (Convert.ToDouble(_DivisionFunds.Jul) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jul += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Aug":
                                        _DivisionFunds.Aug = (Convert.ToDouble(_DivisionFunds.Aug) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Aug += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Sep":
                                        _DivisionFunds.Sep = (Convert.ToDouble(_DivisionFunds.Sep) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Sep += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Oct":
                                        _DivisionFunds.Oct = (Convert.ToDouble(_DivisionFunds.Oct) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Oct += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Nov":
                                        _DivisionFunds.Nov = (Convert.ToDouble(_DivisionFunds.Nov) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Nov += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Dec":
                                        _DivisionFunds.Dec = (Convert.ToDouble(_DivisionFunds.Dec) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Dec += Convert.ToDouble(item_expenses.total);
                                        break;
                                    default:
                                        break;
                                }


                            }

                            _DivisionFunds.Total = LineTotal.ToString("#,##0.00");
                            _Source.Add(_DivisionFunds);

                        }

                        MainConsolidated _DivisionLine = new MainConsolidated();

                        _DivisionLine.Description = "------------------------------------------------------------------------------------------------------------------------------------";
                        _DivisionLine.Allocation = "----------------------";
                        _DivisionLine.Jan = "----------------------";
                        _DivisionLine.Feb = "----------------------";
                        _DivisionLine.Mar = "----------------------";
                        _DivisionLine.Apr = "----------------------";
                        _DivisionLine.May = "----------------------";
                        _DivisionLine.Jun = "----------------------";
                        _DivisionLine.Jul = "----------------------";
                        _DivisionLine.Aug = "----------------------";
                        _DivisionLine.Sep = "----------------------";
                        _DivisionLine.Oct = "----------------------";
                        _DivisionLine.Nov = "----------------------";
                        _DivisionLine.Dec = "----------------------";
                        _DivisionLine.Total = "----------------------";
                        _Source.Add(_DivisionLine);

                        MainConsolidated _DivisionTotals = new MainConsolidated();
                        double OverAll = _Jan + _Feb + _Mar + _Apr + _May + _Jun + _Jul + _Aug + _Sep + _Oct + _Nov + _Dec;
                        _DivisionTotals.Description = "Total :";
                        _DivisionTotals.Allocation = Totals.ToString("#,##0.00");
                        _DivisionTotals.Jan = _Jan.ToString("#,##0.00");
                        _DivisionTotals.Feb = _Feb.ToString("#,##0.00");
                        _DivisionTotals.Mar = _Mar.ToString("#,##0.00");
                        _DivisionTotals.Apr = _Apr.ToString("#,##0.00");
                        _DivisionTotals.May = _May.ToString("#,##0.00");
                        _DivisionTotals.Jun = _Jun.ToString("#,##0.00");
                        _DivisionTotals.Jul = _Jul.ToString("#,##0.00");
                        _DivisionTotals.Aug = _Aug.ToString("#,##0.00");
                        _DivisionTotals.Sep = _Sep.ToString("#,##0.00");
                        _DivisionTotals.Oct = _Oct.ToString("#,##0.00");
                        _DivisionTotals.Nov = _Nov.ToString("#,##0.00");
                        _DivisionTotals.Dec = _Dec.ToString("#,##0.00");
                        _DivisionTotals.Total = OverAll.ToString("#,##0.00");
                        _Source.Add(_DivisionTotals);
                        _Source.Add(_DivisionLine);
                    }
                }

                




            }

            grdData.ItemsSource = null;
            grdData.ItemsSource = _Source;

            Column col_uacs = grdData.Columns.DataColumns["Description"];
            col_uacs.Width = new ColumnWidth(400, false);
            col_uacs.IsFixed = FixedState.Left;


            Column col_alloc = grdData.Columns.DataColumns["Allocation"];
            col_alloc.Width = new ColumnWidth(100, false);
            col_alloc.IsFixed = FixedState.Left;
            col_alloc.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;

            Column col_jan = grdData.Columns.DataColumns["Jan"];
            Column col_feb = grdData.Columns.DataColumns["Feb"];
            Column col_mar = grdData.Columns.DataColumns["Mar"];
            Column col_apr = grdData.Columns.DataColumns["Apr"];
            Column col_may = grdData.Columns.DataColumns["May"];
            Column col_jun = grdData.Columns.DataColumns["Jun"];
            Column col_jul = grdData.Columns.DataColumns["Jul"];
            Column col_aug = grdData.Columns.DataColumns["Aug"];
            Column col_sep = grdData.Columns.DataColumns["Sep"];
            Column col_oct = grdData.Columns.DataColumns["Oct"];
            Column col_nov = grdData.Columns.DataColumns["Nov"];
            Column col_dec = grdData.Columns.DataColumns["Dec"];
            Column col_total = grdData.Columns.DataColumns["Total"];

            col_jan.Width = new ColumnWidth(100, false);
            col_feb.Width = new ColumnWidth(100, false);
            col_mar.Width = new ColumnWidth(100, false);
            col_apr.Width = new ColumnWidth(100, false);
            col_may.Width = new ColumnWidth(100, false);
            col_jun.Width = new ColumnWidth(100, false);
            col_jul.Width = new ColumnWidth(100, false);
            col_aug.Width = new ColumnWidth(100, false);
            col_sep.Width = new ColumnWidth(100, false);
            col_oct.Width = new ColumnWidth(100, false);
            col_nov.Width = new ColumnWidth(100, false);
            col_dec.Width = new ColumnWidth(100, false);
            col_total.Width = new ColumnWidth(100, false);

            col_jan.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_feb.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_mar.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_apr.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_may.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_jun.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_jul.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_aug.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_sep.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_oct.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_nov.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_dec.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_total.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;


            //    grdData.Columns["SUBPAP_ID"].Visibility = System.Windows.Visibility.Collapsed;

        }

        private void GenerateDataTotalOffice()
        {
            List<MainConsolidated> _Source = new List<MainConsolidated>();
            double _OverTotals = 0.00;
            double _OverLineTotal = 0.00;
            double _OverAllocationTotal = 0.00;

            double _OverJanTotal = 0.00;
            double _OverFebTotal = 0.00;
            double _OverMarTotal = 0.00;
            double _OverAprTotal = 0.00;
            double _OverMayTotal = 0.00;
            double _OverJunTotal = 0.00;
            double _OverJulTotal = 0.00;
            double _OverAugTotal = 0.00;
            double _OverSepTotal = 0.00;
            double _OverOctTotal = 0.00;
            double _OverNovTotal = 0.00;
            double _OverDecTotal = 0.00;
            foreach (Consolidated_DBMSUBPAP item in _ListOffice)
            {
               

                MainConsolidated _OfficeData = new MainConsolidated();

                _OfficeData.Description = item.Description;
                _OfficeData.Allocation = "";
                _OfficeData.Jan = "";
                _OfficeData.Feb = "";
                _OfficeData.Mar = "";
                _OfficeData.Apr = "";
                _OfficeData.May = "";
                _OfficeData.Jun = "";
                _OfficeData.Jul = "";
                _OfficeData.Aug = "";
                _OfficeData.Sep = "";
                _OfficeData.Oct = "";
                _OfficeData.Nov = "";
                _OfficeData.Dec = "";

                _Source.Add(_OfficeData);

                List<Consolidated_SubOffice> _x_office = _ListSubOffice.Where(x => x.DBM_Sub_Id == item.SUBPAP_ID).ToList();

                foreach (var item_x_office in _x_office)
                {
                    double _Totals = 0.00;
                    double _LineTotal = 0.00;
                    double _AllocationTotal = 0.00;

                    double _JanTotal = 0.00;
                    double _FebTotal = 0.00;
                    double _MarTotal = 0.00;
                    double _AprTotal = 0.00;
                    double _MayTotal = 0.00;
                    double _JunTotal = 0.00;
                    double _JulTotal = 0.00;
                    double _AugTotal = 0.00;
                    double _SepTotal = 0.00;
                    double _OctTotal = 0.00;
                    double _NovTotal = 0.00;
                    double _DecTotal = 0.00;

                    MainConsolidated _SubOfficeData = new MainConsolidated();

                    _SubOfficeData.Description = "                          " + item_x_office.Description;
                

                    List<Consolidated_Division> _x_division = _ListDivision.Where(x => x.PAP == item_x_office.PAP).ToList();

                    foreach (var item_x_division in _x_division)
                    {
                        MainConsolidated _DivisionData = new MainConsolidated();

                        _DivisionData.Description = "                                       " + item_x_division.Description;
                        _DivisionData.Allocation = "";
                     

                        List<Consodlidated_FundExpense> _x_fundsource = _ListFundSources.Where(x => x.Division_Id == item_x_division.DIVISION_ID).ToList();

                        double Totals = 0.00;
                        double LineTotal = 0.00;
                        double AllocationTotal = 0.00;

                        double _Jan = 0.00;
                        double _Feb = 0.00;
                        double _Mar = 0.00;
                        double _Apr = 0.00;
                        double _May = 0.00;
                        double _Jun = 0.00;
                        double _Jul = 0.00;
                        double _Aug = 0.00;
                        double _Sep = 0.00;
                        double _Oct = 0.00;
                        double _Nov = 0.00;
                        double _Dec = 0.00;
                        LineTotal = 0;
                        foreach (var item_x_fundsource in _x_fundsource)
                        {
                            MainConsolidated _DivisionFunds = new MainConsolidated();
                            Totals += Convert.ToDouble(item_x_fundsource.Amount);
                            _DivisionFunds.Description = "                                                  " + item_x_fundsource.Expense;
                            _DivisionFunds.Allocation = item_x_fundsource.Amount;

                            List<Consolidated_Expenses> _expenses = _ListExpenses.Where(x_e => x_e.fund_source_id == item_x_fundsource.Fund_Source_Id).ToList();
                            List<Consolidated_Expenses> _localExpense = _expenses.Where(x_f => x_f.UACS == item_x_fundsource.UACS).ToList();

                            AllocationTotal += Convert.ToDouble(item_x_fundsource.Amount);
                            foreach (Consolidated_Expenses item_expenses in _localExpense)
                            {
                                LineTotal += Convert.ToDouble(item_expenses.total);
                                switch (item_expenses.Months)
                                {
                                    case "Jan":
                                        _DivisionFunds.Jan = (Convert.ToDouble(_DivisionFunds.Jan) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jan += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Feb":
                                        _DivisionFunds.Feb = (Convert.ToDouble(_DivisionFunds.Feb) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Feb += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Mar":
                                        _DivisionFunds.Mar = (Convert.ToDouble(_DivisionFunds.Mar) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Mar += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Apr":
                                        _DivisionFunds.Apr = (Convert.ToDouble(_DivisionFunds.Apr) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Apr += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "May":
                                        _DivisionFunds.May = (Convert.ToDouble(_DivisionFunds.May) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _May += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Jun":
                                        _DivisionFunds.Jun = (Convert.ToDouble(_DivisionFunds.Jun) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jun += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Jul":
                                        _DivisionFunds.Jul = (Convert.ToDouble(_DivisionFunds.Jul) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Jul += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Aug":
                                        _DivisionFunds.Aug = (Convert.ToDouble(_DivisionFunds.Aug) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Aug += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Sep":
                                        _DivisionFunds.Sep = (Convert.ToDouble(_DivisionFunds.Sep) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Sep += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Oct":
                                        _DivisionFunds.Oct = (Convert.ToDouble(_DivisionFunds.Oct) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Oct += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Nov":
                                        _DivisionFunds.Nov = (Convert.ToDouble(_DivisionFunds.Nov) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Nov += Convert.ToDouble(item_expenses.total);
                                        break;
                                    case "Dec":
                                        _DivisionFunds.Dec = (Convert.ToDouble(_DivisionFunds.Dec) + Convert.ToDouble(item_expenses.total)).ToString("#,##0.00");
                                        _Dec += Convert.ToDouble(item_expenses.total);
                                        break;
                                    default:
                                        break;
                                }


                            }
                         
                        }


                        _DivisionData.Allocation = AllocationTotal.ToString("#,##0.00");
                        _DivisionData.Jan = _Jan.ToString("#,##0.00");
                        _DivisionData.Feb = _Feb.ToString("#,##0.00");
                        _DivisionData.Mar = _Mar.ToString("#,##0.00");
                        _DivisionData.Apr = _Apr.ToString("#,##0.00");
                        _DivisionData.May = _May.ToString("#,##0.00");
                        _DivisionData.Jun = _Jun.ToString("#,##0.00");
                        _DivisionData.Jul = _Jul.ToString("#,##0.00");
                        _DivisionData.Aug = _Aug.ToString("#,##0.00");
                        _DivisionData.Sep = _Sep.ToString("#,##0.00");
                        _DivisionData.Oct = _Oct.ToString("#,##0.00");
                        _DivisionData.Nov = _Nov.ToString("#,##0.00");
                        _DivisionData.Dec = _Dec.ToString("#,##0.00");
                        _DivisionData.Total = LineTotal.ToString("#,##0.00");

                        _Source.Add(_DivisionData);

                        _AllocationTotal += AllocationTotal;
                        _JanTotal += _Jan;
                        _FebTotal += _Feb;
                        _MarTotal += _Mar;
                        _AprTotal += _Apr;
                        _MayTotal += _May;
                        _JunTotal += _Jun;
                        _JulTotal += _Jul;
                        _AugTotal += _Aug;
                        _SepTotal += _Sep;
                        _OctTotal += _Oct;
                        _NovTotal += _Nov;
                        _DecTotal += _Dec;
                       
                      
                    }

                    MainConsolidated _DivisionLine = new MainConsolidated();

                    _DivisionLine.Description = "------------------------------------------------------------------------------------------------------------------------------------";
                    _DivisionLine.Allocation = "----------------------";
                    _DivisionLine.Jan = "----------------------";
                    _DivisionLine.Feb = "----------------------";
                    _DivisionLine.Mar = "----------------------";
                    _DivisionLine.Apr = "----------------------";
                    _DivisionLine.May = "----------------------";
                    _DivisionLine.Jun = "----------------------";
                    _DivisionLine.Jul = "----------------------";
                    _DivisionLine.Aug = "----------------------";
                    _DivisionLine.Sep = "----------------------";
                    _DivisionLine.Oct = "----------------------";
                    _DivisionLine.Nov = "----------------------";
                    _DivisionLine.Dec = "----------------------";
                    _DivisionLine.Total = "----------------------";
                    _Source.Add(_DivisionLine);
                    
                    double _TotaMain = _JanTotal + _FebTotal + _MarTotal + _AprTotal + _MayTotal + _JunTotal + _JulTotal + _AugTotal + _SepTotal + _OctTotal + _NovTotal + _DecTotal;
                    
                    MainConsolidated _DivisionTotals = new MainConsolidated();
                    _DivisionTotals.Description = "Total :";
                    _DivisionTotals.Allocation = _AllocationTotal.ToString("#,##0.00");
                    _DivisionTotals.Jan = _JanTotal.ToString("#,##0.00");
                    _DivisionTotals.Feb = _FebTotal.ToString("#,##0.00");
                    _DivisionTotals.Mar = _MarTotal.ToString("#,##0.00");
                    _DivisionTotals.Apr = _AprTotal.ToString("#,##0.00");
                    _DivisionTotals.May = _MayTotal.ToString("#,##0.00");
                    _DivisionTotals.Jun = _JunTotal.ToString("#,##0.00");
                    _DivisionTotals.Jul = _JulTotal.ToString("#,##0.00");
                    _DivisionTotals.Aug = _AugTotal.ToString("#,##0.00");
                    _DivisionTotals.Sep = _SepTotal.ToString("#,##0.00");
                    _DivisionTotals.Oct = _OctTotal.ToString("#,##0.00");
                    _DivisionTotals.Nov = _NovTotal.ToString("#,##0.00");
                    _DivisionTotals.Dec = _DecTotal.ToString("#,##0.00");
                    _DivisionTotals.Total = _TotaMain.ToString("#,##0.00");
                    
                    _Source.Add(_DivisionTotals);
                    _Source.Add(_DivisionLine);

                    _OverAllocationTotal += _AllocationTotal;
                    _OverJanTotal += _JanTotal;
                    _OverFebTotal += _FebTotal;
                    _OverMarTotal += _MarTotal;
                    _OverAprTotal += _AprTotal;
                    _OverMayTotal += _MayTotal;
                    _OverJunTotal += _JunTotal;
                    _OverJulTotal += _JulTotal;
                    _OverAugTotal += _AugTotal;
                    _OverSepTotal += _SepTotal;
                    _OverOctTotal += _OctTotal;
                    _OverNovTotal += _NovTotal;
                    _OverDecTotal += _DecTotal;
                    _OverJanTotal += _JanTotal;
                }

             




            }

            MainConsolidated _DivisionLines = new MainConsolidated();

            _DivisionLines.Description = "------------------------------------------------------------------------------------------------------------------------------------";
            _DivisionLines.Allocation = "----------------------";
            _DivisionLines.Jan = "----------------------";
            _DivisionLines.Feb = "----------------------";
            _DivisionLines.Mar = "----------------------";
            _DivisionLines.Apr = "----------------------";
            _DivisionLines.May = "----------------------";
            _DivisionLines.Jun = "----------------------";
            _DivisionLines.Jul = "----------------------";
            _DivisionLines.Aug = "----------------------";
            _DivisionLines.Sep = "----------------------";
            _DivisionLines.Oct = "----------------------";
            _DivisionLines.Nov = "----------------------";
            _DivisionLines.Dec = "----------------------";
            _DivisionLines.Total = "----------------------";
            _Source.Add(_DivisionLines);

            double _OveralTotalMain = _OverJanTotal + _OverFebTotal + _OverMarTotal + _OverAprTotal + _OverMayTotal + _OverJunTotal + _OverJulTotal + _OverAugTotal + _OverSepTotal + _OverOctTotal + _OverNovTotal + _OverDecTotal;

            MainConsolidated _DivisionOver = new MainConsolidated();
            _DivisionOver.Description = "Total :";
            _DivisionOver.Allocation = _OverAllocationTotal.ToString("#,##0.00");
            _DivisionOver.Jan = _OverJanTotal.ToString("#,##0.00");
            _DivisionOver.Feb = _OverFebTotal.ToString("#,##0.00");
            _DivisionOver.Mar = _OverMarTotal.ToString("#,##0.00");
            _DivisionOver.Apr = _OverAprTotal.ToString("#,##0.00");
            _DivisionOver.May = _OverMayTotal.ToString("#,##0.00");
            _DivisionOver.Jun = _OverJunTotal.ToString("#,##0.00");
            _DivisionOver.Jul = _OverJulTotal.ToString("#,##0.00");
            _DivisionOver.Aug = _OverAugTotal.ToString("#,##0.00");
            _DivisionOver.Sep = _OverSepTotal.ToString("#,##0.00");
            _DivisionOver.Oct = _OverOctTotal.ToString("#,##0.00");
            _DivisionOver.Nov = _OverNovTotal.ToString("#,##0.00");
            _DivisionOver.Dec = _OverDecTotal.ToString("#,##0.00");
            _DivisionOver.Total = _OveralTotalMain.ToString("#,##0.00");

            _Source.Add(_DivisionOver);
            _Source.Add(_DivisionLines);


            grdData.ItemsSource = null;
            grdData.ItemsSource = _Source;

            Column col_uacs = grdData.Columns.DataColumns["Description"];
            col_uacs.Width = new ColumnWidth(400, false);
            col_uacs.IsFixed = FixedState.Left;


            Column col_alloc = grdData.Columns.DataColumns["Allocation"];
            col_alloc.Width = new ColumnWidth(100, false);
            col_alloc.IsFixed = FixedState.Left;
            col_alloc.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;

            Column col_jan = grdData.Columns.DataColumns["Jan"];
            Column col_feb = grdData.Columns.DataColumns["Feb"];
            Column col_mar = grdData.Columns.DataColumns["Mar"];
            Column col_apr = grdData.Columns.DataColumns["Apr"];
            Column col_may = grdData.Columns.DataColumns["May"];
            Column col_jun = grdData.Columns.DataColumns["Jun"];
            Column col_jul = grdData.Columns.DataColumns["Jul"];
            Column col_aug = grdData.Columns.DataColumns["Aug"];
            Column col_sep = grdData.Columns.DataColumns["Sep"];
            Column col_oct = grdData.Columns.DataColumns["Oct"];
            Column col_nov = grdData.Columns.DataColumns["Nov"];
            Column col_dec = grdData.Columns.DataColumns["Dec"];
            Column col_total = grdData.Columns.DataColumns["Total"];

            col_jan.Width = new ColumnWidth(100, false);
            col_feb.Width = new ColumnWidth(100, false);
            col_mar.Width = new ColumnWidth(100, false);
            col_apr.Width = new ColumnWidth(100, false);
            col_may.Width = new ColumnWidth(100, false);
            col_jun.Width = new ColumnWidth(100, false);
            col_jul.Width = new ColumnWidth(100, false);
            col_aug.Width = new ColumnWidth(100, false);
            col_sep.Width = new ColumnWidth(100, false);
            col_oct.Width = new ColumnWidth(100, false);
            col_nov.Width = new ColumnWidth(100, false);
            col_dec.Width = new ColumnWidth(100, false);
            col_total.Width = new ColumnWidth(100, false);

            col_jan.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_feb.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_mar.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_apr.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_may.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_jun.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_jul.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_aug.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_sep.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_oct.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_nov.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_dec.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            col_total.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;


            //    grdData.Columns["SUBPAP_ID"].Visibility = System.Windows.Visibility.Collapsed;

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmCons_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSUBPAP();
        }

        private void btnHideMonths_Click(object sender, RoutedEventArgs e)
        {
            Column col_jan = grdData.Columns.DataColumns["Jan"];
            Column col_feb = grdData.Columns.DataColumns["Feb"];
            Column col_mar = grdData.Columns.DataColumns["Mar"];
            Column col_apr = grdData.Columns.DataColumns["Apr"];
            Column col_may = grdData.Columns.DataColumns["May"];
            Column col_jun = grdData.Columns.DataColumns["Jun"];
            Column col_jul = grdData.Columns.DataColumns["Jul"];
            Column col_aug = grdData.Columns.DataColumns["Aug"];
            Column col_sep = grdData.Columns.DataColumns["Sep"];
            Column col_oct = grdData.Columns.DataColumns["Oct"];
            Column col_nov = grdData.Columns.DataColumns["Nov"];
            Column col_dec = grdData.Columns.DataColumns["Dec"];
            switch (btnHideMonths.Content.ToString())
            {
                case "Hide Months":
                    col_jan.Visibility = System.Windows.Visibility.Collapsed;
                    col_feb.Visibility = System.Windows.Visibility.Collapsed;
                    col_mar.Visibility = System.Windows.Visibility.Collapsed;
                    col_apr.Visibility = System.Windows.Visibility.Collapsed;
                    col_may.Visibility = System.Windows.Visibility.Collapsed;
                    col_jun.Visibility = System.Windows.Visibility.Collapsed;
                    col_jul.Visibility = System.Windows.Visibility.Collapsed;
                    col_aug.Visibility = System.Windows.Visibility.Collapsed;
                    col_sep.Visibility = System.Windows.Visibility.Collapsed;
                    col_oct.Visibility = System.Windows.Visibility.Collapsed;
                    col_nov.Visibility = System.Windows.Visibility.Collapsed;
                    col_dec.Visibility = System.Windows.Visibility.Collapsed;

                    btnHideMonths.Content = "Show Months";
                    break;
                case "Show Months":
                    col_jan.Visibility = System.Windows.Visibility.Visible;
                    col_feb.Visibility = System.Windows.Visibility.Visible;
                    col_mar.Visibility = System.Windows.Visibility.Visible;
                    col_apr.Visibility = System.Windows.Visibility.Visible;
                    col_may.Visibility = System.Windows.Visibility.Visible;
                    col_jun.Visibility = System.Windows.Visibility.Visible;
                    col_jul.Visibility = System.Windows.Visibility.Visible;
                    col_aug.Visibility = System.Windows.Visibility.Visible;
                    col_sep.Visibility = System.Windows.Visibility.Visible;
                    col_oct.Visibility = System.Windows.Visibility.Visible;
                    col_nov.Visibility = System.Windows.Visibility.Visible;
                    col_dec.Visibility = System.Windows.Visibility.Visible;
                    btnHideMonths.Content = "Hide Months";
                    break;

                default:
                    break;
            }
        }

        private void btnShowOfficeTotals_Click(object sender, RoutedEventArgs e)
        {
            GenerateDataTotalOffice();
        }

        private void btnShowAllData_Click(object sender, RoutedEventArgs e)
        {
            GenerateData();
        }

      
       
    }
}

