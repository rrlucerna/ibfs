﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetOBR : ChildWindow
    {
        public String _DivisionId { get; set; }
        public String _FundSource { get; set; }
        public String _Year { get; set; }
      

        public event EventHandler ReloadData;

        private Double SelectedRate = 0;
        private String SelectedMealType = "";
        private List<OBR_Data> ListGridData = new List<OBR_Data>();
        private List<OBR_Details> ListGridDataDetails = new List<OBR_Details>();
        private List<OBR_MOOE> ListMOOE = new List<OBR_MOOE>();
        private List<OBR_MOOE_DETAILS> ListMOOEDetails = new List<OBR_MOOE_DETAILS>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsOBRData c_obr = new clsOBRData();
        private List<OBR_DIV_EXPENSE> ListDivisionExpense = new List<OBR_DIV_EXPENSE>();
        public frmBudgetOBR()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted+=svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_obr.Process)
            {

                case "FetchDivisionExpenses":
                    XDocument oDocKeyFetchDivisionExpenses = XDocument.Parse(_results);
                    var _dataListsFetchDivisionExpenses = from info in oDocKeyFetchDivisionExpenses.Descendants("Table")
                                                  select new OBR_DIV_EXPENSE
                                                  {
                                                      name = Convert.ToString(info.Element("name").Value),
                                                      total = Convert.ToString(info.Element("total").Value),
                                                      uacs_code = Convert.ToString(info.Element("uacs_code").Value)


                                                  };

                    ListDivisionExpense.Clear();

                    foreach (var item in _dataListsFetchDivisionExpenses)
                    {
                        OBR_DIV_EXPENSE _varDetails = new OBR_DIV_EXPENSE();

                        _varDetails.name = item.name;
                        _varDetails.total = item.total;
                        _varDetails.uacs_code = item.uacs_code;


                        ListDivisionExpense.Add(_varDetails);

                    }
                    FetchMOOEData();
                    break;
                case "FetchMOOEData":
                    XDocument oDocKeyFetchMOOEData = XDocument.Parse(_results);
                    var _dataListsFetchMOOEData = from info in oDocKeyFetchMOOEData.Descendants("Table")
                                                  select new OBR_MOOE
                                                  {
                                                      code = Convert.ToString(info.Element("code").Value),
                                                      name = Convert.ToString(info.Element("name").Value),
                                                      type_service = Convert.ToString(info.Element("type_service").Value)


                                                  };

                    ListMOOE.Clear();

                    foreach (var item in _dataListsFetchMOOEData)
                    {
                        OBR_MOOE _varDetails = new OBR_MOOE();

                        _varDetails.code = item.code;
                        _varDetails.name = item.name;
                        _varDetails.type_service = item.type_service;


                        ListMOOE.Add(_varDetails);

                    }
                    FetchMOOEDetails();
                    break;
                case "FetchMOOEDetails":
                    XDocument oDocKeyFetchMOOEDetails = XDocument.Parse(_results);
                    var _dataListsFetchMOOEDetails = from info in oDocKeyFetchMOOEDetails.Descendants("Table")
                                                     select new OBR_MOOE_DETAILS
                                                     {
                                                         name = Convert.ToString(info.Element("name").Value),
                                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                         mooe_id = Convert.ToString(info.Element("mooe_id").Value)

                                                     };

                    ListMOOEDetails.Clear();

                    foreach (var item in _dataListsFetchMOOEDetails)
                    {
                        OBR_MOOE_DETAILS _varDetails = new OBR_MOOE_DETAILS();

                        _varDetails.name = item.name;
                        _varDetails.uacs_code = item.uacs_code;
                        _varDetails.mooe_id = item.mooe_id;
                        ListMOOEDetails.Add(_varDetails);

                    }
                    GenerateData();
                    break;
                default:
                    break;
            }
        }

      

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_obr.Process)
            {  
                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)
                                                     
                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan" ;break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }
                        
                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    FetchDivisionExpenses();
                    
                    break;
                    
       
            }
        }

        private void GenerateData() 
        {
            ListGridData.Clear();
            List<OBR_Data> _TargetFile = new List<OBR_Data>();
            foreach (var item in ListMOOE)
            {
                List<OBR_MOOE_DETAILS> x_data = ListMOOEDetails.Where(x => x.mooe_id == item.code).ToList();
               

               
                foreach (var item_data in x_data)
                {
                    List<OBR_Details> _obr = ListGridDataDetails.Where(y => y.AccountCode==item_data.uacs_code).ToList();

                    if (_obr.Count!=0)
                    {
                        List<OBR_Data> _x = ListGridData.Where(x => x.MOOE == item.name).ToList();
                        if (_x.Count==0)
                        {
                            OBR_Data x_new = new OBR_Data();
                             x_new.MOOE = item.name;
                             ListGridData.Add(x_new);
                        }
                        OBR_Data x_new_detail = new OBR_Data();

                        x_new_detail.MOOE = "";
                        x_new_detail.Expenditure = item_data.name;
                        List<OBR_DIV_EXPENSE> _x_allocation = ListDivisionExpense.Where(x => x.uacs_code == item_data.uacs_code).ToList();
                        double _total_Allocation = 0.00;
                        foreach (var item_allocation in _x_allocation)
                        {
                            _total_Allocation += Convert.ToDouble(item_allocation.total);
                        }

                        x_new_detail.Allocation =_total_Allocation.ToString("#,##0.00");

                        double Totals_Jan = 0;
                        double Totals_Feb = 0;
                        double Totals_Mar = 0;
                        double Totals_Apr = 0;
                        double Totals_May = 0;
                        double Totals_Jun = 0;
                        double Totals_Jul = 0;
                        double Totals_Aug = 0;
                        double Totals_Sep = 0;
                        double Totals_Oct = 0;
                        double Totals_Nov = 0;
                        double Totals_Dec = 0;
                        foreach (var item_obr in _obr)
                        {
                            switch (item_obr._Month)
                            {
                               case "Jan":

                                    Totals_Jan += Convert.ToDouble(item_obr.Total);
                                    x_new_detail.Jan = Totals_Jan.ToString("#,##0.00");

                                    break;
                                case "Feb":
                                     Totals_Feb += Convert.ToDouble(item_obr.Total);
                                     x_new_detail.Feb = Totals_Feb.ToString("#,##0.00");
                                    break;
                                 case "Mar":
                                     Totals_Mar += Convert.ToDouble(item_obr.Total);
                                     x_new_detail.Mar = Totals_Mar.ToString("#,##0.00");
                                    break;
                                 case "Apr":
                                     Totals_Apr += Convert.ToDouble(item_obr.Total);
                                     x_new_detail.Apr = Totals_Apr.ToString("#,##0.00");
                                    break;
                                 case "May":
                                      Totals_May += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.May = Totals_May.ToString("#,##0.00");
                                    break;
                                 case "Jun":
                                       Totals_Jun += Convert.ToDouble(item_obr.Total);
                                       x_new_detail.Jun = Totals_Jun.ToString("#,##0.00");
                                    break;
                                 case "Jul":
                                      Totals_Jul += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Jul = Totals_Jul.ToString("#,##0.00");
                                    break;
                                 case "Aug":
                                      Totals_Aug += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Aug = Totals_Aug.ToString("#,##0.00");
                                    break;
                                 case "Sep":
                                      Totals_Sep += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Sep = Totals_Sep.ToString("#,##0.00");
                                    break;
                                 case "Oct":
                                      Totals_Oct += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Oct = Totals_Oct.ToString("#,##0.00");
                                    break;
                                 case "Nov":
                                      Totals_Nov += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Nov = Totals_Nov.ToString("#,##0.00");
                                    break;
                                 case "Dec":
                                      Totals_Dec += Convert.ToDouble(item_obr.Total);
                                      x_new_detail.Dec = Totals_Dec.ToString("#,##0.00");
                                    break;
          
                            }
                        }
                        x_new_detail.Total = (Totals_Jan +
                         Totals_Feb +
                         Totals_Mar +
                         Totals_Apr +
                         Totals_May +
                         Totals_Jun +
                         Totals_Jul +
                         Totals_Aug +
                         Totals_Sep +
                         Totals_Oct +
                         Totals_Nov +
                         Totals_Dec).ToString("#,##0.00");

                        x_new_detail.Balance = (_total_Allocation - Convert.ToDouble(x_new_detail.Total)).ToString("#,##0.00");
                        ListGridData.Add(x_new_detail);
                    }
                }
              
                grdData.ItemsSource = null;
                grdData.ItemsSource = ListGridData;
                ArrangeColumn();
            }
  

           
        }

        private void ArrangeColumn() 
        {
            Column col_Allocation = grdData.Columns.DataColumns["Allocation"];
            col_Allocation.Width = new ColumnWidth(90, false);

            Column col_Total = grdData.Columns.DataColumns["Total"];
            col_Total.Width = new ColumnWidth(90, false);
            Column col_Balance = grdData.Columns.DataColumns["Balance"];
            col_Balance.Width = new ColumnWidth(90, false);
            Column col_jan = grdData.Columns.DataColumns["Jan"];
            col_jan.Width = new ColumnWidth(90, false);

            Column col_Feb = grdData.Columns.DataColumns["Feb"];
            col_Feb.Width = new ColumnWidth(90, false);

            Column col_Mar = grdData.Columns.DataColumns["Mar"];
            col_Mar.Width = new ColumnWidth(90, false);

            Column col_Apr = grdData.Columns.DataColumns["Apr"];
            col_Apr.Width = new ColumnWidth(90, false);

            Column col_May = grdData.Columns.DataColumns["May"];
            col_May.Width = new ColumnWidth(90, false);

            Column col_Jun = grdData.Columns.DataColumns["Jun"];
            col_Jun.Width = new ColumnWidth(90, false);

            Column col_Jul = grdData.Columns.DataColumns["Jul"];
            col_Jul.Width = new ColumnWidth(90, false);

            Column col_Aug = grdData.Columns.DataColumns["Aug"];
            col_Aug.Width = new ColumnWidth(90, false);

            Column col_Sep = grdData.Columns.DataColumns["Sep"];
            col_Sep.Width = new ColumnWidth(90, false);

            Column col_Oct = grdData.Columns.DataColumns["Oct"];
            col_Oct.Width = new ColumnWidth(90, false);

            Column col_Nov = grdData.Columns.DataColumns["Nov"];
            col_Nov.Width = new ColumnWidth(90, false);

            Column col_Dec = grdData.Columns.DataColumns["Dec"];
            col_Dec.Width = new ColumnWidth(90, false);
        }
        private void FetchMOOEDetails()
        {
            c_obr.Process = "FetchMOOEDetails";
            svc_mindaf.ExecuteSQLAsync(c_obr.FetchMOOEDetails());
        }
          private void FetchDivisionExpenses()
                {
                    c_obr.Process = "FetchDivisionExpenses";
                    svc_mindaf.ExecuteSQLAsync(c_obr.FetchDivisionExpenses(this._DivisionId,this._Year));
                }
        private void FetchOBRData() 
        {
            lblTitle.Content = "Obligation Expense and Balances for Fund Source : " + _FundSource;
            c_obr.Process = "FetchOBRData";
            svc_mindaf.ExecuteImportDataSQLAsync(c_obr.FetchOBRData(_Year, _FundSource));
        }
        private void FetchMOOEData()
        {
            c_obr.Process = "FetchMOOEData";
            svc_mindaf.ExecuteSQLAsync(c_obr.FetchMOOEData());
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmBudgt_Loaded(object sender, RoutedEventArgs e)
        {
            FetchOBRData();
        }
    }
}

