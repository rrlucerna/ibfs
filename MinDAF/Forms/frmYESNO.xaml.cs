﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmYESNO : ChildWindow
    {
        public Boolean IsModify { get; set; }
        public event EventHandler ResultData;
        public frmYESNO()
        {
            InitializeComponent();
        }

      

        private void btnAddOutput_Click(object sender, RoutedEventArgs e)
        {
            IsModify = false;

            if (ResultData!=null)
	        {
                ResultData(this, new EventArgs());
	        }
            this.DialogResult = false;
        }

        private void ModifyData_Click(object sender, RoutedEventArgs e)
        {
            IsModify = true;
            if (ResultData != null)
            {
                ResultData(this, new EventArgs());
            }
            this.DialogResult = false;
        }
    }
}

