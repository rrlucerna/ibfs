﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmSettingEibanez : ChildWindow
    {
        

        public event EventHandler ReloadData;


        private List<EB_Variables> ListGridData = new List<EB_Variables>();
       
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsEbSettings c_ebs = new clsEbSettings();

        public frmSettingEibanez()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;

        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_ebs.Process)
            {

                case "FetchSettings":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new EB_Variables
                                                  {
                                                     database_name = Convert.ToString(info.Element("database_name").Value),
                                                     id  = Convert.ToString(info.Element("id").Value),
                                                     password  = Convert.ToString(info.Element("password").Value),
                                                     username  = Convert.ToString(info.Element("username").Value),
                                                     host  = Convert.ToString(info.Element("host").Value)
                                                     

                                                  };

                    ListGridData.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        EB_Variables _varDetails = new EB_Variables();

                        _varDetails.database_name = item.database_name;
                        _varDetails.id = item.id;
                        _varDetails.password = item.password;
                        _varDetails.username = item.username;
                        _varDetails.host = item.host;

                        ListGridData.Add(_varDetails);
        
                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListGridData;
                  
                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;

                    grdData.Columns["database_name"].HeaderText = "Database";
                    grdData.Columns["password"].HeaderText = "Password";
                    grdData.Columns["username"].HeaderText = "Username";
                    grdData.Columns["host"].HeaderText = "Database Host";
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void FetchSettings() 
        {
            c_ebs.Process = "FetchSettings";
            svc_mindaf.ExecuteSQLAsync(c_ebs.FetchSettings());

        }
        private void UpdateSettings()
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["id"].Value.ToString();
            String _db = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["database_name"].Value.ToString();
            String _username = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["username"].Value.ToString();
            String _pass = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["password"].Value.ToString();
            String _host = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["host"].Value.ToString();

            c_ebs.Process = "UpdateSettings";
            c_ebs.UpdateSettings(_id, _db, _username, _pass, _host);
            c_ebs.SQLOperation += c_ebs_SQLOperation;

        }

        void c_ebs_SQLOperation(object sender, EventArgs e)
        {
            FetchSettings();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmSett_Loaded(object sender, RoutedEventArgs e)
        {
            FetchSettings();
        }

        private void frmlocaltravel_Closed(object sender, EventArgs e)
        {

        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            UpdateSettings();
        }
    }
}

