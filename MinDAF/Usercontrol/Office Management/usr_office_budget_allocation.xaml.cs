﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Office_Management
{
    public partial class usr_office_budget_allocation : UserControl
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsOfficeDivision cls_office = new clsOfficeDivision();
        public event EventHandler CancelProcess;
        private List<Office> _office_data = new List<Office>();

        public usr_office_budget_allocation()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (cls_office.Process)
            {
                case "OfficeData":
                    XDocument oDoc = XDocument.Parse(_result);
                    var _data = from info in oDoc.Descendants("Table")
                                select new Office
                                {
                                    Office_Id = Convert.ToString(info.Element("Office_Id").Value),
                                    Office_Code = Convert.ToString(info.Element("Office_Code").Value),
                                    Office_Desc = Convert.ToString(info.Element("Office_Desc").Value),

                                };


                    cmbOffice.Items.Clear();

                    List<String> ComboItems = new List<string>();
                    foreach (var item in _data)
                    {
                        Office _varDetails = new Office();

                        _varDetails.Office_Id = item.Office_Id;
                        _varDetails.Office_Code = item.Office_Code;
                        _varDetails.Office_Desc = item.Office_Desc;

                        ComboItems.Add(item.Office_Code);
                        _office_data.Add(_varDetails);
                    }

                    cmbOffice.ItemsSource = ComboItems;


                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeBudget":
                    XDocument oDocDivision = XDocument.Parse(_result);
                    var _dataDivision = from info in oDocDivision.Descendants("Table")
                                        select new BudgetOfficeList
                                        {

                                            Year = Convert.ToString(info.Element("Year").Value),
                                             Office = Convert.ToString(info.Element("Office").Value),
                                             Description = Convert.ToString(info.Element("Description").Value),
                                             BudgetAllocation = Convert.ToDouble(info.Element("Budget").Value).ToString("#,##0.00")
                                             

                                        };


                    List<BudgetOfficeList> BudgetDetails = new List<BudgetOfficeList>();

                    foreach (var item in _dataDivision)
                    {
                        BudgetOfficeList _varDetails = new BudgetOfficeList();

                        _varDetails.Year = item.Year;
                        _varDetails.Office = item.Office;
                        _varDetails.Description = item.Description;

                        _varDetails.BudgetAllocation = item.BudgetAllocation;

                        BudgetDetails.Add(_varDetails);
                    }


                    grdData.ItemsSource = BudgetDetails;

                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void SaveBudget() 
        {
            String _id = "";

            foreach (var item in _office_data)
            {
                if (cmbOffice.SelectedItem.ToString() == item.Office_Code)
                {
                    _id = item.Office_Id;
                    break;
                }
            }
            cls_office.Process = "SaveOfficeBudget";
            cls_office.SaveBudgetOffice(_id, Convert.ToDouble(txtBudget.Value), "1");
            

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveBudget();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            grdScreener.Visibility = System.Windows.Visibility.Visible;
        }

        private void usr_office_budget_Loaded(object sender, RoutedEventArgs e)
        {
            cls_office.Process = "OfficeData";
            svc_mindaf.ExecuteSQLAsync("SELECT * FROM mnda_office");
         
            cls_office.OfficeBudgetData += cls_office_OfficeBudgetData;

        }

        void cls_office_OfficeBudgetData(object sender, EventArgs e)
        {
            LoadOfficeBudget();
       
        }
        private void LoadOfficeBudget() 
        {
            string _id = "";

            foreach (var item in _office_data)
            {
                if (cmbOffice.SelectedItem.ToString() == item.Office_Code)
                {
                    _id = item.Office_Id;
                    break;
                }
            }
            cls_office.Process = "FetchOfficeBudget";

            svc_mindaf.ExecuteSQLAsync(cls_office.LoadBudgetData(_id));
        }
        private void cmbOffice_DropDownClosed(object sender, EventArgs e)
        {
            LoadOfficeBudget();
        }

        private void Image_MouseEnter_1(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void Image_MouseLeave_1(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }


        private void imgMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grdScreener.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (CancelProcess != null)
            {
                CancelProcess(this, new EventArgs());
            }
        }

      
    }
    public class BudgetOfficeList 
    {
        public String Office { get; set; }
        public String Description { get; set; }
        public String BudgetAllocation { get; set; }
        public String Year { get; set; }
    }
}
