﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class BudgetPPMPApproval : UserControl
    {
     
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }

        public String ServiceType { get; set; }
        public String PAPCode { get; set; }

        private clsPPMP c_ppmp = new clsPPMP();
        private List<PPMPDataApproval> ListApproval = new List<PPMPDataApproval>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public BudgetPPMPApproval()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;      
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
              var _results = e.Result.ToString();
                switch (c_ppmp.Process)
                 {
                     case "FetchAprovals":
                        XDocument oDocKeyResultsFetchAlignmentData = XDocument.Parse(_results);
                     var _dataLists = from info in oDocKeyResultsFetchAlignmentData.Descendants("Table")
                                      select new PPMPDataApproval
                                      {
                                          Apr = Convert.ToString(info.Element("Apr").Value),
                                          Aug = Convert.ToString(info.Element("Aug").Value),
                                          Dec = Convert.ToString(info.Element("Dec").Value),
                                          Description = Convert.ToString(info.Element("Description").Value),
                                          Division = Convert.ToString(info.Element("Division").Value),
                                          EstimateBudget = Convert.ToString(info.Element("EstimateBudget").Value),
                                          Feb = Convert.ToString(info.Element("Feb").Value),
                                          Jan = Convert.ToString(info.Element("Jan").Value),
                                          Jul = Convert.ToString(info.Element("Jul").Value),
                                          Jun = Convert.ToString(info.Element("Jun").Value),
                                          Mar = Convert.ToString(info.Element("Mar").Value),
                                          May = Convert.ToString(info.Element("May").Value),
                                          ModeOfProcurement = Convert.ToString(info.Element("ModeOfProcurement").Value),
                                          Nov = Convert.ToString(info.Element("Nov").Value),
                                          Octs = Convert.ToString(info.Element("Octs").Value),
                                          PAP = Convert.ToString(info.Element("PAP").Value),
                                          Quantity = Convert.ToString(info.Element("Quantity").Value),
                                          Sep = Convert.ToString(info.Element("Sep").Value),
                                          Total = Convert.ToString(info.Element("Total").Value),
                                          Uacs = Convert.ToString(info.Element("Uacs").Value),
                                          Yearssss = Convert.ToString(info.Element("Yearssss").Value)
                               
                                      };

                     ListApproval.Clear();

                     foreach (var item in _dataLists)
                     {

                             PPMPDataApproval _varProf = new PPMPDataApproval();

                              _varProf.Apr = item.Apr;
                              _varProf.Aug = item.Aug;
                              _varProf.Dec = item.Dec;
                              _varProf.Description = item.Description;
                              _varProf.Division = item.Division;
                              _varProf.EstimateBudget = item.EstimateBudget;
                              _varProf.Feb = item.Feb;
                              _varProf.Jan = item.Jan;
                              _varProf.Jul = item.Jul;
                              _varProf.Jun = item.Jun;
                              _varProf.Mar = item.Mar;
                              _varProf.May = item.May;
                              _varProf.ModeOfProcurement = item.ModeOfProcurement;
                              _varProf.Nov = item.Nov;
                              _varProf.Octs = item.Octs;
                              _varProf.PAP = item.PAP;
                              _varProf.Quantity = item.Quantity;
                              _varProf.Sep = item.Sep;
                              _varProf.Total = item.Total;
                              _varProf.Uacs = item.Uacs;
                              _varProf.Yearssss = item.Yearssss;
                       

                             ListApproval.Add(_varProf);


                     }
                     grdData.ItemsSource = null;
                     grdData.ItemsSource = ListApproval;
                     
                     grdData.Columns["PAP"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Division"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Yearssss"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Total"].Visibility = System.Windows.Visibility.Collapsed;
                     this.Cursor = Cursors.Arrow;

                     break;
                     case "FetchReviewPPMP":
                     XDocument oDocKeyResultsFetchReviewPPMP = XDocument.Parse(_results);
                     var _dataListsFetchReviewPPMP = from info in oDocKeyResultsFetchReviewPPMP.Descendants("Table")
                                      select new PPMPDataApproval
                                      {
                                          Apr = Convert.ToString(info.Element("Apr").Value),
                                          Aug = Convert.ToString(info.Element("Aug").Value),
                                          Dec = Convert.ToString(info.Element("Dec").Value),
                                          Description = Convert.ToString(info.Element("Description").Value),
                                          Division = Convert.ToString(info.Element("Division").Value),
                                          EstimateBudget = Convert.ToString(info.Element("EstimateBudget").Value),
                                          Feb = Convert.ToString(info.Element("Feb").Value),
                                          Jan = Convert.ToString(info.Element("Jan").Value),
                                          Jul = Convert.ToString(info.Element("Jul").Value),
                                          Jun = Convert.ToString(info.Element("Jun").Value),
                                          Mar = Convert.ToString(info.Element("Mar").Value),
                                          May = Convert.ToString(info.Element("May").Value),
                                          ModeOfProcurement = Convert.ToString(info.Element("ModeOfProcurement").Value),
                                          Nov = Convert.ToString(info.Element("Nov").Value),
                                          Octs = Convert.ToString(info.Element("Octs").Value),
                                          PAP = Convert.ToString(info.Element("PAP").Value),
                                          Quantity = Convert.ToString(info.Element("Quantity").Value),
                                          Sep = Convert.ToString(info.Element("Sep").Value),
                                          Total = Convert.ToString(info.Element("Total").Value),
                                          Uacs = Convert.ToString(info.Element("Uacs").Value),
                                          Yearssss = Convert.ToString(info.Element("Yearssss").Value)

                                      };

                     ListApproval.Clear();

                     foreach (var item in _dataListsFetchReviewPPMP)
                     {

                         PPMPDataApproval _varProf = new PPMPDataApproval();

                         _varProf.Apr = item.Apr;
                         _varProf.Aug = item.Aug;
                         _varProf.Dec = item.Dec;
                         _varProf.Description = item.Description;
                         _varProf.Division = item.Division;
                         _varProf.EstimateBudget = item.EstimateBudget;
                         _varProf.Feb = item.Feb;
                         _varProf.Jan = item.Jan;
                         _varProf.Jul = item.Jul;
                         _varProf.Jun = item.Jun;
                         _varProf.Mar = item.Mar;
                         _varProf.May = item.May;
                         _varProf.ModeOfProcurement = item.ModeOfProcurement;
                         _varProf.Nov = item.Nov;
                         _varProf.Octs = item.Octs;
                         _varProf.PAP = item.PAP;
                         _varProf.Quantity = item.Quantity;
                         _varProf.Sep = item.Sep;
                         _varProf.Total = item.Total;
                         _varProf.Uacs = item.Uacs;
                         _varProf.Yearssss = item.Yearssss;


                         ListApproval.Add(_varProf);


                     }
                     grdData.ItemsSource = null;
                     grdData.ItemsSource = ListApproval;

                     grdData.Columns["PAP"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Division"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Yearssss"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Total"].Visibility = System.Windows.Visibility.Collapsed;
                     btnApprove.IsEnabled = false;
                     this.Cursor = Cursors.Arrow;

                     break;
                                                
                }
                
        }


        private void ApprovePPMP()
        {
            c_ppmp.Process = "ApprovePPMP";
            c_ppmp.ApprovePPMP(this.PAPCode,"1");
            c_ppmp.SQLOperation += c_ppmp_SQLOperation;
        }

        void c_ppmp_SQLOperation(object sender, EventArgs e)
        {
            switch (this.c_ppmp.Process)
            {
                case "ApprovePPMP":
                    FetchPPMPApproval();
                    break;
                case "RevisePPMP":
                    FetchPPMPApproval();
                    break;
                default:
                    break;
            }
        }
        private void RevisePPMP()
        {
            c_ppmp.Process = "RevisePPMP";
            c_ppmp.ApprovePPMP(this.PAPCode, "2");
            c_ppmp.SQLOperation += c_ppmp_SQLOperation;
        }

        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void FetchPPMPApproval() 
        {
            c_ppmp.Process = "FetchAprovals";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchApprovals(this.PAPCode));
        }
        private void FetchReviewPPMP()
        {
            c_ppmp.Process = "FetchReviewPPMP";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchReview(this.PAPCode,cmbYear.SelectedItem.ToString()));
        }

        private void usr_PPMPApproval_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchPPMPApproval();
        }

        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            ApprovePPMP();
        }

        private void btnApprove_Copy_Click(object sender, RoutedEventArgs e)
        {
            RevisePPMP();
        }

        private void btnReviewPPMP_Click(object sender, RoutedEventArgs e)
        {
            FetchReviewPPMP();
        }
    }
}
