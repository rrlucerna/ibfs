﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrCreateProject : UserControl
    {

        public String MainID { get; set; }
        public String Program { get; set; }
        public String DivisionID { get; set; }
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProgram cls_program = new clsProgram();
        private List<KeyResultArea> ListKeyResults = new List<KeyResultArea>();
        private List<Mindanao2020Contribution> ListMindanao2020 = new List<Mindanao2020Contribution>();
        private List<MindaScopes> ListScopes = new List<MindaScopes>();
        private List<FundSource> ListFundSource = new List<FundSource>();
        private List<AccountableDivision> ListAccountableDivision = new List<AccountableDivision>();
        private List<EncodedPrograms> ListEncodedPrograms = new List<EncodedPrograms>();


        public usrCreateProject()
        {
            InitializeComponent();
         
        }

        void cls_program_SQLOperation(object sender, EventArgs e)
        {
            switch (cls_program.Process)
            {
                case "SaveProjectProgram":

                    MessageBox.Show("Data Saved");
                    LoadEncodedPrograms();
                    break;
                case "SaveExpectedOutput":

                    MessageBox.Show("Data Saved");
                    LoadExpectedOutput(grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Id"].Value.ToString());
                    txtProjectOutput.Text = "";
                    break;
                default:
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
               var _results = e.Result.ToString();

               switch (cls_program.Process)
               {
                   case "FetchKeyResults":
                       XDocument oDocKeyResults = XDocument.Parse(_results);
                       var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                        select new  KeyResultArea
                                        {   
                                            KeyResult_Id = Convert.ToInt32(info.Element("KeyResult_Id").Value),
                                            KeyResult_Code = Convert.ToString(info.Element("KeyResult_Code").Value),
                                            KeyResult_Desc = Convert.ToString(info.Element("KeyResult_Desc").Value)
                                          
                                        };



                       ListKeyResults.Clear();
                       cmbKeyResultArea.Items.Clear();
                       foreach (var item in _dataLists)
                       {
                           KeyResultArea _varDetails = new KeyResultArea();


                           _varDetails.KeyResult_Id = item.KeyResult_Id;
                           _varDetails.KeyResult_Code = item.KeyResult_Code;
                           _varDetails.KeyResult_Desc = item.KeyResult_Desc;

                           ListKeyResults.Add(_varDetails);
                           cmbKeyResultArea.Items.Add(item.KeyResult_Desc);
                       }
                      
                       this.Cursor = Cursors.Arrow;
                       LoadMindanao2020();
                       break;
                   case "FetchMindanao2020":
                       XDocument oDocKeyMindanao2020 = XDocument.Parse(_results);
                       var _dataListsMindanao2020 = from info in oDocKeyMindanao2020.Descendants("Table")
                                        select new Mindanao2020Contribution
                                        {
                                            Mindanao_2020_Id = Convert.ToString(info.Element("Mindanao_2020_Id").Value),
                                            Mindanao_2020_Code = Convert.ToString(info.Element("Mindanao_2020_Code").Value),
                                            Mindanao_2020_Desc = Convert.ToString(info.Element("Mindanao_2020_Desc").Value)
                                        };



                       ListMindanao2020.Clear();
                       cmbMindanao2020Contribution.Items.Clear();
                       foreach (var item in _dataListsMindanao2020)
                       {
                           Mindanao2020Contribution _varDetails = new Mindanao2020Contribution();


                           _varDetails.Mindanao_2020_Id = item.Mindanao_2020_Id;
                           _varDetails.Mindanao_2020_Code = item.Mindanao_2020_Code;
                           _varDetails.Mindanao_2020_Desc = item.Mindanao_2020_Desc;

                           ListMindanao2020.Add(_varDetails);
                           cmbMindanao2020Contribution.Items.Add(item.Mindanao_2020_Desc);
                       }

                       LoadScope();
                       this.Cursor = Cursors.Arrow;
                       break;
                   case "FetchScope":
                       XDocument oDocKeyScopes = XDocument.Parse(_results);
                       var _dataListsScopes = from info in oDocKeyScopes.Descendants("Table")
                                                    select new MindaScopes
                                                    {
                                                        Scope_Id = Convert.ToString(info.Element("Scope_Id").Value),
                                                        Scope_Code = Convert.ToString(info.Element("Scope_Code").Value),
                                                        Scope_Desc = Convert.ToString(info.Element("Scope_Desc").Value)
                                                    };



                       ListScopes.Clear();
                       cmbScope.Items.Clear();
                       foreach (var item in _dataListsScopes)
                       {
                           MindaScopes _varDetails = new MindaScopes();


                           _varDetails.Scope_Id = item.Scope_Id;
                           _varDetails.Scope_Code = item.Scope_Code;
                           _varDetails.Scope_Desc = item.Scope_Desc;

                           ListScopes.Add(_varDetails);
                           cmbScope.Items.Add(item.Scope_Desc);
                       }
                       LoadFundSource();
                       this.Cursor = Cursors.Arrow;
                       break;
                   case "FetchFundSource":
                       XDocument oDocFundSource = XDocument.Parse(_results);
                       var _dataListsFundSource = from info in oDocFundSource.Descendants("Table")
                                              select new FundSource
                                              {
                                                  Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value),
                                                  Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                              };



                       ListFundSource.Clear();
                       cmbFundSource.Items.Clear();

                       foreach (var item in _dataListsFundSource)
                       {
                           FundSource _varDetails = new FundSource();


                           _varDetails.Fund_Source_Id = item.Fund_Source_Id;
                           _varDetails.Fund_Name = item.Fund_Name;

                           ListFundSource.Add(_varDetails);
                           cmbFundSource.Items.Add(item.Fund_Name);
                       }
                       LoadAccountableDivision();
                       this.Cursor = Cursors.Arrow;
                       break;
                   case "FetchDivision":
                       XDocument oDocAccountableDivision = XDocument.Parse(_results);
                       var _dataListsAccountableDivision = from info in oDocAccountableDivision.Descendants("Table")
                                                  select new AccountableDivision
                                                  {
                                                      Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                      Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                  };



                       ListAccountableDivision.Clear();
                       cmbAccountDivision.Items.Clear();

                       foreach (var item in _dataListsAccountableDivision)
                       {
                           AccountableDivision _varDetails = new AccountableDivision();


                           _varDetails.Division_Id = item.Division_Id;
                           _varDetails.Division_Desc = item.Division_Desc;

                           ListAccountableDivision.Add(_varDetails);
                           cmbAccountDivision.Items.Add(item.Division_Desc);
                       }
                       LoadEncodedPrograms();
                       this.Cursor = Cursors.Arrow;
                       break;
                   case "FetchEncodedPrograms":
                       XDocument oDocFetchEncodedPrograms = XDocument.Parse(_results);
                       var _dataListsFetchEncodedPrograms = from info in oDocFetchEncodedPrograms.Descendants("Table")
                                                           select new EncodedPrograms
                                                           {
                                                               Id = Convert.ToString(info.Element("Id").Value),
                                                               Division = Convert.ToString(info.Element("Division").Value),
                                                               Fund_Source = Convert.ToString(info.Element("Fund_Source").Value),
                                                               KeyResult_Desc = Convert.ToString(info.Element("KeyResult_Desc").Value),
                                                               Mindanao_2020_Desc = Convert.ToString(info.Element("Mindanao_2020_Desc").Value),
                                                               Project_Name = Convert.ToString(info.Element("Project_Name").Value),
                                                               Project_Year = Convert.ToString(info.Element("Project_Year").Value),
                                                               Scope = Convert.ToString(info.Element("Scope").Value)
                                                           };



                       ListEncodedPrograms.Clear();
                  

                       foreach (var item in _dataListsFetchEncodedPrograms)
                       {
                           EncodedPrograms _varDetails = new EncodedPrograms();

                           _varDetails.Id = item.Id;
                           _varDetails.Division = item.Division;
                           _varDetails.Fund_Source = item.Fund_Source;
                           _varDetails.KeyResult_Desc = item.KeyResult_Desc;
                           _varDetails.Mindanao_2020_Desc = item.Mindanao_2020_Desc;
                           _varDetails.Project_Name = item.Project_Name;
                           _varDetails.Project_Year = item.Project_Year;
                           _varDetails.Scope = item.Scope;

                           ListEncodedPrograms.Add(_varDetails);
               
                       }
                       if (ListEncodedPrograms.Count!=0)
                       {
                           grdProgramList.ItemsSource = null;
                           grdProgramList.ItemsSource = ListEncodedPrograms;
                           grdProgramList.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                       }
                       else
                       {
                           grdProgramList.ItemsSource = null;
                       }
                  
                       
                       this.Cursor = Cursors.Arrow;
                       break;
                   case "FetchExpectedOutPut":
                       XDocument oDocFetchExpectedOutPut = XDocument.Parse(_results);
                       var _dataListsFetchExpectedOutPut = from info in oDocFetchExpectedOutPut.Descendants("Table")
                                                           select new ExpectedOutput
                                                           {
                                                               id = Convert.ToString(info.Element("id").Value),
                                                               program_code = Convert.ToString(info.Element("program_code").Value),
                                                               name = Convert.ToString(info.Element("name").Value)
                                                             
                                                           };



                       List<ExpectedOutput> _listoutput = new List<ExpectedOutput>();

                       foreach (var item in _dataListsFetchExpectedOutPut)
                       {
                           ExpectedOutput _varDetails = new ExpectedOutput();

                           _varDetails.id = item.id;
                           _varDetails.program_code = item.program_code;
                           _varDetails.name = item.name;


                           _listoutput.Add(_varDetails);
               
                       }
                       if (_listoutput.Count != 0)
                       {
                           grdProjectOutput.ItemsSource = null;
                           grdProjectOutput.ItemsSource = _listoutput;
                           grdProjectOutput.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                           grdProjectOutput.Columns["program_code"].Visibility = System.Windows.Visibility.Collapsed;
                       }
                       else
                       {
                           grdProjectOutput.ItemsSource = null;
                       }
                  
                       
                       this.Cursor = Cursors.Arrow;
                       break;
               }
        }
    

        private void SaveProgram() 
        {
         String key_result_code ="";
         String mindanao_2020_code = "";
         String scope_code = "";
         String fund_source_code = "";
         String acountable_division_code = "";
         String project_name = "";
         String project_year = cmbYear.SelectedItem.ToString();

         List<KeyResultArea> x = ListKeyResults.Where(item => item.KeyResult_Desc == cmbKeyResultArea.SelectedItem.ToString()).ToList();
         foreach (var item in x)
	    {
		     key_result_code = item.KeyResult_Code;
                 break;
	    }
         List<Mindanao2020Contribution> x1 = ListMindanao2020.Where(item => item.Mindanao_2020_Desc == cmbMindanao2020Contribution.SelectedItem.ToString()).ToList();
         foreach (var item in x1)
	    {
		     mindanao_2020_code = item.Mindanao_2020_Code;
                 break;
	    }
          List<MindaScopes> x2 = ListScopes.Where(item => item.Scope_Desc == cmbScope.SelectedItem.ToString()).ToList();
         foreach (var item in x2)
	    {
		     scope_code = item.Scope_Code;
                 break;
	    }
          List<FundSource> x3 = ListFundSource.Where(item => item.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();
         foreach (var item in x3)
	    {
		     fund_source_code = item.Fund_Source_Id;
                 break;
	    }
         List<AccountableDivision> x4 = ListAccountableDivision.Where(item => item.Division_Desc == cmbAccountDivision.SelectedItem.ToString()).ToList();
         foreach (var item in x4)
	    {
            acountable_division_code = item.Division_Id;
                 break;
	    }

         project_name = txtProjectName.Text;
         cls_program.Process = "SaveProjectProgram";
         cls_program.SaveProgram(MainID,key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year);
       
         
        }
        private void LoadEncodedPrograms()
        {
            cls_program.Process = "FetchEncodedPrograms";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchEncodedPrograms(this.MainID));
        }
        private void LoadAccountableDivision()
        {
            cls_program.Process = "FetchDivision";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchAccountableDivision());
        }
        private void LoadFundSource()
        {
            cls_program.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchFundSource());
        }
        private void LoadScope()
        {
            cls_program.Process = "FetchScope";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchScope());
        }
        private void LoadMindanao2020()
        {
            cls_program.Process = "FetchMindanao2020";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchMindanao2020());
        }
        private void LoadExpectedOutput(string _project_id) 
        {
            cls_program.Process = "FetchExpectedOutPut";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchExpectedOutput(_project_id));
        }

        private void ClearData() 
        {
           

        }

        private void usrProgramCreate_Loaded(object sender, RoutedEventArgs e)
        {
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            cls_program.SQLOperation += cls_program_SQLOperation;
            cls_program.Process = "FetchKeyResults";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchKeyResultArea());
            GenerateYear();
            if (grdProgramList.Rows.Count!=0)
            {
                grdProgramList.Rows[0].IsSelected = true;
                LoadSelected();
            }
          
            

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void cmbProgram_DropDownClosed(object sender, EventArgs e)
        {
            
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveProgram();
        }
        private void LoadSelected()
        {
        
            txtProgram.Text =Program;
            txtName.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Project_Name"].Value.ToString();
            txtKeyResultArea.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["KeyResult_Desc"].Value.ToString();
            txtAccountableDivision.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Division"].Value.ToString();
            txtMindanao2020.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Mindanao_2020_Desc"].Value.ToString();
            txtFundSource.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Fund_Source"].Value.ToString();
            txtScope.Text = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Scope"].Value.ToString();

            LoadExpectedOutput(grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Id"].Value.ToString());
        }
        private void grdProgramList_ActiveCellChanged(object sender, EventArgs e)
        {
            LoadSelected();
        }
        private void SaveOutput() 
        {
            string _project_id = grdProgramList.Rows[grdProgramList.ActiveCell.Row.Index].Cells["Id"].Value.ToString();
            string _output = txtProjectOutput.Text;
            cls_program.Process = "SaveExpectedOutput";
            cls_program.SaveExpectedOutput(_project_id,_output);
       
         
        }
        private void btnOutput_Click(object sender, RoutedEventArgs e)
        {
            SaveOutput();
        }

        private void btnUpdateActivity_Click(object sender, RoutedEventArgs e)
        {
          
        }

        void f_activity_Closed(object sender, EventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Visible;
        }

        private void grdProjectOutput_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            frmActivity f_activity = new frmActivity();
            f_activity.OutputID = grdProjectOutput.Rows[grdProjectOutput.ActiveCell.Row.Index].Cells["id"].Value.ToString();
            f_activity.DivisionID = this.DivisionID;
            f_activity.Closed += f_activity_Closed;
            f_activity.Show();

            
        }
    }
}
