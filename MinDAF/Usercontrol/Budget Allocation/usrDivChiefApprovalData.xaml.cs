﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrDivChiefApprovalData : UserControl
    {
        public String DivisionId { get; set; }
        public string _User { get; set; }
        public string AttachedFundId { get; set; }
        
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
       
        private List<ChiefData_FundSourceDASH> cd_FundSource = new List<ChiefData_FundSourceDASH>();
        private List<ChiefData_FundSourceMainDetailsDASH> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetailsDASH>();
        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private bool firstLoad;

        public usrDivChiefApprovalData()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;

            usrdcd.Loaded += usrdcd_Loaded;
        }

        void usrdcd_Loaded(object sender, RoutedEventArgs e)
        {
            FetchFundSourceMain();
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            switch (c_office_dash.Process)
            {
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_result);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value)
                                             };



                    cd_FundSourceMain.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;

                        cd_FundSourceMain.Add(_varData);
                    }
                    FetchExpenditureDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_result);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchFundSourceMainDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceMainDetails":
                    XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_result);
                    var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                          select new ChiefData_FundSourceMainDetailsDASH
                                                          {
                                                              allocation = Convert.ToString(info.Element("Allocation").Value),
                                                              fund_name = Convert.ToString(info.Element("Fund_Name").Value),
                                                              id = Convert.ToString(info.Element("id").Value),
                                                              name = Convert.ToString(info.Element("Name").Value)
                                                          };



                    cd_FundSourceMainDetails.Clear();

                    foreach (var item in _dataFetchFundSourceMainDetails)
                    {
                        ChiefData_FundSourceMainDetailsDASH _varData = new ChiefData_FundSourceMainDetailsDASH();

                        _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                        _varData.fund_name = item.fund_name;
                        _varData.id = item.id;
                        _varData.name = item.name;

                        cd_FundSourceMainDetails.Add(_varData);
                    }


                    cd_FundSource.Clear();

                    foreach (var item in cd_FundSourceMain)
                    {
                        List<ChiefData_FundSourceMainDetailsDASH> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                        ChiefData_FundSourceDASH x_data = new ChiefData_FundSourceDASH();

                        x_data.FundName = item.fund_source;

                        foreach (var item_data in x_details)
                        {
                            List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                            if (x_exp.Count != 0)
                            {
                                double _total_exp = 0.00;

                                foreach (var item_r in x_exp)
                                {
                                    _total_exp += Convert.ToDouble(item_r.total);
                                }

                                item_data.total_expenditures = _total_exp.ToString("#,##0.00");
                                item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");

                            }
                            item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                        }

                        //List<BudgetBalances> _BudgetBalance = BBDetails.Where(x_bal => x_bal.Division == item.fund_source).ToList();
                        //foreach (var item_b in x_details)
                        //{
                        //    item_b.
                        //}
                        x_data.FundAllocation = x_details;

                        cd_FundSource.Add(x_data);

                    }


                    grdDivisionData.ItemsSource = null;
                    grdDivisionData.ItemsSource = cd_FundSource;
                    try
                    {
                        grdDivisionData.Columns[""].HeaderText = "Name";
                        grdDivisionData.Columns[""].HeaderText = "NEP Allocation";
                        grdDivisionData.Columns[""].HeaderText = "Expenditures";
                        grdDivisionData.Columns[""].HeaderText = "Remaining Balance";

                    }
                    catch (Exception)
                    {

                    }
                    foreach (var item in grdDivisionData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Name";
                                item_c.Columns["allocation"].HeaderText = "NEP Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Expenditures";
                                item_c.Columns["total_balance"].HeaderText = "Remaining Balance";


                            }
                        }
                    }

                    //   FetchFundSourceMain();
                    firstLoad = false;
                    this.Cursor = Cursors.Arrow;
                    break;
                  
            }
        }
        private void FetchFundSourceMain()
        {
            c_office_dash.Process = "FetchFundMain";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSource(DivisionId, "2017"));
        }
        private void FetchExpenditureDetails()
        {
            c_office_dash.Process = "FetchExpenditureDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionExpenditureDetails(this._User,"2017", this.AttachedFundId));
        }
        private void FetchFundSourceMainDetails()
        {
            c_office_dash.Process = "FetchFundSourceMainDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSourceDetails(DivisionId,"2017"));
        }
        private void FetchBudgetAllocation()
        {
            
            c_office_dash.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchBudgetAllocation(DivisionId, "2017"));
        }

       
    }
}
