﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol.PopBox;
using Infragistics;
using Infragistics.Controls.Grids;
using Infragistics.Controls.Grids.Primitives;
using Infragistics.Controls.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usr_budget_creation : UserControl
    {
        public String DivisionID { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
 
        private clsBudget c_budget = new clsBudget();

        private List<BudgetExpenditures> LocalData_Budget = new List<BudgetExpenditures>();
        private List<ProjectBudgetDetails> BudgetData = new List<ProjectBudgetDetails>();
        private Double AllocatedBudget = 0.00;

       

        public usr_budget_creation()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_budget.SQLOperation += c_budget_SQLOperation;
            grdMOOE.FilteringSettings.AllowFiltering = FilterUIType.FilterRowTop;
            grdMOOE.FilteringSettings.FilteringScope = FilteringScope.ColumnLayout;

        }

        void c_budget_SQLOperation(object sender, EventArgs e)
        {
            switch (c_budget.Process)
            {
                case "SaveProjectBudget":
                    if (c_budget.ReturnCode=="True")
                    {
                        MessageBox.Show("Project has been saved successfully", "Project Saved", MessageBoxButton.OK);

                        ResetData();

                    }
                    else
                    {
                        MessageBox.Show("There was an error in saving the project", "Error", MessageBoxButton.OK);
                    }
                   

                    break;
            }
        }


        private void ResetData() 
        {
            txtProjectName.Text = "";
            txtYear.Text = "";
            grdBudgetAllocation.ItemsSource = null;
            txtOverAllTotal.Value = 0.00;
            txtRemainingBalance.Value = 0.00;
            lblProject.Content = "";
            BudgetData.Clear();
            ClearDetails();
          //  FetchDivisionLimit();
            DisableDataEntry();
            txtProjectName.Focus();
            txtProjectName.SelectAll();

        }
    

     
       public void ClearAllXamGridFilters(XamGrid grid)
        {
            grid.FilteringSettings.RowFiltersCollection.Clear();
            ClearFilterCells(grid.Rows);
        }

      

       public void FetchDivisionLimit() 
       {
           c_budget.Process = "GetDivisionLimit";

           svc_mindaf.ExecuteSQLAsync(c_budget.GetDivisionBudget(this.DivisionID,this.txtYear.Text));
       }

       public void ClearFilterCells(RowCollection rows)
       {
           if (rows != null)
           {
               if (rows[0].RowType == RowType.DataRow)
               {
                   foreach (Cell cell in ((RowsManager)rows[0].Manager).FilterRowTop.Cells)
                   {
                       (cell as FilterRowCell).FilterCellValue = null;
                   }

                   foreach (Row dr in rows)
                   {
                       if (dr.HasChildren)
                       {
                           foreach (ChildBand childBand in dr.ChildBands)
                           {
                               childBand.RowFiltersCollection.Clear();
                           }
                           ClearFilterCells(dr.ChildBands[0].Rows);
                       }
                   }
               }
               else
               {
                   if (rows[0].GetType() == typeof(GroupByRow))
                   {
                       foreach (GroupByRow gbr in rows)
                       {
                           gbr.RowFiltersCollection.Clear();
                           ClearFilterCells(gbr.Rows);

                           foreach (Cell cell in ((RowsManager)gbr.Rows[0].Manager).FilterRowTop.Cells)
                           {
                               (cell as FilterRowCell).FilterCellValue = null;
                           }
                       }
                   }
               }
           }
       }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            switch (c_budget.Process)
            {
                case "GetExpenditures":


                      XDocument oDocExpenditures = XDocument.Parse(_result);
                      var _dataExpenditures = from info in oDocExpenditures.Descendants("Table")
                                 select new  BudgetExpenditures
                                 {
                                     ID = Convert.ToInt32(info.Element("ID").Value),
                                     MOOE_ID = Convert.ToString(info.Element("MOOE_ID").Value),
                                     Description  = Convert.ToString(info.Element("Description").Value)
                        
                                 };


                      List<BudgetExpenditures> ExpendituresDetails = new List<BudgetExpenditures>();

                      foreach (var item in _dataExpenditures)
                    {
                        BudgetExpenditures _varDetails = new BudgetExpenditures();

                   
                        _varDetails.ID = item.ID;
                        _varDetails.MOOE_ID = item.MOOE_ID;
                        _varDetails.Description = item.Description;


                        LocalData_Budget.Add(_varDetails);
                    }


                      grdMOOE.ItemsSource = LocalData_Budget;
                      foreach (var item in grdMOOE.Columns)
                      {
                          if (item.HeaderText =="ID" || item.HeaderText=="MOOE_ID")
                          {
                              item.Visibility = System.Windows.Visibility.Collapsed;
                          }
                      }

                    this.Cursor = Cursors.Arrow;
               
                    break;
                case "GetDivisionLimit":


                    XDocument oDocLimits = XDocument.Parse(_result);
                    var _dataLimits = from info in oDocLimits.Descendants("Table")
                                      select new DivisionLimit
                                      {
                                          Amount = Convert.ToDouble(info.Element("Amount").Value)

                                      };


                     

                    foreach (var item in _dataLimits)
                    {
                        txtRemainingBalance.Value = item.Amount;
                        AllocatedBudget = item.Amount;
                    }


                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void LoadExpenditures() 
        {
            c_budget.Process = "GetExpenditures";

            svc_mindaf.ExecuteSQLAsync(c_budget.GetExpenditures());
        }

        private void usr_budget_Loaded(object sender, RoutedEventArgs e)
        {
           LoadExpenditures();
           
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            //this.ApplyFilter();
        }

        private void txtProjectName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                txtYear.IsEnabled = true;
                txtYear.Focus();
                txtYear.SelectAll();
            }
        }

     

        private void grdMOOE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                txtMOOETotal.Focus();
                txtMOOETotal.SelectAll();
            }
        }

        private void txtMOOETotal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                lblProject.Content = txtProjectName.Text;


                AddBudget(grdMOOE.Rows[grdMOOE.ActiveCell.Row.Index].Cells[0].Value.ToString(), grdMOOE.ActiveCell.Value.ToString(), Convert.ToString(txtMOOETotal.Value));
                
                ComputeTotal();
                ClearDetails();
                grdMOOE.Focus();

            }
        }
        private void ClearDetails() 
        {
            txtMOOETotal.Value = 0.00;
        }
        private void ComputeTotal() 
        {
            try
            {
                double _Total = 0.00;
                double _Remaining =AllocatedBudget;

                foreach (var item in BudgetData)
                {
                    _Total += Convert.ToDouble(item.Amount);
                }
                _Remaining -= _Total;
                txtRemainingBalance.Value = _Remaining;
                txtOverAllTotal.Value = _Total;
            }
            catch (Exception)
            {
            
            }
          
        }

        private void AddBudget(String Exp_id, String Description, String Amount) 
        {
            ProjectBudgetDetails c_details = new ProjectBudgetDetails();
            c_details.Amount = Convert.ToDouble(Amount).ToString("#,##0.00");
            c_details.Description = Description;
            c_details.ExpenditureID = Exp_id;
            c_details.Active = true;
            c_details.Year = txtYear.Text;
            BudgetData.Add(c_details);



            RefreshData();

            

        }

        private void RefreshData() 
        {
            grdBudgetAllocation.ItemsSource = null;
            grdBudgetAllocation.ItemsSource = BudgetData;

            grdBudgetAllocation.Columns["ExpenditureID"].Visibility = System.Windows.Visibility.Collapsed;
            grdBudgetAllocation.Columns["Year"].Visibility = System.Windows.Visibility.Collapsed;

            grdMOOE.Focus();
        }

        private void btnSff_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveProjectProposal();
        }

        private void SaveProjectProposal() 
        {
            c_budget.Process = "SaveProjectBudget";
            c_budget.SaveProjectBudget(BudgetData, DivisionID, lblProject.Content.ToString(), "0");
            
            
        }

        private void RemoveItem() 
        {
            string _selected_id = grdBudgetAllocation.ActiveCell.Row.Cells["ExpenditureID"].Value.ToString();

            foreach (var item in BudgetData)
            {
                if (item.ExpenditureID == _selected_id)
                {
                    BudgetData.Remove(item);
                    break;
                }
            }
            RefreshData();
        }


        private void grdBudgetAllocation_CellClicked(object sender, CellClickedEventArgs e)
        {
            if (e.Cell.Value.ToString().ToLower() == "true")
            {
                foreach (var item in BudgetData)
                {
                    if (item.ExpenditureID == grdBudgetAllocation.ActiveCell.Row.Cells["ExpenditureID"].Value.ToString())
                    {
                        item.Active = false;
                        break;
                    }
                }
                RefreshData();
            }
            else if(e.Cell.Value.ToString().ToLower() == "false")
            {
                foreach (var item in BudgetData)
                {
                    if (item.ExpenditureID == grdBudgetAllocation.ActiveCell.Row.Cells["ExpenditureID"].Value.ToString())
                    {
                        item.Active = true;
                        break;
                    }
                }
                RefreshData();
            }
           
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

            if ( grdBudgetAllocation.ActiveCell==null)
            {
                MessageBox.Show("Please Select Item to remove", "No Item Selected", MessageBoxButton.OK);
            }
            else
            {
                if (MessageBox.Show("Confirm Delete Record", "Delete Record", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    foreach (var item in BudgetData)
                    {
                        if (item.ExpenditureID == grdBudgetAllocation.ActiveCell.Row.Cells["ExpenditureID"].Value.ToString())
                        {
                            BudgetData.Remove(item);
                            break;
                        }
                    }
                    RefreshData();
                }
            }
         
          
        }

        private void txtYear_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key== Key.Enter)
            {
                if (txtYear.Text=="")
                {
                    MessageBox.Show("Please Enter Year First", "Year Entry is Empty", MessageBoxButton.OK);

                }
                else
                {
                    FetchDivisionLimit();
                    EnableDataEntry();
                    grdMOOE.Focus();
                }
            
            }
        }

        private void EnableDataEntry() 
        {
            grdMOOE.IsEnabled = true;
            txtMOOETotal.IsEnabled = true;
            txtOverAllTotal.IsEnabled = true;
            txtRemainingBalance.IsEnabled = true;
        }
        private void DisableDataEntry()
        {
            grdMOOE.IsEnabled = false;
            txtMOOETotal.IsEnabled = false;
            txtOverAllTotal.IsEnabled = false;
            txtRemainingBalance.IsEnabled = false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ResetData();
        }
    

    }

   
}
