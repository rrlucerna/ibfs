﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrChiefData : UserControl
    {

        public String DivisionId { get; set; }
        private String SelectedYear { get; set; }
        public String OfficeId { get; set; }
        public String PAPCODE { get; set; }
        public String _User { get; set; }

        public String AttachedFundId { get; set; }

        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<BudgetExpenitureMain> ListBudgetExpense = new List<BudgetExpenitureMain>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<BudgetBalances> BBDetails = new List<BudgetBalances>();
        private List<ExpenseItemsList> ExpenseItemData = new List<ExpenseItemsList>();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetailsDASH> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetailsDASH>();
        private List<ChiefData_FundSourceDASH> cd_FundSource = new List<ChiefData_FundSourceDASH>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<ChiefData_DivisionFundSource> cd_DivisionFundSource = new List<ChiefData_DivisionFundSource>();
        public usrChiefData()
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
        }

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (c_office_dash.Process)
            {
                case "FetchBalances":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new BudgetBalances
                                             {
                                                 Division = Convert.ToString(info.Element("Division").Value),
                                                 Office = Convert.ToString(info.Element("Office").Value),
                                                 Total = Convert.ToString(info.Element("Total").Value)

                                             };



                    BBDetails.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        BudgetBalances _varData = new BudgetBalances();

                        _varData.Division = item.Division;
                        _varData.Office = item.Office;
                        _varData.Total = item.Total;

                        BBDetails.Add(_varData);

                    }
                    FetchBudgetAllocation();
                   


                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             string _result = e.Result.ToString();
             switch (c_office_dash.Process)
             {
                 case "FetchBudgetAllocation":
                     
                        XDocument oDocFetchBudgetAllocation = XDocument.Parse(_result);
                        var _dataFetchBudgetAllocation = from info in oDocFetchBudgetAllocation.Descendants("Table")
                                                         select new BudgetExpenditureList
                                                         {
                                                             mo_name = Convert.ToString(info.Element("mo_name").Value),
                                                             Fund_Source = Convert.ToString(info.Element("fund_name").Value),
                                                             Amount = Convert.ToString(info.Element("Amount").Value),
                                                             Name = Convert.ToString(info.Element("Name").Value),
                                                             pap_code = Convert.ToString(info.Element("pap_code").Value),
                                                             pap_sub = Convert.ToString(info.Element("pap_sub").Value),
                                                             uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                         };

                        List<BudgetExpenditureList> _list = new List<BudgetExpenditureList>();
                        List<BudgetExpenditureList> _list_sub = new List<BudgetExpenditureList>();
                        //ListBudgetAllocated.Clear();

                        ListBudgetExpense.Clear();

                        foreach (var item in _dataFetchBudgetAllocation)
                        {
                            BudgetExpenditureList _varData = new BudgetExpenditureList();

                            _varData.Amount = item.Amount;
                            _varData.mo_name = item.mo_name;
                            _varData.Fund_Source  = item.Fund_Source;
                            _varData.Name = item.Name;
                            _varData.pap_code = item.pap_code;
                            _varData.pap_sub = item.pap_sub;
                            _varData.uacs_code = item.uacs_code;


                            _list.Add(_varData);
                            _list_sub.Add(_varData);
                        }

                        var results = from p in _list
                                      group p by p.pap_code into g
                                      select new
                                      {
                                          PAPCode = g.Key,
                                          ExpenseName = g.Select(m => m.mo_name)
                                      };

                        foreach (var item in results)
                        {
                            BudgetExpenitureMain x_datas = new BudgetExpenitureMain();

                            var x = item.ExpenseName.ToList();

                            x_datas.MOOE_NAME =x[0].ToString();
                           
                            List<BudgetExpenditureList> _xList = _list_sub.Where(items => items.pap_sub ==item.PAPCode).ToList();
                            List<BudgetExpenditureDashBoard> _subdata = new List<BudgetExpenditureDashBoard>();

                            foreach (var itemList in _xList)
                            {
                                BudgetExpenditureDashBoard _xxdata = new BudgetExpenditureDashBoard();

                                _xxdata.Source = itemList.Fund_Source;
                                _xxdata.Expenditure = itemList.Name;
                                _xxdata.UACS = itemList.uacs_code;
                                _xxdata.Total = Convert.ToDouble(itemList.Amount).ToString("#,##0.00");

                                _subdata.Add(_xxdata);
                            }
                            x_datas.ListExpenditure=_subdata;

                            ListBudgetExpense.Add(x_datas);
                           
                        }


                        grdBudget.ItemsSource = null;
                        grdBudget.ItemsSource = ListBudgetExpense;

                   
                        foreach (var item in grdBudget.Rows)
                        {
                            if (item.HasChildren)
                            {
                                item.IsExpanded = true;
                            }
                        }
                        grdBudget.Columns["MOOE_NAME"].HeaderText = "Mode of Expenditure";

                        FetchFundSourceMain();

                        break;
                 case "FetchDivisionFundSource":
                        XDocument oDocFetchDivisionFundSource = XDocument.Parse(_result);
                        var _dataFetchDivisionFundSource = from info in oDocFetchDivisionFundSource.Descendants("Table")
                                                           select new ChiefData_DivisionFundSource
                                                          {
                                                              Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                            
                                                          };



                        cmbFundSource.Items.Clear();

                        foreach (var item in _dataFetchDivisionFundSource)
                            {
                                
                               cmbFundSource.Items.Add(item.Fund_Name);

                            }
                        cmbFundSource.SelectedIndex =0;
                        FetchBalances();
                            this.Cursor = Cursors.Arrow;
                            break;
                     case "FetchOfficeDetails":
                         XDocument oDocFetchOfficeDetails = XDocument.Parse(_result);
                         var _dataFetchOfficeDetails = from info in oDocFetchOfficeDetails.Descendants("Table")
                                                       select new OfficeDetails
                                                       {
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                           division_id = Convert.ToString(info.Element("div_id").Value),
                                                           Fund_Source = Convert.ToString(info.Element("Fund_Name").Value),
                                                           balance = "0.00",
                                                           total_program_budget = "0.00",
                                                           pap_code = ""
                                                       };



                         BalanceDetails.Clear();

                         foreach (var item in _dataFetchOfficeDetails)
                         {
                             OfficeDetails _varData = new OfficeDetails();

                             _varData.budget_allocation = item.budget_allocation;
                             _varData.division_id = item.division_id;
                             _varData.Fund_Source = item.Fund_Source;
                             _varData.total_program_budget = item.total_program_budget;
                             _varData.pap_code = item.pap_code;
                             _varData.fund_source_id = item.fund_source_id;
                             BalanceDetails.Add(_varData);

                         }
                         //  FetchOfficeTotals();
                         FetchDivisionFundSource();
                         this.Cursor = Cursors.Arrow;
                         break;
                     case "FetchExpenditureDetails":
                         XDocument oDocFetchExpenditureDetails = XDocument.Parse(_result);
                         var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                      select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)
                                                         
                                                       };



                         cd_ExpenditureLists.Clear();

                         foreach (var item in _dataFetchExpenditureDetails)
                         {
                             ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                             _varData.expenditure = item.expenditure;
                             _varData.fund_source_id = item.fund_source_id;
                             _varData.total = item.total;
                             
                             cd_ExpenditureLists.Add(_varData);

                         }
                         FetchFundSourceMainDetails();
                         this.Cursor = Cursors.Arrow;
                         break;
                     case "FetchFundMain":
                         XDocument oDocFetchFundMain = XDocument.Parse(_result);
                         var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                                       select new ChiefData_FundSourceMain
                                                       {
                                                          id  = Convert.ToString(info.Element("id").Value),
                                                          fund_source  = Convert.ToString(info.Element("fund_name").Value)
                                                       };



                         cd_FundSourceMain.Clear();

                         foreach (var item in _dataFetchFundMain)
                         {
                             ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                             _varData.id = item.id;
                             _varData.fund_source = item.fund_source;
                           
                             cd_FundSourceMain.Add(_varData);
                         }
                         FetchExpenditureDetails();
                         this.Cursor = Cursors.Arrow;
                         break;
                     case "FetchFundSourceMainDetails":
                         XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_result);
                         var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                  select new ChiefData_FundSourceMainDetailsDASH
                                                  {
                                                     allocation  = Convert.ToString(info.Element("Allocation").Value),
                                                     fund_name  = Convert.ToString(info.Element("Fund_Name").Value),
                                                     id  = Convert.ToString(info.Element("id").Value),
                                                     name  = Convert.ToString(info.Element("Name").Value)
                                                  };



                         cd_FundSourceMainDetails.Clear();

                         foreach (var item in _dataFetchFundSourceMainDetails)
                         {
                             ChiefData_FundSourceMainDetailsDASH _varData = new ChiefData_FundSourceMainDetailsDASH();

                             _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                             _varData.fund_name = item.fund_name;
                             _varData.id = item.id;
                             _varData.name = item.name;

                             cd_FundSourceMainDetails.Add(_varData);
                         }


                         cd_FundSource.Clear();

                         foreach (var item in cd_FundSourceMain)
                         {
                             List<ChiefData_FundSourceMainDetailsDASH> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                             ChiefData_FundSourceDASH x_data = new ChiefData_FundSourceDASH();

                             x_data.FundName = item.fund_source;
                             
                             foreach (var item_data in x_details)
                             {
                                 List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                                 if (x_exp.Count!=0)
                                 {
                                      double _total_exp =0.00;

                                     foreach (var item_r in x_exp)
                                     {
                                         _total_exp += Convert.ToDouble(item_r.total);
                                     }
                                     
                                     item_data.total_expenditures = _total_exp.ToString("#,##0.00");
                                     item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");
                                     
                                 }
                                 item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                             }

                             //List<BudgetBalances> _BudgetBalance = BBDetails.Where(x_bal => x_bal.Division == item.fund_source).ToList();
                             //foreach (var item_b in x_details)
                             //{
                             //    item_b.
                             //}
                             x_data.FundAllocation = x_details;

                             cd_FundSource.Add(x_data);

                         }
                       

                         grdDivisionData.ItemsSource = null;
                         grdDivisionData.ItemsSource = cd_FundSource;
                         try
                         {
                             grdDivisionData.Columns[""].HeaderText = "Name";
                             grdDivisionData.Columns[""].HeaderText = "NEP Allocation";
                             grdDivisionData.Columns[""].HeaderText = "Expenditures";
                             grdDivisionData.Columns[""].HeaderText = "Remaining Balance";

                         }
                         catch (Exception)
                         {

                         }
                         foreach (var item in grdDivisionData.Rows)
                         {
                             if (item.HasChildren)
                             {
                                 item.IsExpanded = true;
                                 foreach (var item_c in item.ChildBands)
                                 {
                                     item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                     item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                     item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;

                                     item_c.Columns["name"].HeaderText = "Name";
                                     item_c.Columns["allocation"].HeaderText = "NEP Allocation";
                                     item_c.Columns["total_expenditures"].HeaderText = "Expenditures";
                                     item_c.Columns["total_balance"].HeaderText = "Remaining Balance";
                                     
                                   
                                 }
                             }
                         }

                      //   FetchFundSourceMain();
                         firstLoad = false;
                         this.Cursor = Cursors.Arrow;
                         break;
                  
            }
             
        }

        private void FetchFundSourceMainDetails()
        {
            c_office_dash.Process = "FetchFundSourceMainDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSourceDetails(DivisionId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchExpenditureDetails()
        {
            c_office_dash.Process = "FetchExpenditureDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionExpenditureDetails(this._User, cmbYear.SelectedItem.ToString(),this.AttachedFundId));
        }
        private void FetchFundSourceMain()
        {
            c_office_dash.Process = "FetchFundMain";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSource(DivisionId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchFundSource()
        {
            c_office_dash.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeTotals(OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchOfficeTotals()
        {
            c_office_dash.Process = "FetchOfficeTotals";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeTotals(OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchBalances()
        {
            c_office_dash.Process = "FetchBalances";
            svc_mindaf.ExecuteImportDataSQLAsync(c_office_dash.FetchBalancesDivision(PAPCODE, cmbYear.SelectedItem.ToString()));
        }
        private void FetchOfficeDetails()
        {
            c_office_dash.Process = "FetchOfficeDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeDetails( OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchBudgetAllocation()
        {
            SelectedYear = cmbYear.SelectedItem.ToString();
            c_office_dash.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchBudgetAllocation(DivisionId, SelectedYear));
        }
         private void FetchDivisionFundSource()
                {

                    c_office_dash.Process = "FetchDivisionFundSource";
                    svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionFundSource(DivisionId, SelectedYear));
                }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            for (int i = 0; i < cmbYear.Items.Count; i++)
            {
                if (cmbYear.Items[i].ToString() == DateTime.Now.Year.ToString())
                {
                    cmbYear.SelectedIndex = 0;
                    break;
                }
            }
         
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
        private bool firstLoad = true;

        private void ctrlDashChief_Loaded(object sender, RoutedEventArgs e)
        {
           
            if (firstLoad == false)
            {
                GenerateYear();
                FetchOfficeDetails();
                firstLoad = true;
            }
            else
            {
                firstLoad = false;
            }
        }

        private void btnOBR_Click(object sender, RoutedEventArgs e)
        {
            frmBudgetOBR b_obr = new frmBudgetOBR();
            b_obr._FundSource = cmbFundSource.SelectedItem.ToString();
            b_obr._Year = cmbYear.SelectedItem.ToString();
            b_obr._DivisionId = this.DivisionId;
            b_obr.Show();
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            if (firstLoad == false)
            {
            //    GenerateYear();
                FetchOfficeDetails();
                firstLoad = true;
            }
            else
            {
                firstLoad = false;
            }
        }
    }
}
