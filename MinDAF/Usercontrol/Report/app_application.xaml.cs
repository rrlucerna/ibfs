﻿using Infragistics.Controls.Grids;
using Infragistics.Controls.Grids.Primitives;
using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class app_application : UserControl
    {

        public String DivId { get; set; }
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }
        public String Division { get; set; }
        public String PAP { get; set; }
        public String User { get; set; }
        public Int32 Level { get; set; }

        public String ServiceType { get; set; }
        public String Revision { get; set; }

      
        private clsAPPData c_app = new clsAPPData();
        private List<PPMPData> _PPMPData = new List<PPMPData>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private MinDAFSVCClient svc_mindaf_refresher = new MinDAFSVCClient();

        private MinDAFSVCClient svc_process_fetch = new MinDAFSVCClient();
        private clsProcurementMode c_procure = new clsProcurementMode();
        private List<ProcurementData> ListProcurementMode = new List<ProcurementData>();
        private List<PPMPFormat> _Main = new List<PPMPFormat>();
        private List<PPMPAlignment> _AlignmentData = new List<PPMPAlignment>();

        private List<APPData_List> _AppData = new List<APPData_List>();
        private List<APP_PROGRAMS> _AppPrograms = new List<APP_PROGRAMS>();
        private List<APP_MOOEList> _AppMooe = new List<APP_MOOEList>();
        private List<APPProcAct_Data> _AppProAct = new List<APPProcAct_Data>();

        private clsReportCache c_cache = new clsReportCache();

        public app_application()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_process_fetch.ExecuteSQLCompleted += svc_process_fetch_ExecuteSQLCompleted;
            ServiceType = "502";
        }

        void svc_process_fetch_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_app.Process)
            {
               
                case "FetchAlignmentData":
                    XDocument oDocKeyResultsFetchAlignmentData = XDocument.Parse(_results);
                    var _dataListsFetchAlignmentData = from info in oDocKeyResultsFetchAlignmentData.Descendants("Table")
                                                       select new PPMPAlignment
                                                       {
                                                           months = Convert.ToString(info.Element("months").Value),
                                                           fundsource = Convert.ToString(info.Element("fundsource").Value),
                                                           mooe = Convert.ToString(info.Element("mooe").Value),
                                                           name = Convert.ToString(info.Element("name").Value),
                                                           from_total = Convert.ToString(info.Element("from_total").Value),
                                                           from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                                           to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                                           total_alignment = Convert.ToString(info.Element("total_alignment").Value)

                                                       };

                    _AlignmentData.Clear();

                    foreach (var item in _dataListsFetchAlignmentData)
                    {

                        PPMPAlignment _varProf = new PPMPAlignment();

                        _varProf.months = item.months;
                        _varProf.fundsource = item.fundsource;
                        _varProf.mooe = item.mooe;
                        _varProf.name = item.name;
                        _varProf.from_uacs = item.from_uacs;
                        _varProf.from_total = item.from_total;
                        _varProf.to_uacs = item.to_uacs;
                        _varProf.total_alignment = item.total_alignment;

                        _AlignmentData.Add(_varProf);

                    }
                    GenerateData();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchMOOE":
                    XDocument oDocKeyResultsFetchMOOE = XDocument.Parse(_results);
                    var _dataListsFetchMOOE = from info in oDocKeyResultsFetchMOOE.Descendants("Table")
                                              select new APP_MOOEList
                                              {
                                                  Code = Convert.ToString(info.Element("code").Value),
                                                  Name = Convert.ToString(info.Element("name").Value)


                                              };

                    _AppMooe.Clear();

                    foreach (var item in _dataListsFetchMOOE)
                    {

                        APP_MOOEList _varProf = new APP_MOOEList();

                        _varProf.Name = item.Name;
                        _varProf.Code = item.Code;

                        _AppMooe.Add(_varProf);

                    }
                    FetchProActData();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchProActData":
                    XDocument oDocKeyFetchProActData = XDocument.Parse(_results);
                    var _dataListsFetchProActData = from info in oDocKeyFetchProActData.Descendants("Table")
                                                    select new APPProcAct_Data
                                                    {
                                                        AdPost = Convert.ToString(info.Element("ad_post_rei").Value),
                                                        Code = Convert.ToString(info.Element("code").Value),
                                                        ContractSign = Convert.ToString(info.Element("contract_signing").Value),
                                                        ModProcure = Convert.ToString(info.Element("mod_procure").Value),
                                                        NoticeAward = Convert.ToString(info.Element("notice_award").Value),
                                                        RefCode = Convert.ToString(info.Element("ref_code").Value),
                                                        SourceFunds = Convert.ToString(info.Element("source_funds").Value),
                                                        SubOpen = Convert.ToString(info.Element("sub_open_bids").Value)


                                                    };


                    _AppProAct.Clear();

                    foreach (var item in _dataListsFetchProActData)
                    {

                        APPProcAct_Data _varProf = new APPProcAct_Data();

                        _varProf.AdPost = item.AdPost;
                        _varProf.Code = item.Code;
                        _varProf.ContractSign = item.ContractSign;
                        _varProf.ModProcure = item.ModProcure;
                        _varProf.NoticeAward = item.NoticeAward;
                        _varProf.RefCode = item.RefCode;
                        _varProf.SourceFunds = item.SourceFunds;
                        _varProf.SubOpen = item.SubOpen;


                        _AppProAct.Add(_varProf);

                    }
                    FetchPrograms();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchPPMPRevision":
                    XDocument oDocKeyResultsFetchPPMPRevision = XDocument.Parse(_results);
                    var _dataListsFetchPPMPRevision = from info in oDocKeyResultsFetchPPMPRevision.Descendants("Table")
                                                      select new PPMPRevision
                                                      {
                                                          Revision = Convert.ToString(info.Element("_rev").Value)

                                                      };


                    Int32 _rev = 0;

                    foreach (var item in _dataListsFetchPPMPRevision)
                    {
                        Revision = item.Revision;
                        _rev += 1;
                    }
                    if (Revision == null)
                    {
                        Revision = "0";
                    }
                    else
                    {
                        Revision = _rev.ToString();
                    }

                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchPrograms":

                    XDocument oDocKeyResultsFetchPrograms = XDocument.Parse(_results);
                    var _dataListsFetchPrograms = from info in oDocKeyResultsFetchPrograms.Descendants("Table")
                                                  select new APP_PROGRAMS
                                                  {
                                                      ID = Convert.ToString(info.Element("id").Value),
                                                      PAPCODE = Convert.ToString(info.Element("pap").Value),
                                                      PMO = Convert.ToString(info.Element("pmo").Value),
                                                      Programs = Convert.ToString(info.Element("project_name").Value)

                                                  };


                    _AppPrograms.Clear();

                    foreach (var item in _dataListsFetchPrograms)
                    {

                        APP_PROGRAMS _varProf = new APP_PROGRAMS();

                        _varProf.ID = item.ID;
                        _varProf.PAPCODE = item.PAPCODE;
                        _varProf.PMO = item.PMO;
                        _varProf.Programs = item.Programs;



                        _AppPrograms.Add(_varProf);

                    }
                    _AppData.Clear();
                    int _counter_1 = 0;
                    int _counter_2 = 0;
                    foreach (var item in _AppPrograms)
                    {


                        APPData_List _xdata = new APPData_List();
                        _counter_1 += 1;
                        _counter_2 = 0;
                        _xdata.code = item.PMO + "." + _counter_1.ToString();
                        _xdata.procurement_project = item.Programs;
                        _xdata.pmo = item.PMO;

                        _AppData.Add(_xdata);

                        List<PPMPData> _activity_data = _PPMPData.Where(items => items._ref == item.ID).ToList();

                        foreach (var itemoe in _AppMooe)
                        {

                            List<PPMPData> _sub = _activity_data.Where(x => x.mooe_id == itemoe.Code).ToList();
                            double _totalMOOE = 0.00;

                            APPData_List _xdatasub = new APPData_List();
                            foreach (var itemsub in _sub)
                            {
                                _totalMOOE += Convert.ToDouble(Convert.ToDouble(itemsub.quantity) * Convert.ToDouble(itemsub.rate));
                            }
                            if (_sub.Count > 0)
                            {
                                _counter_2 += 1;
                                _xdatasub.code = item.PMO + "." + _counter_1.ToString() + "." + _counter_2.ToString();
                                _xdatasub.procurement_project = "     " + itemoe.Name;
                                _xdatasub.pmo = item.PMO;
                                _xdatasub.total = _totalMOOE.ToString("#,##0.00");
                                _xdatasub.mooe_total = _totalMOOE.ToString("#,##0.00");
                                _xdatasub.mooe_code = itemoe.Code;

                                List<APPProcAct_Data> _subproc = _AppProAct.Where(x => x.Code == _xdatasub.code && x.RefCode == itemoe.Code).ToList();

                                if (_subproc.Count != 0)
                                {
                                    _xdatasub.mode_procurement = _subproc[0].ModProcure;
                                    _xdatasub.ad_post_rei = _subproc[0].AdPost;
                                    _xdatasub.sub_open = _subproc[0].SubOpen;
                                    _xdatasub.notice_award = _subproc[0].NoticeAward;
                                    _xdatasub.contract_signing = _subproc[0].ContractSign;
                                    _xdatasub.source_funds = _subproc[0].SourceFunds;
                                }

                                _AppData.Add(_xdatasub);
                            }


                        }
                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _AppData;


                    ArrangeColumn();

                    SavePrintOut();

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void LoadProcurementData()
        {
            c_app.Process = "LoadProcurementData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementLibrary());
        }


        private void FetchPrograms() 
        {
            c_app.Process = "FetchPrograms";
            svc_mindaf.ExecuteSQLAsync(c_app.GetPrograms(cmbYear.SelectedItem.ToString()));

        }

        private void FetchAPP()
        {

            FetchMOOE();
        }
        private void FetchAlignmentData()
        {
            c_app.Process = "FetchAlignmentData";
            svc_mindaf.ExecuteSQLAsync(c_procure.FetchAlignmentDataAPP(cmbYear.SelectedItem.ToString()));
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_app.Process)
            {
                case "FetchPPMPData":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new PPMPData
                                     {
                                         Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         e_date = Convert.ToString(info.Element("e_date").Value),
                                         entry_date = Convert.ToString(info.Element("entry_date").Value),
                                         month = Convert.ToString(info.Element("month").Value),
                                         name = Convert.ToString(info.Element("name").Value),
                                         quantity = Convert.ToString(info.Element("quantity").Value),
                                         rate = Convert.ToString(info.Element("rate").Value),
                                         s_date = Convert.ToString(info.Element("s_date").Value),
                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                         year = Convert.ToString(info.Element("year").Value),
                                         type_service = Convert.ToString(info.Element("type_service").Value),
                                         act_id = Convert.ToString(info.Element("act_id").Value),
                                         mooe_id = Convert.ToString(info.Element("mooe_id").Value),
                                         _ref = Convert.ToString(info.Element("_ref").Value),
                                     };

                    _PPMPData.Clear();

                    foreach (var item in _dataLists)
                    {
                        if (item.uacs_code.Substring(0, 3) == ServiceType)
                        {
                            PPMPData _varProf = new PPMPData();

                            _varProf.Fund_Name = item.Fund_Name;
                            _varProf.Division_Code = item.Division_Code;
                            _varProf.Division_Desc = item.Division_Desc;
                            _varProf.e_date = item.e_date.Replace("T00:00:00+08:00", "");
                            _varProf.entry_date = item.entry_date;
                            _varProf.month = item.month;
                            _varProf.name = item.name;
                            _varProf.quantity = item.quantity;
                            _varProf.rate = item.rate;
                            _varProf.s_date = item.s_date.Replace("T00:00:00+08:00", "");
                            _varProf.uacs_code = item.uacs_code;
                            _varProf.year = item.year;
                            _varProf.type_service = item.type_service;
                            _varProf.act_id = item.act_id;
                            _varProf.mooe_id = item.mooe_id;
                            _varProf._ref = item._ref;
                            _PPMPData.Add(_varProf);
                        }


                    }
                    FetchAlignmentData();

                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchAlignmentData":
                    XDocument oDocKeyResultsFetchAlignmentData = XDocument.Parse(_results);
                    var _dataListsFetchAlignmentData = from info in oDocKeyResultsFetchAlignmentData.Descendants("Table")
                                                       select new PPMPAlignment
                                                       {
                                                           months = Convert.ToString(info.Element("months").Value),
                                                           fundsource = Convert.ToString(info.Element("fundsource").Value),
                                                           mooe = Convert.ToString(info.Element("mooe").Value),
                                                           name = Convert.ToString(info.Element("name").Value),
                                                           from_total = Convert.ToString(info.Element("from_total").Value),
                                                           from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                                           to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                                           total_alignment = Convert.ToString(info.Element("total_alignment").Value)

                                                       };

                    _AlignmentData.Clear();

                    foreach (var item in _dataListsFetchAlignmentData)
                    {

                        PPMPAlignment _varProf = new PPMPAlignment();

                        _varProf.months = item.months;
                        _varProf.fundsource = item.fundsource;
                        _varProf.mooe = item.mooe;
                        _varProf.name = item.name;
                        _varProf.from_uacs = item.from_uacs;
                        _varProf.from_total = item.from_total;
                        _varProf.to_uacs = item.to_uacs;
                        _varProf.total_alignment = item.total_alignment;

                        _AlignmentData.Add(_varProf);

                    }
                    GenerateData();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchMOOE":
                    XDocument oDocKeyResultsFetchMOOE = XDocument.Parse(_results);
                    var _dataListsFetchMOOE = from info in oDocKeyResultsFetchMOOE.Descendants("Table")
                                                       select new APP_MOOEList
                                                       {
                                                           Code = Convert.ToString(info.Element("code").Value),
                                                           Name = Convert.ToString(info.Element("name").Value)
                                                          

                                                       };

                    _AppMooe.Clear();

                    foreach (var item in _dataListsFetchMOOE)
                    {

                        APP_MOOEList _varProf = new APP_MOOEList();

                        _varProf.Name = item.Name;
                        _varProf.Code = item.Code;

                        _AppMooe.Add(_varProf);

                    }
                    FetchProActData();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchProActData":
                    XDocument oDocKeyFetchProActData = XDocument.Parse(_results);
                    var _dataListsFetchProActData = from info in oDocKeyFetchProActData.Descendants("Table")
                                                    select new APPProcAct_Data
                                                       {
                                                         AdPost  = Convert.ToString(info.Element("ad_post_rei").Value),
                                                         Code   = Convert.ToString(info.Element("code").Value),
                                                         ContractSign      = Convert.ToString(info.Element("contract_signing").Value),
                                                         ModProcure     = Convert.ToString(info.Element("mod_procure").Value),
                                                         NoticeAward    = Convert.ToString(info.Element("notice_award").Value),
                                                         RefCode    = Convert.ToString(info.Element("ref_code").Value),
                                                         SourceFunds    = Convert.ToString(info.Element("source_funds").Value),
                                                         SubOpen   = Convert.ToString(info.Element("sub_open_bids").Value)
                

                                                       };
              

                    _AppProAct.Clear();

                    foreach (var item in _dataListsFetchProActData)
                    {

                        APPProcAct_Data _varProf = new APPProcAct_Data();

                         _varProf.AdPost = item.AdPost;
                         _varProf.Code = item.Code;
                         _varProf.ContractSign = item.ContractSign;
                         _varProf.ModProcure = item.ModProcure;
                         _varProf.NoticeAward = item.NoticeAward;
                         _varProf.RefCode = item.RefCode;
                         _varProf.SourceFunds = item.SourceFunds;
                         _varProf.SubOpen = item.SubOpen;
                    

                        _AppProAct.Add(_varProf);

                    }
                    FetchPrograms();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchPPMPRevision":
                    XDocument oDocKeyResultsFetchPPMPRevision = XDocument.Parse(_results);
                    var _dataListsFetchPPMPRevision = from info in oDocKeyResultsFetchPPMPRevision.Descendants("Table")
                                                      select new PPMPRevision
                                                      {
                                                          Revision = Convert.ToString(info.Element("_rev").Value)

                                                      };


                    Int32 _rev = 0;

                    foreach (var item in _dataListsFetchPPMPRevision)
                    {
                        Revision = item.Revision;
                        _rev += 1;
                    }
                    if (Revision == null)
                    {
                        Revision = "0";
                    }
                    else
                    {
                        Revision = _rev.ToString();
                    }

                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchPrograms":
          
                    XDocument oDocKeyResultsFetchPrograms = XDocument.Parse(_results);
                    var _dataListsFetchPrograms = from info in oDocKeyResultsFetchPrograms.Descendants("Table")
                                                  select new APP_PROGRAMS
                                                      {
                                                          ID = Convert.ToString(info.Element("id").Value),
                                                          PAPCODE = Convert.ToString(info.Element("pap").Value),
                                                          PMO  = Convert.ToString(info.Element("pmo").Value),
                                                          Programs = Convert.ToString(info.Element("project_name").Value)

                                                      };


                    _AppPrograms.Clear();

                    foreach (var item in _dataListsFetchPrograms)
                    {

                        APP_PROGRAMS _varProf = new APP_PROGRAMS();

                        _varProf.ID = item.ID;
                        _varProf.PAPCODE = item.PAPCODE;
                        _varProf.PMO = item.PMO;
                        _varProf.Programs = item.Programs;
                        
                        

                        _AppPrograms.Add(_varProf);

                    }
                    _AppData.Clear();
                    int _counter_1 = 0;
                    int _counter_2 = 0;
                    foreach (var item in _AppPrograms)
                    {
                     

                        APPData_List _xdata = new APPData_List();
                        _counter_1 += 1;
                        _counter_2 = 0;
                        _xdata.code = item.PMO +"."+ _counter_1.ToString();
                        _xdata.procurement_project = item.Programs;
                        _xdata.pmo = item.PMO;

                        _AppData.Add(_xdata);

                        List<PPMPData> _activity_data = _PPMPData.Where(items => items._ref == item.ID).ToList();

                        foreach (var itemoe in _AppMooe)
                        {
                           
                            List<PPMPData> _sub = _activity_data.Where(x => x.mooe_id == itemoe.Code).ToList();
                            double _totalMOOE = 0.00;

                            APPData_List _xdatasub = new APPData_List();
                            foreach (var itemsub in _sub)
                            {
                                _totalMOOE += Convert.ToDouble(Convert.ToDouble(itemsub.quantity) * Convert.ToDouble(itemsub.rate));
                            }
                            if (_sub.Count>0)
                            {
                                _counter_2 += 1;
                                _xdatasub.code = item.PMO + "." + _counter_1.ToString() +"." + _counter_2.ToString();
                                _xdatasub.procurement_project ="     " + itemoe.Name;
                                _xdatasub.pmo = item.PMO;
                                _xdatasub.total = _totalMOOE.ToString("#,##0.00");
                                _xdatasub.mooe_total = _totalMOOE.ToString("#,##0.00");
                                _xdatasub.mooe_code = itemoe.Code;

                                List<APPProcAct_Data> _subproc = _AppProAct.Where(x => x.Code == _xdatasub.code && x.RefCode == itemoe.Code).ToList();
                                
                                if (_subproc.Count!=0)
                                {
                                    _xdatasub.mode_procurement = _subproc[0].ModProcure;
                                    _xdatasub.ad_post_rei = _subproc[0].AdPost;
                                    _xdatasub.sub_open = _subproc[0].SubOpen;
                                    _xdatasub.notice_award = _subproc[0].NoticeAward;
                                    _xdatasub.contract_signing = _subproc[0].ContractSign;
                                    _xdatasub.source_funds = _subproc[0].SourceFunds;
                                }

                                _AppData.Add(_xdatasub);
                            }
                         

                        }
                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _AppData;


                    ArrangeColumn();

                    SavePrintOut();
                    
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void FetchProActData() 
        {
            c_app.Process = "FetchProActData";
            svc_mindaf.ExecuteSQLAsync(c_app.GetProActData(cmbYear.SelectedItem.ToString()));
        }

        private void GenerateData()
        {

            double _Totals = 0.00;
            var results_1 = from p in _PPMPData
                            group p by p.Fund_Name into g
                            select new
                            {
                                Id = g.Key
                            };
            _Main.Clear();
            List<PPMPData> _DataFinal = new List<PPMPData>();
            List<PPMPData> _DataReport = new List<PPMPData>();
            foreach (var item in results_1)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.Id;

                _DataFinal.Add(x_var);
            }



            foreach (var item in _AlignmentData)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.to_uacs;
                x_var.month = item.months;
                x_var.name = item.name;
                x_var.Fund_Name = item.fundsource;
                x_var.rate = item.total_alignment;
                x_var.year = cmbYear.SelectedItem.ToString();
                x_var.type_service = item.name.ToString();
                x_var.quantity = "1";
                _PPMPData.Add(x_var);
            }

            foreach (var item in _DataFinal)
            {

                PPMPFormat _Data = new PPMPFormat();

                _Data.ACTID = "";
                _Data.UACS = item.uacs_code.ToString();
                _Data.Description = "";
                _Data.ModeOfProcurement = "";
                _Data.Quantity_Size = "";
                _Data._Year = "";
                _Data._Division = this.Division;
                _Data._Pap = this.PAP;
                _Data.Jan = "";
                _Data.Feb = "";
                _Data.Mar = "";
                _Data.Apr = "";
                _Data.May = "";
                _Data.Jun = "";
                _Data.Jul = "";
                _Data.Aug = "";
                _Data.Sep = "";
                _Data.Oct = "";
                _Data.Nov = "";
                _Data.Dec = "";
                _Data.EstimatedBudget = "";
                _Data._Total = "0.00";
                _Data._Total = _Totals.ToString("#,##0.00");
                _Main.Add(_Data);
                List<PPMPData> x_data = _PPMPData.Where(itempp => itempp.Fund_Name == _Data.UACS).ToList();

                var results2nd = from p in x_data
                                 group p by p.name into g
                                 select new
                                 {
                                     name = g.Key,
                                     ExpenseItems = g.Select(m => m.name)
                                 };

                foreach (var res2nd in results2nd)
                {
                    PPMPFormat _Data2nd = new PPMPFormat();
                    double _Estimate = 0.00;
                    List<PPMPData> x_data2nd = _PPMPData.Where(items => items.name == res2nd.name).ToList();


                    _Data2nd.ACTID = "";
                    _Data2nd.UACS = x_data2nd[0].uacs_code;
                    _Data2nd.Description = x_data2nd[0].name;
                    _Data2nd.ModeOfProcurement = "";
                    _Data2nd.Quantity_Size = "";
                    _Data2nd._Year = x_data2nd[0].year.ToString();
                    _Data2nd._Division = this.Division;
                    _Data2nd._Pap = this.PAP;
                    _Data2nd.Jan = "";
                    _Data2nd.Feb = "";
                    _Data2nd.Mar = "";
                    _Data2nd.Apr = "";
                    _Data2nd.May = "";
                    _Data2nd.Jun = "";
                    _Data2nd.Jul = "";
                    _Data2nd.Aug = "";
                    _Data2nd.Sep = "";
                    _Data2nd.Oct = "";
                    _Data2nd.Nov = "";
                    _Data2nd.Dec = "";
                    _Data2nd.EstimatedBudget = "";
                    _Data2nd._Total = "0.00";
                    _Data2nd._Total = _Totals.ToString("#,##0.00");
                    _Main.Add(_Data2nd);
                    _Totals = 0.00;
                    List<PPMPAlignment> x_dataalign = _AlignmentData.Where(items => items.from_uacs == _Data2nd.UACS).ToList();
                    double totals = 0.00;

                    foreach (var itemal in x_dataalign)
                    {
                        totals += Convert.ToDouble(itemal.total_alignment);
                    }

                    foreach (var item_data in x_data2nd)
                    {
                        PPMPFormat _DataDetail = new PPMPFormat();
                        _DataDetail.ACTID = item_data.act_id;
                        _DataDetail.UACS = "";
                        _DataDetail.Description = item_data.type_service;
                        _DataDetail.ModeOfProcurement = "";
                        _DataDetail.Quantity_Size = item_data.quantity;
                        _DataDetail._Year = item_data.year.ToString();
                        _DataDetail._Division = this.Division;
                        _DataDetail._Pap = this.PAP;


                       
                        _Estimate += Convert.ToDouble(item_data.rate) - totals;
                        _Totals -= totals;
                        _DataDetail.EstimatedBudget = _Estimate.ToString("#,##0.00");
                        _DataDetail._Total = _Totals.ToString("#,##0.00");
                        _Main.Add(_DataDetail);


                        List<PPMPAlignment> x_data3rd = _AlignmentData.Where(items => items.to_uacs == _Data2nd.UACS).ToList();


                    }

                }

              

                //grdData.ItemsSource = null;
                //grdData.ItemsSource = _Main;
                //grdData.Columns["_Year"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Division"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Pap"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["_Total"].Visibility = System.Windows.Visibility.Collapsed;
                //grdData.Columns["ACTID"].Visibility = System.Windows.Visibility.Collapsed;
                //ArrangeColumn();

                //FetchPPMPRevision();
            }
            FetchAPP();
            // LoadReportData(_Main);
        }

        private void FetchMOOE() 
        {
            c_app.Process = "FetchMOOE";
            svc_mindaf.ExecuteSQLAsync(c_app.GetMOOE());
        }

        private void ArrangeColumn()
        {
           // grdData.ColumnsHeaderTemplate = this.Resources["HeaderGridWrap"] as DataTemplate;

            Column col_code = grdData.Columns.DataColumns["code"];
            col_code.Width = new ColumnWidth(80, false);
            col_code.HeaderText = "Code";

            Column col_uacs = grdData.Columns.DataColumns["procurement_project"];
            col_uacs.Width = new ColumnWidth(350, false);
            col_uacs.HeaderText = "Procurement / Program / Project";

           

            Column col_pmo = grdData.Columns.DataColumns["pmo"];
            col_pmo.Width = new ColumnWidth(65, false);
            col_pmo.HeaderText="PMO/End-User";

            Column col_mode_procurement = grdData.Columns.DataColumns["mode_procurement"];
            col_mode_procurement.Width = new ColumnWidth(75, false);
            col_mode_procurement.HeaderText = "Mode of Procurement";

              Column col_ad_post_rei = grdData.Columns.DataColumns["ad_post_rei"];
            col_ad_post_rei.Width = new ColumnWidth(75, false);
            col_ad_post_rei.HeaderText="Ads/Post of IB/REI";

              Column col_sub_open = grdData.Columns.DataColumns["sub_open"];
            col_sub_open.Width = new ColumnWidth(75, false);
            col_sub_open.HeaderText="Sub/Open of Bids";

              Column col_notice_award = grdData.Columns.DataColumns["notice_award"];
            col_notice_award.Width = new ColumnWidth(75, false);
            col_notice_award.HeaderText="Notice of Award";

             Column col_contract_signing = grdData.Columns.DataColumns["contract_signing"];
            col_contract_signing.Width = new ColumnWidth(75, false);
            col_contract_signing.HeaderText="Contract Signing";

            Column col_total = grdData.Columns.DataColumns["total"];
            col_total.Width = new ColumnWidth(90, false);
            col_total.HeaderText = "TOTAL";

            Column col_mooe = grdData.Columns.DataColumns["mooe_total"];
            col_mooe.Width = new ColumnWidth(90, false);
            col_mooe.HeaderText = "MOOE";

            Column col_co = grdData.Columns.DataColumns["co"];
            col_co.Width = new ColumnWidth(90, false);
            col_co.HeaderText = "CO";

            Column col_remarks = grdData.Columns.DataColumns["remarks"];
            col_remarks.Width = new ColumnWidth(90, true);
            col_remarks.HeaderText = "Remarks";


            Column col_ = grdData.Columns.DataColumns["source_funds"];
            col_.Width = new ColumnWidth(75, false);
            col_.HeaderText = "Source of Funds";

            Column col_mooe_code = grdData.Columns.DataColumns["mooe_code"];
            col_mooe_code.Visibility = System.Windows.Visibility.Collapsed;


          

        }
       


        private void FetchPPMPData()
        {

            c_app.Process = "FetchPPMPData";
            svc_mindaf.ExecuteSQLAsync(c_app.FetchDataAPP(cmbYear.SelectedItem.ToString()));


        }
        private void FetchPPMPRevision()
        {

            c_app.Process = "FetchPPMPRevision";
            svc_mindaf.ExecuteSQLAsync(c_app.FetchRevision(this.PAP, cmbYear.SelectedItem.ToString()));


        }
      

        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
       



        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
           
        }

      

     
       

        void c_ppmp_SQLOperation(object sender, EventArgs e)
        {

        }

      

        void f_rel_ReloadData(object sender, EventArgs e)
        {
          FetchPPMPData();

        }

        private void SubmitAlignment()
        {

        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void usr_app_data_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
           // FetchPPMPData();
        }
        private frmProcurementMode f_mode = new frmProcurementMode();
        private frmProcActivityLibrary f_mode_pro = new frmProcActivityLibrary();
        private String SelectedProAct = "";
        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
            if (grdData.Rows[e.Cell.Row.Index].Cells["mooe_code"].Value== null)
            { return; }

            String _code = grdData.Rows[e.Cell.Row.Index].Cells["code"].Value.ToString();
            String _mooe_code = grdData.Rows[e.Cell.Row.Index].Cells["mooe_code"].Value.ToString();
            bool _nodata = false;
            APPProcAct_Data x_data = new APPProcAct_Data();
            try
            {
                x_data = _AppProAct.Where(items => items.Code == _code && items.RefCode == _mooe_code).ToList()[0];
                if (x_data.Years == null)
                {
                    x_data.Years = cmbYear.SelectedItem.ToString();
                }
            }
            catch (Exception)
            {

                _nodata = true;

            }
            f_mode_pro = new frmProcActivityLibrary();
            f_mode_pro.RefreshAPP += f_mode_pro_RefreshAPP;

            if (_nodata)
            {
                f_mode_pro.Data = new APPProcAct_Data();

                f_mode_pro.Data.Code = _code;
                f_mode_pro.Data.RefCode = _mooe_code;
                f_mode_pro.Data.Years = cmbYear.SelectedItem.ToString();

            }
            else
            {
                f_mode_pro.Data = x_data;
            }

            switch (e.Cell.Column.HeaderText.ToString())
            {
                case "Mode of Procurement":
                        f_mode_pro.Process = e.Cell.Column.HeaderText.ToString();
                        f_mode_pro.FormType = "Mode Procurement";
                        f_mode_pro.Show();
                        SelectedProAct = e.Cell.Column.HeaderText.ToString();
                    break;
                case "Source of Funds":
                    f_mode_pro.Process = e.Cell.Column.HeaderText.ToString();
                    f_mode_pro.FormType = "Fund Source";
                    f_mode_pro.Show();
                    SelectedProAct = e.Cell.Column.HeaderText.ToString();
                    break;
                case "Ads/Post of IB/REI": case "Sub/Open of Bids":case "Notice of Award":case "Contract Signing":
 
                        f_mode_pro.Process = e.Cell.Column.HeaderText.ToString();
                        f_mode_pro.FormType = "Procurement Activity";
                        f_mode_pro.Show();
                        SelectedProAct = e.Cell.Column.HeaderText.ToString();
 
                    break;
            }
        }

        void f_mode_pro_RefreshAPP(object sender, EventArgs e)
        {
            FetchAPP();
        }

        private void SavePrintOut() 
        {
            c_app.Process = "SavePrintOut";
            c_app.SQLOperation += c_app_SQLOperation;
            c_app.SavePrintOut(_AppData, cmbYear.SelectedItem.ToString());
        }

        void c_app_SQLOperation(object sender, EventArgs e)
        {
          
          //  HtmlPage.Window.Navigate(new Uri("http://mindafinance/Downloads/ReportTool.application"), "_blank");
        }

        private bool rep_isloaded = false;
        void c_cache_SQLOperation(object sender, EventArgs e)
        {
            switch (c_cache.Process)
            {
                case "APP REPORT":
                    if (rep_isloaded == false)
                    {
                        HtmlPage.Window.Navigate(new Uri("http://mindafinance/Downloads/ReportTool.application"), "_blank");
                        rep_isloaded = true;
                    }

                    break;
            }
        }
        void f_mode_SelectedData(object sender, EventArgs e)
        {
            var _x = _AppData.Where(items => items.code == grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString()).ToList();
            foreach (var item in _x)
            {
                item.mode_procurement = f_mode.SelectMode;
                break;
            }

            grdData.ItemsSource = null;
            grdData.ItemsSource = _AppData;

            ArrangeColumn();
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {
            c_cache.Process = "APP REPORT";
            c_cache.CacheFDReport("APP REPORT", "n-a", "2017");
            c_cache.SQLOperation += c_cache_SQLOperation;
         
        }

        private void btnLists_Click(object sender, RoutedEventArgs e)
        {
            FetchPPMPData();
        }

      

       

    }
}
