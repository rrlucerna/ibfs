﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class ppmp_report_finance : UserControl
    {
        private clsPPMP c_ppmp = new clsPPMP();
        private List<PPMPDataApproval> ListApproval = new List<PPMPDataApproval>();
        private List<PPMPViewDivision> ListDivision = new List<PPMPViewDivision>();
        private List<PPMPFinanceFS> ListFundSource = new List<PPMPFinanceFS>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();


        public ppmp_report_finance()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        private void FetchDivision() 
        {
            c_ppmp.Process = "FetchDivision";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchDivisionWithPap());

        }

        private void FetchViewAccounting()
        {
            String _year = cmbYear.SelectedItem.ToString();
            String _pap = ListDivision.Where(items => items.DivisionName == cmbDivision.SelectedItem.ToString()).ToList()[0].PAP;

            c_ppmp.Process = "FetchViewAccounting";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchView(_pap,_year));

        }
        private void FetchFundSource()
        {
          
            String _pap = ListDivision.Where(items => items.DivisionName == cmbDivision.SelectedItem.ToString()).ToList()[0].DivisionId;

            c_ppmp.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchFundSource(_pap));

        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_ppmp.Process)
            {
                case "FetchViewAccounting":
                    XDocument oDocKeyResultsFetchAlignmentData = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResultsFetchAlignmentData.Descendants("Table")
                                     select new PPMPDataApproval
                                     {
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         Description = Convert.ToString(info.Element("Description").Value),
                                         Division = Convert.ToString(info.Element("Division").Value),
                                         EstimateBudget = Convert.ToString(info.Element("EstimateBudget").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         ModeOfProcurement = Convert.ToString(info.Element("ModeOfProcurement").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Octs = Convert.ToString(info.Element("Octs").Value),
                                         PAP = Convert.ToString(info.Element("PAP").Value),
                                         Quantity = Convert.ToString(info.Element("Quantity").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         Total = Convert.ToString(info.Element("Total").Value),
                                         Uacs = Convert.ToString(info.Element("Uacs").Value),
                                         Yearssss = Convert.ToString(info.Element("Yearssss").Value)

                                     };

                    ListApproval.Clear();
                    Double Total = 0.00;

                    foreach (var item in _dataLists)
                    {

                        PPMPDataApproval _varProf = new PPMPDataApproval();

                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Dec = item.Dec;
                        _varProf.Description = item.Description;
                        _varProf.Division = item.Division;
                        _varProf.EstimateBudget = item.EstimateBudget;
                        _varProf.Feb = item.Feb;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.ModeOfProcurement = item.ModeOfProcurement;
                        _varProf.Nov = item.Nov;
                        _varProf.Octs = item.Octs;
                        _varProf.PAP = item.PAP;
                        _varProf.Quantity = item.Quantity;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.Uacs = item.Uacs;
                        _varProf.Yearssss = item.Yearssss;

                        try
                        {
                            Total += Convert.ToDouble(item.EstimateBudget);

                        }
                        catch (Exception)
                        {

                        }
                  
                        ListApproval.Add(_varProf);


                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListApproval;
                    txtTotal.Value = Total;
                    grdData.Columns["PAP"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Division"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Yearssss"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Total"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;


                    break;

                case "FetchFundSource":
                    XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                    var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                  select new PPMPFinanceFS
                                                  {
                                                      Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value),
                                                      Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),

                                                  };

                    ListFundSource.Clear();
                    cmbFundSource.Items.Clear();
                    foreach (var item in _dataListsFetchFundSource)
                    {

                        PPMPFinanceFS _varProf = new PPMPFinanceFS();

                        _varProf.Fund_Name = item.Fund_Name;
                        _varProf.Fund_Source_Id = item.Fund_Source_Id;

                        ListFundSource.Add(_varProf);
                        cmbFundSource.Items.Add(item.Fund_Name);

                    }

                    this.Cursor = Cursors.Arrow;


                    break;
                case "FetchDivision":
                    XDocument oDocKeyResultsFetchDivision = XDocument.Parse(_results);
                    var _dataListsFetchDivision = from info in oDocKeyResultsFetchDivision.Descendants("Table")
                                     select new PPMPViewDivision
                                     {
                                         PAP = Convert.ToString(info.Element("Division_Code").Value),
                                         DivisionName = Convert.ToString(info.Element("Division_Desc").Value),
                                         DivisionId = Convert.ToString(info.Element("DivisionId").Value)

                                     };

                    ListDivision.Clear();
                    cmbDivision.Items.Clear();
                    foreach (var item in _dataListsFetchDivision)
                    {

                        PPMPViewDivision _varProf = new PPMPViewDivision();

                        _varProf.PAP = item.PAP;
                        _varProf.DivisionName  = item.DivisionName;
                        _varProf.DivisionId = item.DivisionId;

                        ListDivision.Add(_varProf);
                        cmbDivision.Items.Add(item.DivisionName);

                    }
                
                    this.Cursor = Cursors.Arrow;


                    break;

            }
                
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }

            cmbYear.SelectedIndex = 0;
         
        }

        private void usrppmpfinance_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchDivision();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            FetchFundSource();
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            FetchViewAccounting();
        }
    }
}
