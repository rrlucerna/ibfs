﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for frmSecurity.xaml
    /// </summary>
    public partial class frmSecurity : Window
    {
        clsReportFinance c_rf = new clsReportFinance();
        public  UserDetail c_user = new UserDetail();
        public event EventHandler LoginHandler;

        public frmSecurity()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (c_rf.LoginUser(txtcredentials.Password))
            {
                if (LoginHandler!=null)
                {
                    c_user = c_rf.UserInfo;
                    LoginHandler(this, new EventArgs());
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid Credentials, Please try again.");
            }
        }
    }
}
