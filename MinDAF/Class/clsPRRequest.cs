﻿using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsPRRequest
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchMonthlyActivityDetails(String _months, String _division_id)
        {
            var sb = new System.Text.StringBuilder(581);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ma.id as act_id,");
            sb.AppendLine(@"mad.id as det_id,");
            sb.AppendLine(@"mpe.acountable_division_code as div_id,");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"ma.description as activity,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"ISNULL(mpi.item_specifications,'Expense Item') as details,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id  = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
            sb.AppendLine(@"WHERE mad.month ='" + _months + "' and mpe.acountable_division_code =" + _division_id + " AND mad.status='FINANCE APPROVED'");

            return sb.ToString();
        }
        public String LoadForPRRequest(String _months, String _division_id)
        {
            var sb = new System.Text.StringBuilder(581);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mooe.name as ExpenseItem,");
            sb.AppendLine(@"ISNULL(mpi.item_specifications,'Expense Item') as Details,");
            sb.AppendLine(@"mad.total as Total,");
            sb.AppendLine(@"mad.status as Status");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id  = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
            sb.AppendLine(@"WHERE mad.month ='" + _months + "' and mpe.acountable_division_code =" + _division_id + " AND mad.status='PR REQUEST'");

            return sb.ToString();
        }

        public String FetchMonthlyActivities(String _months, String _division_id)
        {
            var sb = new System.Text.StringBuilder(672);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ma.id as act_id,");
            sb.AppendLine(@"ma.description as activity");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id  = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
            sb.AppendLine(@"WHERE mad.month ='"+ _months +"' and mpe.acountable_division_code = "+ _division_id +" AND mad.status='FINANCE APPROVED'");
            sb.AppendLine(@"GROUP BY ma.id,div.Division_Id,div.Division_Desc,ma.description");
            sb.AppendLine(@"ORDER BY div.Division_Id ASC");


            return sb.ToString();
        }
    }


    public class _LibPRRequest
    {
        public String Status { get; set; }
        public String ExpenseItem { get; set; }
        public String Details { get; set; }
        public String Total { get; set; }
        
     
    }

    public class _LibMainActivity 
    {
        public String id { get; set; }
        public String activity { get; set; }
        public List<_libMonthActivityDetails> details { get; set; }
    }
    public class _libMonthActivity
    {
        public String id { get; set; }
        public String activity { get; set; }
    }
    public class _libMonthActivityDetails
    {
        public String act_id { get; set; }
        public String det_id { get; set; }
        public String div_id { get; set; }
        public String uacs_code { get; set; }
        public String activity { get; set; }
        public String name { get; set; }
        public String details { get; set; }
        public String month { get; set; }
        public String total { get; set; }
       
    }
}
