﻿using MinDAF.MinDAFS;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class BudgetChartData
    {
        public String Process { get; set; }

        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
    
        public String FetchDashBoardData(String _id,String Year)
        {
            StringBuilder sb = new StringBuilder(249);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mnd.project_name as Project,");
            sb.AppendLine(@"mex.name as Expenditure,");
            sb.AppendLine(@"mnd.amount as Amount");
            sb.AppendLine(@"FROM mnda_project_proposals mnd");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mex ON mex.id =mnd.expenditure_id");
            sb.AppendLine(@"WHERE mnd.division_id = " + _id + "");
            sb.AppendLine(@"AND NOT mnd.project_name = 'NULL' AND  mnd.year = "+ Year +"");
            sb.AppendLine(@"ORDER BY mnd.project_name ASC");
            sb.AppendLine(@" ");

            return sb.ToString();
        }
        public String FetchProjectLists(String _id, String Year)
        {
   
            StringBuilder sb = new StringBuilder(282);
            sb.AppendLine(@"            SELECT");
            sb.AppendLine(@"            mnd.project_name as Project");
            sb.AppendLine(@"            FROM mnda_project_proposals mnd");
            sb.AppendLine(@"            WHERE mnd.division_id = "+ _id +" ");
            sb.AppendLine(@"            AND NOT mnd.project_name = 'NULL' AND  mnd.year = "+ Year +" AND mnd.status ='CREATED'");
            sb.AppendLine(@"            GROUP BY mnd.project_name");
            sb.AppendLine(@"            ORDER BY mnd.project_name ASC");


            return sb.ToString();
        }

        public String FetchProjectDetailsLists(String _id, String Year,String project_name)
        {

            StringBuilder sb = new StringBuilder(383);
            sb.AppendLine(@"  SELECT");
            sb.AppendLine(@"            mme.name as Expenditure,");
            sb.AppendLine(@"            mnd.amount as Amount");
            sb.AppendLine(@"            FROM mnda_project_proposals mnd");
            sb.AppendLine(@"            INNER JOIN mnda_mooe_expenditures mme on mme.id = mnd.expenditure_id");
            sb.AppendLine(@"            WHERE mnd.division_id = " + _id + " and mnd.project_name ='" + project_name + "'");
            sb.AppendLine(@"            AND NOT mnd.project_name = 'NULL' AND  mnd.year = "+ Year +" AND mnd.status ='CREATED'");
            sb.AppendLine(@"            ORDER BY mnd.project_name ASC");



            return sb.ToString();
        }
        public String BudgetActiviyLog(String _id)
        {
            StringBuilder sb = new StringBuilder(168);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"( 'A Project named ''' + mpl.project_name + ''' has been ' + mpl.project_action_log) as Status");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_project_logs  mpl");
            sb.AppendLine(@"WHERE mpl.div_id ="+ _id +";");



            return sb.ToString();
        }

        public String FetchChartData(String _id, String Year)
        {
            StringBuilder sb = new StringBuilder(372);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mnd.project_name as Project,");
            sb.AppendLine(@"SUM(mnd.amount) as Amount");
            sb.AppendLine(@"FROM mnda_project_proposals mnd");
            sb.AppendLine(@"WHERE mnd.division_id = "+ _id +"  and mnd.year ="+ Year +"");
            sb.AppendLine(@"AND NOT mnd.project_name = 'NULL'");
            sb.AppendLine(@"GROUP BY mnd.project_name");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Remaining Balance' as Project,");
            sb.AppendLine(@"mba.budget_allocation as Amount");
            sb.AppendLine(@"FROM mnda_budget_allocation mba");
            sb.AppendLine(@"WHERE mba.division_id = "+ _id +" and mba.entry_year = "+ Year +"");

            return sb.ToString();
        }

        public String FetchOverAllData(String _id, String Year)
        {
            StringBuilder sb = new StringBuilder(473);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Allocated Budget' as Title,");
            sb.AppendLine(@"SUM(mba.budget_allocation) as Amount");
            sb.AppendLine(@"FROM mnda_budget_allocation mba");
            sb.AppendLine(@"WHERE mba.division_id = "+  _id +" AND mba.entry_year = "+ Year +"");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Budget Proposal' as Title,");
            sb.AppendLine(@" SUM(mpp.amount) as Amount");
            sb.AppendLine(@"FROM mnda_project_proposals  mpp");
            sb.AppendLine(@"WHERE mpp.division_id = "+ _id +" and mpp.year ="+ Year +"");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Total Projects' as Title, ");
            sb.AppendLine(@"COUNT(mpp.division_id) as Amount");
            sb.AppendLine(@"FROM mnda_project_proposals  mpp");
            sb.AppendLine(@"WHERE mpp.division_id = "+ _id +" and mpp.year ="+ Year +"");


            return sb.ToString();
        }
    }

    public class BudgetChartList 
    {
        public String Project { get; set; }
        public String Expenditure { get; set; }
        public String Amount { get; set; }
    }

    public class BudgetPieList 
    {
        public String Project { get; set; }
        public Double Amount { get; set; }
    }
    public class BudgetOverAllData
    {
        public String Title { get; set; }
        public Double Amount { get; set; }
    }
    public class Logs 
    {
        public String Status { get; set; }
  
    }
    public class ProjectLists 
    {
        public String Project { get; set; }
    }
    public class ProjectDetailLists
    {
        public String Expenditure { get; set; }
        public String Amount { get; set; }
    }

}
