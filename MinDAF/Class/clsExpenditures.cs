﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsExpenditures
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchExpenditures() 
        {
           StringBuilder sb = new StringBuilder(103);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  name,is_dynamic");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  mnda_mooe_sub_expenditures mooe");
            sb.AppendLine(@"WHERE mooe.is_active = 1 ORDER BY name ASC;");

            return sb.ToString();
        }
        public String FetchFundsSource(String _div_id)
        {
            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code as Fund_Source_Id,");
            sb.AppendLine(@"  Fund_Name");
            sb.AppendLine(@"FROM   mnda_fund_source mfs ");
            sb.AppendLine(@"WHERE mfs.division_id =" + _div_id + "");
            sb.AppendLine(@";");


            return sb.ToString();

        }
   
        public String FetchGasolineExpense()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,mel.[day],");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.mooe_id");
            sb.AppendLine(@"WHERE mmo_sub.mooe_id = '6' and mmo_sub.is_lib = 1");


            return sb.ToString();
        }
        public String FetchExpenseLibrary()
        {
            StringBuilder sb = new StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  is_active");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  mnda_mooe_sub_expenditures;");


            return sb.ToString();
        }
        public String FetchGasolineTypes()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.mooe_id");
            sb.AppendLine(@"WHERE mmo_sub.mooe_id = '42' and mmo_sub.is_lib = 1");


            return sb.ToString();
        }

        public String FetchForeignTravel()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.id");
            sb.AppendLine(@"WHERE mmo_sub.uacs_code = '5020102000' and mmo_sub.is_active = 1");


            return sb.ToString();
        }
        public String FetchForeignAllowance()
        {
            StringBuilder sb = new StringBuilder(238);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.mooe_id");
            sb.AppendLine(@"WHERE mmo_sub.mooe_id = '109' and mmo_sub.is_lib = 1");


            return sb.ToString();
        }
        public String FetchLocalTravel() 
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.id");
            sb.AppendLine(@"WHERE mmo_sub.uacs_code = '5020101000' and mmo_sub.is_active = 1");


            return sb.ToString();
        }

       

        public String FetchLocalAllowance() 
        {
            StringBuilder sb = new StringBuilder(238);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.mooe_id");
            sb.AppendLine(@"WHERE mmo_sub.mooe_id = '108' and mmo_sub.is_lib = 1");


            return sb.ToString();
        }
        public void UpdateMOOEStats(String _id, String _val) 
        {
          

            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_active ="+ _val +"");
            sb.AppendLine(@"WHERE id = "+ _id +";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + "");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
         public String FetchExpenditureDetails(String _code) 
                {
                    StringBuilder sb = new StringBuilder(155);
                    sb.AppendLine(@"SELECT ");
                    sb.AppendLine(@"  sub_expenditure_code,library_type,");
                    sb.AppendLine(@"  item_code,");
                    sb.AppendLine(@"  item_name,");
                    sb.AppendLine(@"  day,");
                    sb.AppendLine(@"  rate,");
                    sb.AppendLine(@"  rate_year");
                    sb.AppendLine(@"FROM mnda_expenditures_library mel");
                    sb.AppendLine(@"WHERE mel.sub_expenditure_code ='" + _code + "';");


                    return sb.ToString();
                }

        public String FetchLocalData(string _activity_id,string _month , string year , String mooe_id,String FundSource) 
        {
            StringBuilder sb = new StringBuilder(526);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.id  as act_id,");
            sb.AppendLine(@"mda.description as Activity,");
            sb.AppendLine(@"mdad.accountable_id as Assigned,");
            sb.AppendLine(@"mdad.remarks as Remarks,");
            sb.AppendLine(@"mdad.start as DateStart,");
            sb.AppendLine(@"mdad.[end] as DateEnd,");
            sb.AppendLine(@"mdad.destination as Destination,");
            sb.AppendLine(@"mdad.no_days as No_Days,");
            sb.AppendLine(@"mdad.no_staff as No_Staff,");
            sb.AppendLine(@"mdad.rate as Fare_Rate,");
            sb.AppendLine(@"mdad.travel_allowance as Travel_Allowance,");
            sb.AppendLine(@"mdad.total as Total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERe mdad.fund_source_id = '"+ FundSource +"' AND mdad.activity_id = '" + _activity_id + "' and mdad.month = '" + _month + "' and mdad.year = " + year + " and mooe_sub_expenditure_id =" + mooe_id + "and is_suspended = 0");


            return sb.ToString();
        }
        public String FetchLocalDataGas(string _activity_id, string _month, string year, String mooe_id, String FundSource)
        {
            StringBuilder sb = new StringBuilder(526);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.id  as ActId,");
            sb.AppendLine(@"mda.description as Activity,");
            sb.AppendLine(@"mdad.accountable_id as Assigned,");
            sb.AppendLine(@"mdad.remarks as Remarks,");
            sb.AppendLine(@"mdad.start as DateStart,");
            sb.AppendLine(@"mdad.[end] as DateEnd,");
            sb.AppendLine(@"mdad.destination as Destination,");
            sb.AppendLine(@"mdad.no_days as No_Days,");
            sb.AppendLine(@"mdad.no_staff as No_Staff,");
            sb.AppendLine(@"mdad.rate as Fare_Rate,");
            sb.AppendLine(@"mdad.type_service as TypeService,");
            sb.AppendLine(@"mdad.travel_allowance as Travel_Allowance,");
            sb.AppendLine(@"mdad.total as Total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERe mdad.fund_source_id = '" + FundSource + "' AND mdad.activity_id = '" + _activity_id + "' and mdad.month = '" + _month + "' and mdad.year = " + year + " and mooe_sub_expenditure_id =" + mooe_id + "and is_suspended = 0");


            return sb.ToString();
        }
        public void SaveExpenditureLibrary(String sub_expenditure_code, String library_type, String item_code, String item_name, String day, double _rate, String _year)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(205);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@" mnda_expenditures_library");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  sub_expenditure_code,");
            sb.AppendLine(@"  library_type,");
            sb.AppendLine(@"  item_code,");
            sb.AppendLine(@"  item_name,");
            sb.AppendLine(@"  day,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  rate_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ sub_expenditure_code +"',");
            sb.AppendLine(@"  '"+ library_type +"',");
            sb.AppendLine(@"  '"+ item_code +"',");
            sb.AppendLine(@"  '" + item_name + "',");
            sb.AppendLine(@"  '" + day + "',");
            sb.AppendLine(@"  "+ _rate +",");
            sb.AppendLine(@"  " + _year + "");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveProjectBudget(String _activity_id,String _accountable_id,String _remarks,DateTime _start,DateTime _end,
            String _destination, String _no_staff, double _rate, double _travel_allowance, double _total, String _month, String _year, string mooe_id, string no_days,
            String FundSource,String _type_service)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  mnda_activity_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  activity_id,");
            sb.AppendLine(@"  accountable_id,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"  start,");
            sb.AppendLine(@"  [end],");
            sb.AppendLine(@"  destination,");
            sb.AppendLine(@"  no_staff,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  travel_allowance,");
            sb.AppendLine(@"  total,");
            sb.AppendLine(@"  month,");
            sb.AppendLine(@"  year,mooe_sub_expenditure_id,no_days,fund_source_id,type_service");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _activity_id +"',");
            sb.AppendLine(@"  '"+ _accountable_id +"',");
            sb.AppendLine(@" '"+ _remarks +"',");
            sb.AppendLine(@" '"+ _start +"',");
            sb.AppendLine(@" '"+ _end +"',");
            sb.AppendLine(@"  '"+ _destination +"',");
            sb.AppendLine(@" "+ _no_staff +",");
            sb.AppendLine(@" "+ _rate +",");
            sb.AppendLine(@"  "+ _travel_allowance +",");
            sb.AppendLine(@"  "+ _total +",");
            sb.AppendLine(@" '"+ _month +"',");
            sb.AppendLine(@" "+ _year +",");
            sb.AppendLine(@" '" + mooe_id + "',");
            sb.AppendLine(@" '" + no_days + "',");
            sb.AppendLine(@" '" + FundSource + "',");
            sb.AppendLine(@" '" + _type_service + "'");
            sb.AppendLine(@");");


                _sqlString += sb.ToString();
        



            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveData":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "EnableExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "DisableExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "Suspend":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
              
            }
        }
    }
    public class ExpenditureDetails 
    {
        public String sub_expenditure_code { get; set; }
                    public String  library_type {get;set;}
                    public String  item_code {get;set;}
                    public String  item_name {get;set;}
                    public String  day {get;set;}
                    public String  rate {get;set;}
                    public String rate_year { get; set; }
    }
    public class mooe_sub_expenditures 
    {
        public String  id {get;set;}
        public String mooe_id{get;set;}
        public String name{get;set;}
        public String is_active { get; set; }
        public String is_dynamic { get; set; }
    }
    public class GridData
    {
        public String ActId { get; set; }
        public String Activity { get; set; }
        public String Assigned { get; set; }
        public String Remarks { get; set; }
        public String DateStart { get; set; }
        public String DateEnd { get; set; }
        public String Destination { get; set; }
        public String Service_Type { get; set; }
        public String  Breakfast{ get; set; }
        public String  AM_Snacks{ get; set; }
        public String  Lunch{ get; set; }
        public String  PM_Snacks{ get; set; }
        public String  Dinner{ get; set; }
        public String No_Days { get; set; }
        public String No_Staff { get; set; }
        public String Quantity { get; set; }
        public String Fare_Rate { get; set; }
        public String Travel_Allowance { get; set; }
        public String Total { get; set; }
    }
    public class GasolineTypeData
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public String day { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }
    public class GasolineData
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public String day { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }
    public class LocalAllowance 
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }
    public class ForeignAllowance
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }
    public class ForeignTravel
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }

    public class LocalTravel 
    {
        public String item_code {get;set;}
        public String item_name {get;set;}
        public Decimal rate {get;set;}
        public Decimal rate_year { get; set; }
    }

    public class Expenditures 
    {
        public Int32 id { get; set; }
        public String mooe_id { get; set; }
        public String name { get; set; }
        public Boolean isdynamic { get; set; }
    }
    public class FundSourceLib
    {
        public String Fund_Source_Id { get; set; }
        public String Fund_Name { get; set; }
    }
}
