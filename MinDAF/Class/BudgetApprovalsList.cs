﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class BudgetApprovalsList
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        public void UpdateActivity(List<ApproveList> _data,String _value)
        {
            String _sqlString = "";

            foreach (ApproveList item in _data)
            {
                StringBuilder sb = new StringBuilder(285);

               

                if (_value == "1")
                {
                    sb.AppendLine(@"UPDATE mnda_activity_data  SET is_approved = " + _value + ", status  = 'For Approval By Finance' WHERE id ='" + item._activity_id + "';");
                    sb.AppendLine(@"INSERT INTO dbo.mnda_approved_projects_division (program_id,project_id,output_id,activity_id,main_activity_id) VALUES ( '" + item._program_id + "','" + item._project_id + "','" + item._output_id + "','" + item._activity_id + "','" + item._activity_id_main + "');"); 
                }
                else
                {
                    sb.AppendLine(@"UPDATE ");
                    sb.AppendLine(@"  mnda_activity_data");
                    sb.AppendLine(@"  SET is_approved = " + _value + ",");
                    sb.AppendLine(@"  status  = 'For Approval By Division Head'");
                    sb.AppendLine(@"  WHERE id ='" + item._activity_id + "'");
                    sb.AppendLine(@";");

                    sb.AppendLine(@"DELETE FROM ");
                    sb.AppendLine(@"  dbo.mnda_approved_projects_division ");
                    sb.AppendLine(@"WHERE activity_id ='" + item._activity_id + "';");

                }

                _sqlString += sb.ToString();
            }

          




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void DisapproveActivity(String _activity_id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);

            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  mnda_activity_data");
            sb.AppendLine(@"  SET is_submitted = 0,");
            sb.AppendLine(@"  status  = ''");
            sb.AppendLine(@"  WHERE id ='" + _activity_id + "'");
            sb.AppendLine(@";");
          

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
      
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "UpdateActivity":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this,new EventArgs());
                    }
                    break;
                case "UpdateFinance":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "DisapproveActivity":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
        public String FetchTotals()
        {
            StringBuilder sb = new StringBuilder(302);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.activity_id ,");
            sb.AppendLine(@"mdad.month ,");
            sb.AppendLine(@"mdad.total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERE mdad.is_submitted = 1");

            return sb.ToString();
        }

        public String FetchTotalsFinance()
        {

            StringBuilder sb = new StringBuilder(157);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mad.activity_id ,");
            sb.AppendLine(@"mad.month ,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_approved_projects_division mapd");
            sb.AppendLine(@"INNER JOIN mnda_activity_data mad on mad.id = mapd.activity_id");

       

            return sb.ToString();
        }

        public void ApproveFinance(String _id,String _val)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(260);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_approved_projects_division  ");
            sb.AppendLine(@"SET ");
             sb.AppendLine(@"  isapproved = "+ _val +"");
            sb.AppendLine(@"WHERE   activity_id = "+ _id+";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn+=c_ops_DataReturn;


        }

        public String FetchApprovalDataList() 
        {
            StringBuilder sb = new StringBuilder(1608);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(div.Division_Desc,'-') as Division,ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(div.Division_Id,'-') as div_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec,                                           ");
            sb.AppendLine(@"                        0.00 as Total                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");
            return sb.ToString();
        }
        public String FetchApprovalDataListFinance()
        {
        
            StringBuilder sb = new StringBuilder(1772);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(div.Division_Desc,'-') as Division,ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(div.Division_Id,'-') as div_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'-') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'-') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'-') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'-') as activity,");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'-') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id  ");
            sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");

            
            return sb.ToString();

        

        }
        public String FetchActivities(string _output_id,string _year,string _month, string _activity_id,String fund_source)
        {
            StringBuilder sb = new StringBuilder(1160);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id as Id,");
            sb.AppendLine(@"ISNULL(mooe.name,'-') as Name,");
            sb.AppendLine(@"ISNULL(mpi.item_specifications,'None') as Item,");
            sb.AppendLine(@"ISNULL(mad.accountable_id,'-') as Accountable,");
            sb.AppendLine(@"ISNULL(mad.start,'01/01/1900') as Date_Start,");
            sb.AppendLine(@"ISNULL(mad.[end],'01/01/1900') as Date_End,");
            sb.AppendLine(@"ISNULL(mad.no_days,0) as No_Days,");
            sb.AppendLine(@"ISNULL(mad.no_staff,0) as No_Staff,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as Qty,");
            sb.AppendLine(@"ISNULL(mad.remarks,'-') as Remarks,");
            sb.AppendLine(@"ISNULL(mad.rate,0) as Rate,");
            sb.AppendLine(@"ISNULL(mad.total,0) as Total,");
            sb.AppendLine(@"mad.is_approved as IsApproved");
            sb.AppendLine(@"FROM mnda_activity ma ");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
       //1/11/2017     sb.AppendLine(@"WHERE ma.output_id = '" + _output_id + "' and mad.year =" + _year + " and mad.month ='" + _month + "' and mad.activity_id ='"+ _activity_id +"'");
            sb.AppendLine(@"WHERE  mad.year =" + _year + " and mad.month ='" + _month + "' AND mad.fund_source_id ='" + fund_source + "'");
            sb.AppendLine(@"AND mad.is_submitted = 1 and mad.status ='Submitted'");

            return sb.ToString();
        }
        public String FetchActivitiesFinance(string _activity_id, string _year, string _month)
        {
            StringBuilder sb = new StringBuilder(1160);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id as Id,");
            sb.AppendLine(@"ISNULL(mooe.name,'-') as Name,");
            sb.AppendLine(@"ISNULL(mpi.item_specifications,'None') as Item,");
            sb.AppendLine(@"ISNULL(mad.accountable_id,'-') as Accountable,");
            sb.AppendLine(@"ISNULL(mad.start,'01/01/1900') as Date_Start,");
            sb.AppendLine(@"ISNULL(mad.[end],'01/01/1900') as Date_End,");
            sb.AppendLine(@"ISNULL(mad.no_days,0) as No_Days,");
            sb.AppendLine(@"ISNULL(mad.no_staff,0) as No_Staff,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as Qty,");
            sb.AppendLine(@"ISNULL(mad.remarks,'-') as Remarks,");
            sb.AppendLine(@"ISNULL(mad.rate,0) as Rate,");
            sb.AppendLine(@"ISNULL(mad.total,0) as Total,");
            sb.AppendLine(@"mapd.isapproved as IsApproved");
            sb.AppendLine(@"FROM mnda_approved_projects_division mapd ");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = mapd.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
            sb.AppendLine(@"WHERE mad.year =" + _year + " and mad.month ='" + _month + "' and mad.is_approved =1 AND mapd.main_activity_id= '"+ _activity_id +"'");


            return sb.ToString();
        }
    }


    public class ApproveList
    {
       public String _activity_id {get;set;}
       public String _value  {get;set;}
       public String _program_id {get;set;}
       public String _project_id {get;set;}
       public String _output_id {get;set;}
       public String _activity_id_main { get; set; }
    }
    public class ApprovalData 
    {
         public String  Id {get;set;}
         public String  Name {get;set;}
         public String  Item { get; set; }
         public String  Accountable {get;set;}
         public String  Date_Start {get;set;}
         public String  Date_End {get;set;}
         public String  No_Days {get;set;}
         public String  No_Staff {get;set;}
         public String  Qty { get; set; }
         public String  Remarks {get;set;}
         public String  Rate {get;set;}
         public String  Total {get;set;}
         public Boolean  IsApproved { get; set; }
         public Boolean Disapprove { get; set; }
    }
  
}
