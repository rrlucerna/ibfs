﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsProcurementActivityLibrary
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String LoadProcurementActivityLibrary()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"id as Id,");
            sb.AppendLine(@"name as Library");
            sb.AppendLine(@"FROM  dbo.mnda_schedule_procact_library;");


            return sb.ToString();
        }

        public String LoadFundSource()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"id as Id,");
            sb.AppendLine(@"fund_source as Library");
            sb.AppendLine(@"FROM  dbo.mnda_fund_main;");


            return sb.ToString();
        }
        public String LoadProcurementLibrary()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"id as Id,");
            sb.AppendLine(@"mode_procurement as Library");
            sb.AppendLine(@"FROM  dbo.mnda_mode_procurement_lib;");


            return sb.ToString();
        }
        public void UpdateRow(String _id, String _name)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_schedule_procact_library");
            sb.AppendLine(@"SET  ");
            sb.AppendLine(@"name = '" + _name + "'");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        public void UpdateProActData(String _code, String _ref, String years, String mod_procure, String ad_post_rei, 
            String sub_open_bids, String notice_award, String contract_signing)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(228);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  mod_procure = '"+ mod_procure +"',");
            sb.AppendLine(@"  ad_post_rei = '"+ ad_post_rei +"',");
            sb.AppendLine(@"  sub_open_bids = '"+ sub_open_bids +"',");
            sb.AppendLine(@"  notice_award = '"+ notice_award +"',");
            sb.AppendLine(@"  contract_signing = '"+ contract_signing +"'");
            sb.AppendLine(@"WHERE   code = '"+ _code +"' AND  ref_code = '"+ _ref +"' AND years ='"+ years +"';");

            _sqlString += sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        public void SaveProActData(String _code, String _ref, String years, String mod_procure, String ad_post_rei,
           String sub_open_bids, String notice_award, String contract_signing,String fund_source)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(266);
            sb.AppendLine(@"DELETE dbo.mnda_proc_act_data WHERE code ='"+ _code +"' and ref_code = '"+ _ref +"';");
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  ref_code,");
            sb.AppendLine(@"  mod_procure,");
            sb.AppendLine(@"  ad_post_rei,");
            sb.AppendLine(@"  sub_open_bids,");
            sb.AppendLine(@"  notice_award,");
            sb.AppendLine(@"  contract_signing,");
            sb.AppendLine(@"  source_funds,");
            sb.AppendLine(@"  years");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"    '"+ _code  +"',");
            sb.AppendLine(@"    '" + _ref + "',");
            sb.AppendLine(@"    '" + mod_procure +"',");
            sb.AppendLine(@"    '" + ad_post_rei +"',");
            sb.AppendLine(@"    '" + sub_open_bids +"',");
            sb.AppendLine(@"    '" + notice_award +"',");
            sb.AppendLine(@"    '" + contract_signing +"',");
            sb.AppendLine(@"    '" + fund_source + "',");
            sb.AppendLine(@"    '" + years + "'");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        public void AddNewRow()
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_schedule_procact_library");
            sb.AppendLine(@" ( name)");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '  '");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        private bool _is_done = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "AddNewRow":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveProc":
                    if (_is_done ==false)
                    {
                        if (SQLOperation != null)
                        {
                            SQLOperation(this, new EventArgs());
                            _is_done = true;
                        }
                    }
                   
                    break;
                case "UpdateRow":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }

    }

    public class ProcurementActivityData
    {
        public String Id { get; set; }
        public String Library { get; set; }
    }
}
