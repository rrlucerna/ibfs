﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsMain
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchDivisionData() 
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc ");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  Division;");

            return sb.ToString();

        }

        public String LoginUserAccount(string _username, string _password) 
        {
            StringBuilder sb = new StringBuilder(500);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"d.Division_Id,");
            sb.AppendLine(@"d.Division_Desc,");
            sb.AppendLine(@"mua.User_Fullname,mua.is_admin as IsAdmin,mua.User_Id as UserId,");
            sb.AppendLine(@"dsp.DBM_Sub_Pap_id as OfficeId,");
            sb.AppendLine(@"d.Division_Code as PAP,");
            sb.AppendLine(@"d.UnitCode as UnitCode,");
            sb.AppendLine(@"mfsl.Fund_Source_Id as FundId");
            sb.AppendLine(@"FROM mnda_user_accounts mua");
            sb.AppendLine(@"INNER JOIN Division d on d.Division_Id = mua.Division_Id");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = d.DBM_Sub_Pap_Id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.division_id = d.Division_Id");
            sb.AppendLine(@"WHERE  mua.Username ='"+ _username +"' and mua.Password = '"+ _password +"'");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"d.Division_Id,");
            sb.AppendLine(@"d.Division_Desc,");
            sb.AppendLine(@"mua.User_Fullname,mua.is_admin as IsAdmin,mua.User_Id as UserId,");
            sb.AppendLine(@"'-' as OfficeId,");
            sb.AppendLine(@"d.Division_Code as PAP,");
            sb.AppendLine(@"d.UnitCode as UnitCode,");
            sb.AppendLine(@"mfsl.Fund_Source_Id as FundId");
            sb.AppendLine(@"FROM mnda_user_accounts mua");
            sb.AppendLine(@"INNER JOIN Division d on d.Division_Code = mua.unit_code");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.division_id = d.Division_Id");
            sb.AppendLine(@"WHERE  mua.Username ='" + _username + "' and mua.Password = '" + _password + "'");

            return sb.ToString();
        }

       
      
    }
   
    public class UserVerify 
    {
        public String  Division_Id {get;set;}
        public String  Division_Desc {get;set;}
        public String  User_Fullname { get; set; }
        public String  IsAdmin { get; set; }
        public String  UserId { get; set; }
        public String OfficeId { get; set; }
        public String PAP { get; set; }
        public String UnitCode { get; set; }
        public String AttachedFundId { get; set; }
    }

    public class DivisionFields    
    {

       public String   Division_Id {get;set;}
       public String   DBM_Sub_Pap_Id{get;set;}
       public String   Division_Code{get;set;}
       public String   Division_Desc{get;set;}
    }
}
