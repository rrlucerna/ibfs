﻿using MinDAF.MinDAFS;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace MinDAF.Class
{
    public class clsActivityReport
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();


        public String FetchActivityData(string _division,string _year) 
        {
            //var sb = new System.Text.StringBuilder(775);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"mp.name as program,");
            //sb.AppendLine(@"mpe.project_name as project,");
            //sb.AppendLine(@"mpo.name as output,");
            //sb.AppendLine(@"ma.description as activity,");
            //sb.AppendLine(@"mooe.name as expense_item,");
            //sb.AppendLine(@"FORMAT(ISNULL(SUM(mad.total),0),'N2') as Total");
            //sb.AppendLine(@"FROM mnda_activity ma");
            //sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"INNER JOIN mnda_project_output mpo  on mpo.program_code = ma.output_id");
            //sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            //sb.AppendLine(@"INNER JOIN mnda_main_program_encoded mmpe on mmpe.id = mpe.main_program_id");
            //sb.AppendLine(@"INNER JOIN mnda_programs mp on mp.id = mmpe.program_code");
            //sb.AppendLine(@"WHERE mmpe.accountable_division = "+ _division +" and mad.year =  "+ _year +" and mad.is_suspended = 0");
            //sb.AppendLine(@"GROUP BY mp.name,mpe.project_name,mpo.name,ma.description,mooe.name");
            var sb = new System.Text.StringBuilder(725);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mp.name as program,");
            sb.AppendLine(@"mpe.project_name as project,");
            sb.AppendLine(@"mpo.name as output,");
            sb.AppendLine(@"ma.description as activity,");
            sb.AppendLine(@"mooe.name as expense_item,");
            sb.AppendLine(@"FORMAT(ISNULL(SUM(mad.total),0),'N2') as Total");
            sb.AppendLine(@"FROM mnda_activity ma");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_main_program_encoded mmpe on mmpe.id = mpe.main_program_id");
            sb.AppendLine(@"LEFT JOIN mnda_programs mp on mp.id = mmpe.program_code");
            sb.AppendLine(@"WHERE mpe.acountable_division_code= "+ _division +" and mad.year = "+ _year +"");
            sb.AppendLine(@"GROUP BY mp.name,mpe.project_name,mpo.name,ma.description,mooe.name");

            return sb.ToString();
        }

        public String FetchPrograms(String _divid,String _year) 
        {
            var sb = new System.Text.StringBuilder(221);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.id as project_id,");
            sb.AppendLine(@"mp.name as program,");
            sb.AppendLine(@"mpe.project_name as project");
            sb.AppendLine(@"FROM mnda_main_program_encoded mmpe");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mmpe.id");
            sb.AppendLine(@"INNER JOIN mnda_programs mp on mp.id = mmpe.program_code");
            sb.AppendLine(@"WHERE mmpe.accountable_division = "+ _divid +" and mmpe.fiscal_year = 2017");
            return sb.ToString();
           
        }

        public String FetchOutputs(String _divid, String _year)
        {
            var sb = new System.Text.StringBuilder(256);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.id as program_id,");
            sb.AppendLine(@"mpo.id as output_id,");
            sb.AppendLine(@"mpo.name as output");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = "+ _divid +" and mpe.project_year = "+ _year +"");
            sb.AppendLine(@"GROUP BY mpe.id,mpo.id,mpo.name");
            sb.AppendLine(@"ORDER BY mpo.id ASC");

            return sb.ToString();

        }
        public String FetchActivities(String _divid, String _year)
        {
            var sb = new System.Text.StringBuilder(343);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpo.id as output_id,");
            sb.AppendLine(@"ma.id as act_id,");
            sb.AppendLine(@"ma.description");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"WHERE mpe.acountable_division_code ="+ _divid +" and mpe.project_year = "+ _year +"");
            sb.AppendLine(@"GROUP BY mpo.id,");
            sb.AppendLine(@"ma.id,");
            sb.AppendLine(@"ma.description");
            sb.AppendLine(@"ORDER BY mpo.id ASC");

            return sb.ToString();

        }
        public String FetchExpenseItems(String _divid, String _year)
        {
            var sb = new System.Text.StringBuilder(507);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.activity_id as act_id,");
            sb.AppendLine(@"mooe.name expense_item,");
            sb.AppendLine(@"FORMAT(ISNULL(SUM(mad.total),0),'N2') as total");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mpe.acountable_division_code =" + _divid + " and mpe.project_year = " + _year + " AND  mad.is_suspended =0");
            sb.AppendLine(@"GROUP BY mad.activity_id,mooe.name");


            return sb.ToString();

        }
    }
    public class ActivityData_Main 
    {
        public String Program_Project { get; set; }
     
    }
    public class ActivityData_Programs 
    {
        public String Id { get; set; }
        public String Program { get; set; }
        public String Project { get; set; }
        public List<ActivityData_Output> Outputs { get; set; }
    }

    public class ActivityData_Output
    {
        public String Id { get; set; }
        public String OutputId { get; set; }
        public String Output { get; set; }
        public List<ActivityData_Activity> Activity { get; set; }
    }
    public class ActivityData_Activity 
    {
        public String Id { get; set; }
        public String OutputId { get; set; }
        public String Activity { get; set; }
      
    }
    public class ActivityData_ExpenseItem
    {
        public String ActId { get; set; }
        public String Expense_Item { get; set; }
        public String Total { get; set; }
    }

    public class ActivityData_Report 
    {
       public String program {get;set;}
       public String project{get;set;}
       public String output{get;set;}
       public String activity{get;set;}
       public String expense_item{get;set;}
       public String Total { get; set; }
    }
}
