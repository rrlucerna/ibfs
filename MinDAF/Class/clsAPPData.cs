﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsAPPData
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String GetData(String _year)
        {


            var sb = new System.Text.StringBuilder(1679);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'M' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mpe.project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"'' as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'S' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mooe.name as project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"''  as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");


            return sb.ToString();
        }

        public String GetPrograms(string _year)
        {

            var sb = new System.Text.StringBuilder(554);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpe.id,0) as id,");
            sb.AppendLine(@"ISNULL(div.Division_Code,'-') as pap,");
            sb.AppendLine(@"ISNULL(mpe.project_name,'-') as project_name,");
            sb.AppendLine(@"ISNULL(div.DivisionAccro,'-') as pmo");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");
            sb.AppendLine(@"GROUP BY mpe.id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"mpe.project_name,");
            sb.AppendLine(@"div.DivisionAccro");



            return sb.ToString();
        }
        public void SavePrintOut(List<APPData_List> _Data,String _Year)
        {
            String _sqlString = "";
            _sqlString = "DELETE FROM dbo.mnda_report_data_app;";
            foreach (var item in _Data)
            {
                if (item.ad_post_rei==null)
                {
                    item.ad_post_rei = "-";
                } 
                if (item.co==null)
                {
                    item.co = "";
                }
                if (item.code==null)
                {
                    item.code = "";
                }
                if (item.contract_signing==null)
                {
                    item.contract_signing = "";
                }
                if (item.mode_procurement==null)
                {
                    item.mode_procurement = "";
                }
                if (item.mooe_code==null)
                {
                    item.mooe_code = "";
                }
                if (item.mooe_total==null)
                {
                    item.mooe_total = "0";
                }
                if (item.notice_award==null)
                {
                    item.notice_award = "";
                }
                if (item.pmo==null)
                {
                    item.pmo = "";
                }
                if (item.procurement_project==null)
                {
                    item.procurement_project = "";
                }
                else if (item.procurement_project.Contains("'"))
                {
                  item.procurement_project=  item.procurement_project.Replace("'", "");
                }
                if (item.remarks==null)
                {
                    item.remarks = "";
                }
                if (item.source_funds==null)
                {
                    item.source_funds = "";
                }
                if (item.sub_open==null)
                {
                    item.sub_open = "";
                }
                if (item.total==null)
                {
                    item.total = "0";
                }
                _sqlString += @"INSERT INTO   dbo.mnda_report_data_app(  code,  procurement_project, pmo_end_user,  mode_of_procurement,  ads_post_rei,  sub_open_bids,
                          notice_award,  contract_signing,  source_fund,  eb_total,  eb_mooe,  eb_co,  remarks,  pap_code, yearsss
                        ) VALUES ('" + item.code + "', '" + item.procurement_project + "', '" + item.pmo + "', '" + item.mode_procurement + "', '" + item.ad_post_rei + "', '" + item.sub_open + "', '" + item.notice_award + "', '" +
                        item.contract_signing + "', '" + item.source_funds + "', '" + item.total + "', '" + item.mooe_total + "', '" + item.co + "', '" + item.remarks + "', '-', '" + _Year + "');";
            }
        


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        private Boolean isProcesses = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SavePrintOut":
                    if (SQLOperation!=null)
                    {
                        if (isProcesses ==false)
                        {
                            SQLOperation(this, new EventArgs());
                            isProcesses = true;
                        }
                        else
                        {
                            isProcesses = false;
                        }
                       
                    }
                    break;
                default:
                    break;
            }
          
        }

        public String GetProActData(string _year)
        {

            var sb = new System.Text.StringBuilder(184);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" ISNULL(code,'-') as code,");
            sb.AppendLine(@" ISNULL(ref_code,'-') as ref_code,");
            sb.AppendLine(@" ISNULL(mod_procure,'-') as mod_procure,");
            sb.AppendLine(@" ISNULL(ad_post_rei,'-') as ad_post_rei,");
            sb.AppendLine(@" ISNULL(sub_open_bids,'-') as sub_open_bids,");
            sb.AppendLine(@" ISNULL(notice_award,'-') as notice_award,");
            sb.AppendLine(@" ISNULL(contract_signing,'-') as contract_signing,");
            sb.AppendLine(@" ISNULL(source_funds,'-') as source_funds,");
            sb.AppendLine(@" ISNULL(years,'-') as years");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_proc_act_data WHERE years = '"+ _year +"';");



            return sb.ToString();
        }
        public String GetDivisionList()
        {

            var sb = new System.Text.StringBuilder(102);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"div.DivisionAccro,");
            sb.AppendLine(@"div.Division_Desc");
            sb.AppendLine(@"FROM Division div");

            return sb.ToString();
        }
        public String GetMOOE()
        {

            var sb = new System.Text.StringBuilder(59);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mo.code,");
            sb.AppendLine(@" mo.name");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mo");

            return sb.ToString();
        }
        public String GetMOOESub()
        {

            var sb = new System.Text.StringBuilder(133);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mosub.uacs_code,");
            sb.AppendLine(@"mosub.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mosub ");
            sb.AppendLine(@"WHERE mosub.is_active = 1 and not mosub.uacs_code ='-'");

            return sb.ToString();
        }

     // ---------------------- PPMP Class -----------------------------


        public String FetchData(string _div_id, string _year)
        {

            StringBuilder sb = new StringBuilder(402);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Name,");
            sb.AppendLine(@"div.Division_Desc,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.total as rate,");
            sb.AppendLine(@"mad.quantity,mad.type_service,ISNULL(mad.id,0) as act_id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.year,");
            sb.AppendLine(@"ISNULL(mad.entry_date,'') as entry_date,");
            sb.AppendLine(@"ISNULL(mad.start,'') as s_date,");
            sb.AppendLine(@"ISNULL(mad.[end],'') as e_date");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = " + _div_id + " AND mad.year = " + _year + " and madp.is_submitted =0 and mfs.service_type = 1");

            return sb.ToString();
        }
        public String FetchDataAPP(string _year)
        {

            StringBuilder sb = new StringBuilder(402);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpe.id,'') as _ref,");
            sb.AppendLine(@"ISNULL(mfs.Fund_Name,'') as Fund_Name,");
            sb.AppendLine(@"ISNULL(div.Division_Desc,'') as Division_Desc,");
            sb.AppendLine(@"ISNULL(div.Division_Code,'') as Division_Code,");
            sb.AppendLine(@"ISNULL(mooe.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mooe.mooe_id,'') as mooe_id, ");
            sb.AppendLine(@"ISNULL(mooe.name,'') as name,");
            sb.AppendLine(@"ISNULL(mad.total,0) as rate,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as quantity,ISNULL(mad.type_service,'') as type_service,ISNULL(mad.id,0) as act_id,");
            sb.AppendLine(@"ISNULL(mad.month,'') as month,");
            sb.AppendLine(@"ISNULL(mad.year,'') as year,");
            sb.AppendLine(@"ISNULL(mad.entry_date,'') as entry_date,");
            sb.AppendLine(@"ISNULL(mad.start,'') as s_date,");
            sb.AppendLine(@"ISNULL(mad.[end],'') as e_date");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE  mad.year = " + _year + " and madp.is_submitted =0 and mfs.service_type = 1");

            return sb.ToString();
        }
        public String FetchRevision(string _pap, String _year)
        {
            var sb = new System.Text.StringBuilder(149);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"COUNT(ppmp.revision) as _rev");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp ppmp");
            sb.AppendLine(@"WHERE ppmp.PAP ='" + _pap + "' and ppmp.Yearssss = '" + _year + "'");
            sb.AppendLine(@"GROUP BY ppmp.revision");

            return sb.ToString();
        }
        public String FetchApprovals()
        {
            var sb = new System.Text.StringBuilder(263);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Uacs,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  Quantity,");
            sb.AppendLine(@"  EstimateBudget,");
            sb.AppendLine(@"  ModeOfProcurement,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Division,");
            sb.AppendLine(@"  Yearssss,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE approved = 0;");


            return sb.ToString();
        }
        public String FetchFundSource(String div)
        {
            var sb = new System.Text.StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Fund_Source_Id,");
            sb.AppendLine(@"  Fund_Name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fund_source mfs");
            sb.AppendLine(@"WHERE mfs.division_id='" + div + "';");



            return sb.ToString();
        }
        public String FetchReview(String _pap, String _year)
        {
            var sb = new System.Text.StringBuilder(263);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Uacs,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  Quantity,");
            sb.AppendLine(@"  EstimateBudget,");
            sb.AppendLine(@"  ModeOfProcurement,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Division,");
            sb.AppendLine(@"  Yearssss,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE approved = 1 AND PAP ='" + _pap + "' AND Yearssss ='" + _year + "';");


            return sb.ToString();
        }
        public String FetchView(String _pap, String _year)
        {
            var sb = new System.Text.StringBuilder(263);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Uacs,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  Quantity,");
            sb.AppendLine(@"  EstimateBudget,");
            sb.AppendLine(@"  ModeOfProcurement,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Division,");
            sb.AppendLine(@"  Yearssss,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE approved = 1 AND PAP ='" + _pap + "' AND Yearssss ='" + _year + "';");

            return sb.ToString();
        }

     // ---------------------------------------------------------------
    }


    public class APP_PROGRAMS 
    {
        public String ID { get; set; }
        public String PAPCODE { get; set; }
        public String Programs { get; set; }
        public String PMO { get; set; }
   
    }

    public class APP_MOOEList 
    {
        public String Code {get;set;} 
        public String Name {get;set;}

    }
    public class APP_MOEESUBList 
    {
        public String Code { get; set; }
        public String MooeSub { get; set; }

    }

    public class APPData_Expenditures 
    {
         public String  id {get;set;}
         public String  pap{get;set;}
         public String  expenditure_name { get; set; }
    }
    public class APPData_Programs
    {
        public String id { get; set; }
        public String pap { get; set; }
        public String program_name { get; set; }
    }

    public class APPData_List 
    {
             public String code {get;set;}
             public String mooe_code { get; set; }   
          
             public String procurement_project{get;set;}
             public String pmo { get; set; }
             public String mode_procurement{get;set;}
             public String ad_post_rei{get;set;}
             public String sub_open{get;set;}
             public String notice_award{get;set;}
             public String contract_signing{get;set;}
             public String source_funds{get;set;}
             public String total{get;set;}
             public String mooe_total{get;set;}
             public String co{get;set;}
             public String remarks { get; set; }
    }
    public class APPData_Division
    {
      public String  Division_Id {get;set;}
      public String  Division_Code{get;set;}
      public String  DivisionAccro{get;set;}
      public String  Division_Desc { get; set; }
    }
    public class APPProcAct_Data
    {
        public String Code { get; set; }
        public String RefCode { get; set; }
        public String ModProcure { get; set; }
        public String AdPost { get; set; }
        public String SubOpen { get; set; }
        public String NoticeAward { get; set; }
        public String ContractSign { get; set; }
        public String Years { get; set; }
        public String SourceFunds { get; set; }
    }

}
