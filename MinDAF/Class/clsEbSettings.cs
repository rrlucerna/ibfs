﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsEbSettings
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchSettings()
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  database_name,");
            sb.AppendLine(@"  username,");
            sb.AppendLine(@"  password,");
            sb.AppendLine(@"  host");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_settings;");

            return sb.ToString();
        }
        public void UpdateSettings(String _id, String _db,String _username, String _pass, String host)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(122);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_settings  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  database_name ='"+ _db +"',");
            sb.AppendLine(@"  username = '"+ _username +"',");
            sb.AppendLine(@"  password ='"+ _pass +"',");
            sb.AppendLine(@"  host ='"+ host +"'");
            sb.AppendLine(@"WHERE   id = " + _id + " ;");
            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "UpdateSettings":
                    if (this.SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
               
            }
        }
    }

    public class EB_Variables 
    {
          public String  id {get;set;}
          public String  database_name{ get; set; }
          public String  username{ get; set; }
          public String  password { get; set; }
          public String  host { get; set; } 
    }
}
