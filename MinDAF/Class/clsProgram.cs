﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsProgram
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchProgramList()
        {

            StringBuilder sb = new StringBuilder(90);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");


            return sb.ToString();
        }

        public String FetchKeyResultArea() 
        {
            StringBuilder sb = new StringBuilder(97);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  KeyResult_Id,");
            sb.AppendLine(@"  KeyResult_Code,");
            sb.AppendLine(@"  KeyResult_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_key_result_area;");

            return sb.ToString();
        }

        public String FetchPAPGroup(string _ID) 
        {
            StringBuilder sb = new StringBuilder(91);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"dbmp.DBM_Pap_Code,");
            sb.AppendLine(@"dbmp.DBM_Pap_Desc");
            sb.AppendLine(@"FROM DBM_Pap dbmp");
            sb.AppendLine(@"WHERE dbmp.DBM_Pap_Id ="+ _ID +" ;");

            return sb.ToString();

        }

        public String FetchMindanao2020() 
        {
            StringBuilder sb = new StringBuilder(114);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Mindanao_2020_Id,");
            sb.AppendLine(@"  Mindanao_2020_Code,");
            sb.AppendLine(@"  Mindanao_2020_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"mnda_mindanao_2020_contribution;");


            return sb.ToString();
        }
        public String FetchFundSource()
        {
            StringBuilder sb = new StringBuilder(71);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Fund_Source_Id,");
            sb.AppendLine(@"  Fund_Name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fund_source;");


            return sb.ToString();
        }
        public String FetchAccountableDivision()
        {
            StringBuilder sb = new StringBuilder(63);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division;");



            return sb.ToString();
        }
        public String FetchMajorFinalOutput()
        {
            StringBuilder sb = new StringBuilder(56);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@" mnda_major_final_output;");



            return sb.ToString();
        }

        public String FetchPerformanceIndicator()
        {
            StringBuilder sb = new StringBuilder(67);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"mnda_performance_indicator;");


            return sb.ToString();
        }
        public String FetchPrograms()
        {

         StringBuilder sb = new StringBuilder(51);
        sb.AppendLine(@"SELECT ");
        sb.AppendLine(@"  id,");
        sb.AppendLine(@"  name");
        sb.AppendLine(@"FROM ");
        sb.AppendLine(@"  dbo.mnda_programs;");


            return sb.ToString();
        }
        public String FetchCategory()
        {

            StringBuilder sb = new StringBuilder(62);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  weight");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_category;");


            return sb.ToString();
        }
        public String FetchAccountableOffice()
        {

            StringBuilder sb = new StringBuilder(50);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_offices;");


            return sb.ToString();
        }
        public String FetchScope()
        {
            StringBuilder sb = new StringBuilder(76);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Scope_Id,");
            sb.AppendLine(@"  Scope_Code,");
            sb.AppendLine(@"  Scope_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_scopes;");


            return sb.ToString();
        }

        public String FetchEncodedPrograms(String _MainID) 
        {
            StringBuilder sb = new StringBuilder(597);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mpe.id as Id, mpe.project_year as Project_Year,");
            sb.AppendLine(@"mpe.project_name as Project_Name,");
            sb.AppendLine(@"mka.KeyResult_Desc,");
            sb.AppendLine(@"mmc.Mindanao_2020_Desc,");
            sb.AppendLine(@"ms.Scope_Desc as Scope,");
            sb.AppendLine(@"mfs.Fund_Name as Fund_Source,");
            sb.AppendLine(@"d.Division_Desc as Division");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_key_result_area mka on mka.KeyResult_Code = mpe.key_result_code");
            sb.AppendLine(@"INNER JOIN mnda_mindanao_2020_contribution mmc on mmc.Mindanao_2020_Code = mpe.mindanao_2020_code");
            sb.AppendLine(@"INNER JOIN mnda_scopes ms on ms.Scope_Code = mpe.scope_code");
            sb.AppendLine(@"INNER JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mpe.fund_source_code");
            sb.AppendLine(@"INNER JOIN Division d on d.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mpe.main_program_id = '" + _MainID + "'");

            return sb.ToString();
        } 
        
        public String FetchProgramsMain() 
        {
            StringBuilder sb = new StringBuilder(563);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mmpe.id as Id, mmpe.Fiscal_Year,");
            sb.AppendLine(@"mmfo.name as Major_Final_Output,");
            sb.AppendLine(@"mpi.name as Performance_Indicator,");
            sb.AppendLine(@"mp.name as Program,");
            sb.AppendLine(@"mc.name as Category,");
            sb.AppendLine(@"mo.name as Accountable_Office");
            sb.AppendLine(@"FROM mnda_main_program_encoded mmpe");
            sb.AppendLine(@"INNER JOIN mnda_major_final_output mmfo on mmfo.id = mmpe.major_final_output_code");
            sb.AppendLine(@"INNER JOIN mnda_performance_indicator mpi on mpi.id = mmpe.performance_indicator_code");
            sb.AppendLine(@"INNER JOIN mnda_programs mp on mp.id = mmpe.program_code");
            sb.AppendLine(@"INNER JOIN mnda_category mc on mc.id = mmpe.category_code");
            sb.AppendLine(@"INNER JOIN mnda_offices mo  on mo.id = mmpe.accountable_office_code");


            return sb.ToString();
        }
        public String FetchExpectedOutput(String _project_id) 
                {
                    StringBuilder sb = new StringBuilder(67);
                    sb.AppendLine(@"SELECT ");
                    sb.AppendLine(@"  id,program_code,");
                    sb.AppendLine(@"  name");
                    sb.AppendLine(@"FROM ");
                    sb.AppendLine(@"  dbo.mnda_project_output WHERE program_code ='"+ _project_id +"';");

                    return sb.ToString();
                }
        public void SaveExpectedOutput(String project_code, String expected_output)
        {
            String _sqlString = "";


            StringBuilder sb = new StringBuilder(112);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + project_code + "',");
            sb.AppendLine(@"  '" + expected_output + "'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();



            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveProgram(String main_program_code, String key_result_code, String mindanao_2020_code, String scope_code, String fund_source_code, String acountable_division_code, String project_name, String project_year)
        {
            String _sqlString = "";


            StringBuilder sb = new StringBuilder(250);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_program_encoded");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  main_program_id, key_result_code,");
            sb.AppendLine(@"  mindanao_2020_code,");
            sb.AppendLine(@"  scope_code,");
            sb.AppendLine(@"  fund_source_code,");
            sb.AppendLine(@"  acountable_division_code,");
            sb.AppendLine(@"  project_name,");
            sb.AppendLine(@"  project_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + main_program_code + "',");
            sb.AppendLine(@"  '"+  key_result_code +"',");
            sb.AppendLine(@"  '"+ mindanao_2020_code +"',");
            sb.AppendLine(@"  '"+ scope_code +"',");
            sb.AppendLine(@"  '"+ fund_source_code +"',");
            sb.AppendLine(@"  '"+ acountable_division_code +"',");
            sb.AppendLine(@"  '"+ project_name +"',");
            sb.AppendLine(@"  '"+ project_year +"'");
            sb.AppendLine(@");");


                _sqlString += sb.ToString();
          


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveProgramMain(String fiscal_year, String major_final_output_code, String performance_indicator_code, String program_code, String category_code, String accountable_office_code)
                {
                    String _sqlString = "";


                    StringBuilder sb = new StringBuilder(244);
                    sb.AppendLine(@"INSERT INTO ");
                    sb.AppendLine(@"  mnda_main_program_encoded");
                    sb.AppendLine(@"(");
                    sb.AppendLine(@"  fiscal_year,");
                    sb.AppendLine(@"  major_final_output_code,");
                    sb.AppendLine(@"  performance_indicator_code,");
                    sb.AppendLine(@"  program_code,");
                    sb.AppendLine(@"  category_code,");
                    sb.AppendLine(@"  accountable_office_code");
                    sb.AppendLine(@") ");
                    sb.AppendLine(@"VALUES (");
                    sb.AppendLine(@"  '"+ fiscal_year +"',");
                    sb.AppendLine(@"  '"+ major_final_output_code +"',");
                    sb.AppendLine(@"  '"+ performance_indicator_code +"',");
                    sb.AppendLine(@"  '"+ program_code +"',");
                    sb.AppendLine(@"  '"+ category_code +"',");
                    sb.AppendLine(@"  '"+ accountable_office_code +"'");
                    sb.AppendLine(@");");


                    _sqlString += sb.ToString();
          


                    c_ops.InstantiateService();
                    c_ops.ExecuteSQL(_sqlString);
                    c_ops.DataReturn += c_ops_DataReturn;


                }


        void c_ops_DataReturn(object sender, EventArgs e)
        {
            ReturnCode = c_ops.ReturnCode;
            switch (Process)
            {
                case "SaveProjectProgram":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveProgram":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveExpectedOutput":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveProjectData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            } 
        }
    }
    public class ExpectedOutput 
    {
        public String id { get; set; }
         public String  program_code { get; set; }
         public String  name { get; set; }
    }
    public class ProgramsMain
    {
        public String Id { get; set; }
        public String Fiscal_Year  { get; set; }
        public String Major_Final_Output  { get; set; }
        public String Performance_Indicator  { get; set; }
        public String Program  { get; set; }
        public String Category  { get; set; }
        public String Accountable_Office  { get; set; }
    }

    public class AccountableOffice 
    {
        public String id { get; set; }
        public String name { get; set; }
    }
    public class Categories 
    {
      public String     id {get;set;}
  public String name {get;set;}
  public String weight { get; set; }
    }
    public class PPrograms
    {
      public String     id{get;set;}
     public String name{get;set;}
    }
    public class PerformanceIndicator
    {
      public String  id{get;set;}
      public String  code{get;set;}
      public String  name{get;set;}
    }
    public class MajorFinalOutput 
    {
      public String    id{get;set;}
      public String  name { get; set; }
    }

    public class AccountableDivision 
    {
     public String    Division_Id {get;set;}
     public String  Division_Desc { get; set; }
    }
    public class FundSource 
    {
      public String   Fund_Source_Id{get;set;}
      public String Fund_Name { get; set; }
    }
    public class MindaScopes 
    {
    public String      Scope_Id {get;set;}
    public String Scope_Code{get;set;}
    public String  Scope_Desc { get; set; }
    }
    public class Mindanao2020Contribution 
    {
     public String  Mindanao_2020_Id{get;set;}
     public String  Mindanao_2020_Code{get;set;}
     public String  Mindanao_2020_Desc { get; set; }
    }
    public class KeyResultArea 
    {
     public Int32 KeyResult_Id {get;set;}
     public String KeyResult_Code{get;set;}
     public String KeyResult_Desc { get; set; }
    }

    public class ProgramLists
    {
      public String  DBM_Pap_Id {get;set;}
      public String  DBM_Sub_Pap_Code {get;set;}
      public String  DBM_Sub_Pap_Desc { get; set; }
    }

    public class PAPGroup 
    {
        public String DBM_Pap_Code { get; set; }
        public String DBM_Pap_Desc { get; set; }
    }

    public class EncodedPrograms 
    {
      public String Id { get; set; }
      public String  Project_Year{ get; set; }
      public String  Project_Name{ get; set; }
      public String  KeyResult_Desc{ get; set; }
      public String  Mindanao_2020_Desc{ get; set; }
      public String  Scope{ get; set; }
      public String  Fund_Source{ get; set; }
      public String  Division { get; set; }
    }
}
