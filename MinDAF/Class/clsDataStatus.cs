﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsDataStatus
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String GetDivisions()
        {
            var sb = new System.Text.StringBuilder(238);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Code as PAP,");
            sb.AppendLine(@"div.Division_Desc as Division,");
            sb.AppendLine(@"'-' as Planning_Stage,");
            sb.AppendLine(@"'-' as Submit_Stage,");
            sb.AppendLine(@"'-' as Division_Approval_Stage,");
            sb.AppendLine(@"'-' as Budget_Approval_Stage,");
            sb.AppendLine(@"'-' as PPMP_Stage");
            sb.AppendLine(@"FROM Division div");



            return sb.ToString();
        }
        public String GetDivisionPrograms()
        {
            var sb = new System.Text.StringBuilder(437);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mmpe.accountable_division,");
            sb.AppendLine(@"div.Division_Desc,");
            sb.AppendLine(@"ma.description,ma.id as act_id");
            sb.AppendLine(@"FROM mnda_main_program_encoded mmpe");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mmpe.accountable_division");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mmpe.id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"GROUP BY mmpe.accountable_division,div.Division_Desc,ma.description,ma.id");

            return sb.ToString();
        }
        public String GetDivisionActivities()
        {
            var sb = new System.Text.StringBuilder(65);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.activity_id,");
            sb.AppendLine(@"mad.status");
            sb.AppendLine(@"FROM mnda_activity_data mad");


            return sb.ToString();
        }
    }

    public class DivisionActivities 
    {
        public String activity_id { get; set; }
        public String status { get; set; }
    }
    public class DivisionPrograms 
    {
        public String accountable_division { get; set; }
        public String Division_Desc { get; set; }
        public String description { get; set; }
        public String act_id { get; set; }

    }
    public class DataStatus 
    {
 
        public String PAP{ get; set; }
        public String Division{ get; set; }
        public String Planning_Stage{ get; set; }
        public String Submit_Stage{ get; set; }
        public String Division_Approval_Stage{ get; set; }
        public String Budget_Approval_Stage{ get; set; }
        public String PPMP_Stage { get; set; }
    }
}
