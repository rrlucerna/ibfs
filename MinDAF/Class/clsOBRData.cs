﻿using MinDAF.MinDAFS;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsOBRData
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();


        public String FetchOBRData(String _year , String _fundsource)
        {
            var sb = new System.Text.StringBuilder(629);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_a.AccountCode,");
            sb.AppendLine(@"lib_res.Division,");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _Month,");
            sb.AppendLine(@"obr.DateCreated,");
            sb.AppendLine(@"SUM(obr_d.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequest obr");
            sb.AppendLine(@"INNER JOIN or_ObligationRequestParticular obr_d on obr.ID = obr_d.ObligationRequestID");
            sb.AppendLine(@"INNER JOIN  lib_ResponsibilityCenter lib_res on lib_res.ID = obr_d.ResponsibilityCenterID");
            sb.AppendLine(@"INNER JOIN lib_Account lib_a on lib_a.ID =obr_d.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated)= "+ _year +" and lib_res.Division ='"+ _fundsource +"' and CHARINDEX('502', lib_a.AccountCode)>0");
            sb.AppendLine(@"Group By obr_d.AcctID,lib_res.Division,MONTH(obr.DateCreated),obr.DateCreated,lib_a.AccountCode");
            sb.AppendLine(@"ORDER BY obr.DateCreated");

            return sb.ToString();
        }

        public String FetchMOOEData() 
        {
            var sb = new System.Text.StringBuilder(84);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ISNULL(mooe.code,'') as code,");
            sb.AppendLine(@"ISNULL(mooe.name,'') as name,");
            sb.AppendLine(@"ISNULL(mooe.type_service,'') as type_service");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mooe");

            return sb.ToString();
        }
        public String FetchMOOEDetails() 
        {
            var sb = new System.Text.StringBuilder(160);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mooe_det.mooe_id,'') as mooe_id,");
            sb.AppendLine(@"ISNULL(mooe_det.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mooe_det.name,'') as name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mooe_det");
            sb.AppendLine(@"WHERE mooe_det.is_active = 1 AND NOT CHARINDEX('-', mooe_det.uacs_code) >0");


            return sb.ToString();
        }
        public String FetchDivisionExpenses(string _div_code , string _year) 
        {
            var sb = new System.Text.StringBuilder(452);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = " + _div_code + " and mad.year = "+ _year +"");
            sb.AppendLine(@"GROUP BY msub.uacs_code,msub.name");


            return sb.ToString();
        }

    }

    public class OBR_DIV_EXPENSE 
    {
            public String uacs_code { get; set; }
            public String name { get; set; }
            public String total { get; set; }
    }
    public class OBR_MOOE_DETAILS 
    {
        public String mooe_id { get; set; }
          public String uacs_code { get; set; }
          public String name { get; set; }
    }

    public class OBR_MOOE 
    {
        public String code { get; set; }
        public String name { get; set; }
        public String type_service { get; set; }
    }

    public class OBR_Data 
    {
        public String MOOE { get; set; }
        public String Expenditure { get; set; }
        public String Allocation { get; set; }
        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }

        public String Total { get; set; }

        public String Balance { get; set; }
    }

    public class OBR_Details 
    {
         public String AccountCode{ get; set; }
         public String Division{ get; set; }
         public String  _Month{ get; set; }
         public String DateCreated{ get; set; }
         public String Total { get; set; }
    }
 
}
