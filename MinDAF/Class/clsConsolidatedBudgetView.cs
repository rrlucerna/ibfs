﻿using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsConsolidatedBudgetView
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchDBMSubPAP()
        {
            var sb = new System.Text.StringBuilder(124);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id as Id,");
            sb.AppendLine(@" CONCAT(DBM_Sub_Pap_Code,' - ',DBM_Sub_Pap_Desc) as Description");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");

            return sb.ToString();
        }

        public String FetchSubOffice()
        {
            var sb = new System.Text.StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Sub_Id,");
            sb.AppendLine(@"  DBM_Sub_Id,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Office;");

            return sb.ToString();
        }

        public String FetchDivisionList()
        {
            var sb = new System.Text.StringBuilder(128);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  CONCAT(Division_Code,' - ',Division_Desc) as Description");
            sb.AppendLine(@"");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division;");
            return sb.ToString();
        }

        public String FetchFundSourceDivisionList()
        {
            var sb = new System.Text.StringBuilder(260);
            sb.AppendLine(@"SELECT DISTINCT");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mfsl.Fund_Source_Id,");
            sb.AppendLine(@"mfsl.division_id as Division_Id,");
            sb.AppendLine(@"mooe.name as Expense,");
            sb.AppendLine(@"FORMAT(mfsl.Amount,'N2') as Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item  mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");

            return sb.ToString();
        }
        public String FetchDivisionExpenseItems(String _year)
        {
            var sb = new System.Text.StringBuilder(413);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mpe.acountable_division_code as div_id,");
            sb.AppendLine(@"mad.fund_source_id,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id  = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"WHERE mad.year = " + _year + " AND mad.is_suspended = 0 ");


            return sb.ToString();
        }

    }


    public class Consolidated_SubOffice 
    {
         public String  Sub_Id { get; set; }
         public String  DBM_Sub_Id { get; set; }
         public String  Description { get; set; }
         public String  PAP { get; set; }
    }

    public class Consolidated_Expenses
    {
        public String UACS { get; set; }
        public String Months { get; set; }
        public String div_id { get; set; }
        public String fund_source_id { get; set; }
        public String name { get; set; }
        public String total { get; set; }
    }
    public class Consolidated_DBMSUBPAP
    {
        public String SUBPAP_ID { get; set; }
        public String Description { get; set; }
        public List<Consolidated_Division> Division { get; set; }
    }

    public class Consolidated_Division
    {
        public String PAP { get; set; }
        public String SUBPAP_ID { get; set; }
        public String DIVISION_ID { get; set; }

        public String Description { get; set; }

        public List<Consodlidated_FundExpense> FundSource { get; set; }
    }

    public class Consodlidated_FundExpense
    {

        public String UACS { get; set; }
        public String EXPId { get; set; }
        public String Fund_Source_Id { get; set; }
        public String Division_Id { get; set; }
        public String Expense { get; set; }
        public String Amount { get; set; }

    }

    public class MainConsolidated
    {
        public string Description { get; set; }
        public string Allocation { get; set; }

        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }

        public String Total { get; set; }
    }
}
