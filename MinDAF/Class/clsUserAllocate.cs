﻿using MinDAF.MinDAFS;
using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsUserAllocate
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();



        public String FetchExpenditureItems()
        {
            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  is_active,");
            sb.AppendLine(@"  is_dynamic");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures WHERE is_active = 1 ORDER BY name ASC;");


            return sb.ToString();
        }
      
        public String FetchDivisionUnits(String _id) 
        {
            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"div.Division_Desc");
            sb.AppendLine(@"FROM  Division div");
            sb.AppendLine(@"WHERE div.UnitCode = '"+ _id +"'");

            return sb.ToString();
        }

        public String FetchRespoLibrary()
        {
            var sb = new System.Text.StringBuilder(62);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code as id,");
            sb.AppendLine(@"  respo_name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library WHERE is_division =1;");


            return sb.ToString();
        }
        
    }

    public class UnitLists 
    {
        public String DivisionId { get; set; }
        public String UnitCode { get; set; }
        public String Name { get; set; }
    }

}
