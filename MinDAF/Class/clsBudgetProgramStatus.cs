﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetProgramStatus
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        public String FetchExpenditureTemplate()
        {
            var sb = new System.Text.StringBuilder(318);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code as id,");
            sb.AppendLine(@"  name as Name,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures;");


            return sb.ToString();
        }
        public String FetchDataMBASummary(String _year, String pap)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _month,");
            sb.AppendLine(@"lrc.Division as FundSource,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"la.AccountCode as uacs,");
            sb.AppendLine(@"la.AccountDescription as name,");
            sb.AppendLine(@"oorp.Amount as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"LEFT  JOIN lib_Account la on la.ID = oorp.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated) = " + _year + "   AND lppa.PPACode ='" + pap + "'");


            return sb.ToString();
        }
        public String FetchAllocation(String _div_id, String _year,String fund_source_id)
        {
            //StringBuilder sb = new StringBuilder(868);
            //sb.AppendLine(@"SELECT ");
            //sb.AppendLine(@"mba.budget_allocation  as total");
            //sb.AppendLine(@"FROM mnda_budget_allocation mba");
            //sb.AppendLine(@"WHERE mba.division_id = " + _div_id + " and mba.entry_year = " + _year + "");
            var sb = new System.Text.StringBuilder(152);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ISNULL(SUM(mfsl.Amount),0) as total");
            sb.AppendLine(@"FROM mnda_fund_source mfs ");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfs.Year ="+ _year +" and mfs.division_id = "+ _div_id +" AND mfs.code = '"+ fund_source_id +"';");


            return sb.ToString();
        }
        public String FetchCumulative(String _div_id, String _year,String _user)
        {
            StringBuilder sb = new StringBuilder(443);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(SUM(mad.total),0) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
            sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = "+ _year +"");
            sb.AppendLine(@"AND mpe.accountable_division = "+ _div_id +"");
            sb.AppendLine(@"AND mad.accountable_id != '"+_user +"'");


            return sb.ToString();
        }
        public String FetchAllocationExpenditure(String _div_id, String _year)
        {
            StringBuilder sb = new StringBuilder(254);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"sub.name,");
            sb.AppendLine(@"mbe.allocated_budget");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"LEFT JOIN mnda_budget_expenditure mbe on mbe.division_id = div.Division_Id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mbe.expenditure_id");
            sb.AppendLine(@"WHERE div.Division_Id = " + _div_id + " AND mbe.year = " + _year + "");



            return sb.ToString();
        }
        public String FetchFundSource(String _div_id,String _year)
        {
            StringBuilder sb = new StringBuilder(125);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" code as Fund_Source_Id,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  Fund_Name,");
            sb.AppendLine(@"  details,");
            sb.AppendLine(@"  Amount");
            sb.AppendLine(@"FROM mnda_fund_source mfs ");
         //   sb.AppendLine(@"WHERE mfs.division_id ="+ _div_id +"  and  mfs.Year ="+ _year +";");
            sb.AppendLine(@"WHERE mfs.division_id =" + _div_id + ";");



            return sb.ToString();
        }
        public void SetActivitySuspended(String _id,Boolean _va)
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            

            if (_va)
            {
                sb.AppendLine(@"  status = 'Suspended' , is_suspended =1,is_submitted = 0");
            }
            else
            {
                sb.AppendLine(@"  status = '' , is_suspended =0");
            }
            
            sb.AppendLine(@"WHERE id = "+ _id +";");
            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteListSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        public void SetSubmitActivity(List<ActivityStatus> _data , String _id, Boolean _va)
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(79);
            List<String> SQLCommand = new List<string>();

            foreach (ActivityStatus item in _data)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_activity_data SET ");
                if (item.submit)
                {
                    sb.AppendLine(@"  is_submitted = 1 , status = 'Submitted',is_suspended =0");
                }
                else
                {
                    sb.AppendLine(@"  is_submitted = 0 ,status = ''");
                }               

                sb.AppendLine(@"WHERE id = " + item.activity_id + ";");
                _sqlString += sb.ToString();

                SQLCommand.Add(sb.ToString());
            }
          



            
            c_ops.InstantiateService();
            c_ops.ExecuteListSQL(SQLCommand);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SuspendActivity":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this,new EventArgs());
                    }
                    break;
                case "SubmitActivity":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
        public String FetchExpenditureDetailTemplate()
        {
            var sb = new System.Text.StringBuilder(345);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  sub_ex.mooe_id as id,");
            sb.AppendLine(@"  sub_ex.name as Name,");
            sb.AppendLine(@"  sub_ex.uacs_code as uacs_code,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  mnda_mooe_sub_expenditures sub_ex ;");


            return sb.ToString();
        }

        public String FetchActivityStatus(string _year,string _month)
        {
            StringBuilder sb = new StringBuilder(565);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mad.id,'-') as activity_id,");
            sb.AppendLine(@"ISNULL(mad.accountable_id,'-') as accountable_id,");
            sb.AppendLine(@"ISNULL(mooe.name,'-') as name,");
            sb.AppendLine(@"CASE WHEN  mad.start IS NULL THEN '' ELSE  mad.start END as start,");
            sb.AppendLine(@"CASE WHEN  mad.[end] IS NULL THEN '' ELSE  mad.[end] END as [end],");
            sb.AppendLine(@"ISNULL(mad.remarks,'-') as remarks ,");
            sb.AppendLine(@"ISNULL(mad.type_service,'-') as type_service ,");
            sb.AppendLine(@"ISNULL(mad.destination,'-') as destination ,");
            sb.AppendLine(@"ISNULL(mad.no_days,0) as no_days,");
            sb.AppendLine(@"ISNULL(mad.no_staff,0) as no_staff,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as quantity,");
            sb.AppendLine(@"ISNULL(mad.rate,0) as  rate,");
            sb.AppendLine(@"ISNULL(mad.total,0) as total,");
            sb.AppendLine(@"ISNULL(mad.is_suspended,'0') as suspend_stats,ISNULL(mad.is_submitted,'0') as submit,");
            sb.AppendLine(@"ISNULL(mad.status,'-') as status");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
            sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "'");
        

            return sb.ToString();
        }
        public String FetchActivityStatusUser(string _year, string _month,string _user,String fund_source)
        {
            StringBuilder sb = new StringBuilder(565);
            sb.AppendLine(@"SELECT DISTINCT");
            sb.AppendLine(@"ISNULL(mooe.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mad.id,'-') as activity_id,");
            sb.AppendLine(@"ISNULL(mad.accountable_id,'-') as accountable_id,");
            sb.AppendLine(@"ISNULL(mooe.name,'-') as name,");
            sb.AppendLine(@"CASE WHEN  mad.start IS NULL THEN '' ELSE  mad.start END as start,");
            sb.AppendLine(@"CASE WHEN  mad.[end] IS NULL THEN '' ELSE  mad.[end] END as [end],");
            sb.AppendLine(@"ISNULL(mad.remarks,'-') as remarks ,");
            sb.AppendLine(@"ISNULL(mad.type_service,'-') as type_service ,");
            sb.AppendLine(@"ISNULL(mad.destination,'-') as destination ,");
            sb.AppendLine(@"ISNULL(mad.no_days,0) as no_days,");
            sb.AppendLine(@"ISNULL(mad.no_staff,0) as no_staff,");
            sb.AppendLine(@"ISNULL(mad.quantity,0) as quantity,");
            sb.AppendLine(@"ISNULL(mad.rate,0) as  rate,");
            sb.AppendLine(@"ISNULL(mad.total,0) as total,");
            sb.AppendLine(@"ISNULL(mad.is_suspended,'0') as suspend_stats,ISNULL(mad.is_submitted,'0') as submit,");
            sb.AppendLine(@"ISNULL(mad.status,'-') as status");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
            sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "' AND mad.fund_source_id ='" + fund_source + "' AND mad.is_submitted = 0");
           // sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "' and mad.accountable_id='" + _user + "' AND mad.fund_source_id ='" + fund_source + "'");


            return sb.ToString();
        }


        public String FetchDataMBA(String _year, String _division_id, String _user,String fund_source_id)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id,");
            sb.AppendLine(@"mad.year,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.total,mad.is_suspended");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
           // sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"left JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + "");
           // sb.AppendLine(@"AND mpe.acountable_division_code = " + _division_id + "");
            //sb.AppendLine(@"AND mad.accountable_id = '"+ _user +"'");
            sb.AppendLine(@"AND mad.fund_source_id = '" + fund_source_id + "'");

            return sb.ToString();
        }
    }

    public class ActivityStatus 
    {
          public String uacs_code { get; set; }
          public String     activity_id{get;set;}
          public String     accountable_id{get;set;}
          public String     name{get;set;}
          public String     start{get;set;}
          public String     end{get;set;}
          public String     remarks{get;set;}
          public String     type_service{get;set;}
          public String     destination{get;set;} 
          public String     no_days{get;set;}
          public String     no_staff{get;set;}
          public String     quantity{get;set;}
          public String     rate{get;set;}
          public String     total{get;set;}
          public Boolean    suspend_activity { get; set; }
          public Boolean    submit { get; set; }
          public Int32      suspend_stats { get; set; }
          public Int32      submit_stats { get; set; }
          public String     status{get;set;}
    }
    public class FundSourceData 
    {
          public String  Fund_Source_Id { get; set; }
           public String  division_id { get; set; }
           public String  Fund_Name { get; set; }
           public String  details { get; set; }
           public String Amount { get; set; }
    }

    public class ActivityAllocated
    {
        public Double AllocatedAmount { get; set; }
    }
    public class CumulativeAmount 
    {
        public Double Amount { get; set; }
    }
}
