﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF
{
    public partial class MainPage : UserControl
    {
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private Boolean isVerified = false;
        private usr_Menu ctrlMenu = new usr_Menu();
        private usr_Menu_Admin ctrlAdmin = new usr_Menu_Admin();
        private clsMain c_main = new clsMain();

        private usr_bottom_menu ctrlBottom = new usr_bottom_menu();

        private usr_office_management ctrlOffice = new usr_office_management();
        private usr_Menu_Approvals ctrlApprovals = new usr_Menu_Approvals();
        private List<DivisionFields> ListDivisions = new List<DivisionFields>();
        private usr_report_dashboard usr_dashboard = new usr_report_dashboard();
        private String Username = "";
        private String UserID = "";
        private String Division = "";
        private String DivisionId = "";
        private String IsAdmin = "";
        private String OfficeId = "";
        private String PAP= "";
        public MainPage()
        {
            InitializeComponent();
            App.Current.Host.Content.Resized += Content_Resized;
            svc_mindaf.VerifyUserCompleted += new EventHandler<VerifyUserCompletedEventArgs>(svc_mindaf_VerifyUserCompleted);
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            ctrlBottom.MenuClick += ctrlBottom_MenuClick;
            ctrlBottom.SignOut += ctrlBottom_SignOut;


        }

        void ctrlBottom_SignOut(object sender, EventArgs e)
        {
            stkBottomMenu.Children.Clear();
            stkChild.Children.Clear();
            grdLogin.Visibility = System.Windows.Visibility.Visible;
            txtUsername.Text = "";
            txtPassword.Password = "";
            txtUsername.Focus();
        }


        private void LoadDivisions()
        {

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteSQLAsync(c_main.FetchDivisionData());
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            switch (c_main.Process)
            {
                case "VerifyUser":
                    XDocument oDoc = XDocument.Parse(_result);
                    var _data = from info in oDoc.Descendants("Table")
                                select new UserVerify
                                {

                                    Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                    Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                    User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                    IsAdmin = Convert.ToString(info.Element("IsAdmin").Value),
                                    UserId = Convert.ToString(info.Element("UserId").Value),
                                    OfficeId = Convert.ToString(info.Element("OfficeId").Value),
                                    PAP = Convert.ToString(info.Element("PAP").Value)
                                };



                    List<UserVerify> _user = new List<UserVerify>();
                    foreach (var item in _data)
                    {
                        UserVerify _dataUser = new UserVerify();

                        this.Username = item.User_Fullname;
                        this.Division = item.Division_Desc;
                        this.DivisionId = item.Division_Id;
                        this.UserID = item.UserId;
                        this.IsAdmin = item.IsAdmin;
                        this.OfficeId = item.OfficeId;
                        this.PAP = item.PAP;
                        _dataUser.Division_Id = item.Division_Id;
                        _dataUser.Division_Desc = item.Division_Desc;
                        _dataUser.User_Fullname = item.User_Fullname;
                        _dataUser.IsAdmin = item.IsAdmin;
                        _dataUser.UserId = item.UserId;
                        _dataUser.OfficeId = item.OfficeId;

                        _user.Add(_dataUser);

                        break;
                    }

                    if (_user.Count != 0)
                    {
                        grdLogin.Visibility = System.Windows.Visibility.Collapsed;

                        ctrlBottom.Height = stkBottomMenu.ActualHeight;
                        ctrlBottom.Width = stkBottomMenu.ActualWidth;

                        if (IsAdmin == "0")
                        {
                            ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Collapsed;
                            ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                            ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;
                        }
                        else if (IsAdmin == "2")
                        {
                            ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                            ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                            ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;
                        }
                        else if (IsAdmin == "1")
                        {
                            ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                            ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;
                            ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Visible;

                            usr_dashboard.Width = grdChild.ActualWidth;
                            usr_dashboard.Height = grdChild.ActualHeight;
                            usr_dashboard.OfficeID = this.OfficeId;
                            usr_dashboard.DivisionID = this.DivisionId;
                            usr_dashboard.User = this.Username;

                            stkChild.Children.Clear();
                            stkChild.Children.Add(usr_dashboard);
                        }


                        stkBottomMenu.Children.Clear();
                        stkBottomMenu.Children.Add(ctrlBottom);

                    }
                    else
                    {
                        MessageBox.Show("Login Failed");
                    }



                    this.Cursor = Cursors.Arrow;
                    break;
            }

        }



        void ctrlBottom_MenuClick(object sender, EventArgs e)
        {
            switch (ctrlBottom.MenuType)
            {
                case "Admin":
                    ctrlAdmin = new usr_Menu_Admin();
                    ctrlAdmin.Width = grdChild.ActualWidth;
                    ctrlAdmin.Height = grdChild.ActualHeight;
                    ctrlAdmin.Division = this.Division;
                    ctrlAdmin.IsAdmin = this.IsAdmin;
                    ctrlAdmin.UserId = this.UserID;
                    ctrlAdmin.DivisionID = this.DivisionId;
                    ctrlAdmin.User = this.Username;

                    grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrlAdmin);
                    break;
                case "BudgetDivision":
                    ctrlMenu = new usr_Menu();
                    ctrlMenu.Width = grdChild.ActualWidth;
                    ctrlMenu.Height = grdChild.ActualHeight;
                    ctrlMenu.Division = this.Division;
                    ctrlMenu.IsAdmin = this.IsAdmin;
                    ctrlMenu.UserId = this.UserID;
                    ctrlMenu.DivisionID = this.DivisionId;
                    ctrlMenu.OfficeID = this.OfficeId;
                    ctrlMenu.User = "Welcome " + this.Username;
                    ctrlMenu.PAP = this.PAP;
                    grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrlMenu);
                    break;
                case "BudgetApproval":
                    ctrlApprovals = new usr_Menu_Approvals();
                    ctrlApprovals.Width = grdChild.ActualWidth;
                    ctrlApprovals.Height = grdChild.ActualHeight;
                    ctrlApprovals.User = "Welcome " + this.Username;
                    ctrlApprovals.Level = IsAdmin;
                    ctrlApprovals.Division = Division;
                    ctrlApprovals.DivisionID = this.DivisionId;
                    ctrlApprovals.PaP = this.PAP;

                    grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrlApprovals);
                    break;
            }
        }



        void svc_mindaf_VerifyUserCompleted(object sender, VerifyUserCompletedEventArgs e)
        {
            var _result = e.Result;
            isVerified = _result;

            if (isVerified)
            {

                grdLogin.Visibility = System.Windows.Visibility.Collapsed;

                ctrlBottom.Height = stkBottomMenu.ActualHeight;
                ctrlBottom.Width = stkBottomMenu.ActualWidth;

                stkBottomMenu.Children.Add(ctrlBottom);

            }
            else
            {
                MessageBox.Show("Login Failed");
            }
        }

        void Content_Resized(object sender, EventArgs e)
        {
            this.Width = App.Current.Host.Content.ActualWidth;

            this.Height = App.Current.Host.Content.ActualHeight;

            ctrlBottom.Height = stkBottomMenu.ActualHeight;
            ctrlBottom.Width = stkBottomMenu.ActualWidth;


        }

        private void lblUser_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void lblUser_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void VerifyUserAccount()
        {
            c_main.Process = "VerifyUser";
            svc_mindaf.ExecuteSQLAsync(c_main.LoginUserAccount(txtUsername.Text, txtPassword.Password));
        }
        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            // svc_mindaf.VerifyUserAsync(txtUsername.Text, txtPassword.Password);
            VerifyUserAccount();

        }

        private void Grid_Loaded_1(object sender, RoutedEventArgs e)
        {
            LoadDivisions();
            //frmReportDownloadTool f_repo = new frmReportDownloadTool();
            //f_repo.Show();
        }

        private void btnHr_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://192.167.1.57:8080/mindata"), "_blank");
        }

        private void btnAccounting_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAccounting_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnHr_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnHr_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnAccounting_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://eibanez/"), "_blank");
        }
    }
}
