﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmLibraryFundSource : ChildWindow
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRespoLibrary c_respo = new clsRespoLibrary();
        private List<RespoData> ListRespo = new List<RespoData>();
        public frmLibraryFundSource()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_respo.Process)
            {
                case "FetchRespo":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new RespoData
                                     {
                                         Id = Convert.ToString(info.Element("id").Value),
                                        Name  = Convert.ToString(info.Element("respo_name").Value)
                                     };

                    ListRespo.Clear();
                

                    foreach (var item in _dataLists)
                    {
                        RespoData _varDetails = new RespoData();


                        _varDetails.Id = item.Id;
                        _varDetails.Name= item.Name;


                        ListRespo.Add(_varDetails);
                        
                    }


                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRespo;

                    try
                    {
                        grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                        
                    
                    }
                   
                    this.Cursor = Cursors.Arrow;

                   
                    break;
            }
        }
        private void FetchRespo() 
        {
            c_respo.Process = "FetchRespo";
            svc_mindaf.ExecuteSQLAsync(c_respo.FetchRespoLibrary());
        }

        private void SaveRespo() 
        {
            c_respo.Process = "SaveRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;
            c_respo.SaveRespo(txtRespo.Text);
        }
        private void UpdateRespo(String _id,String _name)
        {
            c_respo.Process = "UpdateRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;
            c_respo.UpdateRespo(_id, _name);
        }
        private void DeleteRespo(String _id)
        {
            c_respo.Process = "DeleteRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;
            c_respo.DeleteRespo(_id);
        }
        
        void c_respo_SQLOperation(object sender, EventArgs e)
        {
            switch (c_respo.Process)
            {
                case "SaveRespo":
                    FetchRespo();
                    break;
                case"UpdateRespo":
                    FetchRespo();
                    break;
                case "DeleteRespo":
                    FetchRespo();
                    break;
            }
        }
       

        private void frm_respo_lib_Loaded(object sender, RoutedEventArgs e)
        {
            FetchRespo();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveRespo();
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            try
            {
                String _name = e.Cell.Value.ToString();
                String _id = grdData.Rows[e.Cell.Row.Index].Cells["Id"].Value.ToString();
                UpdateRespo(_id, _name);
            }
            catch (Exception)
            {
               
              
            }
          

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Id"].Value.ToString();

                DeleteRespo(_id);
            }
            catch (Exception)
            {
                
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

