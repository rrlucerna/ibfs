﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmPAPManagementOffices : ChildWindow
    {
        public event EventHandler ReloadData;
        private List<Offices> ListOffice = new List<Offices>();
        private List<OfficesPAP> ListOfficePAP = new List<OfficesPAP>();
        private List<Divisions> ListDivision = new List<Divisions>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetPapOfficeManagement c_pap_office = new clsBudgetPapOfficeManagement();


        public frmPAPManagementOffices()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_pap_office.SQLOperation += c_pap_office_SQLOperation;
        }

        void c_pap_office_SQLOperation(object sender, EventArgs e)
        {
            switch (c_pap_office.Process)
            {
                case "UpdateOffice":
                    FetchOffices();
                    break;
                case "UpdateSubPap":
                    try
                    {
                        String _id = grdOffice.Rows[grdOffice.ActiveCell.Row.Index].Cells["DBM_Pap_Id"].Value.ToString();
                        FetchSubPap(_id);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "UpdateDivision":
                    try
                    {
                        String _idPAPs = grdOfficeSubPap.Rows[grdOfficeSubPap.ActiveCell.Row.Index].Cells["DBM_Sub_Pap_id"].Value.ToString();

                        FetchDivision(_idPAPs);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_pap_office.Process)
            {
                case "FetchOffices":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new Offices
                                     {
                                         DBM_Pap_Code = Convert.ToString(info.Element("DBM_Pap_Code").Value),
                                         DBM_Pap_Desc = Convert.ToString(info.Element("DBM_Pap_Desc").Value),
                                         DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value)

                                     };


                 ListOffice.Clear();

                    foreach (var item in _dataLists)
                    {
                        Offices _varProf = new Offices();

                        _varProf.DBM_Pap_Code = item.DBM_Pap_Code;
                        _varProf.DBM_Pap_Desc = item.DBM_Pap_Desc;
                        _varProf.DBM_Pap_Id = item.DBM_Pap_Id;

                        ListOffice.Add(_varProf);

                    }
                    grdOffice.ItemsSource = null;
                    grdOffice.ItemsSource = ListOffice;
                    if (grdOffice.Columns.Count!=0)
                    {
                        grdOffice.Columns["DBM_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchSubPap":
                    XDocument oDocKeyResultsFetchSubPap = XDocument.Parse(_results);
                    var _dataListsFetchSubPap = from info in oDocKeyResultsFetchSubPap.Descendants("Table")
                                     select new OfficesPAP
                                     {
                                         DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                         DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                         DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                         DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value)
                                     };


                 ListOfficePAP.Clear();

                 foreach (var item in _dataListsFetchSubPap)
                    {
                        OfficesPAP _varProf = new OfficesPAP();

                        _varProf.DBM_Pap_Id = item.DBM_Pap_Id;
                        _varProf.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                        _varProf.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Desc;
                        _varProf.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;

                        ListOfficePAP.Add(_varProf);

                    }
                    grdOfficeSubPap.ItemsSource = null;
                    grdOfficeSubPap.ItemsSource = ListOfficePAP;
                    grdOfficeSubPap.Columns["DBM_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                     grdOfficeSubPap.Columns["DBM_Sub_Pap_id"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivision":
                    XDocument oDocKeyResultsFetchDivision = XDocument.Parse(_results);
                    var _dataListsFetchDivision = from info in oDocKeyResultsFetchDivision.Descendants("Table")
                                     select new Divisions
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value)
                                     };


                 ListDivision.Clear();

                 foreach (var item in _dataListsFetchDivision)
                    {
                        Divisions _varProf = new Divisions();

                        _varProf.DBM_Sub_Pap_Id= item.DBM_Sub_Pap_Id;
                        _varProf.Division_Code = item.Division_Code;
                        _varProf.Division_Desc = item.Division_Desc;
                        _varProf.Division_Id = item.Division_Id;

                        ListDivision.Add(_varProf);

                    }
                    grdDivision.ItemsSource = null;
                    grdDivision.ItemsSource = ListDivision;
                    grdDivision.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivision.Columns["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                    break;
              
            }
        }

        private void FetchOffices()
        {
            c_pap_office.Process = "FetchOffices";
            svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchOffices());
        }

      
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_pap_office_Closed(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                ReloadData(this, new EventArgs());
            }
        }

        private void frm_pap_office_Loaded(object sender, RoutedEventArgs e)
        {
            FetchOffices();
        }

        private void FetchSubPap(String _id) 
        {
            grdDivision.ItemsSource = null;
            c_pap_office.Process = "FetchSubPap";
            svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchOfficesSubPap(_id));
        }
        private void UpdateSubPap(String _id,String _pap_code, String _desc)
        {
            c_pap_office.Process = "UpdateSubPap";
             c_pap_office.UpdatePapSub(_id,_pap_code,_desc);
        }
        private void FetchDivision(String _id)
        {
            c_pap_office.Process = "FetchDivision";
            svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchDivision(_id));
        }
        private void grdOffice_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)

        {
              String _id = grdOffice.Rows[e.Cell.Row.Index].Cells["DBM_Pap_Id"].Value.ToString();
              String _code = grdOffice.Rows[e.Cell.Row.Index].Cells["DBM_Pap_Code"].Value.ToString();
              String _desc = grdOffice.Rows[e.Cell.Row.Index].Cells["DBM_Pap_Desc"].Value.ToString();
                if (e.Cell.Column.HeaderText.ToString() == "DBM_Pap_Desc" || e.Cell.Column.HeaderText.ToString() == "DBM_Pap_Code")
            {
               
                c_pap_office.UpdatePapOffice(_id, _code, _desc);

            }

            try
            {
                FetchSubPap(_id);
            }
            catch (Exception)
            {
            }
        }

        private void grdOffice_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            try
            {
                 String _id = grdOffice.Rows[e.Cell.Row.Index].Cells["DBM_Pap_Id"].Value.ToString();
                 FetchSubPap(_id);
            }
            catch (Exception)
            {
            }
        }

        private void grdOfficeSubPap_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
   
            String _id = grdOfficeSubPap.Rows[e.Cell.Row.Index].Cells["DBM_Pap_Id"].Value.ToString();
            String _code_pap = grdOfficeSubPap.Rows[e.Cell.Row.Index].Cells["DBM_Sub_Pap_Code"].Value.ToString();
            String _desc = grdOfficeSubPap.Rows[e.Cell.Row.Index].Cells["DBM_Sub_Pap_Desc"].Value.ToString();
            String _sub_papdesc_id = grdOfficeSubPap.Rows[e.Cell.Row.Index].Cells["DBM_Sub_Pap_id"].Value.ToString();
            if (e.Cell.Column.HeaderText.ToString() == "DBM_Sub_Pap_Desc" || e.Cell.Column.HeaderText.ToString() == "DBM_Sub_Pap_Code")
            {

                c_pap_office.UpdatePapSub(_id, _code_pap ,_desc);

            }

           
        }

        private void grdOfficeSubPap_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            String _id = grdOfficeSubPap.Rows[e.Cell.Row.Index].Cells["DBM_Sub_Pap_id"].Value.ToString();
       

            try
            {
                FetchDivision(_id);
            }
            catch (Exception)
            {
            }
        }

        private void grdDivision_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            String _id = grdDivision.Rows[e.Cell.Row.Index].Cells["Division_Id"].Value.ToString();
            String _desc = grdDivision.Rows[e.Cell.Row.Index].Cells["Division_Desc"].Value.ToString();
            String _division_code = grdDivision.Rows[e.Cell.Row.Index].Cells["Division_Code"].Value.ToString();
            if (e.Cell.Column.HeaderText.ToString() == "Division_Desc" || e.Cell.Column.HeaderText.ToString() == "Division_Code")
            {
                c_pap_office.Process = "UpdateDivision";
                c_pap_office.UpdateDivision(_id, _division_code, _desc);

            }

           
        }

      
    }
}

