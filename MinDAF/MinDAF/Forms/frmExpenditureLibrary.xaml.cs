﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmExpenditureLibrary : ChildWindow
    {

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsExpenditures c_expenditures = new clsExpenditures();
        private List<mooe_sub_expenditures> ListSubExpenditures = new List<mooe_sub_expenditures>();
        private List<ExpenditureDetails> ListExpenditureLibrary = new List<ExpenditureDetails>();
        public frmExpenditureLibrary()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_expenditures.SQLOperation += c_expenditures_SQLOperation;
        }
        private bool isReload = false;
        void c_expenditures_SQLOperation(object sender, EventArgs e)
        {
            switch (c_expenditures.Process)
            {
                case "SaveData":
                    FetchLibraries();
                    break;
                case "EnableExpenditure":
                    FetchLibraries();
                    isReload = true;
                    break;
                case "DisableExpenditure":
                    FetchLibraries();
                    isReload = true;
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_expenditures.Process)
             {
                 case "FetchLibraries":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                     var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                      select new mooe_sub_expenditures
                                      {
                                          id = Convert.ToString(info.Element("id").Value),
                                          is_active = Convert.ToString(info.Element("is_active").Value),
                                          mooe_id = Convert.ToString(info.Element("mooe_id").Value),
                                          name = Convert.ToString(info.Element("name").Value)
                                         
                                      };

                     ListSubExpenditures.Clear();
                     cmbExpenditure.Items.Clear();

                     foreach (var item in _dataLists)
                     {
                         mooe_sub_expenditures _varDetails = new mooe_sub_expenditures();

                         _varDetails.id = item.id;
                         _varDetails.is_active = item.is_active;
                         _varDetails.mooe_id = item.mooe_id;
                         _varDetails.name = item.name;
                    

                         ListSubExpenditures.Add(_varDetails);
                         cmbExpenditure.Items.Add(item.name);
                     }



                     this.Cursor = Cursors.Arrow;
                     AddDays();
                     GenerateYear();
                     if (isReload)
                     {
                         FetchExpenditureDetails();
                         isReload = false;
                     }
                     break;
                 case "FetchExpDetails":
                     XDocument oDocKeyFetchExpDetails = XDocument.Parse(_results);
                     var _dataListsFetchExpDetails = from info in oDocKeyFetchExpDetails.Descendants("Table")
                                      select new ExpenditureDetails
                                      {
                                          day = Convert.ToString(info.Element("day").Value),
                                          item_code = Convert.ToString(info.Element("item_code").Value),
                                          item_name = Convert.ToString(info.Element("item_name").Value),
                                          library_type = Convert.ToString(info.Element("library_type").Value),
                                          rate = Convert.ToString(info.Element("rate").Value),
                                          rate_year = Convert.ToString(info.Element("rate_year").Value),
                                          sub_expenditure_code = Convert.ToString(info.Element("sub_expenditure_code").Value)
                                      };

                     ListExpenditureLibrary.Clear();
                    

                     foreach (var item in _dataListsFetchExpDetails)
                     {
                         ExpenditureDetails _varDetails = new ExpenditureDetails();

                         _varDetails.day = item.day;
                         _varDetails.item_code = item.item_code;
                         _varDetails.item_name = item.item_name;
                         _varDetails.library_type = item.library_type;
                         _varDetails.rate = item.rate;
                         _varDetails.rate_year = item.rate_year;
                         _varDetails.sub_expenditure_code = item.sub_expenditure_code;

                         ListExpenditureLibrary.Add(_varDetails);
                       
                     }

                     grdData.ItemsSource = null;
                     grdData.ItemsSource = ListExpenditureLibrary;
                     grdData.Columns["sub_expenditure_code"].Visibility = System.Windows.Visibility.Collapsed;
                     this.Cursor = Cursors.Arrow;

                     break;
             }
        }
        private void AddDays() 
        {
            cmbDay.Items.Clear();
            cmbDay.Items.Add("1D");
            cmbDay.Items.Add("2-3D");
            cmbDay.Items.Add("4D-MORE");

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void SaveData() 
        {
            string _day = "";
            try
            {
                _day = cmbDay.SelectedItem.ToString();
            }
            catch (Exception)
            {
                _day = "-";
            }
            c_expenditures.SaveExpenditureLibrary(_ActiveLibraryCode, txtLibraryType.Text, txtItemCode.Text, txtItemName.Text,_day,Convert.ToDouble(txtRate.Value), cmbYear.SelectedItem.ToString());
            ClearData();
            FetchExpenditureDetails();
        }

        private void ClearData() 
        {
            txtItemCode.Text = "";
            txtItemName.Text = "";
            txtLibraryType.Text = "";
            txtRate.Value = 0.00;
            cmbDay.SelectedIndex = -1;
            cmbYear.SelectedIndex = -1;

        }
        private String _ActiveLibraryCode = "";
        private void FetchExpenditureDetails()
        {
            if ( cmbExpenditure.SelectedItem!=null)
            {
                string _code = "";
                btnEnabled.IsEnabled = true;
                List<mooe_sub_expenditures> _d = ListSubExpenditures.Where(item => item.name == cmbExpenditure.SelectedItem.ToString()).ToList();
                foreach (var item in _d)
                {
                    _code = item.id;
                    if (item.is_active.ToString().ToLower() == "true")
                    {
                        btnEnabled.Content = "Disable Expenditure";
                        ControlStats(true);
                    }
                    else
                    {
                        btnEnabled.Content = "Enable Expenditure";
                        ControlStats(false);
                    }
                    break;
                }
                _ActiveLibraryCode = _code;
                c_expenditures.Process = "FetchExpDetails";
                svc_mindaf.ExecuteSQLAsync(c_expenditures.FetchExpenditureDetails(_code));
            }
           
            
        }
        private void ControlStats(Boolean _val) 
        {
            txtItemCode.IsEnabled = _val;
            txtItemName.IsEnabled = _val;
            txtLibraryType.IsEnabled = _val;
            txtRate.IsEnabled = _val;
            cmbDay.IsEnabled = _val;
            btnAdd.IsEnabled = _val;
            cmbYear.IsEnabled = _val;
            grdData.IsEnabled = _val;
        }

        private void FetchLibraries() 
        {
            c_expenditures.Process = "FetchLibraries";
            svc_mindaf.ExecuteSQLAsync(c_expenditures.FetchExpenseLibrary());
        }
        

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void frmexpenditure_Loaded(object sender, RoutedEventArgs e)
        {
            FetchLibraries();
        }

        private void cmbExpenditure_DropDownClosed(object sender, EventArgs e)
        {
            FetchExpenditureDetails();
        }

        private void btnEnabled_Click(object sender, RoutedEventArgs e)
        {
            string _code = "";

            List<mooe_sub_expenditures> _d = ListSubExpenditures.Where(item => item.name == cmbExpenditure.SelectedItem.ToString()).ToList();
            foreach (var item in _d)
            {
                _code = item.id;
               
                break;
            }

            if (btnEnabled.Content.ToString() == "Enable Expenditure")
            {
                c_expenditures.Process = "EnableExpenditure";
                c_expenditures.UpdateMOOEStats(_code, "1");
           

            }
            else
            {
                c_expenditures.Process = "DisableExpenditure";
                c_expenditures.UpdateMOOEStats(_code, "0");
           
            }
        }
    }
}

