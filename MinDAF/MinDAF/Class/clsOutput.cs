﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsOutput
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchProcurementSupplies()
        {
            StringBuilder sb = new StringBuilder(163);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items;");

            return sb.ToString();
        }

    }
}
