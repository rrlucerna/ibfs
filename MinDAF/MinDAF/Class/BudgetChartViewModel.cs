﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class BudgetChartViewModel
    {
        
        public Data DataToBind { get; set; }
        public BudgetChartViewModel(List<BudgetPieList> _data) 
        {
            DataToBind = new Data(_data);

        }
    }
    //public class DataItem
    //{
    //    public string Label { get; set; }
    //    public double Value { get; set; }
    //}


    public class Data : Dictionary<string, double>
    {
        public Data(List<BudgetPieList> _data) 
        {
            double TotalCost = 0;
            foreach (var item in _data)
            {
                if (item.Project!="Remaining Balance")
                {
                    TotalCost += item.Amount;
                    Add(item.Project + Environment.NewLine + item.Amount.ToString("#,##0.00"), item.Amount);
                }
               
          
            }
            foreach (var item in _data)
            {
                if (item.Project=="Remaining Balance")
                {
                       Add("Remaining Funds " + Environment.NewLine + (item.Amount - TotalCost).ToString("#,##0.00"), (item.Amount - TotalCost));
                       break;
                }
            }
        }

    }

  

}
