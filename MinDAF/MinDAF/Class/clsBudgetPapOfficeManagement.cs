﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetPapOfficeManagement
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchOffices()
        {
            StringBuilder sb = new StringBuilder(78);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Pap_Code,");
            sb.AppendLine(@"  DBM_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Pap;");


            return sb.ToString();
        }
        public String FetchFundsSource(String _div_id)
        {
            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Fund_Source_Id,");
            sb.AppendLine(@"  Fund_Name");
            sb.AppendLine(@"FROM   mnda_fund_source mfs ");
            sb.AppendLine(@"WHERE mfs.division_id =" + _div_id + "");
            sb.AppendLine(@";");


            return sb.ToString();
        }
         public String FetchOfficesSubPap(String _id)
                {
                    StringBuilder sb = new StringBuilder(134);
                    sb.AppendLine(@"SELECT ");
                    sb.AppendLine(@"  DBM_Sub_Pap_id,");
                    sb.AppendLine(@"  DBM_Pap_Id,");
                    sb.AppendLine(@"  DBM_Sub_Pap_Code,");
                    sb.AppendLine(@"  DBM_Sub_Pap_Desc");
                    sb.AppendLine(@"FROM dbo.DBM_Sub_Pap sub");
                    sb.AppendLine(@"WHERE sub.DBM_Pap_Id = "+ _id +" ;");


                    return sb.ToString();
                }

         public String FetchDivision(String _id)
         {
             StringBuilder sb = new StringBuilder(101);
             sb.AppendLine(@"SELECT ");
             sb.AppendLine(@"  Division_Id,");
             sb.AppendLine(@"  DBM_Sub_Pap_Id,");
             sb.AppendLine(@"  Division_Code,");
             sb.AppendLine(@"  Division_Desc");
             sb.AppendLine(@"FROM ");
             sb.AppendLine(@"  dbo.Division WHERE DBM_Sub_Pap_Id ="+ _id +";");


             return sb.ToString();
         }
         public String FetchDivisionAll()
         {
             StringBuilder sb = new StringBuilder(101);
             sb.AppendLine(@"SELECT ");
             sb.AppendLine(@"  Division_Id,");
             sb.AppendLine(@"  DBM_Sub_Pap_Id,");
             sb.AppendLine(@"  Division_Code,");
             sb.AppendLine(@"  Division_Desc");
             sb.AppendLine(@"FROM ");
             sb.AppendLine(@"  dbo.Division;");


             return sb.ToString();
         }    
        public String FetchSubPAP()
         {
    

             StringBuilder sb = new StringBuilder(109);
             sb.AppendLine(@"SELECT ");
             sb.AppendLine(@"  DBM_Sub_Pap_id,");
             sb.AppendLine(@"  DBM_Pap_Id,");
             sb.AppendLine(@"  DBM_Sub_Pap_Code,");
             sb.AppendLine(@"  DBM_Sub_Pap_Desc");
             sb.AppendLine(@"FROM ");
             sb.AppendLine(@"  dbo.DBM_Sub_Pap;");

             return sb.ToString();
         }
        public void UpdatePapOffice(String _id, String _pap_code, String _office_name)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(107);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.DBM_Pap  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  DBM_Pap_Code = '"+ _pap_code +"',");
            sb.AppendLine(@"  DBM_Pap_Desc = '"+ _office_name +"'");
            sb.AppendLine(@"WHERE  DBM_Pap_Id = "+ _id +"");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdatePapSub(String _id, String _pap_code, String _office_name)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(110);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  DBM_Sub_Pap_Code ='"+ _pap_code +"',");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc ='"+ _office_name  +"'");
            sb.AppendLine(@"WHERE DBM_Sub_Pap_id = "+ _id +" ;");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateDivision(String _id, String _division_code, String _division_desc)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.Division  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  Division_Code = '"+ _division_code +"',");
            sb.AppendLine(@"  Division_Desc = '"+ _division_desc +"'");
            sb.AppendLine(@"WHERE  Division_Id =  "+ _id +";");


            _sqlString += sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (Process)
            {
                case "UpdateOffice":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());   
                    }
                    break;
                case "UpdateSubPap":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateDivision":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }
    }

    public class Offices 
    {
       public String   DBM_Pap_Id {get;set;}
       public String   DBM_Pap_Code{get;set;}
       public String   DBM_Pap_Desc { get; set; }
    }

    public class OfficesPAP 
    {
          public String   DBM_Sub_Pap_id {get;set;}
          public String   DBM_Pap_Id {get;set;}
          public String   DBM_Sub_Pap_Code {get;set;}
          public String   DBM_Sub_Pap_Desc { get; set; }
    }

    public class Divisions     
    {
         public String   Division_Id { get; set; }
         public String   DBM_Sub_Pap_Id { get; set; }
         public String   Division_Code { get; set; }
         public String   Division_Desc { get; set; }
    }


}
