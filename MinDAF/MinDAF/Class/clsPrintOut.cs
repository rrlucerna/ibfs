﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsPrintOut
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public void SaveReportPrintOutPPMP(List<PPMPFormat> _data)
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(1000);
            sb.AppendLine(@"DELETE FROM dbo.mnda_report_data_ppmp;");
            foreach (var item in _data)
            {

                sb.AppendLine(@" INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_report_data_ppmp ");
                sb.AppendLine(@"( ");
                sb.AppendLine(@"  Uacs, ");
                sb.AppendLine(@"  Description, ");
                sb.AppendLine(@"  Quantity, ");
                sb.AppendLine(@"  EstimateBudget, ");
                sb.AppendLine(@"  ModeOfProcurement, ");
                sb.AppendLine(@"  Jan, ");
                sb.AppendLine(@"  Feb, ");
                sb.AppendLine(@"  Mar, ");
                sb.AppendLine(@"  Apr, ");
                sb.AppendLine(@"  May, ");
                sb.AppendLine(@"  Jun, ");
                sb.AppendLine(@"  Jul, ");
                sb.AppendLine(@"  Aug, ");
                sb.AppendLine(@"  Sep, ");
                sb.AppendLine(@"  Octs, ");
                sb.AppendLine(@"  Nov, ");
                sb.AppendLine(@"  Dec, ");
                sb.AppendLine(@"  Total, ");
                sb.AppendLine(@"  Division, ");
                sb.AppendLine(@"  Yearssss, ");
                sb.AppendLine(@"  PAP, ");
                sb.AppendLine(@"  approved ");
                sb.AppendLine(@")  ");
                sb.AppendLine(@"VALUES ( ");
                sb.AppendLine(@"  '" + item.UACS + "', ");
                sb.AppendLine(@"  '" + item.Description + "', ");
                sb.AppendLine(@"  '" + item.Quantity_Size + "', ");
                sb.AppendLine(@"  '" + item.EstimatedBudget + "', ");
                sb.AppendLine(@"  '" + item.ModeOfProcurement + "', ");
                sb.AppendLine(@"  '" + item.Jan + "', ");
                sb.AppendLine(@"  '" + item.Feb + "', ");
                sb.AppendLine(@"  '" + item.Mar + "', ");
                sb.AppendLine(@"  '" + item.Apr + "', ");
                sb.AppendLine(@"  '" + item.May + "', ");
                sb.AppendLine(@"  '" + item.Jun + "', ");
                sb.AppendLine(@"  '" + item.Jul + "', ");
                sb.AppendLine(@"  '" + item.Aug + "', ");
                sb.AppendLine(@"  '" + item.Sep + "', ");
                sb.AppendLine(@"  '" + item.Oct + "', ");
                sb.AppendLine(@"  '" + item.Nov + "', ");
                sb.AppendLine(@"  '" + item.Dec + "', ");
                sb.AppendLine(@"  '" + item._Total + "', ");
                sb.AppendLine(@"  '" + item._Division + "', ");
                sb.AppendLine(@"  '" + item._Year + "', ");
                sb.AppendLine(@"  '" + item._Pap + "', ");
                sb.AppendLine(@"  '0' ");
                sb.AppendLine(@"  ); ");
                _sqlString += sb.ToString();
            }



            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveReportPrintOutMAO(List<ReportMBA> _data,String Division,String FundSource,String Pap)
        {


            c_ops.InstantiateService();
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(1000);
            sb.AppendLine(@"DELETE FROM dbo.mnda_report_data_mao_urs;");
            foreach (var item in _data)
            {
                _sqlString = "";
                sb.AppendLine(@" INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ");
                sb.AppendLine(@"( ");
                sb.AppendLine(@"  ReportHeader, ");
                sb.AppendLine(@"  UACS, ");
                sb.AppendLine(@"  Allocation, ");
                sb.AppendLine(@"  Jan, ");
                sb.AppendLine(@"  Feb, ");
                sb.AppendLine(@"  Mar, ");
                sb.AppendLine(@"  Apr, ");
                sb.AppendLine(@"  May, ");
                sb.AppendLine(@"  Jun, ");
                sb.AppendLine(@"  Jul, ");
                sb.AppendLine(@"  Aug, ");
                sb.AppendLine(@"  Sep, ");
                sb.AppendLine(@"  Octs, ");
                sb.AppendLine(@"  Nov, ");
                sb.AppendLine(@"  Dec, ");
                sb.AppendLine(@"  Total, ");
                sb.AppendLine(@"  Balance, ");
                sb.AppendLine(@"  Divisions, ");
                sb.AppendLine(@"  FundSource, ");
                sb.AppendLine(@"  PAP ");
                sb.AppendLine(@")  ");
                sb.AppendLine(@"VALUES ( ");
                sb.AppendLine(@"  '" + item.ReportHeader + "', ");
                sb.AppendLine(@"  '" + item.UACS + "', ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  '" + Division + "', ");
                sb.AppendLine(@"  '" + FundSource + "', ");
                sb.AppendLine(@"  '" + Pap + "' ");
                sb.AppendLine(@"  ) ");

                Int32 count = 0;
                foreach (var itemdetail in item.ReportDetail)
                {
                    if (count==3)
                    {
                        break;
                    }
                    sb.AppendLine(@" INSERT INTO ");
                    sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ");
                    sb.AppendLine(@"( ");
                    sb.AppendLine(@"  ReportHeader, ");
                    sb.AppendLine(@"  UACS, ");
                    sb.AppendLine(@"  Allocation, ");
                    sb.AppendLine(@"  Jan, ");
                    sb.AppendLine(@"  Feb, ");
                    sb.AppendLine(@"  Mar, ");
                    sb.AppendLine(@"  Apr, ");
                    sb.AppendLine(@"  May, ");
                    sb.AppendLine(@"  Jun, ");
                    sb.AppendLine(@"  Jul, ");
                    sb.AppendLine(@"  Aug, ");
                    sb.AppendLine(@"  Sep, ");
                    sb.AppendLine(@"  Octs, ");
                    sb.AppendLine(@"  Nov, ");
                    sb.AppendLine(@"  Dec, ");
                    sb.AppendLine(@"  Total, ");
                    sb.AppendLine(@"  Balance, ");
                    sb.AppendLine(@"  Divisions, ");
                    sb.AppendLine(@"  FundSource, ");
                    sb.AppendLine(@"  PAP ");
                    sb.AppendLine(@")  ");
                    sb.AppendLine(@"VALUES ( ");
                    sb.AppendLine(@"  '          " + itemdetail.Name + "', ");
                    sb.AppendLine(@"  '" + itemdetail.UACS + "', ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Alloted) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jan) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Feb) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Mar) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Apr) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.May) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jun) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jul) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Aug) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Sep) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Oct) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Nov) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Dec) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Total) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Balance) + ", ");
                    sb.AppendLine(@"  '" + Division + "', ");
                    sb.AppendLine(@"  '" + FundSource + "', ");
                    sb.AppendLine(@"  '" + Pap + "' ");
                    sb.AppendLine(@"  ); ");
                    _sqlString += sb.ToString();
                    count += 1;
                }
              
            }




            c_ops.ExecuteSQL(_sqlString);
             c_ops.DataReturn+=c_ops_DataReturn;

        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveReportPrintOutPPMP":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveReportPrintOutMAO":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }

    }
}
