﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsDivisionBudgetAllocation
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRespoLibrary()
        {
            var sb = new System.Text.StringBuilder(62);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  respo_name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library;");


            return sb.ToString();
        }
        public String FetchDivisionPerOffice(String _id)
        {
            StringBuilder sb = new StringBuilder(130);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division");
            //sb.AppendLine(@"  WHERE DBM_Sub_Pap_Id ='"+ _id +"';");


            return sb.ToString();
        }
        public String FetchExpenditureItems()
        {
            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  is_active,");
            sb.AppendLine(@"  is_dynamic");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures WHERE is_active = 1;");


            return sb.ToString();
        }
        public String FetchOfficeData(String _id, String _year)
        {
            var sb = new System.Text.StringBuilder(355);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  SUM(ISNULL(mfsl.Amount,0.00)) as budget_allocation,");
            sb.AppendLine(@"  mfs.Status,");
            sb.AppendLine(@"  mfs.Year as entry_year");
            sb.AppendLine(@"FROM  mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.Fund_Source_Id");
            sb.AppendLine(@"WHERE mfs.division_id ="+ _id +" and  mfs.Year ="+ _year +"");
            sb.AppendLine(@"GROUP BY mfs.Fund_Source_Id,mfs.Fund_Name,mfs.Status, mfs.Year");



            return sb.ToString();
        }
        public String FetchOfficeData(String _id)
        {
        

            StringBuilder sb = new StringBuilder(238);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  mfs.Amount as budget_allocation,");
            sb.AppendLine(@"  mfs.status,");
            sb.AppendLine(@"  mfs.Year as entry_year");
            sb.AppendLine(@"FROM  mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"WHERE mfs.division_id =" + _id + "");


            return sb.ToString();
        }
        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + "");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        public String FetchBudgetExpenditureAllocation(String _year,String _id,String FundSource)
        {
            var sb = new System.Text.StringBuilder(276);
            sb.AppendLine(@"SELECT mfsl.id,");
            sb.AppendLine(@"mooe_sub.name,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.Fund_Source_Id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe_sub on mooe_sub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mooe_sub.is_active = 1 AND mfs.Year =" + _year + " AND mfs.division_id =" + _id + " and mfs.Fund_Source_Id = " + FundSource + "");



            return sb.ToString();
        }
        public void SaveDivisionBudget(String division_id, String Fund_Name, String Amount, String entry_year)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(132);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_fund_source");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  Fund_Name,");
            sb.AppendLine(@"  details,");
            sb.AppendLine(@"  Amount,");
            sb.AppendLine(@"  Year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"'"+ division_id +"',");
            sb.AppendLine(@"'"+ Fund_Name +"',");
            sb.AppendLine(@"'"+ Fund_Name +"',");
            sb.AppendLine(@""+ Amount +",");
            sb.AppendLine(@""+ entry_year +"");
            sb.AppendLine(@");");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveDivisionBudgetExpenditure(String Fund_Source_Id, String division_id, String mooe_id, String Amount, String entry_year)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(204);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_fund_source_line_item");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Fund_Source_Id,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  Amount,");
            sb.AppendLine(@"  Year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  " + Fund_Source_Id +",");
            sb.AppendLine(@"  '" + division_id +"',");
            sb.AppendLine(@"  '" + mooe_id +"',");
            sb.AppendLine(@"  '" + Amount +"',");
            sb.AppendLine(@"  '" + entry_year +"'");
            sb.AppendLine(@");");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateDivisionBudget(String fund_source_id,string _amount)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(96);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_fund_source  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  Amount =" + _amount +"");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  Fund_Source_Id ="+ fund_source_id +"");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
      public void RemoveBudget(String id)
            {
                String _sqlString = "";
                StringBuilder sb = new StringBuilder(56);
                sb.AppendLine(@"DELETE FROM ");
                sb.AppendLine(@"  dbo.mnda_fund_source_line_item ");
                sb.AppendLine(@"WHERE Fund_Source_Id =" + id + ";");


                _sqlString += sb.ToString();

                c_ops.InstantiateService();
                c_ops.ExecuteSQL(_sqlString);
                c_ops.DataReturn += c_ops_DataReturn;


            }
      public void UpdateAmountExpenditure(String id,String amount)
      {
          String _sqlString = "";
          var sb = new System.Text.StringBuilder(186);
          sb.AppendLine(@"UPDATE ");
          sb.AppendLine(@"  dbo.mnda_fund_source_line_item  ");
          sb.AppendLine(@"SET ");
          sb.AppendLine(@"  Amount = "  + amount +  "");
          sb.AppendLine(@"WHERE ");
          sb.AppendLine(@"  id ="+ id +"");

          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }
      public void RemoveBudgetAllocation(String id)
      {
          String _sqlString = "";
          StringBuilder sb = new StringBuilder(56);
          sb.AppendLine(@"DELETE FROM ");
          sb.AppendLine(@"  dbo.mnda_fund_source ");
          sb.AppendLine(@"WHERE Fund_Source_Id =" + id + ";");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }
      public void RemoveBudgetExpenditure(String id, String div_id, String mooe, String _year)
      {
          String _sqlString = "";
          StringBuilder sb = new StringBuilder(56);
          sb.AppendLine(@"DELETE FROM ");
          sb.AppendLine(@"  dbo.mnda_fund_source_line_item ");
          sb.AppendLine(@"WHERE Fund_Source_Id =" + id + "  AND division_id =" + div_id + " AND mooe_id = " + mooe + " AND Year = " + _year + ";");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }


        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveDivisionBudget":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveDivisionBudgetExpenditure":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateDivisionBudget":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateAmount":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveBudget":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveBudgetExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public class lib_BudgetAllocationExpenditure 
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Amount { get; set; }
    }

    public class lib_DivisionBudgetExpeniture
    {
          public String  mooe_id {get;set;}
             public String  uacs_code{get;set;}
           public String  name {get;set;}
              public String  is_active {get;set;}
              public String is_dynamic { get; set; }
    }
    public class DivisionBudgetAllocationFields     
    {
      public String Division_Id {get;set;}
      public String DBM_Sub_Pap_Id {get;set;}
      public String Division_Code {get;set;}
      public String Division_Desc { get; set; }
    }

    public class OfficeDataFields

    {
        public String id { get; set; }
        public String Fund_Name { get; set; }
         public String Amount { get; set; }
         public String status { get; set; }
         public String entry_year { get; set; }
    }
}
