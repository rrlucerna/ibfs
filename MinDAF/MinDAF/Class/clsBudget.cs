﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudget
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String GetExpenditures() 
        {
            StringBuilder sb = new StringBuilder(76);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ID,");
            sb.AppendLine(@"  MOOE_ID,");
            sb.AppendLine(@"  name as Description");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"mnda_mooe_sub_expenditures;");
  

            return sb.ToString();
        }


        public String GetDivisionBudget(string _id , String _year)
        {
           

            StringBuilder sb = new StringBuilder(241);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ISNULL(mba.budget_allocation - (SELECT SUM(mpp.amount)");
            sb.AppendLine(@"FROM mnda_project_proposals mpp");
            sb.AppendLine(@"where mpp.division_id = mba.division_id AND mpp.year = mba.entry_year),0.00) as Amount");
            sb.AppendLine(@"FROM mnda_budget_allocation mba");
            sb.AppendLine(@"WHERE mba.division_id = "+ _id +"");
            sb.AppendLine(@"AND mba.entry_year = "+ _year +"");

                       
            return sb.ToString();
        }


        
       

        public void SaveProjectBudget(List<ProjectBudgetDetails> _data,String DivisionID,String ProjectName, String is_template)
        {
            String _sqlString = "";
            foreach (var item in _data)
            {
                int _active = 0;
                if (item.Active)
                {
                    _active = 1;
                }
                else
                {
                    _active = 0;
                }

                StringBuilder sb = new StringBuilder(143);
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_project_proposals");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  division_id,");
                sb.AppendLine(@"  expenditure_id,");
                sb.AppendLine(@"  project_name,");
                sb.AppendLine(@"  amount,");
                sb.AppendLine(@"  is_template,");
                sb.AppendLine(@"  is_active,");
                sb.AppendLine(@"  year,status");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  " +  DivisionID + ",");
                sb.AppendLine(@"  " + item.ExpenditureID + ",");
                sb.AppendLine(@"  '" + ProjectName + "',");
                sb.AppendLine(@"  " +  Convert.ToDouble(item.Amount) + ",");
                sb.AppendLine(@"  " + is_template + ",");
                sb.AppendLine(@"  " + _active + ",");
                sb.AppendLine(@"  " + item.Year + ",'CREATED'");
                sb.AppendLine(@");");
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_project_logs");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  div_id,project_name,");
                sb.AppendLine(@"  project_action_log");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  '" + DivisionID + "','" + ProjectName + "',");
                sb.AppendLine(@"  'CREATED'");
                sb.AppendLine(@");");


                _sqlString += sb.ToString();
            }
        


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

            
        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
             ReturnCode = c_ops.ReturnCode;
            switch (Process)
            {             
                case "SaveProjectBudget":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
            //throw new NotImplementedException();
        }

       
    }
    public class BudgetExpenditures
    {
        public Int32 ID { get; set; }
        public String MOOE_ID { get; set; }
        public String Description { get; set; }

    }

    public class ProjectBudgetDetails 
    {
        public Boolean Active { get; set; }
        public String Description { get; set; }
        public String Amount { get; set; }
        public String ExpenditureID { get; set; }
        public String Year { get; set; }
       
   
    }
    public class DivisionLimit
    {
        public Double Amount { get; set; }
    }
}
