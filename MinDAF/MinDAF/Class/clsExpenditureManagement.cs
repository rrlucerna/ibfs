﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsExpenditureManagement
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchExpenditureLists()
        {
            StringBuilder sb = new StringBuilder(90);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  is_active");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures;");


            return sb.ToString();
        }
        public String FetchMainExpenditureLists()
        {
            //var sb = new System.Text.StringBuilder(219);
            //sb.AppendLine(@"SELECT ");
            //sb.AppendLine(@"  mooe.id,");
            //sb.AppendLine(@"  mooe.code,");
            //sb.AppendLine(@"  mooe.name,");
            //sb.AppendLine(@"  mooe_sub.name as expenditure,");
            //sb.AppendLine(@"  mooe_sub.uacs_code");
            //sb.AppendLine(@"FROM  mnda_mooe_expenditures mooe");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe_sub on mooe_sub.mooe_id = mooe.code ;");

            var sb = new System.Text.StringBuilder(254);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mooe.id,");
            sb.AppendLine(@"  mooe.code,");
            sb.AppendLine(@"  mooe.name,");
            sb.AppendLine(@" ISNULL( mooe_sub.name,'') as expenditure,");
            sb.AppendLine(@"   ISNULL(mooe_sub.uacs_code,'') as uacs_code");
            sb.AppendLine(@"FROM  mnda_mooe_expenditures mooe");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe_sub on mooe_sub.mooe_id = mooe.code ;");


            return sb.ToString();
        }
        public void SaveMainExpenditure(String _code, String _name)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(99);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _code + "',");
            sb.AppendLine(@" '" + _name + "'");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveExpenditure(String _code, String _name,String _uacs)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(99);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+_code +"',");
            sb.AppendLine(@"  '" + _uacs + "',");
            sb.AppendLine(@" '"+ _name +"'");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void RemoveExpenditure(String _uacs_code)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(56);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures ");
            sb.AppendLine(@"WHERE uacs_code ='"+ _uacs_code +"';");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        } 
        public void RemoveMainExpenditure(String code)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(56);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures ");
            sb.AppendLine(@"WHERE code ='"+ code +"';");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateMainExpenditure(String _id, String _code, String _name,String _oldVal)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(97);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  code = '" + _code + "',");
            sb.AppendLine(@"  name = '" + _name + "'");
            sb.AppendLine(@"WHERE id =" + _id + " ;");

            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  mooe_id = '" + _code + "'");
            sb.AppendLine(@"WHERE mooe_id ='" + _oldVal + "';");


            _sqlString += sb.ToString();
                        
            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateExpenditure(String _id ,String _code, String _name,string _uacs)
        {
            String _sqlString = "";

           StringBuilder sb = new StringBuilder(97);
                        sb.AppendLine(@"UPDATE ");
                        sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures  ");
                        sb.AppendLine(@"SET ");
                        sb.AppendLine(@"  ");
                        sb.AppendLine(@"  mooe_id = '"+ _code +"',");
                        sb.AppendLine(@"  name = '"+ _name +"',");
                        sb.AppendLine(@"  uacs_code = '" + _uacs + "'");
                        sb.AppendLine(@"WHERE id ="+ _id +" ;");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (Process)
            {
                case "SaveData":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveMainExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveDataExpenseItem":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveMainExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }
    public class MainExpenditures
    {
      public String  id  {get;set;}
       public String code {get;set;}
       public String name { get; set; }
       public String clone { get; set; }
       public String expenditure { get; set; }
       public String uacs_code { get; set; }
       public List<SubLibrary> ExpendituresItems { get; set; }
    }
    public class SubLibrary 
    {
        public String Expenditure { get; set; }
        public String UACS { get; set; }
    }
    public class SubExpenditures 
    {
         public String   id {get;set;}
         public String    mooe_id{get;set;}
         public String uacs_code { get; set; }
         public String    name{get;set;}
         public String is_active { get; set; }
         
      
    }
}
