﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class usr_main_dashboard : UserControl
    {
        public String DivisionId { get; set; }
        public String UserId { get; set; }
        public String Username { get; set; }
        private String SelectedExpenditure { get; set; }
        private String _MonthSelected { get; set; }
        public String IsAdmin { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsMainData c_main_data = new clsMainData();
        private frmListExpenditures f_expenditures = new frmListExpenditures();

        private List<MainData> _ListMainData = new List<MainData>();
        private List<MainTotals> _ListMainTotals = new List<MainTotals>();
        private List<Main_OverAllYear> _ListOverAllYear = new List<Main_OverAllYear>();
        private List<MainDashboardActivityData> ListActivityStatus = new List<MainDashboardActivityData>();


        private String _updateProgram = "";
        private String _updateProject = "";
        private String _updateProjectOutput = "";
        private String _updateOutputActivity = "";
       private frmEmployeeList f_employee = new frmEmployeeList();
        public usr_main_dashboard()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            f_expenditures.SelectedExpenditure+=f_expenditures_SelectedExpenditure;
            c_main_data.SQLOperation += c_main_data_SQLOperation;

            f_expenditures = new frmListExpenditures(this.DivisionId);
        }

        void c_main_data_SQLOperation(object sender, EventArgs e)
        {
            switch (c_main_data.Process)
            {
                case "AddNewRow":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowYear":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowProgram":
                    UpdateTableProgramID(_updateProgram);
                    break;
                case "UpdateMainProgramID":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowProject":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateProjectOutput":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateOutputActivity":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateAssignedEmployee":
                    RefreshData(this.DivisionId);
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_main_data.Process)
            {
                case "FetchAll":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new MainData
                                     {
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         activity = Convert.ToString(info.Element("activity").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         fiscal_year = Convert.ToString(info.Element("fiscal_year").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         output = Convert.ToString(info.Element("output").Value),
                                         program_id = Convert.ToInt32(info.Element("program_id").Value),
                                         program_name = Convert.ToString(info.Element("program_name").Value),
                                         project_name = Convert.ToString(info.Element("project_name").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         project_id = Convert.ToString(info.Element("project_id").Value),
                                         output_id = Convert.ToString(info.Element("output_id").Value)
                                     };

                    _ListMainData.Clear();
                   
                    foreach (var item in _dataLists)
                    {
                        MainData _varDetails = new MainData();

                        _varDetails.activity_id = item.activity_id;
                         _varDetails.Apr = item.Apr;
                         _varDetails.Aug = item.Aug;
                         _varDetails.Dec = item.Dec;
                         _varDetails.activity = item.activity;
                         _varDetails.Feb = item.Feb;
                         _varDetails.fiscal_year = item.fiscal_year;
                         _varDetails.Jan = item.Jan;
                         _varDetails.Jul = item.Jul;
                         _varDetails.Jun = item.Jun;
                         _varDetails.Mar = item.Mar;
                         _varDetails.May = item.May;
                         _varDetails.Nov = item.Nov;
                         _varDetails.Oct = item.Oct;
                         _varDetails.output = item.output;
                         _varDetails.program_id = item.program_id;
                         _varDetails.program_name = item.program_name;
                         _varDetails.project_name = item.project_name;
                         _varDetails.Sep = item.Sep;
                         _varDetails.User_Fullname = item.User_Fullname;
                         _varDetails.project_id = item.project_id;
                         _varDetails.output_id = item.output_id;
                        _ListMainData.Add(_varDetails);
                    }
                    if (IsAdmin=="0")
                    {
                        List<MainData> _EmptyProgram = _ListMainData.Where(item => item.User_Fullname == "").ToList();
                        List<MainData> _Temp = _ListMainData.Where(item => item.User_Fullname == Username.Replace("Welcome ", "").Trim()).ToList();
                        _ListMainData = _Temp;
                        foreach (var item in _EmptyProgram)
                        {
                            _ListMainData.Add(item);
                        }
                    
                    }
                    
                   
                  
                   
                  
                    this.Cursor = Cursors.Arrow;

                    GetTotals();
                    break;
               case "FetchTotals":
                    XDocument oDocKeyFetchTotals = XDocument.Parse(_results);
                    var _dataListsFetchTotals = from info in oDocKeyFetchTotals.Descendants("Table")
                                     select new MainTotals
                                     {
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         month = Convert.ToString(info.Element("month").Value),
                                         total = Convert.ToString(info.Element("total").Value)
                                        
                                     };

                    _ListMainTotals.Clear();


                    foreach (var item in _dataListsFetchTotals)
                    {
                        MainTotals _varDetails = new MainTotals();

                        _varDetails.activity_id = item.activity_id;
                        _varDetails.month = item.month;
                        _varDetails.total = item.total;

                         _ListMainTotals.Add(_varDetails);
                    }

                    DistributeData();
                    this.Cursor = Cursors.Arrow;
                                       
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _ListMainData;

                    grdData.Columns.DataColumns["activity"].AllowToolTips = AllowToolTips.Always;
                    grdData.Columns.DataColumns["project_name"].AllowToolTips = AllowToolTips.Overflow;
                    grdData.Columns.DataColumns["program_name"].AllowToolTips = AllowToolTips.Overflow;
                    grdData.Columns.DataColumns["output"].AllowToolTips = AllowToolTips.Overflow;

                    Column col_project_name = grdData.Columns.DataColumns["project_name"];
                    Column col_program_name = grdData.Columns.DataColumns["program_name"];
                    Column col_User_Fullname = grdData.Columns.DataColumns["User_Fullname"];
                    Column col_fiscal_year = grdData.Columns.DataColumns["fiscal_year"];
                    Column col_activity = grdData.Columns.DataColumns["activity"];
                    Column col_output = grdData.Columns.DataColumns["output"];

                    //col_fiscal_year.Width = new ColumnWidth(70, false);
                    //col_program_name.Width = new ColumnWidth(250, false);
                    //col_User_Fullname.Width = new ColumnWidth(100, false);
                    //col_activity.Width = new ColumnWidth(120,false);
                    //col_output.Width = new ColumnWidth(120,false);
                    //col_project_name.Width = new ColumnWidth(150, false);
                
                    col_fiscal_year.Width = new ColumnWidth(60, false);
                    col_program_name.Width = new ColumnWidth(200, false);
                    col_User_Fullname.Width = new ColumnWidth(75, false);
                    col_activity.Width = new ColumnWidth(200, false);
                    col_output.Width = new ColumnWidth(100, false);
                    col_project_name.Width = new ColumnWidth(200, false);

                    Column col_jan = grdData.Columns.DataColumns["Jan"];
                    Column col_feb = grdData.Columns.DataColumns["Feb"];
                    Column col_mar = grdData.Columns.DataColumns["Mar"];
                    Column col_apr = grdData.Columns.DataColumns["Apr"];
                    Column col_may = grdData.Columns.DataColumns["May"];
                    Column col_jun = grdData.Columns.DataColumns["Jun"];
                    Column col_jul = grdData.Columns.DataColumns["Jul"];
                    Column col_aug = grdData.Columns.DataColumns["Aug"];
                    Column col_sep = grdData.Columns.DataColumns["Sep"];
                    Column col_oct = grdData.Columns.DataColumns["Oct"];
                    Column col_nov = grdData.Columns.DataColumns["Nov"];
                    Column col_dec = grdData.Columns.DataColumns["Dec"];

                    col_jan.Width = new ColumnWidth(100,false);
                    col_feb.Width = new ColumnWidth(100, false);
                    col_mar.Width = new ColumnWidth(100, false);
                    col_apr.Width = new ColumnWidth(100, false);
                    col_may.Width = new ColumnWidth(100, false);
                    col_jun.Width = new ColumnWidth(100, false);
                    col_jul.Width = new ColumnWidth(100, false);
                    col_aug.Width = new ColumnWidth(100, false);
                    col_sep.Width = new ColumnWidth(100, false);
                    col_oct.Width = new ColumnWidth(100, false);
                    col_nov.Width = new ColumnWidth(100, false);
                    col_dec.Width = new ColumnWidth(100, false);

                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_fiscal_year);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_program_name);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_project_name);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_output);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_activity);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_User_Fullname);

                    switch (IsAdmin)
	                {
                        case "1":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                            break;

                        case "2":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                            break;
                        case "0":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Collapsed;
                            break;
	                }
                    

                    grdData.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["program_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["project_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["output_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
                    FetchYearPrograms();
                    break;
               case "FetchYear":
                    XDocument oDocKeyFetchYear = XDocument.Parse(_results);
                    var _dataListsFetchYear = from info in oDocKeyFetchYear.Descendants("Table")
                                                select new Main_OverAllYear
                                                {
                                                    Fiscal_Year = Convert.ToString(info.Element("Fiscal_Year").Value)

                                                };

                    _ListOverAllYear.Clear();


                    foreach (var item in _dataListsFetchYear)
                    {
                        Main_OverAllYear _varDetails = new Main_OverAllYear();

                        _varDetails.Fiscal_Year = item.Fiscal_Year;

                        _ListOverAllYear.Add(_varDetails);
                    }
                   
                    this.Cursor = Cursors.Arrow;

                    break;
               case "FetchProgramID":
                    XDocument oDocKeyFetchProgramID = XDocument.Parse(_results);
                    var _dataListsFetchProgramID = from info in oDocKeyFetchProgramID.Descendants("Table")
                                                select new FetchProgramId
                                                {
                                                    id = Convert.ToString(info.Element("id").Value)

                                                };

                    String _program_id = "";
                    String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();

                    foreach (var item in _dataListsFetchProgramID)
                    {
                        FetchProgramId _varDetails = new FetchProgramId();

                        _program_id = item.id;
                        break;

                    }
                    UpdateMainProgramID(_program_id, _id);
                    this.Cursor = Cursors.Arrow;

                    break;  
                case "FetchProjectID":
                    XDocument oDocKeyFetchProjectID = XDocument.Parse(_results);
                    var _dataListsFetchProjectID = from info in oDocKeyFetchProjectID.Descendants("Table")
                                                select new FetchProjectId
                                                {
                                                    id = Convert.ToString(info.Element("id").Value)

                                                };

                    String _project_id = "";
                     _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();

                     foreach (var item in _dataListsFetchProjectID)
                    {
                        FetchProgramId _varDetails = new FetchProgramId();

                        _project_id = item.id;
                        break;

                    }
                     UpdateMainProjectID(_project_id, _id);
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchActivityStatus":
                    XDocument oDocKeyResultsFetchActivityStatus = XDocument.Parse(_results);
                    var _dataListsFetchActivityStatus = from info in oDocKeyResultsFetchActivityStatus.Descendants("Table")
                                                        select new MainDashboardActivityData
                                                        {
                                                            accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                            activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                            destination = Convert.ToString(info.Element("destination").Value),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            no_days = Convert.ToString(info.Element("no_days").Value),
                                                            no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                            quantity = Convert.ToString(info.Element("quantity").Value),
                                                            remarks = Convert.ToString(info.Element("remarks").Value),
                                                            total = Convert.ToString(info.Element("total").Value),
                                                            type_service = Convert.ToString(info.Element("type_service").Value)

                                                        };


                    ListActivityStatus.Clear();

                    foreach (var item in _dataListsFetchActivityStatus)
                    {
                        MainDashboardActivityData _varProf = new MainDashboardActivityData();

                        _varProf.accountable_id = item.accountable_id;
                        _varProf.activity_id = item.activity_id;
                        _varProf.destination= item.destination;
                        _varProf.name = item.name;
                        _varProf.no_days = item.no_days;
                        _varProf.no_staff = item.no_staff;
                        _varProf.quantity = item.quantity;
                        _varProf.remarks = item.remarks;
                        _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                        _varProf.type_service = item.type_service;
                        ListActivityStatus.Add(_varProf);

                    }
                    grdStatus.ItemsSource = null;
                    grdStatus.ItemsSource = ListActivityStatus;

                    grdData.Columns["activity_id"].Visibility  = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                    break;
              
            }
        }
        public void UpdateMainProgramID(string _program_id,string _id) 
        {
            c_main_data.Process = "UpdateMainProgramID";
            c_main_data.UpdateProgramID(_program_id,_id);
        }
        public void UpdateMainProjectID(string _program_id, string _id)
        {
            c_main_data.Process = "UpdateMainProjectID";
            c_main_data.UpdateProgramID(_program_id, _id);
        }
        public void AddNewData() 
        {
            c_main_data.Process = "AddNewRow";
            c_main_data.AddNewRow(this.DivisionId);
        }
        private void FetchYearPrograms() 
        {
            //c_main_data.Process = "FetchYear";
            //svc_mindaf.ExecuteSQLAsync(c_main_data.FetchOverAllYear());
        }
        private void DistributeData() 
        {
               Decimal     _Jan =0;
                  Decimal  _Feb =0;
                  Decimal  _Mar =0;
                  Decimal  _Apr =0;
                  Decimal  _May =0;
                  Decimal  _Jun=0;
                  Decimal  _Jul =0;
                  Decimal  _Aug =0;
                  Decimal  _Sep =0;
                  Decimal  _Oct =0;
                  Decimal  _Nov =0;
                  Decimal  _Dec = 0;
            foreach (var item in _ListMainData)
            {
                if (item.activity_id =="189")
                {

                }
                 List<MainTotals> y = _ListMainTotals.Where(items => items.activity_id == item.activity_id).ToList();
                    foreach (var item_details in y)
                    {
                        switch (item_details.month)
                        {
                            case "Jan":
                                item.Jan = (Convert.ToDecimal(item.Jan) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jan += Convert.ToDecimal(item_details.total);
                                break;
                            case "Feb":
                                item.Feb =(Convert.ToDecimal(item.Feb) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Feb += Convert.ToDecimal(item_details.total);
                                break;
                            case "Mar":
                                item.Mar = (Convert.ToDecimal(item.Mar) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Mar += Convert.ToDecimal(item_details.total);
                                break;
                            case "Apr":
                                item.Apr = (Convert.ToDecimal(item.Apr) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Apr += Convert.ToDecimal(item_details.total);
                                break;
                            case "May":
                                item.May = (Convert.ToDecimal(item.May) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _May += Convert.ToDecimal(item_details.total);
                                break;
                            case "Jun":
                                item.Jun = (Convert.ToDecimal(item.Jun) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jun += Convert.ToDecimal(item_details.total);
                                break;
                            case "Jul":
                                item.Jul = (Convert.ToDecimal(item.Jul) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jul += Convert.ToDecimal(item_details.total);
                                break;
                            case "Aug":
                                item.Aug = (Convert.ToDecimal(item.Aug) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Aug += Convert.ToDecimal(item_details.total);
                                break;
                            case "Sep":
                                item.Sep = (Convert.ToDecimal(item.Sep) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Sep += Convert.ToDecimal(item_details.total);
                                break;
                            case "Oct":
                                item.Oct = (Convert.ToDecimal(item.Oct) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Oct += Convert.ToDecimal(item_details.total);
                                break;
                            case "Nov":
                                item.Nov = (Convert.ToDecimal(item.Nov) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Nov += Convert.ToDecimal(item_details.total);
                                break;
                            case "Dec":
                                item.Dec = (Convert.ToDecimal(item.Dec) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Dec += Convert.ToDecimal(item_details.total);
                                break;
                            default:
                                break;
                        }
                    }
            }
            AddGrandTotals(_Jan, _Feb, _Mar, _Apr, _May, _Jun, _Jul, _Aug, _Sep, _Oct, _Nov, _Dec);
        }

        private void AddGrandTotals(
            Decimal _jan,Decimal _feb,Decimal _mar,
            Decimal _apr,Decimal _may,Decimal _jun,
            Decimal _jul,Decimal _aug,Decimal _sep,
            Decimal _oct,Decimal _nov,Decimal _dec
            ) 
        {
            List<MainData> _OverAll = new List<MainData>();
            MainData _varDetails;

     

             _varDetails = new MainData();

            _varDetails.output = "";
            _varDetails.program_id = 0;
            _varDetails.program_name = "";
            _varDetails.project_name = "";
            _varDetails.User_Fullname = "Grand Total :";
            _varDetails.fiscal_year = "";
            _varDetails.activity = "";
            _varDetails.activity_id = "";
            _varDetails.Jan = Convert.ToDouble(_jan).ToString("#,##0.00");
            _varDetails.Feb = Convert.ToDouble(_feb).ToString("#,##0.00");
            _varDetails.Mar = Convert.ToDouble(_mar).ToString("#,##0.00");
            _varDetails.Apr = Convert.ToDouble(_apr).ToString("#,##0.00");
            _varDetails.May = Convert.ToDouble(_may).ToString("#,##0.00");
            _varDetails.Jun = Convert.ToDouble(_jun).ToString("#,##0.00");
            _varDetails.Jul = Convert.ToDouble(_jul).ToString("#,##0.00");
            _varDetails.Aug = Convert.ToDouble(_aug).ToString("#,##0.00");
            _varDetails.Sep = Convert.ToDouble(_sep).ToString("#,##0.00");
            _varDetails.Oct = Convert.ToDouble(_oct).ToString("#,##0.00");
            _varDetails.Nov = Convert.ToDouble(_nov).ToString("#,##0.00");
            _varDetails.Dec = Convert.ToDouble(_dec).ToString("#,##0.00");

            _ListMainData.Add(_varDetails);

            _OverAll.Add(_varDetails);
            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = _OverAll;
           grdTotals.HeaderVisibility = System.Windows.Visibility.Collapsed;

           switch (IsAdmin)
           {
               case "1":
                   grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                   break;

               case "2":
                   grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                   break;
               case "0":
                   grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Collapsed;
                   break;
           }

            Column col_project_name = grdTotals.Columns.DataColumns["project_name"];
            Column col_program_name = grdTotals.Columns.DataColumns["program_name"];
            Column col_User_Fullname = grdTotals.Columns.DataColumns["User_Fullname"];
            Column col_fiscal_year = grdTotals.Columns.DataColumns["fiscal_year"];
            Column col_activity = grdTotals.Columns.DataColumns["activity"];
            Column col_output = grdTotals.Columns.DataColumns["output"];

            col_fiscal_year.Width = new ColumnWidth(60, false);
            col_program_name.Width = new ColumnWidth(200, false);
            col_User_Fullname.Width = new ColumnWidth(75, false);
            col_activity.Width = new ColumnWidth(150, false);
            col_output.Width = new ColumnWidth(100, false);
            col_project_name.Width = new ColumnWidth(150, false);

            Column col_jan = grdTotals.Columns.DataColumns["Jan"];
            Column col_feb = grdTotals.Columns.DataColumns["Feb"];
            Column col_mar = grdTotals.Columns.DataColumns["Mar"];
            Column col_apr = grdTotals.Columns.DataColumns["Apr"];
            Column col_may = grdTotals.Columns.DataColumns["May"];
            Column col_jun = grdTotals.Columns.DataColumns["Jun"];
            Column col_jul = grdTotals.Columns.DataColumns["Jul"];
            Column col_aug = grdTotals.Columns.DataColumns["Aug"];
            Column col_sep = grdTotals.Columns.DataColumns["Sep"];
            Column col_oct = grdTotals.Columns.DataColumns["Oct"];
            Column col_nov = grdTotals.Columns.DataColumns["Nov"];
            Column col_dec = grdTotals.Columns.DataColumns["Dec"];

            col_jan.Width = new ColumnWidth(100, false);
            col_feb.Width = new ColumnWidth(100, false);
            col_mar.Width = new ColumnWidth(100, false);
            col_apr.Width = new ColumnWidth(100, false);
            col_may.Width = new ColumnWidth(100, false);
            col_jun.Width = new ColumnWidth(100, false);
            col_jul.Width = new ColumnWidth(100, false);
            col_aug.Width = new ColumnWidth(100, false);
            col_sep.Width = new ColumnWidth(100, false);
            col_oct.Width = new ColumnWidth(100, false);
            col_nov.Width = new ColumnWidth(100, false);
            col_dec.Width = new ColumnWidth(100, false);

            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_fiscal_year);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_program_name);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_project_name);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_output);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_activity);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_User_Fullname);

            grdTotals.Columns["project_name"].HeaderText = "";

            grdTotals.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["program_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["project_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["output_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
        }

      
        private void RefreshData(String Division_ID) 
        {
            c_main_data.Process = "FetchAll";
            if (this.IsAdmin=="1")
            {
                svc_mindaf.ExecuteSQLAsync(c_main_data.FetchData(DivisionId));
               
            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_main_data.FetchDataPersonal(DivisionId,this.UserId,Username));
            }
           
    
        }
        private void GetTotals()
        {
            c_main_data.Process = "FetchTotals";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchTotals());

        }
        private void ctrl_main_dashboard_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshData(this.DivisionId);
        }

      

        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
            String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
            if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString() == "Grand Total :")
            {
                return;
            }
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
               e.Cell.Column.HeaderText.ToString() == "Feb" ||
               e.Cell.Column.HeaderText.ToString() == "Mar" ||
               e.Cell.Column.HeaderText.ToString() == "Apr" ||
               e.Cell.Column.HeaderText.ToString() == "May" ||
               e.Cell.Column.HeaderText.ToString() == "Jun" ||
               e.Cell.Column.HeaderText.ToString() == "Jul" ||
               e.Cell.Column.HeaderText.ToString() == "Aug" ||
               e.Cell.Column.HeaderText.ToString() == "Sep" ||
               e.Cell.Column.HeaderText.ToString() == "Oct" ||
               e.Cell.Column.HeaderText.ToString() == "Nov" ||
               e.Cell.Column.HeaderText.ToString() == "Dec"
           )
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString()=="Grand Total :")
                {
                    return;
                }
                if (ActivityId != "0")
                {
                    f_expenditures = new frmListExpenditures();
                    f_expenditures._Month = e.Cell.Column.HeaderText.ToString();
                    f_expenditures.DivisionId = this.DivisionId;
                    f_expenditures.SelectedExpenditure+=f_expenditures_SelectedExpenditure;
                    f_expenditures.Show();
                }
                else
                {
                        MessageBox.Show("Select a program that has Activities and Assigned Person");

                }
                   

           }
            else if (e.Cell.Column.HeaderText.ToString() == "User_Fullname")
                {
                    if (IsAdmin == "1" || IsAdmin == "2")
                    {
                        f_employee.DivisionId = this.DivisionId;
                        f_employee.AssignedUpdate += f_employee_AssignedUpdate;
                        f_employee.Show();
                    }

                    
                }
            else if (e.Cell.Column.HeaderText.ToString() == "output")
                {
                    if (IsAdmin == "1" || IsAdmin =="2")
                    {
                        String _Activity = grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString().Replace("'", "`");

                        UpdateProjectOutput(_Activity, grdData.Rows[e.Cell.Row.Index].Cells["project_id"].Value.ToString(), "");

                    }
                  
                }
            else if (e.Cell.Column.HeaderText.ToString() == "activity")
            {
                String _Activity = grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString().Replace("'","`");
                AddOutputActivity(grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString(),"");

            }     
      
        

        }


        
        void f_employee_AssignedUpdate(object sender, EventArgs e)
        {
            String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
            String OutputId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString();
            UpdateAssignedEmployee(f_employee.AssignedId, ActivityId,OutputId);
        }

        void f_expenditures_SelectedExpenditure(object sender, EventArgs e)
        {
            switch (f_expenditures.SelectedCode)
            {
                case "Local Travel":
                    frmBudgetCreation frm_local_travel = new frmBudgetCreation();
                    String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    String Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    string _Month = f_expenditures._Month;
                    string _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_local_travel.ActivityID = ActivityId;
                    frm_local_travel.AccountableID = Assigned;
                    frm_local_travel._Month = _Month;
                    frm_local_travel._Year = _Year;
                    frm_local_travel.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_local_travel.ReloadData += frm_local_travel_ReloadData;
                    frm_local_travel.TravelType = f_expenditures.SelectedCode;
                    frm_local_travel.FundSource = f_expenditures.FundSourceId;
                    frm_local_travel.DivisionId = this.DivisionId;
                    frm_local_travel.Show();
                    break;
                case "International Travel":
                    frmBudgetCreation frm_foreign_travel = new frmBudgetCreation();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_foreign_travel.ActivityID = ActivityId;
                     frm_foreign_travel.AccountableID = Assigned;
                     frm_foreign_travel._Month = _Month;
                     frm_foreign_travel._Year = _Year;
                     frm_foreign_travel.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_foreign_travel.ReloadData += frm_foreign_travel_ReloadData;
                     frm_foreign_travel.TravelType = f_expenditures.SelectedCode;
                     frm_foreign_travel.FundSource = f_expenditures.FundSourceId;
                     frm_foreign_travel.DivisionId = this.DivisionId;
                     frm_foreign_travel.Show();
                     break;
                case "Gasoline , Oil and Lubricants Expenses":
                     frmBudgetGasoline frm_gasoline = new frmBudgetGasoline();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_gasoline.ActivityID = ActivityId;
                     frm_gasoline.AccountableID = Assigned;
                     frm_gasoline._Month = _Month;
                     frm_gasoline._Year = _Year;
                     frm_gasoline.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_gasoline.ReloadData += frm_gasoline_ReloadData;
                     frm_gasoline.TravelType = f_expenditures.SelectedCode;
                     frm_gasoline.FundSource = f_expenditures.FundSourceId;
                     frm_gasoline.DivisionId = this.DivisionId;
                     frm_gasoline.Show();
                    break;
                case "Supplies":
                    frmBudgetSupplies frm_budget_supplies = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_supplies.ActivityID = ActivityId;
                    frm_budget_supplies.AccountableID = Assigned;
                    frm_budget_supplies._Month = _Month;
                    frm_budget_supplies._Year = _Year;
                    frm_budget_supplies.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_supplies.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_supplies.FundSource = f_expenditures.FundSourceId;
                    frm_budget_supplies.DivisionId = this.DivisionId;
                    frm_budget_supplies.Show();
                    break;
                case "Representation":
                    frmBudgetRepresentation frm_budget_representation = new frmBudgetRepresentation();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_representation.ActivityID = ActivityId;
                    frm_budget_representation.AccountableID = Assigned;
                    frm_budget_representation._Month = _Month;
                    frm_budget_representation._Year = _Year;
                    frm_budget_representation.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_representation.ReloadData += frm_budget_representation_ReloadData;
                    frm_budget_representation.FundSource = f_expenditures.FundSourceId;
                    frm_budget_representation.DivisionId = this.DivisionId;
                    frm_budget_representation.Show();
                    break;
                case "Representation Expense":
                    frmBudgetRepresentation frm_budget_representation_expense = new frmBudgetRepresentation();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_representation_expense.ActivityID = ActivityId;
                    frm_budget_representation_expense.AccountableID = Assigned;
                    frm_budget_representation_expense._Month = _Month;
                    frm_budget_representation_expense._Year = _Year;
                    frm_budget_representation_expense.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_representation_expense.ReloadData += frm_budget_representation_ReloadData;
                    frm_budget_representation_expense.FundSource = f_expenditures.FundSourceId;
                    frm_budget_representation_expense.DivisionId = this.DivisionId;
                    frm_budget_representation_expense.Show();
                    break;
                case "Professional Fee":
                    frmBudgetProfessionalFees frm_prof = new frmBudgetProfessionalFees();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_prof.ActivityID = ActivityId;
                     frm_prof.AccountableID = Assigned;
                     frm_prof._Month = _Month;
                     frm_prof._Year = _Year;
                     frm_prof.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_prof.FundSource = f_expenditures.FundSourceId;
                     frm_prof.DivisionId = this.DivisionId;
                     frm_prof.ReloadData += frm_prof_ReloadData;

                     frm_prof.Show();
                    break;
                case "Printing and Binding":
                    frmBudgetPrintingBinding frm_printbind = new frmBudgetPrintingBinding();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_printbind.ActivityID = ActivityId;
                    frm_printbind.AccountableID = Assigned;
                    frm_printbind._Month = _Month;
                    frm_printbind._Year = _Year;
                    frm_printbind.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_printbind.FundSource = f_expenditures.FundSourceId;
                    frm_printbind.DivisionId = this.DivisionId;
                    frm_printbind.ReloadData += frm_printbind_ReloadData;

                    frm_printbind.Show();
                    break;
                case "Dues":
                    frmBudgetDues frm_due = new frmBudgetDues();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_due.ActivityID = ActivityId;
                    frm_due.AccountableID = Assigned;
                    frm_due._Month = _Month;
                    frm_due._Year = _Year;
                    frm_due.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_due.FundSource = f_expenditures.FundSourceId;
                    frm_due.ReloadData += frm_due_ReloadData;
                    frm_due.DivisionId = this.DivisionId;
                    frm_due.Show();
                    break;
                case "Subscription":
                    frmBudgetSubscription frm_subs = new frmBudgetSubscription();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_subs.ActivityID = ActivityId;
                    frm_subs.AccountableID = Assigned;
                    frm_subs._Month = _Month;
                    frm_subs._Year = _Year;
                    frm_subs.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_subs.DivisionId = this.DivisionId;
                    frm_subs.FundSource = f_expenditures.FundSourceId;
                    frm_subs.ReloadData += frm_subs_ReloadData;

                    frm_subs.Show();
                    break;
                case "Advertising":
                    frmBudgetAdvertising frm_ads = new frmBudgetAdvertising();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_ads.ActivityID = ActivityId;
                    frm_ads.AccountableID = Assigned;
                    frm_ads._Month = _Month;
                    frm_ads._Year = _Year;
                    frm_ads.DivisionId = this.DivisionId;
                    frm_ads.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_ads.FundSource = f_expenditures.FundSourceId;
                    frm_ads.ReloadData += frm_ads_ReloadData;

                    frm_ads.Show();
                    break;
                case "Training Expenses":
                    frmBudgetTraining frm_bud = new frmBudgetTraining();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_bud.ActivityID = ActivityId;
                    frm_bud.AccountableID = Assigned;
                    frm_bud._Month = _Month;
                    frm_bud._Year = _Year;
                    frm_bud.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_bud.FundSource = f_expenditures.FundSourceId;
                    frm_bud.DivisionId = this.DivisionId;
                    frm_bud.ReloadData += frm_ads_ReloadData;

                    frm_bud.Show();
                    break;
                case "Rental Fee":
                    frmBudgetRental frm_rent = new frmBudgetRental();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_rent.ActivityID = ActivityId;
                    frm_rent.AccountableID = Assigned;
                    frm_rent._Month = _Month;
                    frm_rent._Year = _Year;
                    frm_rent.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_rent.FundSource = f_expenditures.FundSourceId;
                    frm_rent.DivisionId = this.DivisionId;
                    frm_rent.ReloadData += frm_rent_ReloadData;

                    frm_rent.Show();
                    break;
                case "Labor and Wages":
                    frmBudgetLaborAndWages frm_law = new frmBudgetLaborAndWages();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_law.ActivityID = ActivityId;
                    frm_law.AccountableID = Assigned;
                    frm_law._Month = _Month;
                    frm_law._Year = _Year;
                    frm_law.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_law.DivisionId = this.DivisionId;
                    frm_law.FundSource = f_expenditures.FundSourceId;
                    frm_law.ReloadData += frm_law_ReloadData;

                    frm_law.Show();
                    break;
                case "Other MOOE":
                    frmBudgetOtherMooe frm_other = new frmBudgetOtherMooe();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_other.ActivityID = ActivityId;
                    frm_other.AccountableID = Assigned;
                    frm_other._Month = _Month;
                    frm_other._Year = _Year;
                    frm_other.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_other.FundSource = f_expenditures.FundSourceId;
                    frm_other.DivisionId = this.DivisionId;
                    frm_other.ReloadData += frm_other_ReloadData;

                    frm_other.Show();
                    break;
                case "Other Maintenance &  Expenses":
                    frmBudgetOtherMooe frm_other_me = new frmBudgetOtherMooe();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_other_me.ActivityID = ActivityId;
                    frm_other_me.AccountableID = Assigned;
                    frm_other_me._Month = _Month;
                    frm_other_me._Year = _Year;
                    frm_other_me.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_other_me.FundSource = f_expenditures.FundSourceId;
                    frm_other_me.DivisionId = this.DivisionId;
                    frm_other_me.ReloadData+=frm_other_me_ReloadData;

                    frm_other_me.Show();
                    break;
                default:
                    if (f_expenditures.IsDynamic)
                    {
                        frmGenericExpenditureCharging f_dynamic = new frmGenericExpenditureCharging();
                        ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                        Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                        _Month = f_expenditures._Month;
                        _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                        f_dynamic.ActivityID = ActivityId;
                        f_dynamic.AccountableID = Assigned;
                        f_dynamic._Month = _Month;
                        f_dynamic._Year = _Year;
                        f_dynamic.MOOE_ID = f_expenditures.MOOE_ID;
                        f_dynamic.FundMOOEName = f_expenditures.SelectedName;
                        f_dynamic.FundSource = f_expenditures.FundSourceId;
                        f_dynamic.DivisionId = this.DivisionId;
                        f_dynamic.ReloadData += f_dynamic_ReloadData;

                        f_dynamic.Show();
                    }
                    break;
            }
         
        }

        void frm_other_me_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void f_dynamic_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_other_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_law_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_rent_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_ads_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_subs_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_due_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_printbind_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_prof_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_budget_representation_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_budget_supplies_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }
        private void UpdateYear(string _year,string _id)
        {
            c_main_data.Process = "UpdateRowYear";
            c_main_data.UpdateYear(_year,_id);
        }
        private void UpdateProgram(string _id, string program)
        {
            c_main_data.Process = "UpdateRowProgram";
            _updateProgram = program;
            c_main_data.UpdateProgram(_id, program);

        }
        private void UpdateProject(string _id, string project,string _year)
        {
            c_main_data.Process = "UpdateRowProject";
            _updateProject = project;
            c_main_data.UpdateProject(_id, project,_year,this.DivisionId);

        }
        private void UpdateProjectOutput(string _output_id, string _id, string project_output)
        {
            c_main_data.Process = "UpdateProjectOutput";
            _updateProjectOutput = project_output;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateProjectOutput(_output_id, _id, project_output,"");

            }
            else
            {
                c_main_data.UpdateProjectOutput(_output_id, _id, project_output,this.UserId);
            }
         

        } private void UpdateDeleteProjectOutput(string _output_id, string _id, string project_output)
        {
            c_main_data.Process = "UpdateProjectOutput";
            _updateProjectOutput = project_output;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateDeleteProjectOutput(_output_id, _id, project_output,"");
            }
            else
            {
                c_main_data.UpdateDeleteProjectOutput(_output_id, _id, project_output,UserId);
            }
       

        }
        private void UpdateOutputActivity(String _activity_id, string _ouput_id, string _activity)
        {
            c_main_data.Process = "UpdateOutputActivity";
            _updateOutputActivity = _activity;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateOutputActivity(_activity_id, _ouput_id, _activity,"-");
            }
            else
            {
                c_main_data.UpdateOutputActivity(_activity_id, _ouput_id, _activity,this.UserId);
            }
        

        }
        private void AddOutputActivity(string _ouput_id, string _activity)
        {
            c_main_data.Process = "UpdateOutputActivity";
            _updateOutputActivity = _activity;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateAddOutputActivity(_ouput_id, _activity,"-");

            }
            else
            {
                c_main_data.UpdateAddOutputActivity(_ouput_id, _activity,this.UserId);

            }
           
        }
        private void UpdateAssignedEmployee(string _user_id, string _activity_id,string _output_id)
        {
            c_main_data.Process = "UpdateAssignedEmployee";

            c_main_data.UpdateAssignedEmployee(_user_id, _activity_id, _output_id);

        }
        private void UpdateTableProgramID(string _program) 
        {
            c_main_data.Process = "FetchProgramID";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchProgramId(_program));
        }
       

        private void UpdateTableProjectID(string _project)
        {
            c_main_data.Process = "FetchProjectID";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchProjectId(_project));
        }
        void frm_gasoline_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_foreign_travel_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_local_travel_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }
        private void AddNewProgram() 
        {

        }
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {

        }

        private void grdData_ColumnSorting(object sender, SortingCancellableEventArgs e)
        {
            e.Cancel = true;
        }

       
        private Boolean isUpdated = false;
        private void grdData_CellExitedEditMode(object sender, CellExitedEditingEventArgs e)
        {
            String _UpData = e.Cell.Value.ToString();
            if (isUpdated)
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString() == "Grand Total:")
                {
                   
                    return;
                }
                if (e.Cell.Value.ToString()!="")
                {
                    if (_UpData.Contains("'"))
                    {
                        _UpData = e.Cell.Value.ToString().Replace("'", "`");
                    }
                    switch (e.Cell.Column.HeaderText)
                    {
                        case "fiscal_year":
                            UpdateYear(_UpData, grdData.Rows[e.Cell.Row.Index].Cells["program_id"].Value.ToString());
                            break;
                        case "program_name":
                            UpdateProgram(grdData.Rows[e.Cell.Row.Index].Cells["program_id"].Value.ToString(), _UpData);
                            break;
                        case "project_name":
                            UpdateProject(grdData.Rows[e.Cell.Row.Index].Cells["program_id"].Value.ToString(), _UpData, grdData.Rows[e.Cell.Row.Index].Cells["fiscal_year"].Value.ToString());
                            break;
                        case "output":
                            UpdateDeleteProjectOutput(grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["project_id"].Value.ToString(), _UpData);
                            break;
                        case "activity":

                            UpdateOutputActivity(grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString(), _UpData);
                            break;
                    }
                }
               
            }
           
           
        }

        private void grdData_CellEnteringEditMode(object sender, BeginEditingCellEventArgs e)
        {
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
              e.Cell.Column.HeaderText.ToString() == "Feb" ||
              e.Cell.Column.HeaderText.ToString() == "Mar" ||
              e.Cell.Column.HeaderText.ToString() == "Apr" ||
              e.Cell.Column.HeaderText.ToString() == "May" ||
              e.Cell.Column.HeaderText.ToString() == "Jun" ||
              e.Cell.Column.HeaderText.ToString() == "Jul" ||
              e.Cell.Column.HeaderText.ToString() == "Aug" ||
              e.Cell.Column.HeaderText.ToString() == "Sep" ||
              e.Cell.Column.HeaderText.ToString() == "Oct" ||
              e.Cell.Column.HeaderText.ToString() == "Nov" ||
              e.Cell.Column.HeaderText.ToString() == "Dec" ||
              e.Cell.Column.HeaderText.ToString() == "User_Fullname"
          )
            {
                e.Cancel = true;

            }
            else
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString()=="Grand Total :")
                {
                    e.Cancel = true;
                }
                if (e.Cell.Value=="")
                {
                    grdData.EditingSettings.AllowEditing = EditingType.Cell;
                  
                }
                else
                {
                  
                    e.Cancel = true;
                }
                
            }
           
        }

        private void grdData_TextInputUpdate(object sender, TextCompositionEventArgs e)
        {

        }

        private void grdData_CellEnteredEditMode(object sender, EditingCellEventArgs e)
        {
            if (e.Cell.Value.ToString() =="")
            {
                isUpdated = true;
            }
            else
            {
                isUpdated = false;

            }
        }
        private void FetchActivityStatus(String _year, String _month,String _user,String act_id)
        {
            c_main_data.Process = "FetchActivityStatus";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchActivityStatus(_year, _month,_user,act_id));
        }
        private void grdData_CellClicked(object sender, CellClickedEventArgs e)
        {

            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
               e.Cell.Column.HeaderText.ToString() == "Feb" ||
               e.Cell.Column.HeaderText.ToString() == "Mar" ||
               e.Cell.Column.HeaderText.ToString() == "Apr" ||
               e.Cell.Column.HeaderText.ToString() == "May" ||
               e.Cell.Column.HeaderText.ToString() == "Jun" ||
               e.Cell.Column.HeaderText.ToString() == "Jul" ||
               e.Cell.Column.HeaderText.ToString() == "Aug" ||
               e.Cell.Column.HeaderText.ToString() == "Sep" ||
               e.Cell.Column.HeaderText.ToString() == "Oct" ||
               e.Cell.Column.HeaderText.ToString() == "Nov" ||
               e.Cell.Column.HeaderText.ToString() == "Dec"
           )
            {
                String _year = grdData.Rows[e.Cell.Row.Index].Cells["fiscal_year"].Value.ToString();
                String _user = grdData.Rows[e.Cell.Row.Index].Cells["User_Fullname"].Value.ToString();
                String _act_id = grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString();
                if (_year !="" && _user !="")
                {
                    FetchActivityStatus(_year, e.Cell.Column.HeaderText.ToString(), _user, _act_id);
                }
                else
                {
                    grdStatus.ItemsSource = null;
                }
               

            }
        }

        

      


    }


    public class StringToIntVC : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Int32.Parse((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();
        }
    }

}
