﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using MinDAF.Forms;

namespace MinDAF.Usercontrol.Report
{
    public partial class ppmp_report_user : UserControl
    {
        public String DivId { get; set; }
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }
        public String Division { get; set; }
        public String PAP { get; set; }
        public String User { get; set; }
        public Int32 Level { get; set; }

        public String ServiceType { get; set; }
        public String Revision { get; set; }

        private clsPPMP c_ppmp = new clsPPMP();
        private List<PPMPData> _PPMPData = new List<PPMPData>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProcurementMode c_procure = new clsProcurementMode();
        private List<ProcurementData> ListProcurementMode = new List<ProcurementData>();
        private List<PPMPFormat> _Main = new List<PPMPFormat>();
        private List<PPMPAlignment> _AlignmentData = new List<PPMPAlignment>();
       
        private frmProcurementMode f_mode = new frmProcurementMode();
        public ppmp_report_user()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            ServiceType = "502";
        }
        private void LoadProcurementData()
        {
            c_procure.Process = "LoadProcurementData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementLibrary());
        }
        private void FetchAlignmentData()
        {
            c_ppmp.Process = "FetchAlignmentData";
            svc_mindaf.ExecuteSQLAsync(c_procure.FetchAlignmentData(DivisionId,cmbYear.SelectedItem.ToString()));
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_ppmp.Process)
             {
                 case "FetchPPMPData":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                     var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                      select new PPMPData
                                      {
                                          Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                          Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                          Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                          e_date = Convert.ToString(info.Element("e_date").Value),
                                          entry_date = Convert.ToString(info.Element("entry_date").Value),
                                          month = Convert.ToString(info.Element("month").Value),
                                          name = Convert.ToString(info.Element("name").Value),
                                          quantity = Convert.ToString(info.Element("quantity").Value),
                                          rate = Convert.ToString(info.Element("rate").Value),
                                          s_date = Convert.ToString(info.Element("s_date").Value),
                                          uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                          year = Convert.ToString(info.Element("year").Value),
                                          type_service = Convert.ToString(info.Element("type_service").Value),
                                          act_id = Convert.ToString(info.Element("act_id").Value)
                                      };

                     _PPMPData.Clear();

                     foreach (var item in _dataLists)
                     {
                         if (item.uacs_code.Substring(0,3)==ServiceType)
                         {
                             PPMPData _varProf = new PPMPData();

                             _varProf.Fund_Name = item.Fund_Name;
                             _varProf.Division_Code = item.Division_Code;
                             _varProf.Division_Desc = item.Division_Desc;
                             _varProf.e_date = item.e_date.Replace("T00:00:00+08:00", "");
                             _varProf.entry_date = item.entry_date;
                             _varProf.month = item.month;
                             _varProf.name = item.name;
                             _varProf.quantity = item.quantity;
                             _varProf.rate = item.rate;
                             _varProf.s_date = item.s_date.Replace("T00:00:00+08:00", "");
                             _varProf.uacs_code = item.uacs_code;
                             _varProf.year = item.year;
                             _varProf.type_service = item.type_service;
                             _varProf.act_id = item.act_id;
                             _PPMPData.Add(_varProf);
                         }
                       

                     }
                     FetchAlignmentData();
                  
                     this.Cursor = Cursors.Arrow;

                     break;
                 case "FetchAlignmentData":
                     XDocument oDocKeyResultsFetchAlignmentData = XDocument.Parse(_results);
                     var _dataListsFetchAlignmentData = from info in oDocKeyResultsFetchAlignmentData.Descendants("Table")
                                      select new PPMPAlignment
                                      {
                                          months = Convert.ToString(info.Element("months").Value),
                                          fundsource = Convert.ToString(info.Element("fundsource").Value),
                                          mooe = Convert.ToString(info.Element("mooe").Value),
                                          name = Convert.ToString(info.Element("name").Value),
                                          from_total = Convert.ToString(info.Element("from_total").Value),
                                          from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                          to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                          total_alignment = Convert.ToString(info.Element("total_alignment").Value)
                               
                                      };

                     _AlignmentData.Clear();

                     foreach (var item in _dataListsFetchAlignmentData)
                     {
                        
                             PPMPAlignment _varProf = new PPMPAlignment();

                             _varProf.months = item.months;
                             _varProf.fundsource = item.fundsource;
                             _varProf.mooe = item.mooe;
                             _varProf.name = item.name;
                             _varProf.from_uacs = item.from_uacs;
                             _varProf.from_total =  item.from_total;
                             _varProf.to_uacs = item.to_uacs;
                             _varProf.total_alignment = item.total_alignment;
                 
                             _AlignmentData.Add(_varProf);
                     
                     }
                     GenerateData();
                     this.Cursor = Cursors.Arrow;

                     break;
                 case "FetchPPMPRevision":
                     XDocument oDocKeyResultsFetchPPMPRevision = XDocument.Parse(_results);
                     var _dataListsFetchPPMPRevision = from info in oDocKeyResultsFetchPPMPRevision.Descendants("Table")
                                                        select new PPMPRevision
                                                        {
                                                            Revision = Convert.ToString(info.Element("_rev").Value)

                                                        };


                     Int32 _rev = 0;

                     foreach (var item in _dataListsFetchPPMPRevision)
                     {
                         Revision = item.Revision;
                         _rev += 1;
                     }
                     if (Revision == null)
                     {
                         Revision = "0";
                     }
                     else
                     {
                         Revision = _rev.ToString();
                     }

                     this.Cursor = Cursors.Arrow;

                     break;
             }
        }
        private void GenerateData() 
        {
           
            double _Totals = 0.00;
            var results_1 = from p in _PPMPData
                          group p by p.Fund_Name into g
                          select new
                          {
                              Id = g.Key
                          };
            _Main.Clear();
            List<PPMPData> _DataFinal = new List<PPMPData>();
            List<PPMPData> _DataReport = new List<PPMPData>();
            foreach (var item in results_1)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.Id;

                _DataFinal.Add(x_var);
            }

        

            foreach (var item in _AlignmentData)
            {
                PPMPData x_var = new PPMPData();

                x_var.uacs_code = item.to_uacs;
                x_var.month = item.months;
                x_var.name = item.name;
                x_var.Fund_Name = item.fundsource;
                x_var.rate = item.total_alignment;
                x_var.year = cmbYear.SelectedItem.ToString();
                x_var.type_service = item.name.ToString();
                x_var.quantity ="1";
                _PPMPData.Add(x_var);
            }

            foreach (var item in _DataFinal)
            {

                PPMPFormat _Data = new PPMPFormat();

                _Data.ACTID = "";
                _Data.UACS = item.uacs_code.ToString();
                _Data.Description ="";
                _Data.ModeOfProcurement = "";
                _Data.Quantity_Size = "";
                _Data._Year = "";
                _Data._Division = this.Division;
                _Data._Pap = this.PAP;
                _Data.Jan = "";
                _Data.Feb = "";
                _Data.Mar = "";
                _Data.Apr = "";
                _Data.May = "";
                _Data.Jun = "";
                _Data.Jul = "";
                _Data.Aug = "";
                _Data.Sep = "";
                _Data.Oct = "";
                _Data.Nov = "";
                _Data.Dec = "";
                _Data.EstimatedBudget = "";
                _Data._Total = "0.00";
                _Data._Total = _Totals.ToString("#,##0.00");
                _Main.Add(_Data);
                List<PPMPData> x_data = _PPMPData.Where(itempp => itempp.Fund_Name == _Data.UACS).ToList();

                var results2nd = from p in x_data
                                 group p by p.name into g
                                 select new
                                 {
                                     name = g.Key,
                                     ExpenseItems = g.Select(m => m.name)
                                 };

                foreach (var res2nd in results2nd)
                {
                    PPMPFormat _Data2nd = new PPMPFormat();
                    double _Estimate = 0.00;
                    List<PPMPData> x_data2nd = _PPMPData.Where(items => items.name == res2nd.name).ToList();
       

                    _Data2nd.ACTID = "";
                    _Data2nd.UACS = x_data2nd[0].uacs_code;
                    _Data2nd.Description = x_data2nd[0].name;
                    _Data2nd.ModeOfProcurement = "";
                    _Data2nd.Quantity_Size = "";
                    _Data2nd._Year = x_data2nd[0].year.ToString();
                    _Data2nd._Division = this.Division;
                    _Data2nd._Pap = this.PAP;
                    _Data2nd.Jan = "";
                    _Data2nd.Feb = "";
                    _Data2nd.Mar = "";
                    _Data2nd.Apr = "";
                    _Data2nd.May = "";
                    _Data2nd.Jun = "";
                    _Data2nd.Jul = "";
                    _Data2nd.Aug = "";
                    _Data2nd.Sep = "";
                    _Data2nd.Oct = "";
                    _Data2nd.Nov = "";
                    _Data2nd.Dec = "";
                    _Data2nd.EstimatedBudget = "";
                    _Data2nd._Total = "0.00";
                    _Data2nd._Total = _Totals.ToString("#,##0.00");
                    _Main.Add(_Data2nd);
                    _Totals = 0.00;
                    List<PPMPAlignment> x_dataalign = _AlignmentData.Where(items => items.from_uacs == _Data2nd.UACS).ToList();
                    double totals = 0.00;

                    foreach (var itemal in x_dataalign)
                    {
                        totals += Convert.ToDouble(itemal.total_alignment);
                    }

                    foreach (var item_data in x_data2nd)
                    {
                        PPMPFormat _DataDetail = new PPMPFormat();
                        _DataDetail.ACTID = item_data.act_id;
                        _DataDetail.UACS = "";
                        _DataDetail.Description = item_data.type_service;
                        _DataDetail.ModeOfProcurement = "";
                        _DataDetail.Quantity_Size = item_data.quantity;
                        _DataDetail._Year = item_data.year.ToString();
                        _DataDetail._Division = this.Division;
                        _DataDetail._Pap = this.PAP;


                        switch (item_data.month)
                        {
                            case "Jan":
                                _DataDetail.Jan = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Feb":
                                _DataDetail.Feb = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Mar":
                                _DataDetail.Mar = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Apr":
                                _DataDetail.Apr = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "May":
                                _DataDetail.May = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Jun":
                                _DataDetail.Jun = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Jul":
                                _DataDetail.Jul = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;

                            case "Aug":
                                _DataDetail.Aug = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Sep":
                                _DataDetail.Sep = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Oct":
                                _DataDetail.Oct = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Nov":
                                _DataDetail.Nov = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;
                            case "Dec":
                                _DataDetail.Dec = "X";
                                _Totals += Convert.ToDouble(item_data.rate);
                                break;

                        }
                        _Estimate += Convert.ToDouble(item_data.rate) - totals;
                        _Totals -= totals;
                        _DataDetail.EstimatedBudget = _Estimate.ToString("#,##0.00");
                        _DataDetail._Total = _Totals.ToString("#,##0.00");
                        _Main.Add(_DataDetail);


                        List<PPMPAlignment> x_data3rd = _AlignmentData.Where(items => items.to_uacs == _Data2nd.UACS).ToList();
                  

                    }

                }
                foreach (var itemd in _Main)
                {
                 


                }


                grdData.ItemsSource = null;
                grdData.ItemsSource = _Main;
                grdData.Columns["_Year"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["_Division"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["_Pap"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["_Total"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["ACTID"].Visibility = System.Windows.Visibility.Collapsed;
                ArrangeColumn();

                FetchPPMPRevision();
            }

         // LoadReportData(_Main);
        }
        private void ArrangeColumn() 
        {
            Column col_uacs = grdData.Columns.DataColumns["UACS"];
            col_uacs.Width = new ColumnWidth(100, false);
            col_uacs.IsFixed = FixedState.Left;

            Column col_desc = grdData.Columns.DataColumns["Description"];
            col_desc.Width = new ColumnWidth(300, false);
            col_desc.IsFixed = FixedState.Left;


            Column col_qs = grdData.Columns.DataColumns["Quantity_Size"];
            col_qs.Width = new ColumnWidth(100, false);
            col_qs.IsFixed = FixedState.Left;
            col_qs.HeaderText = "Quantity / Size";

            Column col_eb = grdData.Columns.DataColumns["EstimatedBudget"];
            col_eb.Width = new ColumnWidth(100, false);
            col_eb.IsFixed = FixedState.Left;
            col_eb.HeaderText = "Estimated Budget";

            Column col_mop = grdData.Columns.DataColumns["ModeOfProcurement"];
            col_mop.Width = new ColumnWidth(100, false);
            col_mop.IsFixed = FixedState.Left;
            col_mop.HeaderText = "Mode of Procurement";
        }
        //private void LoadReportData(List<PPMPFormat> _Data) 
        //{
        //    c_ppmp.Process = "SaveReportPrintOut";
        //    c_ppmp.SaveReportPrintOut(_Data);
        //    c_ppmp.SQLOperation += c_ppmp_SQLOperation;
        //}

        //void c_ppmp_SQLOperation(object sender, EventArgs e)
        //{
        //    switch (c_ppmp.Process)
        //    {
        //        case "SaveReportPrintOut":
        //            RetrievePrintDataPPMP();
        //            break;
        //    }
        //}


        private void FetchPPMPData() 
        {

            c_ppmp.Process = "FetchPPMPData";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchData(this.DivisionId, cmbYear.SelectedItem.ToString()));

          
        }
        private void FetchPPMPRevision()
        {

            c_ppmp.Process = "FetchPPMPRevision";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchRevision(this.PAP, cmbYear.SelectedItem.ToString()));


        }
        //private void RetrievePrintDataPPMP()
        //{
        //    var svc = new C1ReportServiceReference.C1ReportServiceClient();
        //    svc.GetReportAsync("PPMP");
        //    svc.GetReportCompleted += (s, re) =>
        //    {
              
        //        var ms = new System.IO.MemoryStream(re.Result);
        //        _c1rv.LoadDocument(ms);
        //    };
        //}
      
        private void GenerateYear()        
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
        private void usr_budget_monthly_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchPPMPData();
        }

       

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchPPMPData();
        }

        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
            f_mode = new frmProcurementMode();
            f_mode.SelectedData += f_mode_SelectedData;
            f_mode.Show();
        }

        void f_mode_SelectedData(object sender, EventArgs e)
        {
            var _x = _Main.Where(items => items.ACTID == grdData.Rows[grdData.ActiveCell.Row.Index].Cells["ACTID"].Value.ToString()).ToList();
              foreach (var item in _x)
	            {
		            item.ModeOfProcurement = f_mode.SelectMode;
                  break;
	            }

              grdData.ItemsSource = null;
              grdData.ItemsSource = _Main;
              grdData.Columns["_Year"].Visibility = System.Windows.Visibility.Collapsed;
              grdData.Columns["_Division"].Visibility = System.Windows.Visibility.Collapsed;
              grdData.Columns["_Pap"].Visibility = System.Windows.Visibility.Collapsed;
              grdData.Columns["_Total"].Visibility = System.Windows.Visibility.Collapsed;
              grdData.Columns["ACTID"].Visibility = System.Windows.Visibility.Collapsed;
              ArrangeColumn();
        }
        private void SubmitReportPPMP()
        {
            c_ppmp.Process = "SaveReportPrintOutPPMP";
            c_ppmp.SubmitPPMP(_Main,Revision);
            c_ppmp.SQLOperation += c_ppmp_SQLOperation;
        }

        void c_ppmp_SQLOperation(object sender, EventArgs e)
        {
           
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/Downloads/ReportTool.application"), "_blank");
            //frmPrintPreview f_view = new frmPrintPreview();
            //f_view.ReportType = "PPMP";
          
            //f_view.PPMPReport = _Main;
            //f_view.Show();
        }

        private void btnReAlignment_Click(object sender, RoutedEventArgs e)
        {
            frmreAlignment f_rel = new frmreAlignment();
            f_rel.DivisionId = this.DivisionId;
            f_rel.PaP = this.PAP;
            f_rel.ReloadData += f_rel_ReloadData;
            f_rel.Show();
        }

        void f_rel_ReloadData(object sender, EventArgs e)
        {
            FetchPPMPData();

        }

        private void SubmitAlignment() 
        {

        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            SubmitReportPPMP();
        }

       
    }
}
