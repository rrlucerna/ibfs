﻿using MinDAF.Usercontrol.Office_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Usercontrol
{
    public partial class usr_office_management : UserControl
    {
        private usr_office_div_management ctrl_offdiv = new usr_office_div_management();
        private usr_office_budget_allocation ctrl_offbudget = new usr_office_budget_allocation();


        public usr_office_management()
        {
            InitializeComponent();
            ctrl_offdiv.CancelProcess += ctrl_offdiv_CancelProcess;
            ctrl_offbudget.CancelProcess += ctrl_offbudget_CancelProcess;
        }

        void ctrl_offbudget_CancelProcess(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
        }

        void ctrl_offdiv_CancelProcess(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
        }

        private void mnuOfficeCentral_Click(object sender, EventArgs e)
        {
            LoadOfficeDivisionManager();
        }

        private void LoadOfficeDivisionManager() 
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_offdiv);
        }

        private void mnuOfficeBudgetDesignation_Click(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_offbudget);
        }
    }
}
