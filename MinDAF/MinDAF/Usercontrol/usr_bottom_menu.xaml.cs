﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Usercontrol
{
    public partial class usr_bottom_menu : UserControl
    {
        public event EventHandler MenuClick;
        public event EventHandler SignOut;

        public String MenuType { get; set; }

        public usr_bottom_menu()
        {
            InitializeComponent();
        }

        private void btnBudgetDivison_Click(object sender, RoutedEventArgs e)
        {
            if (MenuClick!=null)
            {
                MenuType = "BudgetDivision";
                MenuClick(this, e);
            }
        }

        private void btnOfficeDivision_Click(object sender, RoutedEventArgs e)
        {
            if (MenuClick != null)
            {
                MenuType = "BudgetApproval";
                MenuClick(this, e);
            }
        }

        private void btnSignOff_Click(object sender, RoutedEventArgs e)
        {
            if (SignOut!=null)
            {
                SignOut(this, new EventArgs());
            }
        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e)
        {
            if (MenuClick != null)
            {
                MenuType = "Admin";
                MenuClick(this, e);
            }
        }
    }
}
