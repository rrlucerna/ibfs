﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrCreateProgram : UserControl
    {
        public String DivisionID { get; set; }

        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProgram cls_program = new clsProgram();
        private List<MajorFinalOutput> ListMajorFinalOutput = new List<MajorFinalOutput>();
        private List<PerformanceIndicator> ListPerformanceIndicator = new List<PerformanceIndicator>();
        private List<PPrograms> ListPrograms = new List<PPrograms>();
        private List<Categories> ListCategories = new List<Categories>();
        private List<AccountableOffice> ListAccountableOffice = new List<AccountableOffice>();
        private List<ProgramsMain> ListProgramsMain= new List<ProgramsMain>();

        public usrCreateProgram()
        {
            InitializeComponent();
            cls_program.SQLOperation += cls_program_SQLOperation;
            svc_mindaf.ExecuteSQLCompleted+=svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {   
 	       var _results = e.Result.ToString();

           switch (cls_program.Process)
           {
               case "FetchMajorFinalOutput":
                   XDocument oDocKeyResults = XDocument.Parse(_results);
                   var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                    select new MajorFinalOutput
                                    {
                                        id = Convert.ToString(info.Element("id").Value),
                                        name= Convert.ToString(info.Element("name").Value)
                                    };

                   ListMajorFinalOutput.Clear();
                   cmbMajorFinalOutput.Items.Clear();

                   foreach (var item in _dataLists)
                   {
                       MajorFinalOutput _varDetails = new MajorFinalOutput();


                       _varDetails.id = item.id;
                       _varDetails.name = item.name;


                       ListMajorFinalOutput.Add(_varDetails);
                       cmbMajorFinalOutput.Items.Add(item.name);
                   }
                   FetchPerformanceIndicator();
                   this.Cursor = Cursors.Arrow;
                
                   break;
                 case "FetchPerformanceIndicator":
                   XDocument oDocKeyPerformanceIndicator = XDocument.Parse(_results);
                   var _dataListsPerformanceIndicator = from info in oDocKeyPerformanceIndicator.Descendants("Table")
                                    select new PerformanceIndicator
                                    {
                                        id = Convert.ToString(info.Element("id").Value),
                                        name= Convert.ToString(info.Element("name").Value),
                                        code = Convert.ToString(info.Element("name").Value)
                                    };

                   ListPerformanceIndicator.Clear();
                   cmbPerformanceIndicator.Items.Clear();

                   foreach (var item in _dataListsPerformanceIndicator)
                   {
                       PerformanceIndicator _varDetails = new PerformanceIndicator();


                       _varDetails.id = item.id;
                       _varDetails.name = item.name;
                       _varDetails.code = item.code;

                       ListPerformanceIndicator.Add(_varDetails);
                       cmbPerformanceIndicator.Items.Add(item.name);
                   }
                   this.Cursor = Cursors.Arrow;
                   FetchPrograms();
                   break;
                 case "FetchPrograms":
                   XDocument oDocKeyFetchPrograms = XDocument.Parse(_results);
                   var _dataListsFetchPrograms = from info in oDocKeyFetchPrograms.Descendants("Table")
                                                        select new PPrograms
                                                        {
                                                            id = Convert.ToString(info.Element("id").Value),
                                                            name = Convert.ToString(info.Element("name").Value)
                                                        };

                   ListPrograms.Clear();
                   cmbProgram.Items.Clear();

                   foreach (var item in _dataListsFetchPrograms)
                   {
                       PPrograms _varDetails = new PPrograms();


                       _varDetails.id = item.id;
                       _varDetails.name = item.name;

                       ListPrograms.Add(_varDetails);
                       cmbProgram.Items.Add(item.name);
                   }
                   this.Cursor = Cursors.Arrow;
                   FetchCategory();
                   break;
                 case "FetchCategory":
                   XDocument oDocKeyFetchCategory = XDocument.Parse(_results);
                   var _dataListsFetchCategory = from info in oDocKeyFetchCategory.Descendants("Table")
                                                        select new Categories
                                                        {
                                                            id = Convert.ToString(info.Element("id").Value),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            weight = Convert.ToString(info.Element("weight").Value)
                                                        };

                   ListCategories.Clear();
                   cmbCategory.Items.Clear();

                   foreach (var item in _dataListsFetchCategory)
                   {
                       Categories _varDetails = new Categories();


                       _varDetails.id = item.id;
                       _varDetails.name = item.name;
                       _varDetails.weight = item.weight;

                       ListCategories.Add(_varDetails);
                       cmbCategory.Items.Add(item.name);
                   }
                   this.Cursor = Cursors.Arrow;
                   FetchAccountableOffice();
                   break;
                 case "FetchAccountableOffice":
                   XDocument oDocKeyFetchAccountableOffice = XDocument.Parse(_results);
                   var _dataListsFetchAccountableOffice = from info in oDocKeyFetchAccountableOffice.Descendants("Table")
                                                        select new AccountableOffice
                                                        {
                                                            id = Convert.ToString(info.Element("id").Value),
                                                            name = Convert.ToString(info.Element("name").Value)
                                                        };

                   ListAccountableOffice.Clear();
                   cmbAccountableOffice.Items.Clear();

                   foreach (var item in _dataListsFetchAccountableOffice)
                   {
                       AccountableOffice _varDetails = new AccountableOffice();


                       _varDetails.id = item.id;
                       _varDetails.name = item.name;

                       ListAccountableOffice.Add(_varDetails);
                       cmbAccountableOffice.Items.Add(item.name);
                   }
                   this.Cursor = Cursors.Arrow;
                   FetchProgramsMain();
                   break;
                 case "FetchProgramsMain":
                   XDocument oDocKeyFetchProgramsMain = XDocument.Parse(_results);
                   var _dataListsFetchProgramsMain = from info in oDocKeyFetchProgramsMain.Descendants("Table")
                                                        select new ProgramsMain
                                                        {
                                                            Id=Convert.ToString(info.Element("Id").Value),
                                                            Accountable_Office = Convert.ToString(info.Element("Accountable_Office").Value),
                                                            Category = Convert.ToString(info.Element("Category").Value),
                                                            Fiscal_Year = Convert.ToString(info.Element("Fiscal_Year").Value),
                                                            Major_Final_Output = Convert.ToString(info.Element("Major_Final_Output").Value),
                                                            Performance_Indicator = Convert.ToString(info.Element("Performance_Indicator").Value),
                                                            Program = Convert.ToString(info.Element("Program").Value)
                                                        };

                   ListProgramsMain.Clear();
                  

                   foreach (var item in _dataListsFetchProgramsMain)
                   {
                       ProgramsMain _varDetails = new ProgramsMain();


                       _varDetails.Accountable_Office = item.Accountable_Office;
                       _varDetails.Category = item.Category;
                       _varDetails.Fiscal_Year = item.Fiscal_Year;

                       _varDetails.Major_Final_Output = item.Major_Final_Output;

                       _varDetails.Performance_Indicator = item.Performance_Indicator;
                       _varDetails.Program = item.Program;
                       _varDetails.Id = item.Id;

                       ListProgramsMain.Add(_varDetails);
                      
                   }
                   this.Cursor = Cursors.Arrow;
                   if (ListProgramsMain.Count !=0)
                   {
                       grdData.ItemsSource = null;
                       grdData.ItemsSource = ListProgramsMain;
                       grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                   }
                   else
                   {
                       grdData.ItemsSource = null;
                   }

                   break;
           }
        }

        void cls_program_SQLOperation(object sender, EventArgs e)
        {
            switch (cls_program.Process)
            {
                case "SaveProjectData":
                      FetchMajorFinalOutput();
                        GenerateYear();
                        EnableDisableControls(false);
                    break;
          
            }
          
        }

        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;
            cmbYear.Items.Clear();
            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void usrProgram_Loaded(object sender, RoutedEventArgs e)
        {
            FetchMajorFinalOutput();
            GenerateYear();
            EnableDisableControls(false);
        }
        private void SaveProgram()
        {
            String fiscal_year = cmbYear.SelectedItem.ToString();
            String major_final_output_code="";
            String performance_indicator_code="";
            String program_code="";
            String category_code="";
            String accountable_office_code = "";
 

            List<MajorFinalOutput> x = ListMajorFinalOutput.Where(item => item.name == cmbMajorFinalOutput.SelectedItem.ToString()).ToList();
            foreach (var item in x)
            {
                major_final_output_code = item.id;
                break;
            }
            List<PerformanceIndicator> x1 = ListPerformanceIndicator.Where(item => item.name == cmbPerformanceIndicator.SelectedItem.ToString()).ToList();
            foreach (var item in x1)
            {
                performance_indicator_code = item.id;
                break;
            }
            List<PPrograms> x2 = ListPrograms.Where(item => item.name == cmbProgram.SelectedItem.ToString()).ToList();
            foreach (var item in x2)
            {
                program_code = item.id;
                break;
            }
            List<Categories> x3 = ListCategories.Where(item => item.name == cmbCategory.SelectedItem.ToString()).ToList();
            foreach (var item in x3)
            {
                category_code = item.id;
                break;
            }
            List<AccountableOffice> x4 = ListAccountableOffice.Where(item => item.name == cmbAccountableOffice.SelectedItem.ToString()).ToList();
            foreach (var item in x4)
            {
                accountable_office_code = item.id;
                break;
            }

            
            cls_program.Process = "SaveProjectData";
            cls_program.SaveProgramMain(fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code);

          
        }

        private void EnableDisableControls(bool _val) 
        {
            cmbAccountableOffice.IsEnabled = _val;
            cmbCategory.IsEnabled = _val;
            cmbMajorFinalOutput.IsEnabled = _val;
            cmbPerformanceIndicator.IsEnabled = _val;
            cmbProgram.IsEnabled = _val;
            cmbYear.IsEnabled = _val;
            btnCancel.IsEnabled = _val;
        }
        private void FetchMajorFinalOutput()
        {
            cls_program.Process = "FetchMajorFinalOutput";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchMajorFinalOutput());
        }

        private void FetchPerformanceIndicator() 
        {
            cls_program.Process = "FetchPerformanceIndicator";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchPerformanceIndicator());
        }
        private void FetchPrograms()
        {
            cls_program.Process = "FetchPrograms";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchPrograms());
        }
        private void FetchCategory()
        {
            cls_program.Process = "FetchCategory";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchCategory());
        }
        private void FetchAccountableOffice()
        {
            cls_program.Process = "FetchAccountableOffice";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchAccountableOffice());
        }
        private void FetchProgramsMain()
        {
            cls_program.Process = "FetchProgramsMain";
            svc_mindaf.ExecuteSQLAsync(cls_program.FetchProgramsMain());
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            switch (btnNew.Content.ToString())
            {
                case "New":
                    btnNew.Content = "Save";
                    
                    EnableDisableControls(true);
                    break;
                case "Save":
                    btnNew.Content = "New";
                    SaveProgram();
                    ClearControlData();
                    FetchProgramsMain();
                    EnableDisableControls(false);
                    break;
                default:
                    break;
            }
        }
        private void ClearControlData() 
        {
            cmbAccountableOffice.SelectedIndex = -1;
            cmbCategory.SelectedIndex = -1;
            cmbMajorFinalOutput.SelectedIndex = -1;
            cmbPerformanceIndicator.SelectedIndex = -1;
            cmbProgram.SelectedIndex = -1;
            cmbYear.SelectedIndex = -1;
        }

    

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {

            frmProjectManager f_proj = new frmProjectManager();
            f_proj.MainID = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Id"].Value.ToString();
            f_proj.Program = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Program"].Value.ToString();
            f_proj.DivisionId = this.DivisionID;
            f_proj.Show();
        }

    }
}
