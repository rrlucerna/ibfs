﻿using MinDAF.Forms;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Usercontrol
{
    public partial class usr_Menu_Admin : UserControl
    {
      
        public String Division { get; set; }
        public String DivisionID { get; set; }
        public String UserId { get; set; }
        public String User { get; set; }
        public String IsAdmin { get; set; }

        private app_application ctrl_app = new app_application();

        public usr_Menu_Admin()
        {
            InitializeComponent();
        }


        private void LoadAPPDashboard() 
        {
            ctrl_app = new app_application();
            ctrl_app.Width = stkChild.Width;
            ctrl_app.Height = stkChild.Height;
       //     ctrl_budget_approvals_finance.Level = Level;

         //   ctrl_app. = this.DivisionID;
          //  ctrl_app = this.DivisionID;

            stkChild.Children.Clear();

            stkChild.Children.Add(ctrl_app);
          
        }
       
        private void mnuUserManagement_Click(object sender, EventArgs e)
        {
            frmUserManagement f_user = new frmUserManagement();
            f_user.Show();
        }

        private void mnuModeProcurement_Click(object sender, EventArgs e)
        {
            frmModeProcurementManager f_procurement = new frmModeProcurementManager();
            f_procurement.Show();
        }

        private void ctrl_data_admin_Loaded(object sender, RoutedEventArgs e)
        {
            LoadAPPDashboard();
        }
    }
}
