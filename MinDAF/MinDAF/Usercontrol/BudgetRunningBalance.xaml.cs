﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol
{
    public partial class BudgetRunningBalance : UserControl
    {
        public event EventHandler LockControls;
        private Double TotalExpenditure = 0.00;
        public String _DivisionID { get; set; }
        public String _FundSource { get; set; }
        public String _Year { get; set; }
        public String _UACS { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<BugdetBalanceFields> ListBudgetRunningBalance = new List<BugdetBalanceFields>();
        private clsBudgetRunning c_run = new clsBudgetRunning();
       
      private  System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public BudgetRunningBalance(Double _TotalExpenditure,String Expenditure,String UACS)
        {
            InitializeComponent();
            
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;

            TotalExpenditure = _TotalExpenditure;
            lblCaption.Content = "Allocation and Running Balance For " + Expenditure;
            _UACS = UACS;
        }

       
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
              var _results = e.Result.ToString();
              switch (c_run.Process)
              {
                  case "FetchBudgetRunningBalance":
                      XDocument oDocKeyResults = XDocument.Parse(_results);
                      var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                       select new BugdetBalanceFields
                                       {
                                           balance = Convert.ToString(info.Element("balance").Value),
                                          name  = Convert.ToString(info.Element("name").Value),
                                          total  = Convert.ToString(info.Element("total").Value),
                                           uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                       };


                    ListBudgetRunningBalance.Clear();
                    BugdetBalanceFields _BegBal = new BugdetBalanceFields();
                    var _begBalance = _dataLists.Where(item => item.name == "Beginning Balance").ToList();
                    double RunningBalance = 0.00;
                    RunningBalance = Convert.ToDouble(_begBalance[0].balance);

                    _BegBal.balance =RunningBalance.ToString("#,##0.00");
                    _BegBal.name = _begBalance[0].uacs_code;
                    _BegBal.total ="";
                    _BegBal.uacs_code = _begBalance[0].uacs_code;
                 

                    ListBudgetRunningBalance.Add(_BegBal);

                    
                  

                      foreach (var item in _dataLists)
                      {
                        
                          BugdetBalanceFields _temp = new BugdetBalanceFields();

                          if (item.name != "Beginning Balance")
                          {
                              RunningBalance -= Convert.ToDouble(item.total);
                              _temp.balance = RunningBalance.ToString("#,##0.00");
                              _temp.name =  item.uacs_code;
                              _temp.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                              _temp.uacs_code = item.uacs_code;
                              ListBudgetRunningBalance.Add(_temp);
                          }
                          

                      }
                     
                      grdData.ItemsSource = null;
                      grdData.ItemsSource = ListBudgetRunningBalance;
              
                      try
                      {
                          grdData.Columns["uacs_code"].Visibility = System.Windows.Visibility.Collapsed;
                          grdData.Columns["name"].HeaderText = "UACS Code";
                      }
                      catch (Exception)
                      {
                       
                         
                      }
                    
                     
                      if (TotalExpenditure > Convert.ToDouble(_BegBal.balance) && Convert.ToDouble(_BegBal.balance)!=0)
                      {
                          frmNotifyBalance fBa = new frmNotifyBalance();

                          fBa.Show();
                          if (LockControls != null)
                          {
                              LockControls(this, new EventArgs());
                          }
                      }
                      this.Cursor = Cursors.Arrow;
                      break;
              }
        }

        private void FetchBudgetRunningBalance()
        {
            c_run.Process = "FetchBudgetRunningBalance";
            svc_mindaf.ExecuteSQLAsync(c_run.GetCurrentRunningBalance(_DivisionID, _Year, this._FundSource, _UACS));
        }

        private void frm_bud_run_Loaded(object sender, RoutedEventArgs e)
        {
            FetchBudgetRunningBalance();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            FetchBudgetRunningBalance();
        }
    }
}
