-- SQL Manager Lite for SQL Server 4.1.1.46173
-- ---------------------------------------
-- Host      : LOCALHOST\SQLEXPRESS
-- Database  : minda_financials_dirty
-- Version   : Microsoft SQL Server 2008 (RTM) 10.0.1600.22


--
-- Definition for table DBM_Pap : 
--

CREATE TABLE dbo.DBM_Pap (
  DBM_Pap_Id int NOT NULL,
  DBM_Pap_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  DBM_Pap_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table DBM_Sub_Pap : 
--

CREATE TABLE dbo.DBM_Sub_Pap (
  DBM_Sub_Pap_id int NOT NULL,
  DBM_Pap_Id int NULL,
  DBM_Sub_Pap_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  DBM_Sub_Pap_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table Division : 
--

CREATE TABLE dbo.Division (
  Division_Id int NOT NULL,
  DBM_Sub_Pap_Id int NULL,
  Division_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Division_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table dvDB : 
--

CREATE TABLE dbo.dvDB (
  id int IDENTITY(1, 1) NOT NULL,
  UACS varchar(25) COLLATE Latin1_General_CI_AS NULL,
  PAP varchar(25) COLLATE Latin1_General_CI_AS NULL,
  FUND_SOURCE varchar(30) COLLATE Latin1_General_CI_AS NULL,
  month varchar(25) COLLATE Latin1_General_CI_AS DEFAULT 0 NULL,
  total varchar(25) COLLATE Latin1_General_CI_AS DEFAULT 0 NULL,
  year varchar(30) COLLATE Latin1_General_CI_AS DEFAULT 0 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_activity : 
--

CREATE TABLE dbo.mnda_activity (
  id int IDENTITY(1, 1) NOT NULL,
  output_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  description varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  accountable_member_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '0' NULL,
  member_category_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  weight decimal(3, 2) DEFAULT 0 NULL,
  date_start datetime NULL,
  date_end datetime NULL,
  completion_rate int DEFAULT 0 NULL,
  support_needed varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  is_approved bit DEFAULT 0 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_activity_data : 
--

CREATE TABLE dbo.mnda_activity_data (
  id int IDENTITY(1, 1) NOT NULL,
  activity_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '0' NULL,
  accountable_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '0' NULL,
  remarks varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  start datetime DEFAULT NULL NULL,
  [end] datetime DEFAULT NULL NULL,
  destination varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  no_staff int DEFAULT 0 NULL,
  rate numeric(13, 2) DEFAULT 0 NULL,
  travel_allowance numeric(13, 2) DEFAULT 0 NULL,
  total numeric(13, 2) DEFAULT 0 NULL,
  month varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  year numeric(13, 0) DEFAULT 0 NULL,
  is_approved bit DEFAULT 0 NULL,
  mooe_sub_expenditure_id varchar(30) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  type_service varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  no_days varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '0' NULL,
  stat_1 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  stat_2 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  stat_3 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  stat_4 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  stat_5 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  stat_6 varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  procurement_id varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '0' NULL,
  accountable_name varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  quantity varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '1' NULL,
  entry_date datetime DEFAULT NULL NULL,
  status varchar(max) COLLATE Latin1_General_CI_AS DEFAULT 'For Approval By Division Head' NULL,
  is_suspended int DEFAULT 0 NULL,
  is_submitted int DEFAULT 0 NULL,
  fund_source_id varchar(30) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_activity_data_procurement : 
--

CREATE TABLE dbo.mnda_activity_data_procurement (
  id int IDENTITY(1, 1) NOT NULL,
  activity_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  procurement_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  accountable_name varchar(max) COLLATE Latin1_General_CI_AS NULL,
  remarks varchar(max) COLLATE Latin1_General_CI_AS NULL,
  quantity int DEFAULT 0 NULL,
  rate numeric(13, 2) NULL,
  total numeric(13, 2) NULL,
  month varchar(max) COLLATE Latin1_General_CI_AS NULL,
  year numeric(13, 0) NULL,
  entry_date datetime NULL,
  is_approved bit DEFAULT 0 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_alignment_record : 
--

CREATE TABLE dbo.mnda_alignment_record (
  id int IDENTITY(1, 1) NOT NULL,
  from_uacs varchar(30) COLLATE Latin1_General_CI_AS NULL,
  from_total varchar(30) COLLATE Latin1_General_CI_AS NULL,
  to_uacs varchar(30) COLLATE Latin1_General_CI_AS NULL,
  total_alignment varchar(30) COLLATE Latin1_General_CI_AS NULL,
  division_pap varchar(30) COLLATE Latin1_General_CI_AS NULL,
  division_year varchar(30) COLLATE Latin1_General_CI_AS NULL,
  is_approved bit NULL,
  months varchar(30) COLLATE Latin1_General_CI_AS NULL,
  fundsource varchar(30) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_approved_projects_division : 
--

CREATE TABLE dbo.mnda_approved_projects_division (
  id int IDENTITY(1, 1) NOT NULL,
  program_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  project_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  output_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  activity_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  isapproved bit DEFAULT 0 NULL,
  main_activity_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  is_submitted bit DEFAULT 0 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_budget_allocation : 
--

CREATE TABLE dbo.mnda_budget_allocation (
  id int IDENTITY(1, 1) NOT NULL,
  office_id int NULL,
  division_id varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  budget_allocation decimal(18, 2) NULL,
  status int DEFAULT 0 NULL,
  entry_year int DEFAULT 0 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_budget_expenditure : 
--

CREATE TABLE dbo.mnda_budget_expenditure (
  id int IDENTITY(1, 1) NOT NULL,
  office_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  expenditure_id varchar(max) COLLATE Latin1_General_CI_AS NULL,
  allocated_budget numeric(18, 2) DEFAULT 0.00 NULL,
  year int NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_category : 
--

CREATE TABLE dbo.mnda_category (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL,
  weight decimal(3, 2) NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_expenditures_library : 
--

CREATE TABLE dbo.mnda_expenditures_library (
  id int IDENTITY(1, 1) NOT NULL,
  sub_expenditure_code varchar(30) COLLATE Latin1_General_CI_AS NULL,
  library_type varchar(max) COLLATE Latin1_General_CI_AS NULL,
  item_code varchar(max) COLLATE Latin1_General_CI_AS NULL,
  item_name varchar(max) COLLATE Latin1_General_CI_AS NULL,
  day varchar(30) COLLATE Latin1_General_CI_AS NULL,
  rate numeric(18, 2) NULL,
  rate_year numeric(18, 0) NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_fund_source : 
--

CREATE TABLE dbo.mnda_fund_source (
  Fund_Source_Id int IDENTITY(1, 1) NOT NULL,
  division_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  Fund_Name varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  details varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Amount decimal(10, 2) NULL,
  Year int NULL,
  Status varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT 'n-a' NULL,
  service_type varchar(30) COLLATE Latin1_General_CI_AS NULL,
  CONSTRAINT PK_mnda_fund_source PRIMARY KEY CLUSTERED (Fund_Source_Id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_key_result_area : 
--

CREATE TABLE dbo.mnda_key_result_area (
  KeyResult_Id int IDENTITY(1, 1) NOT NULL,
  KeyResult_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  KeyResult_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  CONSTRAINT PK_mnda_key_result_area PRIMARY KEY CLUSTERED (KeyResult_Id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_main_program_encoded : 
--

CREATE TABLE dbo.mnda_main_program_encoded (
  id int IDENTITY(1, 1) NOT NULL,
  fiscal_year varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  major_final_output_code varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  performance_indicator_code varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  program_code varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  category_code varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  accountable_office_code varchar(max) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  accountable_division varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT '-' NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_major_final_output : 
--

CREATE TABLE dbo.mnda_major_final_output (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_mindanao_2020_contribution : 
--

CREATE TABLE dbo.mnda_mindanao_2020_contribution (
  Mindanao_2020_Id int IDENTITY(1, 1) NOT NULL,
  Mindanao_2020_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Mindanao_2020_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  CONSTRAINT PK_mnda_mindanao_2020_contribution PRIMARY KEY CLUSTERED (Mindanao_2020_Id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_mode_procurement_lib : 
--

CREATE TABLE dbo.mnda_mode_procurement_lib (
  id int IDENTITY(1, 1) NOT NULL,
  mode_procurement varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_mooe_expenditures : 
--

CREATE TABLE dbo.mnda_mooe_expenditures (
  id int IDENTITY(1, 1) NOT NULL,
  code varchar(50) COLLATE Latin1_General_CI_AS NULL,
  name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  type_service varchar(30) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_mooe_sub_expenditures : 
--

CREATE TABLE dbo.mnda_mooe_sub_expenditures (
  id int IDENTITY(1, 1) NOT NULL,
  mooe_id varchar(50) COLLATE Latin1_General_CI_AS NULL,
  uacs_code varchar(30) COLLATE Latin1_General_CI_AS DEFAULT '-' NULL,
  name varchar(255) COLLATE Latin1_General_CI_AS NULL,
  is_active bit DEFAULT 1 NULL,
  is_dynamic bit DEFAULT 1 NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_offices : 
--

CREATE TABLE dbo.mnda_offices (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_performance_indicator : 
--

CREATE TABLE dbo.mnda_performance_indicator (
  id int IDENTITY(1, 1) NOT NULL,
  code varchar(max) COLLATE Latin1_General_CI_AS NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_procurement_items : 
--

CREATE TABLE dbo.mnda_procurement_items (
  id int IDENTITY(1, 1) NOT NULL,
  item_specifications varchar(max) COLLATE Latin1_General_CI_AS NULL,
  general_category varchar(max) COLLATE Latin1_General_CI_AS NULL,
  sub_category varchar(max) COLLATE Latin1_General_CI_AS NULL,
  minda_inventory varchar(max) COLLATE Latin1_General_CI_AS NULL,
  unit_of_measure varchar(max) COLLATE Latin1_General_CI_AS NULL,
  price numeric(18, 5) NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_program_encoded : 
--

CREATE TABLE dbo.mnda_program_encoded (
  id int IDENTITY(1, 1) NOT NULL,
  main_program_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  key_result_code varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  mindanao_2020_code varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  scope_code varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  fund_source_code varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  acountable_division_code varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  project_name varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  project_year varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_programs : 
--

CREATE TABLE dbo.mnda_programs (
  id int IDENTITY(1, 1) NOT NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_project_logs : 
--

CREATE TABLE dbo.mnda_project_logs (
  id int IDENTITY(1, 1) NOT NULL,
  div_id varchar(30) COLLATE Latin1_General_CI_AS NULL,
  project_name varchar(155) COLLATE Latin1_General_CI_AS NULL,
  project_action_log varchar(55) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_project_output : 
--

CREATE TABLE dbo.mnda_project_output (
  id int IDENTITY(1, 1) NOT NULL,
  program_code varchar(max) COLLATE Latin1_General_CI_AS NULL,
  name varchar(max) COLLATE Latin1_General_CI_AS NULL,
  assigned_user varchar(max) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_project_proposals : 
--

CREATE TABLE dbo.mnda_project_proposals (
  id int IDENTITY(1, 1) NOT NULL,
  division_id int NULL,
  expenditure_id int NULL,
  project_name varchar(150) COLLATE Latin1_General_CI_AS NULL,
  amount decimal(18, 0) NULL,
  is_template int DEFAULT 0 NULL,
  is_active bit DEFAULT 1 NULL,
  year int NULL,
  status varchar(30) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_report_data_mao_urs : 
--

CREATE TABLE dbo.mnda_report_data_mao_urs (
  ReportHeader varchar(155) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  UACS varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Allocation numeric(18, 0) NULL,
  Jan numeric(18, 0) NULL,
  Feb numeric(18, 0) NULL,
  Mar numeric(18, 0) NULL,
  Apr numeric(18, 0) NULL,
  May numeric(18, 0) NULL,
  Jun numeric(18, 0) NULL,
  Jul numeric(18, 0) NULL,
  Aug numeric(18, 0) NULL,
  Sep numeric(18, 0) NULL,
  Octs numeric(18, 0) NULL,
  Nov numeric(18, 0) NULL,
  Dec numeric(18, 0) NULL,
  Total numeric(18, 0) NULL,
  Balance varchar(30) COLLATE Latin1_General_CI_AS NULL,
  Divisions varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  FundSource varchar(100) COLLATE Latin1_General_CI_AS NULL,
  id int IDENTITY(1, 1) NOT NULL,
  PAP varchar(30) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_report_data_ppmp : 
--

CREATE TABLE dbo.mnda_report_data_ppmp (
  Uacs varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Description varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Quantity varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  EstimateBudget varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  ModeOfProcurement varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Jan varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Feb varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Mar varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Apr varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  May varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Jun varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Jul varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Aug varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Sep varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Octs varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Nov varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Dec varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Total varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Division varchar(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Yearssss varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  PAP varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  approved bit NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_scopes : 
--

CREATE TABLE dbo.mnda_scopes (
  Scope_Id int IDENTITY(1, 1) NOT NULL,
  Scope_Code varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Scope_Desc varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  CONSTRAINT PK_mnda_scopes PRIMARY KEY CLUSTERED (Scope_Id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_type_service : 
--

CREATE TABLE dbo.mnda_type_service (
  id int IDENTITY(1, 1) NOT NULL,
  uacs varchar(30) COLLATE Latin1_General_CI_AS NULL,
  name varchar(30) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_user_accounts : 
--

CREATE TABLE dbo.mnda_user_accounts (
  User_Id int IDENTITY(1, 1) NOT NULL,
  Division_Id int NULL,
  User_Fullname varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  User_Contact varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  User_Position varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Username varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  Password varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  status varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  is_admin int NULL,
  CONSTRAINT PK_mnda_user_accounts PRIMARY KEY CLUSTERED (User_Id)
    WITH (
      PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF,
      ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
ON [PRIMARY]
GO

--
-- Definition for table mnda_users : 
--

CREATE TABLE dbo.mnda_users (
  id int IDENTITY(1, 1) NOT NULL,
  username varchar(55) COLLATE Latin1_General_CI_AS NULL,
  password varchar(15) COLLATE Latin1_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Data for table dbo.DBM_Pap  (LIMIT 0,500)
--

INSERT INTO dbo.DBM_Pap (DBM_Pap_Id, DBM_Pap_Code, DBM_Pap_Desc)
VALUES 
  (1, N'100000000', N'General Administration and Support')
GO

INSERT INTO dbo.DBM_Pap (DBM_Pap_Id, DBM_Pap_Code, DBM_Pap_Desc)
VALUES 
  (2, N'301010000', N'Development Planning, Progrraming, Policy Advisory and Project Development Program')
GO

INSERT INTO dbo.DBM_Pap (DBM_Pap_Id, DBM_Pap_Code, DBM_Pap_Desc)
VALUES 
  (3, N'301020000', N'Mindanao-Wide and Inter-Regional Program/Project Management, Facilitation and Coordination')
GO

INSERT INTO dbo.DBM_Pap (DBM_Pap_Id, DBM_Pap_Code, DBM_Pap_Desc)
VALUES 
  (4, N'301030000', N'Mindanao and BIMP-EAGA Investment Promotion Program')
GO

--
-- Data for table dbo.DBM_Sub_Pap  (LIMIT 0,500)
--

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (1, 1, N'100010000', N'Office of the Chairperson')
GO

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (2, 2, N'301010001', N'Policy Planinng and Project Development Office')
GO

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (3, 2, N'301010002', N'Office for Finance and Administrative Services')
GO

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (4, 2, N'301030002', N'Monitoring and Evaluation of BIMP-EAGA and Other International Trade and Evets')
GO

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (5, 3, N'301020000', N'Office for Project Management and Area Concerns')
GO

INSERT INTO dbo.DBM_Sub_Pap (DBM_Sub_Pap_id, DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc)
VALUES 
  (6, 4, N'30103000', N'Investment Promotion and Public Affairs Office')
GO

--
-- Data for table dbo.Division  (LIMIT 0,500)
--

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (1, 1, N'100010000', N'General Management and Supervision (GMS)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (2, 1, N'100010000', N'Office of Executve Director (OED)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (3, 3, N'100010000', N'Finance Division (FD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (4, 3, N'100010000', N'Administrative Division (AD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (5, 2, N'301010001', N'Planning and Research Division (PRD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (6, 2, N'301010001', N'Knowledge Management Division (KMD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (7, 2, N'301010002', N'Policy Formulation Division (PFD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (8, 2, N'301010003', N'Project Development Division (PDD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (9, 5, N'301020000', N'Area Management Office Western Mindanao (AMO WM)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (10, 5, N'301020000', N'Area Management Office Northern Mindanao (AMO NM)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (11, 5, N'301020000', N'Area Management Office Northeastern Mindanao (AMO NEM)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (12, 5, N'301020000', N'Area Management Office Central Mindanao (AMO CM)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (13, 6, N'301030001', N'Investment Promotion Division (IPD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (14, 6, N'301030001', N'Public Relation Division (PuRD)')
GO

INSERT INTO dbo.Division (Division_Id, DBM_Sub_Pap_Id, Division_Code, Division_Desc)
VALUES 
  (15, 6, N'301030002', N'International Relations Division (IRD)')
GO

--
-- Data for table dbo.dvDB  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.dvDB ON
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (1, N'5020101000', N'301010001', N'PRD Regular', N'Jan', N'320.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (8, N'5020309000', N'301010001', N'PRD Regular', N'Feb', N'3314.85', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (9, N'5020309000', N'301010001', N'PRD Regular', N'Mar', N'1181.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (10, N'5020309000', N'301010001', N'PRD Regular', N'Apr', N'575.95', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (11, N'5020399000', N'301010001', N'PRD Regular', N'Apr', N'2396.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (6, N'5020101000', N'301010001', N'PRD Regular', N'Feb', N'720.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (7, N'5020101000', N'301010001', N'PRD Regular', N'Mar', N'2850.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (12, N'5029903000', N'301010001', N'PRD Regular', N'Mar', N'395.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (13, N'5029999099', N'301010001', N'PRD Regular', N'Feb', N'8050.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (14, N'5020502001', N'301010001', N'PRD-Fixed Cost', N'Feb', N'6000.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (15, N'5020502001', N'301010001', N'PRD-Fixed Cost', N'Mar', N'6000.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (16, N'5020502001', N'301010001', N'PRD-Fixed Cost', N'Apr', N'6000.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (17, N'5021003000', N'301010001', N'PRD-Fixed Cost', N'Jan', N'2000.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (18, N'5021003000', N'301010001', N'PRD-Fixed Cost', N'Feb', N'14000.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (19, N'5021003000', N'301010001', N'PRD-Fixed Cost', N'Mar', N'8232.00', N'2016')
GO

INSERT INTO dbo.dvDB (id, UACS, PAP, FUND_SOURCE, month, total, year)
VALUES 
  (20, N'5021003000', N'301010001', N'PRD-Fixed Cost', N'Apr', N'12000.00', N'2016')
GO

SET IDENTITY_INSERT dbo.dvDB OFF
GO

--
-- Data for table dbo.mnda_activity  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_activity ON
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (1, N'1', N'Activity', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (3, N'1', N'Host Seminar', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (4, N'2', N'Bills', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (5, N'3', N'EXP', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (6, N'4', N'MDC Setup', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (8, N'4', N'Activity', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (10, N'4', N'Operation', N'45', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (11, N'5', N'Progress Development', N'39', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (13, N'5', N'Supply', N'39', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (15, N'5', N'MinDA Activity', N'39', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (16, N'5', N'', N'39', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

INSERT INTO dbo.mnda_activity (id, output_id, description, accountable_member_id, member_category_id, weight, date_start, date_end, completion_rate, support_needed, is_approved)
VALUES 
  (17, N'6', N'Convention', N'39', N'-', 0, NULL, NULL, 0, N'-', 0)
GO

SET IDENTITY_INSERT dbo.mnda_activity OFF
GO

--
-- Data for table dbo.mnda_activity_data  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_activity_data ON
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (3, N'1', N'Member PRD', N'Travel', '20160104', '20160108', N'Sorsogon', 5, 8500, 1500, 50000, N'Jan', 2016, 1, N'1', N'AH-Elsewhere-Sorsogon', N'5', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (4, N'3', N'Member PRD', N'Seminar', NULL, NULL, N'Baguio', 4, 100000, 0, 100000, N'Jan', 2016, 1, N'44', N'Hotel', N'20', N'Yes', N'Yes', N'Yes', N'No', N'Yes', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (5, N'4', N'Member PRD', N'Globe Payment', NULL, NULL, N'-', 0, 1500, 0, 15000, N'Feb', 2016, 1, N'11', N'Globe', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'43')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (6, N'5', N'Member PRD', N'Expense Category 1', NULL, NULL, N'-', 0, 10000, 0, 10000, N'Jan', 2016, 1, N'15', N'Expense Item 1', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'43')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (7, N'5', N'Member PRD', N'Expense Category 2', NULL, NULL, N'-', 0, 10000, 0, 80000, N'Feb', 2016, 1, N'15', N'Expense Item 1', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'43')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (8, N'5', N'Member PRD', N'Exp Cat 3', NULL, NULL, N'-', 0, 4000, 0, 4000, N'Mar', 2016, 1, N'15', N'Expense Item 2', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'43')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (9, N'6', N'Member PRD', N'Travel 1', '20160101', '20160108', N'Baguio', 5, 10000, 1500, 62000, N'Jan', 2016, 1, N'1', N'AH-Elsewhere-Baguio', N'8', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (10, N'6', N'Member PRD', N'Travel 2', '20160209', '20160212', N'Manila', 5, 10000, 2300, 59200, N'Feb', 2016, 1, N'1', N'AH-Major Cities-Manila', N'4', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (11, N'6', N'Member PRD', N'Travel Sched', '20160314', '20160318', N'Cebu', 21, 10000, 1900, 219500, N'Mar', 2016, 1, N'1', N'AH-Elsewhere+Per Diem-Cebu', N'5', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (12, N'6', N'Member PRD', N'Extra Flight Expense', '20160418', '20160418', N'Extra Bill', 1, 3800, 1500, 5300, N'Apr', 2016, 1, N'1', N'AH-Elsewhere-Extra Bill', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (13, N'8', N'Member PRD', N'Jan Training', NULL, NULL, N'-', 0, 900, 0, 13500, N'Jan', 2016, 1, N'3', N'Metro Manila', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (14, N'8', N'Member PRD', N'Jan Training', NULL, NULL, N'-', 0, 1500, 0, 1500, N'Jan', 2016, 1, N'3', N'Metro Manila', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (15, N'8', N'Member PRD', N'Feb Training', NULL, NULL, N'-', 0, 10000, 0, 130000, N'Feb', 2016, 1, N'3', N'Elsewhere', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (16, N'8', N'Member PRD', N'Office Purchase Items', NULL, NULL, N'-', 0, 1000, 0, 39000, N'Mar', 2016, 1, N'4', N'Office Items', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (17, N'8', N'Member PRD', N'Payment Fees', NULL, NULL, N'-', 0, 1500, 0, 84000, N'May', 2016, 1, N'11', N'Globe', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (18, N'8', N'Member PRD', N'Payment Budget For Internet Provider', NULL, NULL, N'-', 0, 1500, 0, 81000, N'Apr', 2016, 1, N'13', N'PLDT Internet', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (19, N'8', N'Member PRD', N'Consultant Package Fee', NULL, NULL, N'-', 0, 25000, 0, 1675000, N'Jan', 2016, 1, N'17', N'Consultant Fee', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (20, N'8', N'Member PRD', N'Cleaning Services', NULL, NULL, N'-', 0, 5000, 0, 5000, N'Jan', 2016, 1, N'17', N'Janitorial Service', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (21, N'8', N'Member PRD', N'OPS Service', NULL, NULL, N'-', 0, 268000, 0, 268000, N'Feb', 2016, 0, N'18', N'OPS Service', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'Submitted', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (22, N'8', N'Member PRD', N'Labor Charge', NULL, NULL, N'', 20, 5000, 0, 100000, N'Mar', 2016, 1, N'51', N'-', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (23, N'8', N'Member PRD', N'Service Fee', NULL, NULL, N'', 40, 5000, 0, 200000, N'Apr', 2016, 1, N'51', N'-', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (24, N'8', N'Member PRD', N'Fee', NULL, NULL, N'', 71, 1000, 0, 71000, N'Jun', 2016, 1, N'51', N'-', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (25, N'10', N'Member PRD', N'Printing of Documents', NULL, NULL, N'', 0, 11000, 0, 374000, N'Jan', 2016, 1, N'46', N'Print and Bind (Commercial)', N'', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'34', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (26, N'10', N'Member PRD', N'Expense Item', NULL, NULL, N'-', 0, 6660, 0, 333000, N'Feb', 2016, 1, N'44', N'Tagaytay', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (27, N'10', N'Member PRD', N'EXP', NULL, NULL, N'-', 0, 8950, 0, 179000, N'Mar', 2016, 1, N'52', N'Charge Expense and Maintenance', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'50')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (28, N'11', N'Angelo Aduana', N'Travel', '20160101', '20160108', N'Extra Bill', 10, 3800, 1500, 50000, N'Jan', 2016, 1, N'1', N'AH-Elsewhere-Extra Bill', N'8', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'25')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (30, N'13', N'Angelo Aduana', N'Ballpen Restock', NULL, NULL, N'-', 0, 1000, 0, 20000, N'Jan', 2016, 1, N'4', N'Office Items', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'25')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (31, N'15', N'Angelo Aduana', N'REP', NULL, NULL, N'-', 0, 102000, 0, 102000, N'Jan', 2016, 1, N'44', N'Subic', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'25')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (32, N'17', N'Angelo Aduana', N'Payment', NULL, NULL, N'-', 0, 600, 0, 15000, N'Jan', 2016, 1, N'11', N'Smart Communications', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'26')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (33, N'17', N'Angelo Aduana', N'Internet Fee', NULL, NULL, N'-', 0, 384000, 0, 384000, N'Feb', 2016, 1, N'13', N'PLDT Internet', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'26')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (29, N'11', N'Angelo Aduana', N'Plane', '20160201', '20160201', N'Korea', 1, 60000, 0, 60000, N'Feb', 2016, 1, N'2', N' --Korea', N'1', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'25')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (44, N'3', N'Member PRD', N'may', NULL, NULL, N'-', 0, 330723, 0, 330723, N'May', 2016, 1, N'62', N'PS-Salary 5', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (45, N'3', N'Member PRD', N'jun', NULL, NULL, N'-', 0, 330723, 0, 330723, N'Jun', 2016, 1, N'62', N'PS-Salary 6', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (40, N'3', N'Member PRD', N'Jan', NULL, NULL, N'-', 0, 252293, 0, 252293, N'Jan', 2016, 0, N'62', N'PS-Salary 1', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'Submitted', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (47, N'1', N'Member PRD', N'jan', NULL, NULL, N'-', 0, 47000, 0, 47000, N'Jan', 2016, 1, N'64', N'REPJAN', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (42, N'3', N'Member PRD', N'mar', NULL, NULL, N'-', 0, 366818.12, 0, 366818.12, N'Mar', 2016, 1, N'62', N'PS-Salary 3', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (43, N'3', N'Member PRD', N'apr', NULL, NULL, N'-', 0, 360333.25, 0, 360333.25, N'Apr', 2016, 1, N'62', N'PS-Salary 4', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (46, N'3', N'Member PRD', N'Feb', NULL, NULL, N'-', 0, 269312.78, 0, 269312.78, N'Feb', 2016, 1, N'62', N'PS-Salary 2', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (48, N'1', N'Member PRD', N'feb', NULL, NULL, N'-', 0, 23500, 0, 23500, N'Feb', 2016, 1, N'64', N'REPFEB', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (49, N'1', N'Member PRD', N'apr', NULL, NULL, N'-', 0, 23500, 0, 23500, N'Apr', 2016, 1, N'64', N'REPMARAPR', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (50, N'1', N'Member PRD', N'may', NULL, NULL, N'-', 0, 47000, 0, 47000, N'May', 2016, 1, N'64', N'REPMAY', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (51, N'1', N'Member PRD', N'jan', NULL, NULL, N'-', 0, 12000, 0, 12000, N'Jan', 2016, 1, N'63', N'PERAJAN', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (52, N'1', N'Member PRD', N'feb', NULL, NULL, N'-', 0, 15273, 0, 15273, N'Feb', 2016, 1, N'63', N'PERAFEB', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (53, N'1', N'Member PRD', N'mar', NULL, NULL, N'-', 0, 14000, 0, 14000, N'Mar', 2016, 1, N'63', N'PERAMAR', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (54, N'1', N'Member PRD', N'apr', NULL, NULL, N'-', 0, 19091, 0, 19091, N'Apr', 2016, 1, N'63', N'PERAAPR', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (55, N'1', N'Member PRD', N'may', NULL, NULL, N'-', 0, 16000, 0, 16000, N'May', 2016, 1, N'63', N'PERAMAY', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (56, N'1', N'Member PRD', N'jun', NULL, NULL, N'-', 0, 16000, 0, 16000, N'Jun', 2016, 1, N'63', N'PERAJUN', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (57, N'1', N'Member PRD', N'jan', NULL, NULL, N'-', 0, 47000, 0, 47000, N'Jan', 2016, 1, N'65', N'TRANSPOJAN', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (58, N'1', N'Member PRD', N'feb', NULL, NULL, N'-', 0, 23500, 0, 23500, N'Feb', 2016, 1, N'65', N'TRANSPOFEB', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (59, N'1', N'Member PRD', N'apr', NULL, NULL, N'-', 0, 23500, 0, 23500, N'Apr', 2016, 1, N'65', N'TRANSPOAPR', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (60, N'1', N'Member PRD', N'may', NULL, NULL, N'-', 0, 47000, 0, 47000, N'May', 2016, 1, N'65', N'TRANSPOMAY', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

INSERT INTO dbo.mnda_activity_data (id, activity_id, accountable_id, remarks, start, [end], destination, no_staff, rate, travel_allowance, total, month, year, is_approved, mooe_sub_expenditure_id, type_service, no_days, stat_1, stat_2, stat_3, stat_4, stat_5, stat_6, procurement_id, accountable_name, quantity, entry_date, status, is_suspended, is_submitted, fund_source_id)
VALUES 
  (61, N'1', N'Member PRD', N'Cloth June', NULL, NULL, N'-', 0, 30000, 0, 30000, N'Jun', 2016, 1, N'66', N'CLOTHALLOW', N'0', N'-', N'-', N'-', N'-', N'-', N'-', N'0', N'-', N'1', NULL, N'For Approval By Finance', 0, 1, N'42')
GO

SET IDENTITY_INSERT dbo.mnda_activity_data OFF
GO

--
-- Data for table dbo.mnda_alignment_record  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_alignment_record ON
GO

INSERT INTO dbo.mnda_alignment_record (id, from_uacs, from_total, to_uacs, total_alignment, division_pap, division_year, is_approved, months, fundsource)
VALUES 
  (10, N'5020101000', N'42330', N'5020399000', N'10000', N'5', N'2016', 0, N'Jan', N'42')
GO

INSERT INTO dbo.mnda_alignment_record (id, from_uacs, from_total, to_uacs, total_alignment, division_pap, division_year, is_approved, months, fundsource)
VALUES 
  (8, N'5021003000', N'37768', N'5021103000', N'7000', N'5', N'2016', 0, N'Aug', N'43')
GO

SET IDENTITY_INSERT dbo.mnda_alignment_record OFF
GO

--
-- Data for table dbo.mnda_approved_projects_division  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_approved_projects_division ON
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (1, N'1', N'1', N'1', N'3', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (2, N'1', N'1', N'1', N'4', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (3, N'2', N'2', N'2', N'5', 1, N'4', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (4, N'3', N'3', N'3', N'6', 1, N'5', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (5, N'3', N'3', N'3', N'7', 1, N'5', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (6, N'3', N'3', N'3', N'8', 1, N'5', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (7, N'4', N'4', N'4', N'9', 1, N'6', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (8, N'4', N'4', N'4', N'10', 1, N'6', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (11, N'4', N'4', N'4', N'13', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (12, N'4', N'4', N'4', N'14', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (13, N'4', N'4', N'4', N'15', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (14, N'4', N'4', N'4', N'16', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (15, N'4', N'4', N'4', N'17', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (16, N'4', N'4', N'4', N'18', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (17, N'4', N'4', N'4', N'22', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (18, N'4', N'4', N'4', N'23', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (19, N'4', N'4', N'4', N'24', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (20, N'4', N'4', N'4', N'25', 1, N'10', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (21, N'4', N'4', N'4', N'26', 1, N'10', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (22, N'4', N'4', N'4', N'27', 1, N'10', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (23, N'4', N'4', N'4', N'20', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (24, N'4', N'4', N'4', N'19', 1, N'8', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (25, N'5', N'5', N'5', N'28', 1, N'11', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (26, N'5', N'5', N'5', N'30', 1, N'13', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (27, N'5', N'5', N'5', N'31', 1, N'15', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (28, N'5', N'5', N'5', N'29', 1, N'11', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (29, N'6', N'6', N'6', N'33', 1, N'17', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (30, N'6', N'6', N'6', N'32', 1, N'17', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (9, N'4', N'4', N'4', N'11', 1, N'6', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (10, N'4', N'4', N'4', N'12', 1, N'6', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (31, N'1', N'1', N'1', N'34', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (32, N'1', N'1', N'1', N'35', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (33, N'1', N'1', N'1', N'36', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (34, N'1', N'1', N'1', N'37', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (35, N'1', N'1', N'1', N'38', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (36, N'1', N'1', N'1', N'39', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (37, N'1', N'1', N'1', N'46', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (38, N'1', N'1', N'1', N'42', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (39, N'1', N'1', N'1', N'43', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (40, N'1', N'1', N'1', N'44', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (41, N'1', N'1', N'1', N'45', 1, N'3', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (42, N'1', N'1', N'1', N'47', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (43, N'1', N'1', N'1', N'48', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (44, N'1', N'1', N'1', N'49', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (45, N'1', N'1', N'1', N'50', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (46, N'1', N'1', N'1', N'51', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (47, N'1', N'1', N'1', N'52', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (52, N'1', N'1', N'1', N'57', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (53, N'1', N'1', N'1', N'58', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (54, N'1', N'1', N'1', N'59', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (55, N'1', N'1', N'1', N'60', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (56, N'1', N'1', N'1', N'61', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (57, N'1', N'1', N'1', N'62', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (58, N'1', N'1', N'1', N'63', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (48, N'1', N'1', N'1', N'53', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (49, N'1', N'1', N'1', N'54', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (50, N'1', N'1', N'1', N'55', 1, N'1', 0)
GO

INSERT INTO dbo.mnda_approved_projects_division (id, program_id, project_id, output_id, activity_id, isapproved, main_activity_id, is_submitted)
VALUES 
  (51, N'1', N'1', N'1', N'56', 1, N'1', 0)
GO

SET IDENTITY_INSERT dbo.mnda_approved_projects_division OFF
GO

--
-- Data for table dbo.mnda_budget_allocation  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_budget_allocation ON
GO

INSERT INTO dbo.mnda_budget_allocation (id, office_id, division_id, budget_allocation, status, entry_year)
VALUES 
  (6, 2, N'6', 2000000, 1, 2016)
GO

INSERT INTO dbo.mnda_budget_allocation (id, office_id, division_id, budget_allocation, status, entry_year)
VALUES 
  (7, 2, N'5', 5000000, 1, 2016)
GO

INSERT INTO dbo.mnda_budget_allocation (id, office_id, division_id, budget_allocation, status, entry_year)
VALUES 
  (8, 2, N'8', 1000000, 1, 2016)
GO

SET IDENTITY_INSERT dbo.mnda_budget_allocation OFF
GO

--
-- Data for table dbo.mnda_budget_expenditure  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_budget_expenditure ON
GO

INSERT INTO dbo.mnda_budget_expenditure (id, office_id, expenditure_id, allocated_budget, year)
VALUES 
  (1, N'3', N'-', 1000000, 2016)
GO

INSERT INTO dbo.mnda_budget_expenditure (id, office_id, expenditure_id, allocated_budget, year)
VALUES 
  (2, N'4', N'-', 1000000, 2016)
GO

INSERT INTO dbo.mnda_budget_expenditure (id, office_id, expenditure_id, allocated_budget, year)
VALUES 
  (3, N'4', N'-', 20000, 2018)
GO

INSERT INTO dbo.mnda_budget_expenditure (id, office_id, expenditure_id, allocated_budget, year)
VALUES 
  (4, N'5', N'-', 30000, 2016)
GO

SET IDENTITY_INSERT dbo.mnda_budget_expenditure OFF
GO

--
-- Data for table dbo.mnda_category  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_category ON
GO

INSERT INTO dbo.mnda_category (id, name, weight)
VALUES 
  (1, N'Core Function', 0.2)
GO

INSERT INTO dbo.mnda_category (id, name, weight)
VALUES 
  (2, N'Strategic Priority', 0.7)
GO

INSERT INTO dbo.mnda_category (id, name, weight)
VALUES 
  (3, N'Support Function', 0.1)
GO

SET IDENTITY_INSERT dbo.mnda_category OFF
GO

--
-- Data for table dbo.mnda_expenditures_library  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_expenditures_library ON
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (1, N'1', N'Plane Fare (Local)', N'TDP0', N'-', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (2, N'1', N'Plane Fare (Local)', N'TDP1', N'Baguio', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (3, N'1', N'Plane Fare (Local)', N'TDP2', N'Cagayan De Oro', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (4, N'1', N'Plane Fare (Local)', N'TDP3', N'Cebu', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (5, N'1', N'Plane Fare (Local)', N'TDP4', N'Davao', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (6, N'1', N'Plane Fare (Local)', N'TDP5', N'Manila', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (7, N'1', N'Plane Fare (Local)', N'TDP6', N'Others', N'0', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (8, N'1', N'Plane Fare (Local)', N'TDP7', N'Others (MinDA Land Travel)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (9, N'1', N'Plane Fare (Local)', N'TDP8', N'Puerto Prinsesa', N'0', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (10, N'1', N'Plane Fare (Local)', N'TDP9', N'Subic', N'0', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (11, N'1', N'Plane Fare (Local)', N'TDP10', N'Tagaytay', N'0', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (12, N'40', N'Daily Subsistence Allowance (Local)', N'-', N'-', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (13, N'40', N'Daily Subsistence Allowance (Local)', N'AT1', N'AH-Elsewhere', N'0', 1500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (14, N'40', N'Daily Subsistence Allowance (Local)', N'AT2', N'AH-Elsewhere+Per Diem', N'0', 1900, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (15, N'40', N'Daily Subsistence Allowance (Local)', N'AT3', N'AH-Major Cities', N'0', 2300, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (16, N'40', N'Daily Subsistence Allowance (Local)', N'AT4', N'AH-Major Cities+Per Diem', N'0', 2700, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (17, N'40', N'Daily Subsistence Allowance (Local)', N'AT5', N'AH-Metro Manila', N'0', 2500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (18, N'40', N'Daily Subsistence Allowance (Local)', N'AT6', N'AH-Metro Manila+Per Diem', N'0', 2900, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (19, N'40', N'Daily Subsistence Allowance (Local)', N'AT7', N'Full DSA', N'0', 800, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (20, N'40', N'Daily Subsistence Allowance (Local)', N'AT8', N'Per Diem Only', N'0', 400, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (21, N'0', N'Terminal Ticket Rates', N'TTR0', N'-', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (22, N'0', N'Terminal Ticket Rates', N'TTR1', N'Metro Manila and Davao', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (23, N'0', N'Terminal Ticket Rates', N'TTR2', N'Cagayan de Oro', N'0', 30, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (24, N'0', N'Terminal Ticket Rates', N'TTR3', N'General Santos, Zambo, Palawan)', N'0', 40, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (25, N'0', N'Terminal Ticket Rates', N'TTR4', N'From Foreign Country Back to the Phils', N'0', 750, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (26, N'0', N'Terminal Ticket Rates', N'TTR5', N'From Davao to Foreign Country', N'0', 950, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (27, N'0', N'Transportation Allowance (Local)', N'TA1', N'Local Transportation Allowance', N'0', 1200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (28, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA0', N'-', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (29, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA1', N'Bislig', N'1D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (30, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA2', N'Bukidnon', N'1D', 2500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (31, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA3', N'Butuan City', N'1D', 4500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (32, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA4', N'Cagayan de Oro City', N'1D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (33, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA5', N'Compostela Valley', N'1D', 2000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (34, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA6', N'Cotabato City', N'1D', 2000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (35, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA7', N'Digos', N'1D', 1000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (36, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA8', N'Dipolog City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (37, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA9', N'General Santos City', N'1D', 2500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (38, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA10', N'Iligan City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (39, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA11', N'Ipil', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (40, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA12', N'Marawi City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (41, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA13', N'Mati', N'1D', 2500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (42, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA14', N'Others', N'1D', 1500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (43, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA15', N'Ozamis City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (44, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA16', N'Pagadian City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (45, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA17', N'Surigao City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (46, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA18', N'Tagum City', N'1D', 1000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (47, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA19', N'Tandag City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (48, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA20', N'Zamboanga City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (49, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA0', N'0', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (50, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA1', N'Bislig', N'2-3D', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (51, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA2', N'Bukidnon', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (52, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA3', N'Butuan City', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (53, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA4', N'Cagayan de Oro City', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (54, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA5', N'Compostela Valley', N'2-3D', 3500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (55, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA6', N'Cotabato City', N'2-3D', 4000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (56, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA7', N'Digos', N'2-3D', 3000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (57, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA8', N'Dipolog City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (58, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA9', N'General Santos City', N'2-3D', 3500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (59, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA10', N'Iligan City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (60, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA11', N'Ipil', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (61, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA12', N'Marawi City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (62, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA13', N'Mati', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (63, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA14', N'Others', N'2-3D', 2500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (64, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA15', N'Ozamis City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (65, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA16', N'Pagadian City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (66, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA17', N'Surigao City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (67, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA18', N'Tagum City', N'2-3D', 3000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (68, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA19', N'Tandag City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (69, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA20', N'Zamboanga City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (70, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA0', N'0', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (71, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA1', N'Bislig', N'4D-MORE', 12000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (72, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA2', N'Bukidnon', N'4D-MORE', 7000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (73, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA3', N'Butuan City', N'4D-MORE', 7000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (74, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA4', N'Cagayan de Oro City', N'4D-MORE', 7000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (75, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA5', N'Compostela Valley', N'4D-MORE', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (76, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA6', N'Cotabato City', N'4D-MORE', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (77, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA7', N'Digos', N'4D-MORE', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (78, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA8', N'Dipolog City', N'4D-MORE', 14000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (79, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA9', N'General Santos City', N'4D-MORE', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (80, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA10', N'Iligan City', N'4D-MORE', 9000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (81, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA11', N'Ipil', N'4D-MORE', 14000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (82, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA12', N'Marawi City', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (83, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA13', N'Mati', N'4D-MORE', 7000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (84, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA14', N'Others', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (85, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA15', N'Ozamis City', N'4D-MORE', 12500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (86, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA16', N'Pagadian City', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (87, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA17', N'Surigao City', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (88, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA18', N'Tagum City', N'4D-MORE', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (89, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA19', N'Tandag City', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (90, N'6', N'Gasoline Allowance (Meetings/Seminar/Trainings)', N'GA20', N'Zamboanga City', N'4D-MORE', 14000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (91, N'0', N'Gasoline Allowance (Montoring)', N'GA0', N'0', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (92, N'0', N'Gasoline Allowance (Montoring)', N'GA1', N'Bislig', N'1D', 9000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (93, N'0', N'Gasoline Allowance (Montoring)', N'GA2', N'Bukidnon', N'1D', 4500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (94, N'0', N'Gasoline Allowance (Montoring)', N'GA3', N'Butuan City', N'1D', 4000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (95, N'0', N'Gasoline Allowance (Montoring)', N'GA4', N'Cagayan de Oro City', N'1D', 4000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (96, N'0', N'Gasoline Allowance (Montoring)', N'GA5', N'Compostela Valley', N'1D', 3500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (97, N'0', N'Gasoline Allowance (Montoring)', N'GA6', N'Cotabato City', N'1D', 3500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (98, N'0', N'Gasoline Allowance (Montoring)', N'GA7', N'Digos', N'1D', 3000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (99, N'0', N'Gasoline Allowance (Montoring)', N'GA8', N'Dipolog City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (100, N'0', N'Gasoline Allowance (Montoring)', N'GA9', N'General Santos City', N'1D', 3500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (101, N'0', N'Gasoline Allowance (Montoring)', N'GA10', N'Iligan City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (102, N'0', N'Gasoline Allowance (Montoring)', N'GA11', N'Ipil', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (103, N'0', N'Gasoline Allowance (Montoring)', N'GA12', N'Marawi City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (104, N'0', N'Gasoline Allowance (Montoring)', N'GA13', N'Mati', N'1D', 4000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (105, N'0', N'Gasoline Allowance (Montoring)', N'GA14', N'Others', N'1D', 2000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (106, N'0', N'Gasoline Allowance (Montoring)', N'GA15', N'Ozamis City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (107, N'0', N'Gasoline Allowance (Montoring)', N'GA16', N'Pagadian City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (108, N'0', N'Gasoline Allowance (Montoring)', N'GA17', N'Surigao City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (109, N'0', N'Gasoline Allowance (Montoring)', N'GA18', N'Tagum City', N'1D', 3000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (110, N'0', N'Gasoline Allowance (Montoring)', N'GA19', N'Tandag City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (111, N'0', N'Gasoline Allowance (Montoring)', N'GA20', N'Zamboanga City', N'1D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (112, N'0', N'Gasoline Allowance (Montoring)', N'GA0', N'0', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (113, N'0', N'Gasoline Allowance (Montoring)', N'GA1', N'Bislig', N'2-3D', 11000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (114, N'0', N'Gasoline Allowance (Montoring)', N'GA2', N'Bukidnon', N'2-3D', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (115, N'0', N'Gasoline Allowance (Montoring)', N'GA3', N'Butuan City', N'2-3D', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (116, N'0', N'Gasoline Allowance (Montoring)', N'GA4', N'Cagayan de Oro City', N'2-3D', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (117, N'0', N'Gasoline Allowance (Montoring)', N'GA5', N'Compostela Valley', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (118, N'0', N'Gasoline Allowance (Montoring)', N'GA6', N'Cotabato City', N'2-3D', 5500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (119, N'0', N'Gasoline Allowance (Montoring)', N'GA7', N'Digos', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (120, N'0', N'Gasoline Allowance (Montoring)', N'GA8', N'Dipolog City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (121, N'0', N'Gasoline Allowance (Montoring)', N'GA9', N'General Santos City', N'2-3D', 5500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (122, N'0', N'Gasoline Allowance (Montoring)', N'GA10', N'Iligan City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (123, N'0', N'Gasoline Allowance (Montoring)', N'GA11', N'Ipil', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (124, N'0', N'Gasoline Allowance (Montoring)', N'GA12', N'Marawi City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (125, N'0', N'Gasoline Allowance (Montoring)', N'GA13', N'Mati', N'2-3D', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (126, N'0', N'Gasoline Allowance (Montoring)', N'GA14', N'Others', N'2-3D', 3000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (127, N'0', N'Gasoline Allowance (Montoring)', N'GA15', N'Ozamis City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (128, N'0', N'Gasoline Allowance (Montoring)', N'GA16', N'Pagadian City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (129, N'0', N'Gasoline Allowance (Montoring)', N'GA17', N'Surigao City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (130, N'0', N'Gasoline Allowance (Montoring)', N'GA18', N'Tagum City', N'2-3D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (131, N'0', N'Gasoline Allowance (Montoring)', N'GA19', N'Tandag City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (132, N'0', N'Gasoline Allowance (Montoring)', N'GA20', N'Zamboanga City', N'2-3D', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (133, N'0', N'Gasoline Allowance (Montoring)', N'GA0', N'0', N'4D-MORE', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (134, N'0', N'Gasoline Allowance (Montoring)', N'GA1', N'Bislig', N'4D-MORE', 13000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (135, N'0', N'Gasoline Allowance (Montoring)', N'GA2', N'Bukidnon', N'4D-MORE', 8000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (136, N'0', N'Gasoline Allowance (Montoring)', N'GA3', N'Butuan City', N'4D-MORE', 9000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (137, N'0', N'Gasoline Allowance (Montoring)', N'GA4', N'Cagayan de Oro City', N'4D-MORE', 9000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (138, N'0', N'Gasoline Allowance (Montoring)', N'GA5', N'Compostela Valley', N'4D-MORE', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (139, N'0', N'Gasoline Allowance (Montoring)', N'GA6', N'Cotabato City', N'4D-MORE', 8000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (140, N'0', N'Gasoline Allowance (Montoring)', N'GA7', N'Digos', N'4D-MORE', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (141, N'0', N'Gasoline Allowance (Montoring)', N'GA8', N'Dipolog City', N'4D-MORE', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (142, N'0', N'Gasoline Allowance (Montoring)', N'GA9', N'General Santos City', N'4D-MORE', 8000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (143, N'0', N'Gasoline Allowance (Montoring)', N'GA10', N'Iligan City', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (144, N'0', N'Gasoline Allowance (Montoring)', N'GA11', N'Ipil', N'4D-MORE', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (145, N'0', N'Gasoline Allowance (Montoring)', N'GA12', N'Marawi City', N'4D-MORE', 11500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (146, N'0', N'Gasoline Allowance (Montoring)', N'GA13', N'Mati', N'4D-MORE', 8000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (147, N'0', N'Gasoline Allowance (Montoring)', N'GA14', N'Others', N'4D-MORE', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (148, N'0', N'Gasoline Allowance (Montoring)', N'GA15', N'Ozamis City', N'4D-MORE', 14000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (149, N'0', N'Gasoline Allowance (Montoring)', N'GA16', N'Pagadian City', N'4D-MORE', 12000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (150, N'0', N'Gasoline Allowance (Montoring)', N'GA17', N'Surigao City', N'4D-MORE', 12000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (151, N'0', N'Gasoline Allowance (Montoring)', N'GA18', N'Tagum City', N'4D-MORE', 6000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (152, N'0', N'Gasoline Allowance (Montoring)', N'GA19', N'Tandag City', N'4D-MORE', 12000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (153, N'0', N'Gasoline Allowance (Montoring)', N'GA20', N'Zamboanga City', N'4D-MORE', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (154, N'42', N'Gasoline Expense Type', N'GET0', N'0', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (155, N'42', N'Gasoline Expense Type', N'GET1', N'Meeting/Trainings/Seminar', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (156, N'42', N'Gasoline Expense Type', N'GET2', N'Monitoring', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (157, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (158, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH1', N'Baguio', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (159, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH2', N'Cagayan De Oro', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (160, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH3', N'Cebu', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (161, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH4', N'Davao', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (162, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH5', N'Manila', N'0', 250, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (163, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH6', N'Others', N'0', 150, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (164, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH7', N'Subic', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (165, N'44', N'AM/PM  Snacks Rate (Hotel)', N'AMSH8', N'Tagaytay', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (166, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (167, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH1', N'Baguio', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (168, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH2', N'Cagayan De Oro', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (169, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH3', N'Cebu', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (170, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH4', N'Davao', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (171, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH5', N'Manila', N'0', 500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (172, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH6', N'Others', N'0', 300, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (173, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH7', N'Subic', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (174, N'44', N'Breakfast/Lunch/Dinner Rate (Hotel)', N'LRH8', N'Tagaytay', N'0', 350, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (175, N'0', N'Meal Service Type Reference', N'MTR0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (176, N'0', N'Meal Service Type Reference', N'MTR1', N'Hotel', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (177, N'0', N'Meal Service Type Reference', N'MTR2', N'Food Caterer', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (178, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (179, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF1', N'Baguio', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (180, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF2', N'Cagayan De Oro', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (181, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF3', N'Cebu', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (182, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF4', N'Davao', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (183, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF5', N'Manila', N'0', 150, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (184, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF6', N'Others', N'0', 80, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (185, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF7', N'Subic', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (186, N'44', N'AM/PM Snacks Rate (Food Caterer)', N'AMSF8', N'Tagaytay', N'0', 100, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (187, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (188, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF1', N'Baguio', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (189, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF2', N'Cagayan De Oro', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (190, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF3', N'Cebu', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (191, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF4', N'Davao', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (192, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF5', N'Manila', N'0', 300, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (193, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF6', N'Others', N'0', 150, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (194, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF7', N'Subic', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (195, N'44', N'Breakfast/Lunch/Dinner Rate (Food Caterer)', N'LRF8', N'Tagaytay', N'0', 200, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (196, N'0', N'Honorarium Rate per Day', N'HR0', N'', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (197, N'45', N'Honorarium Rate per Day', N'HR1', N'Documenter', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (198, N'45', N'Honorarium Rate per Day', N'HR2', N'Expert', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (199, N'45', N'Honorarium Rate per Day', N'HR3', N'JO-Admin Assistant I', N'0', 549.14, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (200, N'45', N'Honorarium Rate per Day', N'HR4', N'JO-Admin Assistant II', N'0', 641.86, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (201, N'45', N'Honorarium Rate per Day', N'HR5', N'JO-DMO I', N'0', 711.58, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (202, N'45', N'Honorarium Rate per Day', N'HR6', N'JO-DMO III', N'0', 857.54, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (203, N'45', N'Honorarium Rate per Day', N'HR7', N'JO-Driver', N'0', 490.39, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (204, N'45', N'Honorarium Rate per Day', N'HR8', N'JO-EDS I', N'0', 778.31, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (205, N'45', N'Honorarium Rate per Day', N'HR9', N'JO-EDS II', N'0', 900.77, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (206, N'45', N'Honorarium Rate per Day', N'HR10', N'Technical Advisor-M2020', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (207, N'45', N'Honorarium Rate per Day', N'HR11', N'Technical Advisor-Office of the Chairperson', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (208, N'45', N'Honorarium Rate per Day', N'HR12', N'Technical Staff-MPMC', N'0', 1179.87, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (209, N'45', N'Honorarium Rate per Day', N'HR13', N'Technical Staff-NOW', N'0', 1179.87, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (210, N'2', N'Plane Fare (International)', N'TIP0', N'-', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (211, N'2', N'Plane Fare (International)', N'TIP1', N'Brunei Darussalam', N'-', 36000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (212, N'2', N'Plane Fare (International)', N'TIP2', N'Indonesia', N'-', 45000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (213, N'2', N'Plane Fare (International)', N'TIP3', N'Malaysia', N'-', 40000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (214, N'2', N'Plane Fare (International)', N'TIP4', N'Others', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (215, N'41', N'Daily Subsistence Allowance (International)', N'IDAS0', N'-', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (216, N'41', N'Daily Subsistence Allowance (International)', N'IDAS1', N'Brunei Darussalam', N'-', 226, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (217, N'41', N'Daily Subsistence Allowance (International)', N'IDAS2', N'Indonesia', N'-', 236, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (218, N'41', N'Daily Subsistence Allowance (International)', N'IDAS3', N'Malaysia', N'-', 239, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (219, N'41', N'Daily Subsistence Allowance (International)', N'IDAS4', N'Others', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (220, N'41', N'Daily Subsistence Allowance (International)', N'OTFA0', N' -', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (221, N'41', N'One Time Fixed Pre-Travel  Allowance', N'OTFA1', N' 1,500.00', N'-', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (222, N'1', N'Plane Fare (Local)', N'TDP11', N'Tagbilaran', N'1D', 15500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (223, N'40', N'Daily Subsistence Allowance (Local)', N'AT-9', N'AH-BIGTIME', N'-', 50000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (225, N'46', N'Printing and Binding', N'PBT1', N'Others', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (226, N'46', N'Printing and Binding', N'PBT2', N'Print and Bind (Commercial)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (227, N'46', N'Printing and Binding', N'PBT3', N'Print and Bind (Photocopy)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (228, N'46', N'Printing and Binding', N'PBT4', N'Printing (Commercial)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (229, N'46', N'Printing and Binding', N'PBT5', N'Printing (Photocopy)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (230, N'47', N'Subscription Type', N'ST3', N'Monthly', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (231, N'47', N'Subscription Type', N'ST4', N'Quarterly', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (232, N'47', N'Subscription Type', N'ST2', N'Semi-Annual', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (233, N'47', N'Subscription Type', N'ST1', N'Annual', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (235, N'48', N'Subscription Type', N'SB1', N'Monthly', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (236, N'48', N'Subscription Type', N'SB2', N'Quarterly', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (237, N'48', N'Subscription Type', N'SB3', N'Semi-Annual', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (238, N'48', N'Subscription Type', N'SB4', N'Annual', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (239, N'49', N'Material Type
', N'MT1', N'Periodicals', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (240, N'49', N'Material Type', N'MT2', N'Magazine', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (241, N'49', N'Material Type', N'MT3', N'Publications', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (242, N'49', N'Material Type', N'MT4', N'Books', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (243, N'49', N'Material Type', N'MT5', N'Others', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (244, N'3', N'Training Fees', N'TF1', N'Elsewhere', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (245, N'3', N'Training Fees', N'TF2', N'Elsewhere (w/ Actual Hotel)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (246, N'3', N'Training Fees', N'TF3', N'Major Cities', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (247, N'3', N'Training Fees', N'TF4', N'Major Cities (w/ Actual Hotel)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (248, N'3', N'Training Fees', N'TF5', N'Metro Manila', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (249, N'3', N'Training Fees', N'TF6', N'Metro Manila (w/ Actual Hotel)', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (250, N'3', N'Training Fees', N'TF7', N'MinDA-Initiated Training', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (251, N'50', N'Rental Fees', N'RF1', N'Car', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (252, N'50', N'Rental Fees', N'RF2', N'Computer', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (253, N'50', N'Rental Fees', N'RF3', N'Equipment Rental', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (254, N'50', N'Rental Fees', N'RF4', N'Office Space', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (255, N'50', N'Rental Fees', N'RF5', N'Photocopying Machine', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (256, N'50', N'Rental Fees', N'RF6', N'Truck', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (257, N'50', N'Rental Fees', N'RF7', N'Van', N'0', 0, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (224, N'1', N'Plane Fare (Local)', N'TDP12', N'Gensan', N'-', 15000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (258, N'45', N'Honorarium Rate Dev', N'34', N'KMD-DEV', N'-', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (259, N'53', N'PP', N'1', N'1P', N'-', 0.5, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (260, N'54', N'NONE', N'1RG', N'ROG ITEM SP 1', N'-', 1500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (261, N'54', N'NONE', N'2RG', N'ROG ITEM XP 2', N'-', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (262, N'55', N'N TYPE', N'AJS1', N'REGULAR SERVICE', N'1D', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (263, N'58', N'Studio Type', N'BD1', N'Building Cost 1', N'-', 15000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (264, N'58', N'Concrete Type', N'BD2', N'Building Cost 2', N'-', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (265, N'59', N'MINDA', N'T-1', N'Core 1', N'-', 500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (266, N'60', N'-', N'001', N'Gas', N'-', 2015, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (267, N'61', N'222', N'222', N'222', N'-', 500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (268, N'1', N'Plane Fare (Local)', N'TDP13', N'Sorsogon', N'1D', 8500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (269, N'11', N'Smart', N'TLP-01', N'Smart Communications', N'-', 600, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (270, N'11', N'Globe Lines', N'TLP-02', N'Globe', N'-', 1500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (271, N'15', N'E1', N'EME-1', N'Expense Item 1', N'-', 10000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (272, N'15', N'E2', N'EME-2', N'Expense Item 2', N'-', 4000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (273, N'1', N'Plane Fare (Local)', N'TDP14', N'Extra', N'1D', 5300, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (274, N'1', N'Plane Fare (Local)', N'TDP15', N'Extra Bill', N'-', 3800, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (275, N'4', N'OI', N'OFS-1', N'Office Items', N'-', 1000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (276, N'13', N'IE', N'IE-1', N'PLDT Internet', N'-', 1500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (278, N'17', N'JS-Fee', N'CS-2', N'Janitorial Service', N'-', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (279, N'18', N'OPS', N'OPS-1', N'OPS Service', N'-', 268000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (280, N'18', N'OS', N'OPS2', N'Other Services', N'-', 5000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (281, N'52', N'CME-1', N'ITM01', N'Charge Expense and Maintenance', N'-', 8950, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (282, N'2', N'Plane Fare (International)', N'TIP5', N'Korea', N'-', 50000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (277, N'17', N'CF-Fee', N'CS-1', N'Consultant Fee', N'-', 25000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (283, N'6', N'GAS-1', N'GA21', N'Gas Expense 1', N'1D', 3314.85, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (284, N'6', N'GAS02', N'GA-22', N'Gas Expense 2', N'1D', 1181.11, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (285, N'6', N'GAS03', N'GA-23', N'Gas Expense 3', N'1D', 575.95, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (286, N'7', N'OSE1', N'ITM1', N'Expense Items 1', N'-', 2396, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (287, N'44', N'SNK', N'ITM1', N'Test Rep', N'-', 395, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (288, N'52', N'ITM02', N'CME-2', N'Charge', N'-', 8050, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (289, N'62', N'BSJAN', N'ITM-01', N'PS-Salary 1', N'-', 252293, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (290, N'62', N'BSFEB', N'ITM-02', N'PS-Salary 2', N'-', 269312.78, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (291, N'62', N'BSMAR', N'ITM-03', N'PS-Salary 3', N'-', 366818.12, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (292, N'62', N'BSAPR', N'ITM-04', N'PS-Salary 4', N'-', 360333.25, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (293, N'62', N'BSMAY', N'ITM-05', N'PS-Salary 5', N'-', 330723, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (294, N'62', N'BSJUN', N'ITM-06', N'PS-Salary 6', N'-', 330723, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (295, N'64', N'REPALLJAN', N'ITM-01', N'REPJAN', N'-', 47000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (296, N'64', N'REPALLFEB', N'ITM-02', N'REPFEB', N'-', 23500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (297, N'64', N'REPALLAPR', N'ITM-03', N'REPMARAPR', N'-', 23500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (298, N'64', N'REPALLMAY', N'ITM-04', N'REPMAY', N'-', 47000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (299, N'63', N'PERA1', N'ITM-1', N'PERAJAN', N'-', 12000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (300, N'63', N'PERA2', N'ITM-2', N'PERAFEB', N'-', 15273, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (301, N'63', N'PERA3', N'ITM-3', N'PERAMAR', N'-', 14000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (302, N'63', N'PERA4', N'ITM-4', N'PERAAPR', N'-', 19091, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (303, N'63', N'PERA5', N'ITM-5', N'PERAMAY', N'-', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (304, N'63', N'ITM-6', N'ITM-6', N'PERAJUN', N'-', 16000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (305, N'65', N'TRANSALL1', N'ITM-01', N'TRANSPOJAN', N'-', 47000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (306, N'65', N'TRANSALL2', N'ITM-02', N'TRANSPOFEB', N'-', 23500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (307, N'65', N'', N'ITM-02', N'TRANSPOAPR', N'-', 23500, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (308, N'65', N'', N'ITM-03', N'TRANSPOMAY', N'-', 47000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (309, N'66', N'', N'ITM-01', N'CLOTHALLOW', N'-', 30000, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (310, N'67', N'', N'ITM-1', N'BONUSMAY', N'-', 304531, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (311, N'1', N'', N'TDP16', N'JuneTravel', N'-', 3780, 2016)
GO

INSERT INTO dbo.mnda_expenditures_library (id, sub_expenditure_code, library_type, item_code, item_name, day, rate, rate_year)
VALUES 
  (312, N'1', N'', N'TDP17', N'JulyTravel', N'-', 15206, 2016)
GO

SET IDENTITY_INSERT dbo.mnda_expenditures_library OFF
GO

--
-- Data for table dbo.mnda_fund_source  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_fund_source ON
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (1, N'4', N'AD Fixed Cost', N'AD Fixed Cost', 9655265, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (2, N'4', N'AD Regular', N'AD Regular', 961723, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (3, N'12', N'AMO CM Fixed Cost', N'AMO CM Fixed Cost', 1225300, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (4, N'12', N'AMO CM MPMC', N'AMO CM MPMC', 400000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (5, N'12', N'AMO CM NOW', N'AMO CM NOW', 1107719, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (6, N'12', N'AMO CM Regular', N'AMO CM Regular', 224003, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (7, N'0', N'AMO NEM Fixed Cost', N'AMO NEM Fixed Cost', 1506562, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (8, N'0', N'AMO NEM MPMC', N'AMO NEM MPMC', 400000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (9, N'0', N'AMO NEM NOW', N'AMO NEM NOW', 399746, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (10, N'0', N'AMO NEM Regular', N'AMO NEM Regular', 224003, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (11, N'10', N'AMO NM Fixed Cost', N'AMO NM Fixed Cost', 1484023, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (12, N'10', N'AMO NM MPMC', N'AMO NM MPMC', 400000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (13, N'10', N'AMO NM Regular', N'AMO NM Regular', 224003, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (14, N'10', N'AMO NM-NOW', N'AMO NM-NOW', 399746, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (15, N'9', N'AMO WM Fixed Cost', N'AMO WM Fixed Cost', 1628899, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (16, N'9', N'AMO WM MPMC', N'AMO WM MPMC', 400000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (17, N'9', N'AMO WM Regular', N'AMO WM Regular', 224003, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (18, N'9', N'AMO WM-NOW', N'AMO WM-NOW', 399746, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (19, N'3', N'FD Fixed Cost', N'FD Fixed Cost', 385708, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (20, N'3', N'FD Regular', N'FD Regular', 320574, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (21, N'13', N'IPD Fixed Cost', N'IPD Fixed Cost', 166000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (22, N'13', N'IPD Regular', N'IPD Regular', 641149, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (23, N'13', N'IPD-MPMC', N'IPD-MPMC', 1183027, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (24, N'13', N'IRD Regular', N'IRD Regular', 1909730, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (25, N'6', N'KMD Regular', N'KMD Regular', 534291, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (26, N'6', N'KMD-Fixed Cost', N'KMD-Fixed Cost', 1136113, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (27, N'6', N'KMD-MPMC', N'KMD-MPMC', 962887, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (28, N'6', N'KMD-MPMC', N'KMD-MPMC', 1, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (29, N'6', N'KMD-NOW', N'KMD-NOW', 474758, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (30, N'-1', N'OC Fixed Cost', N'OC Fixed Cost', 4662313, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (31, N'-1', N'OC Regular', N'OC Regular', 1602872, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (32, N'2', N'OED Fixed Cost', N'OED Fixed Cost', 1025465, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (33, N'2', N'OED MPMC', N'OED MPMC', 800000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (34, N'2', N'OED NOW', N'OED NOW', 400000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (35, N'2', N'OED Regular', N'OED Regular', 1375440, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (36, N'8', N'PDD Regular', N'PDD Regular', 320574, 2016, N'n-a', N'1')
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (37, N'8', N'PDD-Fixed Cost', N'PDD-Fixed Cost', 122744, 2016, N'n-a', N'1')
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (38, N'7', N'PFD Regular', N'PFD Regular', 641149, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (39, N'7', N'PFD-Fixed Cost', N'PFD-Fixed Cost', 617900, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (40, N'7', N'PFD-MPMC', N'PFD-MPMC', 635869, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (41, N'7', N'PFD-NOW', N'PFD-NOW', 10000000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (42, N'5', N'PRD Regular', N'PRD Regular', 150000, 2016, N'n-a', N'1')
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (43, N'5', N'PRD-Fixed Cost', N'PRD-Fixed Cost', 526656, 2016, N'n-a', N'1')
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (44, N'5', N'PRD-MPMC', N'PRD-MPMC', 1183482, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (45, N'5', N'PRD-NOW', N'PRD-NOW', 241645, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (46, N'14', N'PuRD NOW', N'PuRD NOW', 387303, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (47, N'14', N'PuRD Regular', N'PuRD Regular', 534291, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (48, N'14', N'PuRD-Fixed Cost', N'PuRD-Fixed Cost', 10000000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (49, N'14', N'PuRD-MPMC', N'PuRD-MPMC', 30000, 2016, N'n-a', NULL)
GO

INSERT INTO dbo.mnda_fund_source (Fund_Source_Id, division_id, Fund_Name, details, Amount, Year, Status, service_type)
VALUES 
  (50, N'5', N'PRD-MDC', N'PRD-MDC', 10000000, 2016, N'n-a', NULL)
GO

SET IDENTITY_INSERT dbo.mnda_fund_source OFF
GO

--
-- Data for table dbo.mnda_key_result_area  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_key_result_area ON
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (1, N'a1', N'Monitoring and Evaluation')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (2, N'a2', N'Planning')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (3, N'a3', N'Policy  Development')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (4, N'a4', N'Project Evaluation')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (5, N'a5', N'Institutional Development')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (6, N'a6', N'Knowledge managment')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (7, N'a7', N'Resource Mobilization')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (8, N'a8', N'Monitoring and Evaluation')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (9, N'a9', N'Investment Promotion')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (10, N'b1', N'International Relations')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (11, N'b2', N'Client and Network Mgt.')
GO

INSERT INTO dbo.mnda_key_result_area (KeyResult_Id, KeyResult_Code, KeyResult_Desc)
VALUES 
  (12, N'b3', N'Human Resource Dev''t')
GO

SET IDENTITY_INSERT dbo.mnda_key_result_area OFF
GO

--
-- Data for table dbo.mnda_main_program_encoded  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_main_program_encoded ON
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (1, N'2016', NULL, NULL, N'1', NULL, NULL, N'5')
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (2, N'2016', NULL, NULL, N'2', NULL, NULL, N'5')
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (3, N'2016', NULL, NULL, N'3', NULL, NULL, N'5')
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (4, N'2016', NULL, NULL, N'4', NULL, NULL, N'5')
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (5, N'2016', NULL, NULL, N'5', NULL, NULL, N'6')
GO

INSERT INTO dbo.mnda_main_program_encoded (id, fiscal_year, major_final_output_code, performance_indicator_code, program_code, category_code, accountable_office_code, accountable_division)
VALUES 
  (6, N'2016', NULL, NULL, N'6', NULL, NULL, N'6')
GO

SET IDENTITY_INSERT dbo.mnda_main_program_encoded OFF
GO

--
-- Data for table dbo.mnda_major_final_output  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_major_final_output ON
GO

INSERT INTO dbo.mnda_major_final_output (id, name)
VALUES 
  (1, N'Integrated Policies and Programs for Mindanao')
GO

SET IDENTITY_INSERT dbo.mnda_major_final_output OFF
GO

--
-- Data for table dbo.mnda_mindanao_2020_contribution  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_mindanao_2020_contribution ON
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (1, N'M1', N'(Economy) 100% increase in air passenger traffic with Region X increasing its share to 30%')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (2, N'M2', N'(Economy) 100% increase in Ro-Ro passenger traffic; 50% increase in vehicles carried by Ro-Ro')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (3, N'M3', N'(Economy) 13% increase in installed power generating capacity; about half of which is accounted for by renewable energy')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (4, N'M4', N'(Economy) 70% of irrigable lands have irrigation facilities')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (5, N'M5', N'(Economy) 80% increase in foreign ship calls')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (6, N'M6', N'(Economy) All high schools have computer laboratories and access to Internet')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (7, N'M7', N'(Economy) All national roads are paved and in good condition; 80% of barangay roads are paved')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (8, N'M8', N'(Economy) An organic farming island/district has been identified and receives focused support from the Department of Agriculture.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (9, N'M9', N'(Economy) Annual domestic tourist arrivals in Mindanao reach 6 million, while foreign tourists reach 1 million.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (10, N'M10', N'(Economy) Construction of Mindanao’s first railway system is underway')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (11, N'M11', N'(Economy) Gross value added in food manufacturing is growing in excess of 8 percent annually, propelled by a growing segment of SME processors.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (12, N'M12', N'(Economy) Intra-Mindanao commerce has increased 50% from its 2010 levels.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (13, N'M13', N'(Economy) Mindanao BPOs contribute 5% of nationwide BPO earnings.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (14, N'M14', N'(Economy) The Mindanao economy is growing at an average annual real GDP growth rate of 7-8 percent, average annual income of Mindanawons (GDP per capita) exceeds P16,000 in constant 2009 prices, and unemployment rate is no more than 3 percent.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (15, N'M15', N'(Economy)Export revenues from Halal products exceed $20 million.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (16, N'M16', N'(Environment) Forest cover in Mindanao has been restored to at least 30 percent of land area.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (17, N'M17', N'(Environment) Forest Cover Increased By 30%')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (18, N'M18', N'(Governance & Institutions) 20% drop in IRA dependence on average; 100% increase in share of income of LGUs from local sources on average')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (19, N'M19', N'(Governance & Institutions) 80% of Shariah courts are operational with firm budget allocation')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (20, N'M20', N'(Governance & Institutions) 85% voter turnout is achieved in Mindanao')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (21, N'M21', N'(Governance & Institutions) A significant number of LGUs have embarked on PPP projects for infrastructure, service delivery or local development initiatives for the general welfare')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (22, N'M22', N'(Governance & Institutions) All provincial, city and municipal LGUs have reliable Internet access and computerized financial operations')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (23, N'M23', N'(Governance & Institutions) DILG has issued a directive to  LGUs to adopt distinctive Mindanao designs in public buildings, structures and facilities.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (24, N'M24', N'(Governance & Institutions) Most (at least 60%) of Mindanao LGUs have IP representation in their legislatures')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (25, N'M25', N'(Governance & Institutions) Working alliances or formal collaborative mechanisms among municipalities are present in at least half of Mindanao provinces')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (26, N'M26', N'(Governance & Institutions)All LGUs regularly convene and make effective use of their local development councils, which have active and meaningful civil society and private sector participation')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (27, N'M27', N'(Governance & Institutions)All Mindanao LGUs comply with the LGC and possess current comprehensive development plans (CDPs) and comprehensive land use plans (CLUPs) that incorporate climate change adaptation measures')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (28, N'M28', N'(Institutional Strengthening)  100% inventory of Office Equipment, supplies, etc.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (29, N'M29', N'(Institutional Strengthening) 100% Compliance to Government Rules and Regulations')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (30, N'M30', N'(Institutional Strengthening) All staff are properly evaluated')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (31, N'M31', N'(Institutional Strengthening) Career Improvement Training provided to qualified staff')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (32, N'M32', N'(Institutional Strengthening) Computerized financial operations')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (33, N'M33', N'(Institutional Strengthening) Human Resource Mechanisms Established')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (34, N'M34', N'(InstitutionalStrengthening) Information Management System Established and Updated')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (35, N'M35', N'(Peace and Security) Indigenous peace education is well integrated in all official school curricula at all levels.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (36, N'M36', N'(Peace and Security) Private armies have been outlawed, dismantled and rendered non-existent.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (37, N'M37', N'(Peace and Security) Satisfactory peace agreements have been completed and signed between the government and the MILF and NDF, thereby attaining satisfactory political settlement on all sides.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (38, N'M38', N'(Peace and Security) The justice system has been rid of all discriminatory practices, and a policy environment of legal pluralism prevails.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (39, N'M39', N'(Peace and Security) There are no more internally displaced persons (IDPs) arising from violent conflict.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (40, N'M40', N'(Social) 80% of  households have access to a sanitary toilet (from 67% in 2008)')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (41, N'M41', N'(Social) 85% of all barangays have functioning and amply equipped health centers accessible to all')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (42, N'M42', N'(Social) 95% of  households have access to safe water supply (from 80.5% in 2008).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (43, N'M43', N'(Social) 95% of all barangays have properly equipped classrooms staffed with teachers.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (44, N'M44', N'(Social) Annual population growth rate in  is 2.0% (from 2.5% in 2009).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (45, N'M45', N'(Social) Average life expectancy in Mindanao has improved to 67 years.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (46, N'M46', N'(Social) Basic literacy rate (for 15-24 year olds) is 96% (from 94.4% in 2007 ).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (47, N'M47', N'(Social) Elementary and high school enrollment and completion rates rates have improved by 50 percent over their 2010 levels.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (48, N'M48', N'(Social) Income distribution is 5% better than the 2009 nationwide distribution, i.e., a Gini coefficient of 0.42.')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (49, N'M49', N'(Social) Infant mortality rate is down to 5 per 1,000 live births (from 7 in 2009 ).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (50, N'M50', N'(Social) Maternal mortality is down to 80 per 1,000 live births (from 97 in 2009).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (51, N'M51', N'(Social) Net enrollment rate in primary school is 95% (from 82.5 in 2009-2010), and 60% in secondary school (from 46.2 percent in 2009-2010).')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (52, N'M52', N'Enabling Environment')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (53, N'M53', N'Environmental')
GO

INSERT INTO dbo.mnda_mindanao_2020_contribution (Mindanao_2020_Id, Mindanao_2020_Code, Mindanao_2020_Desc)
VALUES 
  (54, N'M54', N' (Social) Poverty incidence is down to 30% of families (from 32.5%in 2009)')
GO

SET IDENTITY_INSERT dbo.mnda_mindanao_2020_contribution OFF
GO

--
-- Data for table dbo.mnda_mode_procurement_lib  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_mode_procurement_lib ON
GO

INSERT INTO dbo.mnda_mode_procurement_lib (id, mode_procurement)
VALUES 
  (1, N'MMK-1')
GO

INSERT INTO dbo.mnda_mode_procurement_lib (id, mode_procurement)
VALUES 
  (2, N'DDD-2')
GO

INSERT INTO dbo.mnda_mode_procurement_lib (id, mode_procurement)
VALUES 
  (3, N'DDD-3')
GO

SET IDENTITY_INSERT dbo.mnda_mode_procurement_lib OFF
GO

--
-- Data for table dbo.mnda_mooe_expenditures  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_mooe_expenditures ON
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (1, N'50201000', N'Traveling Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (2, N'50202000', N'Training and Scholarship Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (3, N'50203000', N'Supplies and Materials Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (4, N'50204000', N'Utility Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (5, N'50205000', N'Communication Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (6, N'50210000', N'Extraordinary and Miscellaneous Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (7, N'50211000', N'Professional Services', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (8, N'50212000', N'General Services', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (9, N'50213000', N'Repairs and Maintenance', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (10, N'50215000', N'Taxes Insurance Premiums and Other Fees', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (11, N'50216000', N'Labor and Wages', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (12, N'50299000', N'Other Maintenance & Operating Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (13, N'50299050', N'Rent Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (14, N'50299060', N'Membership Dues & Cont''ns to Organizations', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (15, N'50299070', N'Subscription Expenses', N'50200000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (19, N'LE', N'Live Expense', NULL)
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (20, N'0000150244', N'AÑeeÑ', NULL)
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (21, N'50100000', N'Personnel Services', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (22, N'50102000', N'Other Compensation', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (23, N'5010203000', N'Transportation Allowance', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (24, N'5010204000', N'Clothing/Uniform Allowance', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (25, N'5010214000', N'Year End Bonus', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (26, N'5010300000', N'Personnel Benefit Contributions', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (27, N'5010400000', N'Other Personnel Benefits', N'50100000')
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (28, N'2020100000', N'Inter-Agency Payables', NULL)
GO

INSERT INTO dbo.mnda_mooe_expenditures (id, code, name, type_service)
VALUES 
  (29, N'2990000000', N'Other Payables', NULL)
GO

SET IDENTITY_INSERT dbo.mnda_mooe_expenditures OFF
GO

--
-- Data for table dbo.mnda_mooe_sub_expenditures  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_mooe_sub_expenditures ON
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (1, N'50201000', N'5020101000', N'Local Travel', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (2, N'50201000', N'5020102000', N'International Travel', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (3, N'50202000', N'5020201000', N'Training Expenses', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (4, N'50203000', N'5020301000', N'Office Supplies Expenses', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (5, N'50203000', N'5020302000', N'Accountable Forms Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (6, N'50203000', N'5020309000', N'Gasoline , Oil and Lubricants Expenses', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (7, N'50203000', N'5020399000', N'Other Supplies Expenses', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (8, N'50204000', N'5020401000', N'Water Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (9, N'50204000', N'5020402000', N'Electricity Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (10, N'50205000', N'5020501000', N'Postage and Deliveries', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (11, N'50205000', N'5020502001', N'Telephone Expenses - Mobile', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (12, N'50205000', N'5020502000', N'Telephone Expenses - Landline', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (13, N'50205000', N'5020503000', N'Internet Expenses', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (14, N'50205000', N'5020504000', N'Cable Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (15, N'50210000', N'5021003000', N'Extraordinary and Miscellaneous Expenses', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (16, N'50211000', N'5021102000', N'Auditing Services', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (17, N'50211000', N'5021103000', N'Consultancy Services', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (18, N'50211000', N'5021104000', N'Other Professional Services', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (19, N'50212000', N'5021202000', N'Janitorial Services', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (20, N'50212000', N'5021203000', N'Security Services', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (21, N'50212000', N'5021299000', N'Other General Services', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (22, N'50213000', N'5021399000', N'Other Property, Plang and Equipment', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (23, N'50213000', N'5021307000', N'Furniture and Fixtures', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (24, N'50213000', N'5021305000', N'IT Equipment and Software', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (25, N'50213000', N'5021306000', N'Motor Vehicles', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (26, N'50215000', N'5021501000', N'Taxes, Duties and Licenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (27, N'50215000', N'5021502000', N'Fidelity Bond Premiums', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (28, N'50215000', N'5021503000', N'Insurance Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (29, N'50216000', N'5021601000', N'Labor and Wages (Job Order Contracts)', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (30, N'50299000', N'5029901000', N'Advertising Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (31, N'50299000', N'5029902000', N'Printing and Publication Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (32, N'--', N'--', N'--', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (33, N'50299050', N'5029905000', N'Buildings and Structures (Office Space)', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (34, N'50299050', N'5029905000', N'Storage Fee (Bodega Space)', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (35, N'50299050', N'5029905000', N'Plant Rental (Ornamental)', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (36, N'50299050', N'5029905000', N'Motor Vehicles', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (37, N'50299050', N'5029905000', N'Equipment', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (38, N'50299060', N'5029906000', N'Membership Dues & Cont''ns to Organizations', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (39, N'50299070', N'5029907000', N'Subscription Expenses', 0, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (40, N'ALLO', N'-', N'Local Allowance', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (41, N'ALIN', N'-', N'International Allowance', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (42, N'234234234', N'-', N'Gasoline Expense Type', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (43, N'50203010', N'5020301000', N'Supplies', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (44, N'50299000', N'5029903000', N'Representation Expense', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (45, N'50211000', N'5021299000', N'Professional Fee', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (46, N'50210000', N'5029902000', N'Printing and Binding', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (47, N'50299060', N'5029906000', N'Dues', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (48, N'50299070-', N'5029907000-', N'Subscription', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (49, N'50299000', N'5029901000', N'Advertising', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (50, N'50299050', N'5029905000', N'Rental Fee', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (51, N'50216000', N'5021601000', N'Labor and Wages', 1, 0)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (52, N'50299000', N'5029999099', N'Other Maintenance &  Expenses', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (60, N'009929', N'1111111', N'EXpense Item 1', 0, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (61, N'0000150244', N'324234324', N'eÑe', 0, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (62, N'50100000', N'5010101001', N'Basic Salary - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (63, N'50102000', N'5010201001', N'Personnel Economic Relief Allowance (PERA)', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (64, N'50102000', N'5010202000', N'Representation Allowance (RA)', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (65, N'5010203000', N'5010203001', N'Transportation Allowance (TA)', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (66, N'5010204000', N'5010204001', N'Clothing/Uniform Allowance - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (67, N'5010214000', N'5010214001', N'Bonus - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (68, N'5010300000', N'5010302001', N'Pag-IBIG - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (69, N'5010300000', N'5010303001', N'PhilHealth-Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (70, N'5010300000', N'5010304001', N'ECIP- Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (71, N'5010400000', N'5010402001', N'Retirement Gratuity - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (72, N'5010400000', N'5010403001', N'Terminal Leave Benefits - Civilian', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (73, N'5010400000', N'5010499099', N'Other Personnel Benefits', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (74, N'2020100000', N'2020103000', N'Due To Pagibig', 1, 1)
GO

INSERT INTO dbo.mnda_mooe_sub_expenditures (id, mooe_id, uacs_code, name, is_active, is_dynamic)
VALUES 
  (75, N'2990000000', N'29999990', N'Other Payables', 1, 1)
GO

SET IDENTITY_INSERT dbo.mnda_mooe_sub_expenditures OFF
GO

--
-- Data for table dbo.mnda_offices  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_offices ON
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (1, N'Investment Promotions & Public Affairs Office')
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (2, N'Office for Finance & Administrative Services')
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (3, N'Office of the Chairperson')
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (4, N'Office of the Executive Director')
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (5, N'Planning, Policy and Project Development Office')
GO

INSERT INTO dbo.mnda_offices (id, name)
VALUES 
  (6, N'Project Management and Area Concerns')
GO

SET IDENTITY_INSERT dbo.mnda_offices OFF
GO

--
-- Data for table dbo.mnda_performance_indicator  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_performance_indicator ON
GO

INSERT INTO dbo.mnda_performance_indicator (id, code, name)
VALUES 
  (1, N'PI 1', N'PI 1. Number of Mindanao-wide/ inter-regional mechanisms [i.e. (a) Development plans/programs/projects/policies; and (b) Investment Projects] strengthened, facilitated, or implemented')
GO

INSERT INTO dbo.mnda_performance_indicator (id, code, name)
VALUES 
  (2, N'PI 2', N'PI 2. Percent of Mindanao-wide/ inter-regional mechanisms that are rated as good or better by the LGUs/NGAs/POs concerned')
GO

INSERT INTO dbo.mnda_performance_indicator (id, code, name)
VALUES 
  (3, N'PI 3', N'PI 3. Percent of mechanisms (i.e. focus on facilitation work for investment promotions, dialogues, industry matching, etc.) submitted/completed/made available three (3) working days prior to prescribed deadline')
GO

SET IDENTITY_INSERT dbo.mnda_performance_indicator OFF
GO

--
-- Data for table dbo.mnda_procurement_items  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_procurement_items ON
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (1, N'ACETATE, gauge #3, 50m in length', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES 3', N'YES', N'roll', 656.1256)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (2, N'* AIR FRESHENER, 280mL/can', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES 2', N'YES', N'can', 85.5816)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (3, N'* ALCOHOL, 70%, ethyl', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES 1', N'NO', N'bottle', 39.3093)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (4, N'* BATTERY, size AA, alkaline, 2 pcs./packet', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'packet', 16.23645)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (5, N'* BATTERY, size AAA, alkaline, 2 pcs./packet', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'packet', 15)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (6, N'* BATTERY, size D, alkaline, 2 pcs./packet', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'packet', 75.7068)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (7, N'* BINDER, 3-ring, D-type, A4, 64mm(2.5"), with insert clear-view pocket on front, back and spine for label', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pair', 117.62195)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (8, N'* BINDER, 3-ring, D-type, legal, 64MM(2.5"), with insert clear-view pocket on front, back and spine for label', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pair', 124.2579)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (9, N'* BLADE, heavy duty cutter, 10 pcs./pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'pack', 8.17625)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (10, N'* BROOM, soft (tambo)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 104.234)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (11, N'* CALCULATOR, compact, electronic, LCD, desktop, display, 12 digits, two-way power source', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'YES', N'unit', 156.8996)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (12, N'* CARBON FILM, polyehtylene, 216mm x 330mm, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 267.8856)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (13, N'* CARTOLINA, assorted color, 20s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 74.6096)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (14, N'* CARTOLINA, white, 20s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 54.86)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (15, N'* CHAIR, monobloc, without armrest, beige', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'YES', N'unit', 296.244)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (16, N'* CHAIR, monobloc, without armrest, white', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'YES', N'unit', 296.244)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (17, N'* CLEANSER, scouring powder, 350gms.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'canister', 15.192)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (18, N'CLEARBOOK, A4 SIZE, for 210mm x 297mm (A4 size) documents, refillable, plastic, overall size (min) of cover 302mm(L) x 242mm(W) and 0.48mm thickness, assorted colors(black, blue, red, yellow), 0.06mm(min) thickness of pocket, with twenty(20) clear transparent plastic pockets', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 39.4992)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (19, N'* CLEARBOOK, LEGAL SIZE, for 216mm x 330mm (legal size) documents, refillable, plastic, overall size(min) of cover 353mm(L) x 242mm(W) and 0.48mm thickness, assorted colors(black, blue, red, yellow), 0.06mm(min thickness of pocket, with twenty(20) clear transparent plastic pocket', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 43.888)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (20, N'* CLIP, backfold, 19mm, 12s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 7.6804)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (21, N'* CLIP, backfold, 25mm, 12s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 11.5206)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (22, N'* CLIP, backfold, 32mm, 12s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 17.22815)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (23, N'* CLIP, backfold, 50mm, 12s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 41.145)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (24, N'* CLIP, bulldog, 73mm (3")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 8.7776)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (25, N'* COMPACT DISK RECORDABLE, min. of 700MB, 1x - 52x minimum speed, 80 min recording time', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 9.9803)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (26, N'* COMPACT DISK REWRITABLE, 700MB min. capacity, 80 minutes recording time, 4x - 10x min speed', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 18.3781)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (27, N'* COMPACT DISK STORAGE CASE, 50 CDs capacity, min, made of durable plastic or nylon fabric, double sided sleeves, with closure', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 57.80345)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (28, N'* COMPUTER CONTINUOUS FORMS, 1 ply, 11" x 9-1/2", 2000 sheets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'box', 586.4323)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (29, N'* CORRECTION TAPE, disposable, usable length of 6 meters(min), 5mm width', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pair', 12.71275)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (30, N'* CUTTER, heavy duty', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 23.0412)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (31, N'* DATA FILE BOX, (5"x9"x15-3/4")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 73.56515)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (32, N'* DATA FOLDER, w/ finger ring, (3" x 9" x 15")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 81.18225)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (33, N'* DETERGENT POWDER, all purpose, 500gms.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'pouch', 26.3328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (34, N'* DIGITAL VOICE RECORDER, 4GB, memory, stereo channel, MP3, WMA, recording and playback format, with earphone jack, built-in microphone, with USB cable, rechargeable batteries, instructional manual, carrying pouch, hand strap, earphone', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'YES', N'unit', 6528.34)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (35, N'* DISINFECTANT SPRAY, 400 grams net content', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'can', 120.692)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (36, N'* DUST PAN, non-rigid plastic, with detachable handle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 38.2754)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (37, N'* DVD RECORDABLE, 16x speed, 4.7GB capacity', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 11.80545)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (38, N'* DVD REWRITABLE, 4x speed, 4.7GB capacity', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 22.27105)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (39, N'* ENVELOPE, documentary (10"x15"), 500s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 535.307)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (40, N'* ENVELOPE, documentary, A4, 500s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 425.7136)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (41, N'* ENVELOPE, expanding, kraft, legal size, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 633.9284)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (42, N'* ENVELOPE, expanding, plastic, legal size', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 30.7216)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (43, N'* ENVELOPE, mailing white, 500s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 147.0248)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (44, N'* ERASER, felt, for blackboard/whiteboard', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 10.4023)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (45, N'* ERASER, rubber', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 2.2788)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (46, N'* EXTERNAL HARD DRIVE, 1TB, 2.5" HDD, USB 3.0(backward compatible with USB 2.0), 5400 RPM, with dual-color LED light to indicate USB 3.0/USB 2.0 transmission, USB powered', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 3498.9708)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (47, N'* FACSIMILE TRANSCEIVER, uses thermal paper, 50m/roll, for documents 216mm x 600mm, 15 sec, transmission speed, running width 2018mm, document feeder holds 10 pages, with automatic paper cutter, redial, and fax/tel switchover', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'YES', N'unit', 3695.3696)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (48, N'* FILE ORGANIZER, expanding, legal, plastic, assorted colors', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 76.47695)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (49, N'* FILE TAB DIVIDER, A4, for 210mm x 297mm (A4 size) documents, bristol board, 297mm x 210mm Leaf Size(min), 65mm x 12mm Tab Size, 153 gsm., 0.22mm (min) thickness, five (5) colors per set', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'set', 11.5206)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (50, N'* FILE TAB DIVIDER, LEGAL SIZE, for 216mm x 330mm (legal size) documents, bristol board, 330mm x 2216mm Leaf size, 68mm x 15mm Tab size, 153 GSM. 0.22mm (min) thickness, five(5) colors per set', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'set', 17.5552)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (51, N'* FLASH DRIVE, 16GB, USB 2.0, plug and play', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 403.7696)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (52, N'* FLASH DRIVE, 8GB, USB 2.0, plug and play', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 302.8272)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (53, N'* FOLDER, clear plastic, L-type, A4 size, 50s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 163.4828)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (54, N'* FOLDER, clear plastic, L-type, legal size, 50s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 185.4268)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (55, N'* FOLDER, morocco/fancy, legal size, 50s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 307.216)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (56, N'* FOLDER, pressboard, plain, legal, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 987.36395)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (57, N'* FOLDER, tagboard, A4 size, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 257.2301)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (58, N'* FOLDER, tagboard, legal size, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 288.5636)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (59, N'* FURNITURE CLEANER, 300mL/can min', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'can', 89.4218)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (60, N'* GLUE, all purpose, 300 grams min.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'jar', 50.4501)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (61, N'* ILLUSTRATION BOARD, (30"x40"), 2 ply', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 41.145)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (62, N'* INDEX TAB, self-adhesive, 5 set/box, assorted colors', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 53.6362)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (63, N'* INK CARTRIDGE, Brother LC67B, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 960.05)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (64, N'* INK CARTRIDGE, Brother LC67M, Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 576.03)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (65, N'* INK CARTRIDGE, Brother LC67Y, Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 576.03)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (66, N'* INK CARTRIDGE, Brother Part No. LC67C, Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 576.03)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (67, N'* INK CARTRIDGE, Canon Part No. CL-811, colored', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 942.4948)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (68, N'* INK CARTRIDGE, Canon Part No. PG-810, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 759.811)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (69, N'* INK CARTRIDGE, Epson C13T664200 (T6642), Cyan, original, for Epson L100, L110, L200, L210, L300, L350 and L356', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 268.814)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (70, N'* INK CARTRIDGE, EPSON T6441, Black, for Epson L100, L110, L200, L210, L300, L350, L355', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 268.814)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (71, N'* INK CARTRIDGE, EPSON T664,Yellow, for Epson L100, L110, L200, L210, L300, L350, L358', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 268.814)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (72, N'* INK CARTRIDGE, EPSON T6643, Magenta for Epson L100, L110, L200, L210, L300, L350, L357', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 268.814)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (73, N'* INK CARTRIDGE, HP C4844A (HP 10), black, 69mL, for HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100+ printer series, 1200+ printer series, 2000 printer series, 2230+, 2280+ printer series, 2300+ printer series, 2500 printer series, 2600+ printer series, HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (74, N'* INK CARTRIDGE, HP C9351AA (HP 21), black, for HP Deskjet 3920, 3940, HP PSC 1410, 1402', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 661.6116)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (75, N'* INK CARTRIDGE, HP C9352AA (HP 22), tri-color, for HP Deskjet 3920, 3940, HP Officejet 5610, HP PSC 1410, 1402', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 762.554)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (76, N'* INK CARTRIDGE, HP CN692AA(HP 704), black, for HP Ink Advantage 2010 K010a1, Ink Advantage All-in-One 20601 K110a', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 351.104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (77, N'* INK CARTRIDGE, HP CN693AA (HP704), tricolor, for HP Ink Advantage 2010 K010a1, Ink Advantage All-in-One 20601 K110a', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 351.104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (78, N'* INK CARTRIDGE, HP CZ107AA, (HP 678), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 365.3676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (79, N'* INK CARTRIDGE, HP CZ108AA, (HP 678), tricolor', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 380.7284)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (80, N'* INSECTICIDE, 600mL/can', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'can', 123.9836)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (81, N'* LASER POINTER, PEN TYPE, metal, for presentation, green and red color, 50mW laser power, beam light, continuous light, single-point, uses 2 x LR6 1.5V AA or AAA batteries, button switch', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 580.2922)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (82, N'* LEAD, for mechanical pencil,0.5mm, 12 pcs/tube', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'tube', 20.6991)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (83, N'* MAGAZINE FILE BOX, 110mm x 220mm x 265mm, with open end', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 46.2512)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (84, N'* MAGAZINE FILE BOX, 112mm x 200mm x 240mm, with open end', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 36.2076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (85, N'* MANILA PAPER, 1200mm x 900mm, 60gsm., 0.14mm thickness, pale yellow, 10 sheets per sleeves', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'sleeve', 25.98465)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (86, N'* MAP PIN, round head, 100s/case', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'case', 42.56925)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (87, N'* MARKER, fluorescent, 3 colors/set', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'set', 34.76225)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (88, N'* MARKER, permanent, bullet type, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (89, N'* MARKER, permanent, bullet type, blue', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (90, N'* MARKER, permanent, bullet type, red', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (91, N'* MARKER, permanent, chisel type, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (92, N'* MARKER, permanent, chisel type, blue', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (93, N'* MARKER, permanent, chisel type, red', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.81825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (94, N'* MARKING PEN, whiteboard, bullet type, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.94485)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (95, N'* MARKING PEN, whiteboard, bullet type, blue', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.94485)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (96, N'* MARKING PEN, whiteboard, bullet type, red', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 12.94485)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (97, N'* MOP BUCKET, heavy duty, hard plastic, min: 370mm(L), 475mm(W), 245mm(H); 30L capacity, with 4 wheel ball caster, with squeezer, with precautionary markings', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 1974.96)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (98, N'* MOPHANDLE, screw type, aluminum handle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 131.664)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (99, N'* MOPHANDLE, screw type, wooden handle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 98.748)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (100, N'* MOPHEAD, 100% rayon, 400g', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'piece', 87.776)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (101, N'* MOUSE, optical, USB connection type', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 151.9622)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (102, N'* NOTE BOOK, stenographer''s, 40 leaves, ruled both sides', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 9.3262)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (103, N'* NOTE PAD, (2"x2"), 400 sheets/pad', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pad', 104.2129)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (104, N'* NOTE PAD, (2"x3"), 100 sheets/pad', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pad', 32.8949)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (105, N'* NOTE PAD, (3"x3"), 100 sheets/pad', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pad', 40.24825)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (106, N'* NOTE PAD, (3"x4"), 100 sheets/pad', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pad', 56.9278)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (107, N'* ON PHILIPPINE GOVERNMENT PROCUREMENT-RA 9184(6th Edition), 6" x 9", 300 pages,', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'HANDBOOKS ON PROCUREMENT', N'YES', N'piece', 30.4895)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (108, N'* PAPER CLIP, gem type, 32mm, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 6.4566)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (109, N'* PAPER CLIP, gem type,jumbo, 48mm, 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 14.2636)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (110, N'* PAPER FASTENER, for paper, metal, 50 sets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 69.1025)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (111, N'* PAPER, bond, Premium Grade,210mm x 297mm (A4), 70 gsm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 103.0313)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (112, N'* PAPER, multicopy, 210mm x 297mm(A4), 80gsm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 123.0341)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (113, N'* PAPER, multicopy, legal, for laser printing', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'LEGAL SIZE PAPER', N'YES', N'ream', 138.2683)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (114, N'* PAPER, thermal, 216mmx30M, 1/2" core', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 36.97775)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (115, N'* PARCHMENT PAPER, A4 size, 80 gsm,100sheets/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 93.262)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (116, N'* PENCIL, lead, w/eraser, 0ne(1) dozen per box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 20.6991)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (117, N'* PENCIL, mechanical, for 0.5mm lead', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 24.1384)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (118, N'* PHILIPPINE NATIONAL FLAG, Type I, 1828mm x 914.4mm, 100% cotton', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 315.9936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (119, N'* PUNCHER, heavy duty', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 109.6989)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (120, N'* PUSH PIN, flat head type, assorted colors, 100s/case', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'case', 20.1294)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (121, N'* RAG, COTTON, (7") in diameter, assorted colors', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'kilo', 46.0824)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (122, N'* RECORD BOOK, 300 pages, smythe sewn', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'book', 57.1599)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (123, N'* RECORD BOOK, 500 pages, smythe sewn', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'book', 91.62675)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (124, N'* RIBBON CARTRIDGE, Epson Part No. C13SO15632, black for printer LX310', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 80.0956)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (125, N'* RIBBON, nylon, manual typewriter', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'spool', 17.0066)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (126, N'* RING BINDER, 10mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 46.62045)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (127, N'* RING BINDER, 11mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 55.9572)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (128, N'* RING BINDER, 12.7mm x 1.12m (1/2"x44"), plastic', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 55.9572)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (129, N'* RING BINDER, 14mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 78.43925)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (130, N'* RING BINDER, 16mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 96.005)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (131, N'* RING BINDER, 19mm x 1.12m(3/4"x44"), plastic, 10 pcs/ bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 105.8798)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (132, N'* RING BINDER, 22mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 156.34045)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (133, N'* RING BINDER, 25mm x 1.12m (1"x44"), plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 120.1434)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (134, N'* RING BINDER, 28mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 239.72765)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (135, N'* RING BINDER, 32mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 270.99785)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (136, N'* RING BINDER, 50mm x 1.12m(2" x 44"), plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 216.1484)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (137, N'* RING BINDER, 6mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 35.09985)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (138, N'* RING BINDER, 8mm x 1.12m, plastic, 10 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 38.39145)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (139, N'* RUBBER BAND, 1.0mm min thickness, min. 350grams/box or approx 220pcs', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 111.67175)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (140, N'* RULER, plastic, 300mm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 2.1944)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (141, N'* RULER, plastic, 450mm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 15.03375)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (142, N'* SCISSORS, (6")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'pair', 16.38415)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (143, N'* SCOURING PAD, economy size', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'pack', 123.8148)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (144, N'* SHARPENER, single cutterhead', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 186.524)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (145, N'* SIGN PEN, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 46.43055)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (146, N'* SIGN PEN, blue', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 46.43055)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (147, N'* SIGN PEN, red', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 46.43055)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (148, N'* STAMP PAD INK, violet, 50mL', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bottle', 21.944)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (149, N'* STAMP PAD, felt pad, min 60mm x 100mm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 21.944)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (150, N'* STAMPING DATER, self-inking stamp', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 504.6909)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (151, N'* STAPLE REMOVER, plier type', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 18.3992)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (152, N'* STAPLE WIRE, (26/16)standard, 5000 pcs/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 25.109)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (153, N'* STAPLE WIRE, HEAVY DUTY, 23/13, for use with heavy duty staplers, metal, non-rust, chisel point. 0.60mm thickness, 13mm width, 13mm leg length, 100 staples per strip, 1,000 staples per box, 40-90 sheets of 70gsm bond paper', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'box', 25.2356)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (154, N'* STAPLER, binder type, heavy duty for high volume stapling, 25-153 sheets of 70gsm bond paper stapling capacity, min 100 staples, with adjustable stapler guide', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 1118.0468)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (155, N'* STAPLER, heavy duty, standard', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 114.6363)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (156, N'* TAPE DISPENSER, handheld, for 48mm width packaging tape', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 32.35685)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (157, N'* TAPE DISPENSER, heavy duty, for 24mm(1")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 35.1104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (158, N'* TAPE, electrical', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'roll', 18.59965)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (159, N'* TAPE, masking, 24mm, 50 meters length', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 55.9572)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (160, N'* TAPE, masking, 48mm, 50 meters length', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 108.6228)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (161, N'* TAPE, packaging, 48mm, 50 meters length', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 35.1104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (162, N'* TAPE, transparent, 24mm, 50 meters', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 18.32535)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (163, N'* TAPE, transparent, 48mm, 50 meters', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 35.1104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (164, N'* TOILET BOWL & URINAL CLEANER, 900ml', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'bottle', 43.888)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (165, N'* TOILET DEODORANT CAKE, deoderizer/moth proofer 50gms, 3 pcs/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'box', 24.687)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (166, N'* TOILET TISSUE, 12 rolls/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'pack', 79.72635)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (167, N'* TONER CARTRIDGE, HP CB435A, black, for HP Laserjet P1005, P1006 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 2869.178)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (168, N'* TONER CARTRIDGE, HP CB436A, black, for HP Laserjet P1505 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 3145.6724)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (169, N'* TONER CARTRIDGE, HP CE285A, black, for HP LaserJet P1102, P1102w, printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 2856.0116)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (170, N'* TONER CARTRIDGE, HP Q2612A, black, for HP Laserjet 1010, 1012, 1015, 1018, 1020, 1022, 3015, 3020, 3030, 3050, 3052, 3055, M1005, M1319f MFP Printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 3156.982)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (171, N'* TONER CARTRIDGE, Samsung MLTD101S', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 3291.6)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (172, N'* TONER CARTRIDGE, TK 100, for Kyocera Mita KM-1500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'cart', 3058.77205)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (173, N'* TRASHBAG, plastic, black, (XL), 10 pcs per pack per roll', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'YES', N'roll', 152.342)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (174, N'* WASTE BASKET, plastic', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'piece', 28.5272)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (175, N'AIRPOT, 3.8 liters, w/ dispenser', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1316.64)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (176, N'AUDIO CASSETTE TAPE, 90min. recording time', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 24.1384)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (177, N'BALLAST, 18 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 76.6985)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (178, N'BALLAST, 36 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 74.93665)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (179, N'BATHROOM SOAP, 90gms.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'piece', 22.4926)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (180, N'BROOM, STICK (tingting)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'piece', 25.2356)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (181, N'CALCULATOR, desktop, heavy duty printing, 12 digits, two(2) color print/illuminated display, AC power source, Canon', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 2051.764)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (182, N'CALCULATOR, desktop, mini-printer type,12 digits, AC/DC power source, with ink roller & adaptor', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 981.994)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (183, N'CALCULATOR, scientific, 10 digits, casedot matirx, solar/ cell type power source', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 436.6856)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (184, N'CARBON FILM, polyethylene, 210mm x 297mm(A-4), 100s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 235.898)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (185, N'CARTRIDGE, Epson C13T032190, black, for printer Stylus C80/C82/ CX5100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1321.0288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (186, N'CARTRIDGE, Epson C13T107390, Magenta, for printer T10/T11/ T13/ T20E/TX100/TX110/TX111/CX5500) Pigment - 91N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 300.6328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (187, N'CARTRIDGE, Samsung MLT-D205E, Black, original, for ML-3310ND, ML-3710ND, SCX-4833FR, SCX-5637FR, SCX-5737FW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8448.44)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (188, N'CARTRIDGE,Epson C13S050440, Std Cap, for printer AL-M2010D/DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5994.0036)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (189, N'CHALK, white, dustless, 100 pcs/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 27.2823)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (190, N'COLUMNAR NOTEBOOK, 12 cols', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 21.944)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (191, N'COLUMNAR PAD, 14 cols, 50 gsm min.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pad', 38.402)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (192, N'COLUMNAR PAD, 16 cols, 50 gsm min.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pad', 37.8323)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (193, N'COLUMNAR PAD, 18 cols, 50 gsm min.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pad', 58.1516)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (194, N'COMPACT FLUORESCENT LIGHT, 18 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 105.2257)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (195, N'COMPUTER CONTINUOUS FORMS, 1 ply, 11" x 14-7/8", 2000 sheets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'box', 820.3258)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (196, N'COMPUTER CONTINUOUS FORMS, 2 ply, 11" x 14-7/8", 1000 sets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'box', 953.298)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (197, N'COMPUTER CONTINUOUS FORMS, 2 ply, 11" x 9-1/2", 1000 sets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'box', 656.34715)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (198, N'COMPUTER CONTINUOUS FORMS, 3 ply, 11 x 9-1/2", 500 sets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'box', 547.334)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (199, N'COMPUTER CONTINUOUS FORMS, 3 ply, 11" x 14-7/8", 500 sets/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'box', 821.634)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (200, N'DETERGENT BAR, min 392 grams net mass, four(4) pcs per bar', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'bar', 18.2726)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (201, N'DISKETTE, floppy disk, 3.5", double sided, high density, formatted 10s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'pack', 56.83285)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (202, N'DOCUMENT CAMERA, w/ DVI port, four (4), reference points demarcate viewing area, 16 times, (1600%) consecutive zoom, PC and Doc Cam VS', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 26552.24)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (203, N'DRUM UNIT, Brother DR3000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8997.04)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (204, N'DRUM UNIT, Brother DR3115', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7170.202)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (205, N'DRUM UNIT, Brother DR4000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6934.304)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (206, N'DRUM UNIT, Brother DR7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8338.72)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (207, N'EDP BINDER, TB, for 11" X 14-7/8" CCF, plastic, spring rod type, assorted colors', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 43.888)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (208, N'EDP BINDER, TB, for 11" X 9-1/2" CCF', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 40.4276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (209, N'ELECTRIC FAN, industrial, 18" metal blade', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1095.0056)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (210, N'ELECTRIC FAN, orbit type', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1314.4456)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (211, N'ELECTRIC FAN, wall type', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 985.2856)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (212, N'ELECTRIC FAN, with stand', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 864.5936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (213, N'ELECTRONIC TIME RECORDER, (bundy clock), electronic, compact design with large clock face, 2-color ribbon, with dot matrix printer, wall or desk mount, 220 volts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 3224.5442)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (214, N'ENVELOPE, mailing white with window, 500s', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 206.1048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (215, N'ENVELOPE, pay, kraft, (4"x7-1/2"), 500s/box', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 114.6574)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (216, N'EXTERNAL HARD DRIVE, 500GB, 2.5"HDD, USB 3.0 (backward compatible with USB 2.0), 5400 RPM, with dual-color LED light to indicate USB 3.0/ USB 2.0 transmission, USB powered, System Requirements: USB 3.0: Windows XP/Vista/7; MacOSx 10.4 or above, with USB 3.0 cable and product guide', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'piece', 2485.158)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (217, N'FIRE EXTINGUISHER, dry chemical, for ABC class, of fire, stored pressure type,non-electrical conductor,non-toxic, non-corrosive, 4.5kg (10lbs.), brand new', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1367.1112)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (218, N'FIRE EXTINGUISHER, pure HCFC 123, with fire rating of 1A, 1BC for ABC class of fire, stored pressure type, non-electrical conductor, 4.5kg (10 lbs), brand new', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 4156.1936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (219, N'FLOOR WAX, liquid type, 3.75-5.0L/ plastic container', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'container', 390.8775)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (220, N'FLOOR WAX, paste, natural, 2kgs.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'can', 235.898)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (221, N'FLOOR WAX, paste, red, 2kgs.', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'can', 230.412)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (222, N'FLUORESCENT LAMP, tubular, 18 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 37.3048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (223, N'FLUORESCENT LAMP, tubular, 36 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 38.402)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (224, N'FLUORESCENT LIGHTING FIXTURE, 1 x 18W', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'set', 345.618)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (225, N'FLUORESCENT LIGHTING FIXTURE, 1 x 36W', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'set', 405.964)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (226, N'FOLDER, morocco/fancy, A4 size, 50s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'bundle', 246.87)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (227, N'FUSE, 30 amperes', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 13.1664)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (228, N'FUSE, 60 amperes', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 29.6244)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (229, N'IMAGING UNIT, Hewlett Packard Part No. CE314A, color imaging unit', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3993.808)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (230, N'IMAGING UNIT, Samsung Part No.SCXR6555A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (231, N'INDEX CARD BOX, 4-3/8"x5-5/8" x 4"(3" x 5")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 38.402)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (232, N'INDEX CARD BOX, 5-3/8"x8-7/8" x 6"(5" x 8")', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 52.117)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (233, N'INDEX CARD, 5" x 8",ruled both sides, 500s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pack', 147.0037)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (234, N'INDEX CARD,3"x 5",ruled both sides, 500s/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pack', 54.08985)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (235, N'INK CARTRIDGE, Brother LC39BK, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 718.666)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (236, N'INK CARTRIDGE, Brother LC39C, cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 471.796)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (237, N'INK CARTRIDGE, Brother LC39M, magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 471.796)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (238, N'INK CARTRIDGE, Brother LC39Y, yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 471.796)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (239, N'INK CARTRIDGE, Brother LC67HYBK, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1667.744)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (240, N'INK CARTRIDGE, Brother LC67HYC, cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 910.676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (241, N'INK CARTRIDGE, Brother LC67HYM, magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 910.676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (242, N'INK CARTRIDGE, Brother LC67HYY, yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 910.676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (243, N'INK CARTRIDGE, Brother Part No. LC38B, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 707.694)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (244, N'INK CARTRIDGE, Brother Part No. LC38C, Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 466.31)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (245, N'INK CARTRIDGE, Brother Part No. LC38M, Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 466.31)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (246, N'INK CARTRIDGE, Brother Part No. LC38Y, Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 466.31)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (247, N'INK CARTRIDGE, Brother Part No. LC40BK, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 680.264)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (248, N'INK CARTRIDGE, Brother Part No. LC40C, Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 384.02)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (249, N'INK CARTRIDGE, Brother Part No. LC40M, Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 411.45)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (250, N'INK CARTRIDGE, Brother Part No. LC40Y, Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 384.02)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (251, N'INK CARTRIDGE, Canon Part No. CL-741, colored', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 996.2576)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (252, N'INK CARTRIDGE, Canon Part No. CL-831, color', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1073.0616)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (253, N'INK CARTRIDGE, Canon Part No. CL-98, colored', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 729.638)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (254, N'INK CARTRIDGE, Canon Part No. CLI-726, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 565.058)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (255, N'INK CARTRIDGE, Canon Part No. CLI-726, cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 566.113)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (256, N'INK CARTRIDGE, Canon Part No. CLI-726, magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 566.113)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (257, N'INK CARTRIDGE, Canon Part No. CLI-726, Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 566.113)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (258, N'INK CARTRIDGE, Canon Part No. PG-740, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 759.811)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (259, N'INK CARTRIDGE, Canon Part No. PG-830, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 820.7056)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (260, N'INK CARTRIDGE, Canon Part No. PGI-725, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 597.974)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (261, N'INK CARTRIDGE, Epson C13T028091, black, for printer Stylus C60/CX3100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1224.4752)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (262, N'INK CARTRIDGE, Epson C13T029091, color, for printer Stylus C60/ C50/ CX3100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1118.0468)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (263, N'INK CARTRIDGE, Epson C13T032290, Cyan, for printer Stylus C80', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 630.89)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (264, N'INK CARTRIDGE, Epson C13T032390, Magenta, for printer Stylus C80', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 630.89)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (265, N'INK CARTRIDGE, Epson C13T032490, Yellow, for printer Stylus C80', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 630.89)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (266, N'INK CARTRIDGE, Epson C13T038190, black, for printer Stylus C41SX/UX', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 518.9756)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (267, N'INK CARTRIDGE, Epson C13T042290, Cyan, for printer Stylus C82/CX5100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 574.9328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (268, N'INK CARTRIDGE, Epson C13T042390, Magenta, for printer Stylus C82/CX5100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 574.9328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (269, N'INK CARTRIDGE, Epson C13T042490, Yellow, for printer Stylus C82/CX5100', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 574.9328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (270, N'INK CARTRIDGE, Epson C13T046190, black, for printer Stylus C63/C83', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 660.5144)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (271, N'INK CARTRIDGE, Epson C13T047290, Cyan, for printer Stylus C63/C83', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 407.0612)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (272, N'INK CARTRIDGE, Epson C13T047390, Magenta, for printer Stylus C63/C83', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 407.0612)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (273, N'INK CARTRIDGE, Epson C13T047490, Yellow, for printer Stylus C63/C83', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 407.0612)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (274, N'INK CARTRIDGE, Epson C13T062190, black, High Cap, for printer Stylus CX3700/4700/C67', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 675.8752)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (275, N'INK CARTRIDGE, Epson C13T063190, black, Std Cap, for printer Stylus CX3700/4700/C67', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 518.9756)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (276, N'INK CARTRIDGE, Epson C13T063290, Cyan, Std Cap, for printer Stylus CX3700/4700/C67', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 416.936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (277, N'INK CARTRIDGE, Epson C13T063390, Magenta, Std Cap, for printer Stylus CX3700/4700/C67', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 416.936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (278, N'INK CARTRIDGE, Epson C13T063490, Yellow, Std Cap, for printer Stylus CX3700/4700/C67', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 416.936)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (279, N'INK CARTRIDGE, Epson C13T075190, Black, for printer Stylus color C59/CX2900', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 310.5076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (280, N'INK CARTRIDGE, Epson C13T075290, Cyan, for printer Stylus color C59/CX2900', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 310.5076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (281, N'INK CARTRIDGE, Epson C13T075390, Magenta, for printer Stylus color C59/CX2900', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 310.5076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (282, N'INK CARTRIDGE, Epson C13T075490, Yellow, for printer Stylus color C59/CX2900', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 310.5076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (283, N'INK CARTRIDGE, Epson C13T103190, Black, for printer Stylus T40W/TX600FW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 913.9676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (284, N'INK CARTRIDGE, Epson C13T103290, Cyan, for printer StylusT40W/TX600FW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 608.946)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (285, N'INK CARTRIDGE, Epson C13T103390, Magenta, for printer Stylus T40W/TX600FW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 608.946)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (286, N'INK CARTRIDGE, Epson C13T103490, Yellow, for printer Stylus T40W/TX600FW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 608.946)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (287, N'INK CARTRIDGE, Epson C13T104190, black, for printer C110, CX7300/8300, TX200/400, Pigment High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 608.946)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (288, N'INK CARTRIDGE, Epson C13T104193, Black, for printer C110, CX7300/8300, TX200/400 Pigment High Cap - Double Pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1014.91)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (289, N'INK CARTRIDGE, Epson C13T105190, Black, for printer Stylus T10/T11/ T13/ T30/T20E/C79/C90/TX100/TX110/TX111/TX200/TX210/TX400/TX550W/TX300F/TX510FN/TX600FW/CX3900/CX5500/CX5900/CX6900F/CX8300/CX9300F, Pigment - 73N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (290, N'INK CARTRIDGE, Epson C13T105290, Cyan, for printer Stylus T10/T11/ T13/ T30/T20E/C79/C90/TX100/TX110/TX111/TX200/TX210/TX400/TX550W/TX300F/TX510FN/TX600FW/CX3900/CX5500/CX5900/CX6900F/CX8300/CX9300F, Pigment - 73N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (291, N'INK CARTRIDGE, Epson C13T105390, Magenta, for printer Stylus T10/T11/ T13/ T30/T20E/C79/C90/TX100/TX110/TX111/TX200/ TX210/TX400/TX550W/ TX300F/TX510FN/TX600FW/CX3900/CX5500/CX5900/CX6900F/CX8300/CX9300F, Pigment - 73N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (292, N'INK CARTRIDGE, Epson C13T105490, Yellow, for printer Stylus T10/T11/ T13/ T30/T20E/C79/C90/TX100/TX110/TX111/TX200/ TX210/TX400/TX550W/ TX300F/TX510FN/TX600FW/CX3900/CX5500/CX5900/CX6900F/CX8300/CX9300F, Pigment - 73N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (293, N'INK CARTRIDGE, Epson C13T107190, Black, for printer T10/T11/ T13/T20E/ TX100/TX110/TX111/CX5500) Pigment - 91N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 300.6328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (294, N'INK CARTRIDGE, Epson C13T107290, Cyan, for printer T10/T11/ T13/T20E/ TX100/TX110/TX111/CX5500) Pigment - 91N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 300.6328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (295, N'INK CARTRIDGE, Epson C13T107490, Yellow, for printer T10/T11/ T13/T20E/ TX100/TX110/TX111/CX5500) Pigment - 91N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 300.6328)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (296, N'INK CARTRIDGE, Epson C13T111190, Black, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (297, N'INK CARTRIDGE, Epson C13T111290, Cyan, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (298, N'INK CARTRIDGE, Epson C13T111390, Magenta, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (299, N'INK CARTRIDGE, Epson C13T111490, Yellow, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (300, N'INK CARTRIDGE, Epson C13T111590, Light Cyan, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (301, N'INK CARTRIDGE, Epson C13T111690, Light Magenta, for printer Stylus Photo R270/ 290/390/RX590, High Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (302, N'INK CARTRIDGE, Epson C13T112190, Black, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (303, N'INK CARTRIDGE, Epson C13T112290, Cyan, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (304, N'INK CARTRIDGE, Epson C13T112390, Magenta, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (305, N'INK CARTRIDGE, Epson C13T112490, Yellow, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (306, N'INK CARTRIDGE, Epson C13T112590, Light Cyan, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (307, N'INK CARTRIDGE, Epson C13T112690, Light Magenta, for printer Stylus Photo R270/290/390, RX590, Std Cap', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 706.5968)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (308, N'INK CARTRIDGE, Epson C13T141190, Black, for ME 32/320/620F - 141', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 325.8684)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (309, N'INK CARTRIDGE, Epson C13T141290, Cyan, for ME 32/320/620F - 141', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 366.4648)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (310, N'INK CARTRIDGE, Epson C13T141390, Magenta, for ME 32/320/620F - 141', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 366.5703)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (311, N'INK CARTRIDGE, Epson C13T141490, Yellow, for ME 32/320/620F - 141', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 366.4648)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (312, N'INK CARTRIDGE, Epson C13T143190, Black, for printer ME 960FWD/900WD - 143', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 619.918)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (313, N'INK CARTRIDGE, Epson C13T143290, Cyan, for printer ME Office 960FWD/ 900WD - 143', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 488.254)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (314, N'INK CARTRIDGE, Epson C13T143390, Magenta, for printer ME Office 960FWD/ 900WD - 143', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 488.254)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (315, N'INK CARTRIDGE, Epson C13T143490, Yellow, for printer ME Office 960FWD/ 900WD - 143', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 488.254)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (316, N'INK CARTRIDGE, Epson C13TO38191, black, for printer Stylus C41SX/UX (Double Pack)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 897.4674)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (317, N'INK CARTRIDGE, Epson C13TO39090, color, for printer Stylus C41SX/UX', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 813.0252)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (318, N'INK CARTRIDGE, HP C1823DA (HP 23), tricolor, for HP Deskjet 710c, 720c, 810c, 830c, 880c, 890c, 895cxi, 1120c, 1125c HP Officejet Pro 1170c, 1175c, r45, r65, t45, t65 All-in-One HP PSC 500 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1689.688)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (319, N'INK CARTRIDGE, HP C4814AA (HP13), black, 28 ml, for HP Officejet Pro K850, HP Business Inkjet CP1700, 1000, 1100, 1200, 2000, 2200, 2230, 2250, 2280, 2300, 2500, 2600, 2800, 3000 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1055.5064)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (320, N'INK CARTRIDGE, HP 51645A (HP 45), black, for HP Deskjet 710c, 720c, 820cxi, 830c, 850c, 870cxi, 880c, 890c, 895cxi, 930c, 950c, 960c, 970cxi, 990cxi, 1000cxi, 1120c, 1125c, 1180c, 1220c, 1600c, 1600cm, 6122, 9300, HP Officejet G55, G85, G95, K60, K80, r45, r65, t45, t65 All-in-One HP Officejet Pro 1150c, 1170c, 1175c All-in-One, HP Photosmart P1000, P1100, P1215, P1218', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1382.472)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (321, N'INK CARTRIDGE, HP C4815A (HP 13), cyan, for HP Officejet Pro K850, HP Business Inkjet CP1700, 1000, 1100, 1200, 2000, 2200, 2230, 2250, 2280, 2300, 2500, 2600, 2800, 3000 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1055.5064)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (322, N'INK CARTRIDGE, HP C4816A (HP 13), magenta, 14 ml, for HP Officejet Pro K850 HP Business Inkjet CP1700, 1000, 1100, 1200, 2000, 2200, 2230, 2250, 2280, 2300, 2500, 2600, 2800, 3000 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1055.5064)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (323, N'INK CARTRIDGE, HP C4817A (HP 13), yellow , 14 ml, HP Officejet Pro K850, HP Business Inkjet CP1700, 1000, 1100, 1200, 2000, 2200, 2230, 2250, 2280, 2300, 2500, 2600, 2800, 3000 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1055.5064)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (324, N'INK CARTRIDGE, HP C4836A (HP 11), cyan 28 ml, HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100 printer series, 1200 printer series, 2230, 2280 printer series, 2300 printer series, 2600 printer series HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (325, N'INK CARTRIDGE, HP C4837A (HP 11), magenta, 28 ml, for HP Officejet 9110, 9120, 9130 All-in-One HP Business Inkjet 1100 printer series, 1200 printer series, 2230, 2280 printer series, 2300 printer series, 2600 printer series HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (326, N'INK CARTRIDGE, HP C4838A, (HP11), yellow, for HP: Business Inkjet: 1000, 1100d, 1100dtn, 1200, 1200d, 1200dn, 1200dtn, 1200dtwn, 1700d, 1700ps, 2200, 2200se, 2200xi, 2230, 2250, 2250tn, 2280, 2280tn, 2300, 2300dtn, 2300n, 2600, 2600dn, 2800, 2800dt, 2800dtn; Color Inkjet: CP1700, CP1700d, CP1700ps; Color LaserJet: 2600dn, 2600dtn; Designjet: 10ps, 20ps, 50ps, 70, 100, 100 Plus, 110, 110 Plus, 110 Plus NR, 110 Plus R, 111, 120, 120nr, 500, 500 Plus, 500ps, 500ps Plus, 510ps, 800, 800ps, 815; Officejet: 9110, 9120, 9130; Officejet Pro: K8', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (327, N'INK CARTRIDGE, HP C483A (HP 11) , yellow 28 ml, for HP Officejet 9110, 9120, 9130 All-in-One HP Business Inkjet 1100 printer series, 1200 printer series, 2230, 2280 printer series, 2300 printer series, 2600 printer series HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (328, N'INK CARTRIDGE, HP C4902AA (HP940), black, for HP Officejet Pro 8000, 8500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1508.65)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (329, N'INK CARTRIDGE, HP C4906AA (HP 940XL), black, HP Officejet Pro 8000, 8500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1448.304)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (330, N'INK CARTRIDGE, HP C4908AA (HP 940XL), magenta, for HP Officejet Pro 8000, 8500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (331, N'INK CARTRIDGE, HP C4909AA (HP 940XL ), yellow, for HP Officejet Pro 8000, 8500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (332, N'INK CARTRIDGE, HP C4936A (HP 18 ), black, for HP Officejet Pro K5300, K5400dn, K5400dtn, K8600, K8600dn, L7380, L7580, L7590', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 871.1768)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (333, N'INK CARTRIDGE, HP C4937A (HP 18) , cyan, for HP Officejet Pro K5300, K5400dn, K5400dtn, K8600, K8600dn, L7380, L7580, L7590', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 652.834)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (334, N'INK CARTRIDGE, HP C4938A (HP 18) , magenta, for HP Officejet Pro K5300, K5400dn, K5400dtn, K8600, K8600dn, L7380, L7580, L7590', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 652.834)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (335, N'INK CARTRIDGE, HP C4939A (HP18) , yellow, for HP Officejet Pro K5300, K5400dn, K5400dtn, K8600, K8600dn, L7380, L7580, L7590', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 652.834)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (336, N'INK CARTRIDGE, HP C6578A/D (HP 78), tricolor, for HP Deskjet 920c, 930c, 948c, 950c, 960c, 970cxi, 990cxi, 1180c, 1220c, 3820, 6122, 9300 HP Officejet 5110, G55, G85, G95, K60, K80, v40 All-in-One, HP PSC 750, 950 All-in-One, HP Photosmart P1000, P1100, P1215, P1218', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1536.08)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (337, N'INK CARTRIDGE, HP C6614D (HP 20), black, 28mL, for HP Deskjet 610c, 630c, 640c, 656c', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1448.304)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (338, N'INK CARTRIDGE, HP C6615DA (HP 15), black, for HP Deskjet 810c, 840c, 845c, 920c, 948c, 3820, HP Officejet 5110, v40 All-in-One, HP PSC 500, 750, 950 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1305.668)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (339, N'INK CARTRIDGE, HP C6625A (HP 17), tricolor, for HP Deskjet 840c, 845c', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1420.874)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (340, N'INK CARTRIDGE, HP C6656AA (HP 56), black, for HP Deskjet 840c, 845c', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 921.648)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (342, N'INK CARTRIDGE, HP C6658A, colored for HP Photosmart', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1129.0188)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (343, N'INK CARTRIDGE, HP C8721A (HP 02), black , 10 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230 HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 839.358)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (344, N'INK CARTRIDGE, HP C8727AA (HP 27), black, 10mL, for HP Deskjet 3320, 3325, 3420, 3535, 3550, 3650, 3744, 3745, 3845 HP PSC 1110, 1210, 1315 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 844.844)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (345, N'INK CARTRIDGE, HP C8765WA (HP 94), black, 11 mL, for HP Deskjet 3320, 3325, 3420, 3535, 3550, 3650, 3744, 3745, 3845, HP Officejet 4110, 4255 All-in-One, HP PSC 1110, 1210, 1315 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 932.62)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (341, N'INK CARTRIDGE, HP C6657AA (HP 57), tri-color, for HP Deskjet 450 printer series, 5160, 5550, 5650, 5652, 9650, 9680 HP Officejet 4110, 4255, 5510, 6110 All-in-One, HP PSC 1110, 1210, 1315, 1350, 2110, 2210, 2310 All-in-One, HP PSC 2410, 2510, Photosmart All-in-One, HP Photosmart 7150, 7260, 7268, 7450, 7550, 7660, 7760, 7960', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1536.08)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (346, N'INK CARTRIDGE, HP C8766WA (HP 95), tri-color, for HP Deskjet 460, 5740, 6540, 6840, HP Officejet 7410 All-in-One HP PSC 2355 All-in-One, HP Photosmart 325, 375, 2610, 2710, 8150, 8450', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1119.144)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (347, N'INK CARTRIDGE, HP C8767WA(HP 96), black , 21 ml, for HP Deskjet 5740, 6540, 6840, 9800, 9860, 9808, HP Photosmart 8030, 8150, 8450, 875, HP Officejet K7100, 7210, 7410 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1393.444)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (348, N'INK CARTRIDGE, HP C8772WA(HP 02), magenta, 3.5 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230, HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (349, N'INK CARTRIDGE, HP C8773WA(HP 02), yellow, 6 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230, HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (350, N'INK CARTRIDGE, HP C8774WA(HP 02), light cyan , 5.5 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230, HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (351, N'INK CARTRIDGE, HP C8775WA(HP 02), light magenta , 5.5 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230, HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (352, N'INK CARTRIDGE, HP C9361WA (HP 93), tri-color, for HP Deskjet D4160, 5440, HP Officejet 6310 All-in-One, HP PSC 1510 All-in-One, HP Photosmart 7830, C3180, C4180 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 894.218)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (353, N'INK CARTRIDGE, HP C9362WA (HP 92), black, for HP Deskjet 5440, HP Officejet 6310 All-in-One, HP PSC 1510 All-in-One HP Photosmart 7830, C3180 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 633.0844)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (354, N'INK CARTRIDGE, HP C9363WA (HP 97), tricolor , 14 ml, for HP Deskjet 5740, 6540, 6840, 9800, 9860, 9808, HP Photosmart 2575, 2610, 2710 All-in-On, HP PSC 1610, 2355 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (355, N'INK CARTRIDGE, HP C9364WA (HP 98), black , 11 ml, HP Deskjet D4160, HP Photosmart D5160, 8030, HP Photosmart C4180, 2575 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 773.526)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (356, N'INK CARTRIDGE, HP C9391A(HP 88XL), cyan , 17 ml, for HP Officejet Pro K550, K550dtn, K5400dn, K5400dtn, K8600, 600dn, HP Officejet Pro L7580, L7590 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (357, N'INK CARTRIDGE, HP C9392A( HP 88XL), magenta 17 ml, for HP Officejet Pro K550, K550dtn, K5400dn, K5400dtn, K8600, 600dn, HP Officejet Pro L7580, L7590 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (358, N'INK CARTRIDGE, HP C9393A(HP 88XL ), yellow 17 ml, for HP Officejet Pro K550, K550dtn, K5400dn, K5400dtn, K8600, 600dn, HP Officejet Pro L7580, L7590 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (359, N'INK CARTRIDGE, HP C9396A(HP 88XL), black 58.5 ml, for HP Officejet Pro K550, K550dtn, K5400dn, K5400dtn, K8600, 600dn, HP Officejet Pro L7580, L7590 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (360, N'INK CARTRIDGE, HP CB314A (HP 900), black, for HP Deskjet 900, 910, 915', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 275.3972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (361, N'INK CARTRIDGE, HP CB315A (HP 900), tri-color, for HP Deskjet 900, 910, 915', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 373.048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (362, N'INK CARTRIDGE, HP CB316WA( HP 564), black, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 502.5176)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (363, N'INK CARTRIDGE, HP CB317WA(HP 564), photo, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 420.2276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (364, N'INK CARTRIDGE, HP CB318WA(HP 564), cyan, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 420.2276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (365, N'INK CARTRIDGE, HP CB319WA(HP 564), magenta, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 420.2276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (366, N'INK CARTRIDGE, HP CB320WA ( HP 564), yellow, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 420.2276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (367, N'INK CARTRIDGE, HP CB323WA(HP 564XL), cyan, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 834.9692)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (368, N'INK CARTRIDGE, HP CB324WA(564XL), magenta, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 834.9692)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (369, N'INK CARTRIDGE, HP CB325WA(564XL), yellow, for HP Photosmart D5400, C5380, C6380, Photosmart Pro B8550', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 834.9692)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (370, N'INK CARTRIDGE, HP CB335WA(HP74 ), black , 4.5 ml, for HP Deskjet D4260, D4360, HP Photosmart C4280, C4380, C4480, C5280 All-in-One, HP Officejet J5780, J6480 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 675.4532)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (371, N'INK CARTRIDGE, HP CB336WA (HP74XL), black , 18 ml, for HP Deskjet D4260, D4360, HP Photosmart C4280, C4380, C4480, C5280 All-in-One, HP Officejet J5780, J6480 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1398.93)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (372, N'INK CARTRIDGE, HP CB337WA (HP75),tricolor, 3.5 ml, for HP Deskjet D4260, D4360, HP Photosmart C4280, C4380, C4480, C5280 All-in-One, HP Officejet J5780, J6480 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 776.8176)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (373, N'INK CARTRIDGE, HP CB338WA (HP 75XL), tricolor, 12 ml, for HP Deskjet D4260, D4360, HP Photosmart C4280, C4380, C4480, C5280 All-in-One, HP Officejet J5780, J6480 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (374, N'INK CARTRIDGE, HP CC640WA (HP 60), black, 4 ml, for HP Deskjet D2560, F4230, F4250, F4280 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 633.0844)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (375, N'INK CARTRIDGE, HP CC641WA (HP60XL), black 12 ml, for HP Deskjet D2560, F4230, F4250, F4280 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1333.098)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (376, N'INK CARTRIDGE, HP CC643WA (HP 60), tricolor,13 ml, for HP Deskjet D2560, F4230, F4250, F4280 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 760.3596)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (377, N'INK CARTRIDGE, HP CC644WA (HP60XL), tricolor , 11 ml, for HP Deskjet D2560, F4230, F4250, F4280 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1558.024)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (378, N'INK CARTRIDGE, HP CC653AA (HP 901), black officejet , 4 ml', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 634.1816)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (379, N'INK CARTRIDGE, HP CC656AA(HP 901), tricolor officejet, 9 ml', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 972.1192)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (380, N'INK CARTRIDGE, HP CC660AA(HP702), black, 20 ml, for HP Officejet J3508, J3608, J5508 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1058.798)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (381, N'INK CARTRIDGE, HP CD887AA (HP703), black deskjet, for HP Deskjet D730, F735 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'Y', N'cart', 351.104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (382, N'INK CARTRIDGE, HP CD888AA(HP 703), tricolor deskjet, for HP Deskjet D730, F735 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'Y', N'cart', 346.7152)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (383, N'INK CARTRIDGE, HP CD971AA (HP 920), black, for HP Officejet 6000, 7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 804.2476)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (384, N'INK CARTRIDGE, HP CD972AA (HP920XL), cyan, for HP Officejet 6000, 7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 603.46)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (385, N'INK CARTRIDGE, HP CD973AA (HP920XL), magenta, for HP Officejet 6000, 7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 603.46)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (386, N'INK CARTRIDGE, HP CD974AA (HP 920XL), yellow, for HP Officejet 6000, 7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 603.46)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (387, N'INK CARTRIDGE, HP CD975AA (HP920XL), black, for HP Officejet 6000, 7000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1206.92)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (388, N'INK CARTRIDGE, HP CH561WA (HP 61), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 641.862)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (389, N'INK CARTRIDGE, HP CH562WA (HP 61), color ink', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 794.3728)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (390, N'INK CARTRIDGE, HP CN 639WA, black, for HP Deskjet D2650', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 858.0104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (391, N'INK CARTRIDGE, HP CN 640WA, color, for HP Deskjet D2650', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1118.0468)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (392, N'INK CARTRIDGE, HP CN045AA (950XL), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1627.1476)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (393, N'INK CARTRIDGE, HP CN046AA (951XL), Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1206.92)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (394, N'INK CARTRIDGE, HP CN047AA (951XL), Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1206.92)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (395, N'INK CARTRIDGE, HP CN048AA (950XL), Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1206.92)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (396, N'INK CARTRIDGE, HP CZ121A (HP 685A), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 384.02)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (397, N'INK CARTRIDGE, HP CZ121A (HP 685A), Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 375.2424)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (398, N'INK CARTRIDGE, HP CZ121A (HP 685A), Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 252.356)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (399, N'INK CARTRIDGE, HP CZ121A (HP 685A), Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 252.356)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (400, N'INK CARTRIDGE, HP Q8893AA (C8728AA) (HP 28), tri-color, for HP Deskjet 3320, 3325, 3420, 3535, 3550, 3650, 3744, 3745, 3845, HP Officejet 4110, 4255 All-in-One, HP PSC 1110, 1210, 1315 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 965.536)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (401, N'INK CARTRIDGE, HPC4907AA (HP 940XL), cyan, for HP Officejet Pro 8000, 8500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1046.7288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (402, N'INK CARTRIDGE, Lexmark # 17, black, PN 10N0217', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1058.798)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (403, N'INK CARTRIDGE, Lexmark # 27, colored, PN 10N0227', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1261.78)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (404, N'INK CARTRIDGE, Lexmark Black #14, PN 18C2090A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1058.798)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (405, N'INK CARTRIDGE, Lexmark Black #34, PN 18C0034A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1788.436)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (406, N'INK CARTRIDGE, Lexmark Color #15, PN 18C2110A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1314.4456)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (407, N'INK CARTRIDGE, Lexmark Color #35, PN 18C0035A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2161.484)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (408, N'INK CARTRIDGE, Lexmark Photo #31, PN 18C0031A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2232.802)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (409, N'INK CARTRIDGE, EPSON PART NO. C13TO38191, BLACK, (double pack), for Stylus Color C41SX & C41UX', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 915.0648)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (410, N'LOOSELEAF COVER, 216mm x 355mm, 50 pcs/bundle', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'bundle', 568.8982)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (411, N'MEGAPHONE, portable sound system, all ABS resin body, 330mm length, 200mm Horn Diameter, 16 watts(min), 300 meters(min), rechargeable, with buyilt-in siren, red or blue color', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 2852.72)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (412, N'MULTIMEDIA PROJECTOR, 3000 ansi Lumens, 3600 hours lamp life, supports SVGA to SXGA, (compressed) resolution', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 19639.88)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (413, N'NK CARTRIDGE, HP C8771WA(HP 02), cyan , 4 ml, for HP Photosmart D6160, D7160, D7260, D7360, D7460, 8230, HP Photosmart C5180, C6180, C6280, C7180, C7820, C8180, 3110, 3310 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 449.852)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (414, N'OIL, for general purpose, 120 mL', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'bottle', 38.9295)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (415, N'PAPER SHREDDER, 220mm feed opening, 4mm shred width, 0.06m/sec shred speed, 6-8 sheets performance, approx 5kg in weight', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 3291.6)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (416, N'PAPER TRIMMER/CUTTING MACHINE, 350MM cutting size, 30 sheets cutting cap., automatic clamping, stationery blade guard, A4-A6 format indications', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 8859.89)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (417, N'PAPER, bond, Premium Grade', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'LEGAL SIZE PAPER', N'NO', N'ream', 115.0161)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (418, N'PAPER, for Plain Paper Copier', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'LEGAL SIZE PAPER', N'NO', N'ream', 119.20445)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (419, N'PAPER, for Plain Paper Copier, 210mm x 297mm (A4) 70gsm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'ream', 107.9265)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (420, N'PAPER, for Plain Paper Copier, 254mm 356mm(B4), 70gsm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'ream', 156.8996)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (421, N'PAPER, mimeo, Groundwood, 210mm x 297mm (A4), 60gsm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'ream', 90.3924)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (422, N'PAPER, mimeograph, Groundwood', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'LEGAL SIZE PAPER', N'NO', N'ream', 97.5453)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (423, N'PAPER, mimeograph, Whitewove', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'LEGAL SIZE PAPER', N'NO', N'ream', 119.0462)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (424, N'PAPER, mimeograph, Whitewove, 210mm x 297mm, 70 gsm., 480 sheets ream', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'ream', 108.5173)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (425, N'PAPER, ruled pad,216mmx330mm, 90 sheets/pad', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pad', 18.43085)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (426, N'PAPER, thermal,210mmx30M, 1/2" core', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'roll', 36.2076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (427, N'PEA WHISTLE, STAINLESS STEEL, 40mm (min) length, sound ball made of plastic material, with attachment ring', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'NO', N'piece', 30.7216)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (428, N'Photoconductor Kit, HP CE3114A, color, HP Laserjet M175nw, Laserjet Pro CP1025nw/M275', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3993.808)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (429, N'Photoconductor Kit, Lexmark PN 12A8302', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4432.688)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (430, N'Photoconductor Kit, Lexmark PN E250X22G', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2468.7)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (431, N'Photoconductor Kit, Lexmark PN E260X22G', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2468.7)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (432, N'PHOTOCONDUCTOR UNIT, Epson C13S051099, for printer EPL 6200/6200L', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3648.19)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (433, N'PRINTER, automatic data processing, impact, dot matrix, 9-pins, 136 columns, complete w/ ribbons, cables and manual', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 17555.2)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (434, N'PRINTER, impact dot matrix, 24-pins, 136 columns, 480 cps print speed, parallel or USB interface, manual/tractor feeder paper handling, 220VAC/60Hz, with power cable, data cable, software driver (if any), manual and ribbon cartridge', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 19430.11435)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (435, N'PRINTER, impact dot matrix, 9-pin, 80 columns, with ribbon, cables and manual, ribbons, cables and manual, LX-310+ II', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 6152.2536)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (436, N'PRINTER, inkjet, wireless capable, 55 ppm speed , with 2.0" MGD display, 512 MB memory, 500 sheets input tray, duplex printing capable', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 24898)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (437, N'PRINTER, laser, 24ppm printing speed, 1200 x 1200 dpi, with power cord, interface cable, software drivers and complete documentation', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1744.548)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (438, N'PRINTHEAD, HP 11 , yellow C4813A, 8 ml, HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100+ printer series, 1200+ printer series, 2230, 2280 printer series 2300 printer series, 2600 printer series, HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (439, N'PRINTHEAD, HP 11, black, C4810A , 8 ml, for HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100+ printer series, 1200+ printer series, 2230, 2280 printer series 2300 printer series, 2600 printer series, HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (440, N'PRINTHEAD, HP 11, cyan, C4811A, 8 ml, HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100+ printer series, 1200+ printer series, 2230, 2280 printer series 2300 printer series, 2600 printer series , HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1706.146)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (441, N'PRINTHEAD, HP 11, magenta C4812A, 8 ml, HP Officejet 9110, 9120, 9130 All-in-One, HP Business Inkjet 1100+ printer series, 1200+ printer series, 2230, 2280 printer series 2300 printer series, 2600 printer series, HP Colour Printer cp1700 series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1547.052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (442, N'PUNCHING/BINDING MACHINE, two(2) hand lever system, 34cm or 13" (24 holes) punching, width adjustable to any format, binds 425 sheets, or up to 2" thick, all metal construction', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 10970.9028)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (443, N'RIBBON CARTRIDGE, Epson C13SO15505 for printer DFX-9000', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1214.6004)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (444, N'RIBBON CARTRIDGE, Epson C13SO15506(7753) Fabric, for printer LQ 300/300+II & 580', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 128.3724)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (445, N'RIBBON CARTRIDGE, Epson C13SO15508(S015016), black, for printer LQ 670/680/680Pro', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 370.8536)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (446, N'RIBBON CARTRIDGE, Epson C13SO15568, Color, for printer LX-300', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1316.64)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (447, N'RIBBON CARTRIDGE, Epson C13SO15577(#8766), for printer DFX-5000+/ 8000/8500 Black Ribbon (S015055)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1011.6184)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (448, N'RIBBON CARTRIDGE, Epson C13SO15587, black for printer DLQ-3000/3000+/3500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1108.172)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (449, N'RIBBON CARTRIDGE, Epson C13SO15589(S015337), for printer LQ-590', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 401.5752)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (450, N'RIBBON CARTRIDGE, fabric, Iwata E2A time recorder', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 493.74)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (451, N'RIBBON CARTRIDGE, for Fujitsu DL3850', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 471.796)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (452, N'RIBBON CARTRIDGE, Lexmark 11A3550 for Printer Models 23xx series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1228.864)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (453, N'RIBBON, Epson RN 7754', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 137.15)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (454, N'RIBBON, Epson RN 8750', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 80.97125)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (455, N'RIBBON, Epson RN 8755', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 89.4218)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (456, N'RIBBON, Epson RN SO 15083/SO 15086', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 764.7484)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (457, N'RIBBON, Epson RN SO 15327', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 353.2984)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (458, N'SCHOOL FORM, DEPED FORM 1 (SCHOOL REGISTER), 8 sheets with 6-half sheet inserts in between each sheet/16 pages with 6 inserts (6 half-sheet per insert), with "DepEd Form 1 School Register" printed on the outer cover, saddle stitch binding, soft cover in uncoated, glossy or matte finish cover, 0.26mm', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'piece', 31.6922)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (459, N'SCHOOL FORM, DEPED FORM 1 (SF1), Bristol Board, 214mmx340mm, min 220gsm, offset printing, 100 pcs/pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pack', 196.3988)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (460, N'SCHOOL FORM, DEPED FORM 137-E (Grade 1 to K12), Bristol board size (min): 214mm (W) x 340mm (L), Thickness (min) 0.25mm, Weight (min) 220gsm, Offset (back to back) printing, 100s/ pack', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'pack', 196.3988)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (461, N'STAPLE WIRE, HEAVY DUTY, 23/17, for heavy duty staplers, metal, non-rust, chisel point, 0.60mm thickness, 13mm width, 17mm Leg Length, 100 staples per trip, 1,000 staples per box, 90-135 sheets of 70 gsm bond paper', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'box', 32.3674)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (462, N'STARTER, 4-40 watts', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON ELECTRICAL SUPPLIES', N'NO', N'piece', 5.486)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (463, N'STENCIL PAPER, ordinary, universal, 24 pcs/quire', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'quire', 192.01)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (464, N'TABLE, monobloc, square, 36" X 36", beige, four(4) seater, for indoor and outdoor use', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1301.237)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (465, N'TABLE, monobloc, square, 36" X 36", white, four(4) seater, for indoor and outdoor use', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1300.182)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (466, N'TAPE, adding machine, GSP bond', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'roll', 8.1235)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (467, N'TIME CARD, for Amano Bundy Clock,100s/bndl', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'bundle', 71.1703)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (468, N'TIME CARD, for Kitano Time Recorder,100s/bndl', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'bundle', 87.776)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (469, N'TONER CARTRIDGE HP C9720A, original, black, for HP Laserjet 4600/4650 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6923.332)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (470, N'TONER CARTRIDGE HP C9721A, original, Cyan for HP Laserjet 4600/4650 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9370.088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (471, N'TONER CARTRIDGE HP C9722A, original, Yellow for HP Laserjet 4600/4650 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9370.088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (472, N'TONER CARTRIDGE HP C9723A, original, Magenta for HP Laserjet 4600/4650 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9370.088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (473, N'TONER CARTRIDGE, Brother Part No. TN-150BK, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3417.778)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (474, N'TONER CARTRIDGE, Brother Part No. TN-150C, Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3911.518)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (475, N'TONER CARTRIDGE, Brother Part No. TN-150M, Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3911.518)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (476, N'TONER CARTRIDGE, Brother Part No. TN-150Y, Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3911.518)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (477, N'TONER CARTRIDGE, Brother Part No. TN3030', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3730.48)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (478, N'TONER CARTRIDGE, Brother Part No. TN3145, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3105.076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (479, N'TONER CARTRIDGE, Brother Part No. TN3290, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4969.2188)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (480, N'TONER CARTRIDGE, Brother Part No. TN3350, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5244.616)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (481, N'TONER CARTRIDGE, Brother Part No. TN4100, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5097.5912)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (482, N'TONER CARTRIDGE, Brother TN-155BK, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4827.68)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (483, N'TONER CARTRIDGE, Brother TN-155C, cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6473.48)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (484, N'TONER CARTRIDGE, Brother TN-155M, magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6473.48)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (485, N'TONER CARTRIDGE, Brother TN-155Y, yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6473.48)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (486, N'TONER CARTRIDGE, Brother TN-2025', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3000.842)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (487, N'TONER CARTRIDGE, Brother TN-2130', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1920.1)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (488, N'TONER CARTRIDGE, Brother TN-2150', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3017.3)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (489, N'TONER CARTRIDGE, Brother TN3060', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5310.448)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (490, N'TONER CARTRIDGE, Developer, Epson C13S050087, for printer EPL-5900/5900L', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5588.0396)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (491, N'TONER CARTRIDGE, Developer, Epson C13S050166, 6K, for printer EPL-6200', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5815.3288)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (492, N'TONER CARTRIDGE, Developer, Epson C13S050167, 3K, for printer EPL-6200/6200L', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3450.694)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (493, N'TONER CARTRIDGE, Developer, Epson C13S050520, 1.8K, for printer AL-M1200', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3810.5756)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (494, N'TONER CARTRIDGE, Developer, Epson C13S050521, 3.2K, for printer AL-M1200', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5634.122)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (495, N'TONER CARTRIDGE, Developer, Epson C13SO50005, for printer EPL-5500', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3283.1811)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (496, N'TONER CARTRIDGE, Developer, Epson C13SO50010, for printer EPL-5700L/5800', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5481.6112)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (497, N'TONER CARTRIDGE, for Kyocera Mita KM-1505/1510/1810', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4055.2512)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (498, N'TONER CARTRIDGE, Fuji Xerox 106R02625, for Phaser 4600 DN (4K pages)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12556.3568)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (499, N'TONER CARTRIDGE, Fuji Xerox CT201160, for DP-C2255 Heavy Duty A3 size, black (15k pages)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9451.2808)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (500, N'TONER CARTRIDGE, Fuji Xerox CT350567, for DP C3290 FS Multifunction Device, Black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8042.476)
GO

--
-- Data for table dbo.mnda_procurement_items  (LIMIT 500,500)
--

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (501, N'TONER CARTRIDGE, Fuji Xerox CT350670, for DP-3300DX Full Color Laser Printer, black (6k pages, standard yield)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5194.1448)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (502, N'TONER CARTRIDGE, Fuji Xerox CWAA0762, for Phaser 3435D (4K pages, standard yield)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5996.198)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (503, N'TONER CARTRIDGE, HP C4092A, black, for HP Laserjet 1100 printer series, 3200 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3099.59)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (504, N'TONER CARTRIDGE, HP C4096A, balck, for HP Laserjet 2100, 2200 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5647.2884)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (505, N'TONER CARTRIDGE, HP C4127X, high cap, black, for HP Laserjet 2100, 2200 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7885.5764)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (506, N'TONER CARTRIDGE, HP C4129X, black, HP Laserjet 5000, 5100 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10150.1972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (507, N'TONER CARTRIDGE, HP C7115A, black, for HP Laserjet 1000, 1200 printer series, 3300 mfp printer series, 3380 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3464.9576)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (508, N'TONER CARTRIDGE, HP C8543XC, high cap, black, for HP Laserjet 9000, 9040, 9050, HP Laserjet M9040, M9050 MFP series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12509.1772)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (509, N'TONER CARTRIDGE, HP CB380A, black, for HP Color LaserJet CP6015 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9226.7346)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (510, N'TONER CARTRIDGE, HP CB381A, cyan, for HP Color LaserJet CP6015 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 14439.152)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (511, N'TONER CARTRIDGE, HP CB382A, yellow, for HP Color LaserJet CP6015 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 14439.152)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (512, N'TONER CARTRIDGE, HP CB383A, magenta, for HP Color LaserJet CP6015 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 14439.152)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (513, N'TONER CARTRIDGE, HP CB390A, black, for HP Color LaserJet CM6030/CM6040 MFP', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 14439.152)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (514, N'TONER CARTRIDGE, HP CB540A, black, for HP Color LaserJet CP1215, CP1510 printer series, CM1312 MFP', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3105.076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (515, N'TONER CARTRIDGE, HP CB541A, cyan, for HP Color LaserJet CP1215, CP1510 printer series, CM1312 MFP', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2852.72)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (516, N'TONER CARTRIDGE, HP CB542A, yellow, for HP Color LaserJet CP1215, CP1510 printer series, CM1312 MFP', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2852.72)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (517, N'TONER CARTRIDGE, HP CB543A, magenta, for HP Color LaserJet CP1215, CP1510 printer series, CM1312 MFP', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2852.72)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (518, N'TONER CARTRIDGE, HP CC364A, black, for HP LaserJet P4014, P4015, P4515 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6802.64)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (519, N'TONER CARTRIDGE, HP CC364XC, high cap, black, for HP LaserJet P4015, P4515 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12475.164)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (520, N'TONER CARTRIDGE, HP CC530A, black, for HP Color LaserJet CP2025 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5447.598)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (521, N'TONER CARTRIDGE, HP CC531A, cyan, for HP Color LaserJet CP2025 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5109.6604)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (522, N'TONER CARTRIDGE, HP CC532A, yellow, for HP Color LaserJet CP2025 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5109.6604)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (523, N'TONER CARTRIDGE, HP CC533A, magenta, for HP Color LaserJet CP2025 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5109.6604)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (524, N'TONER CARTRIDGE, HP CE250A, black, for HP Color LaserJet CP3520 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5651.6772)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (525, N'TONER CARTRIDGE, HP CE251A, cyan, for HP Color LaserJet CP3520 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11132.1912)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (526, N'TONER CARTRIDGE, HP CE252A, yellow, for HP Color LaserJet CP3520 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11132.1912)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (527, N'TONER CARTRIDGE, HP CE253A, magenta, for HP Color LaserJet CP3520 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11132.1912)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (528, N'TONER CARTRIDGE, HP CE255A, black, for HP LaserJet P3010 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6704.9892)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (529, N'TONER CARTRIDGE, HP CE255X, black, for HP LaserJet P3010 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9476.5164)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (530, N'TONER CARTRIDGE, HP CE260A, black, for HP Color LaserJet Enterprise CP4020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6978.192)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (531, N'TONER CARTRIDGE, HP CE261A, cyan, for HP Color LaserJet Enterprise CP4020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12031.8952)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (532, N'TONER CARTRIDGE, HP CE262A, yellow, for HP Color LaserJet Enterprise CP4020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12031.8952)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (533, N'TONER CARTRIDGE, HP CE263A, magenta, for HP Color LaserJet Enterprise CP4020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12031.8952)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (534, N'TONER CARTRIDGE, HP CE270A, black, for HP Color LaserJet Enterprise CP5520 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10666.9784)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (535, N'TONER CARTRIDGE, HP CE271A, cyan, for HP Color LaserJet Enterprise CP5520 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 17395.0088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (536, N'TONER CARTRIDGE, HP CE272A, yellow, for HP Color LaserJet Enterprise CP5520 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 17395.0088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (537, N'TONER CARTRIDGE, HP CE273A, magenta, for HP Color LaserJet Enterprise CP5520 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 17395.0088)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (538, N'TONER CARTRIDGE, HP CE278A, black, for HP LaserJet P1566, P1606dn, M1536dnf printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3238.9344)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (539, N'TONER CARTRIDGE, HP CE310A, black, for HP Color LaserJet Pro CP1020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2376.5352)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (540, N'TONER CARTRIDGE, HP CE311A, cyan, for HP Color LaserJet Pro CP1020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2589.392)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (541, N'TONER CARTRIDGE, HP CE312A, yellow, for HP Color LaserJet Pro CP1020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2589.392)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (542, N'TONER CARTRIDGE, HP CE313A, magenta, for HP Color LaserJet Pro CP1020 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2589.392)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (543, N'TONER CARTRIDGE, HP CE320A, black, for HP Color LaserJet Pro CP1520, CM1415 Color MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3052.4104)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (544, N'TONER CARTRIDGE, HP CE321A, cyan, for HP Color LaserJet Pro CP1520, CM1415 Color MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2951.468)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (545, N'TONER CARTRIDGE, HP CE322A, yellow, for HP Color LaserJet Pro CP1520, CM1415 Color MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2897.7052)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (546, N'TONER CARTRIDGE, HP CE323A, magenta, for HP Color LaserJet Pro CP1520, CM1415 Color MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2951.468)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (547, N'TONER CARTRIDGE, HP CE390A, black, for HP Laserjet Enterprise 600, M601, 600, M602, 600, M603, M4555 MFP Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7680.4)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (548, N'TONER CARTRIDGE, HP CE390X, high cap, black, for HP Laserjet Enterprise 600, M601, 600, M602, 600, M603, M4555 MFP Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 12824.0736)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (549, N'TONER CARTRIDGE, HP CE400A, black, for HP Laserjet Enterprise 500 Color M551 Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6958.4424)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (550, N'TONER CARTRIDGE, HP CE401A, cyan, for HP Laserjet Enterprise 500 Color M551 Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10186.4048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (551, N'TONER CARTRIDGE, HP CE402A, yellow, for HP Laserjet Enterprise 500 Color M551 Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10186.4048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (552, N'TONER CARTRIDGE, HP CE403A, magenta, for HP Laserjet Enterprise 500 Color M551 Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10186.4048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (553, N'TONER CARTRIDGE, HP CE410A (HP #305), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3840.2)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (554, N'TONER CARTRIDGE, HP CE411A (HP #305), Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5688.982)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (555, N'TONER CARTRIDGE, HP CE412A (HP #305), Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5688.982)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (556, N'TONER CARTRIDGE, HP CE413A (HP #305), Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5688.982)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (557, N'TONER CARTRIDGE, HP CE505A, black, for HP LaserJet P2035, P2055 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3862.144)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (558, N'TONER CARTRIDGE, HP CE505XC, high cap, black, for HP LaserJet P2055 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7091.2036)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (559, N'TONER CARTRIDGE, HP CE740A, black, for HP Color Laserjet Professional CP5220, Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6531.6316)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (560, N'TONER CARTRIDGE, HP CE741A, cyan, for HP Color Laserjet Professional CP5220, Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11585.3348)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (561, N'TONER CARTRIDGE, HP CE742A, yellow, for HP Color Laserjet Professional CP5220, Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11585.3348)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (562, N'TONER CARTRIDGE, HP CE743A, magenta, for HP Color Laserjet Professional CP5220, Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11585.3348)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (563, N'TONER CARTRIDGE, HP CF210A, (#131), black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3145.6724)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (564, N'TONER CARTRIDGE, HP CF211A, (#131), Cyan', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3937.8508)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (565, N'TONER CARTRIDGE, HP CF212A, (#131), Yellow', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4252.7472)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (566, N'TONER CARTRIDGE, HP CF213A, (#131), Magenta', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4253.3802)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (567, N'TONER CARTRIDGE, HP Q1338A, black, for HP Laserjet 4200 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6755.4604)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (568, N'TONER CARTRIDGE, HP Q1339A, black, HP Laserjet 4300 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9236.2296)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (569, N'TONER CARTRIDGE, HP Q2613A, black, for HP Laserjet 1300 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3511.04)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (570, N'TONER CARTRIDGE, HP Q2624A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3756.8128)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (571, N'TONER CARTRIDGE, HP Q3960A, black, for HP Color Laserjet 2550 printer series, 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3730.48)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (572, N'TONER CARTRIDGE, HP Q3961A, cyan, for HP Color Laserjet 2550 printer series, 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4504.006)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (573, N'TONER CARTRIDGE, HP Q3962A, yellow, for HP Color Laserjet 2550 printer series, 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4504.006)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (574, N'TONER CARTRIDGE, HP Q3963A, magenta, for HP Color Laserjet 2550 printer series, 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4504.006)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (575, N'TONER CARTRIDGE, HP Q3971A, cyan, for HP Color LaserJet 2550 Printer Series, HP Color LaserJet 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3385.9592)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (576, N'TONER CARTRIDGE, HP Q3972A, yellow, for HP Color LaserJet 2550 Printer Series, HP Color LaserJet 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3385.9592)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (577, N'TONER CARTRIDGE, HP Q3973A, magenta, for HP Color LaserJet 2550 Printer Series, HP Color LaserJet 2820, 2840 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3385.9592)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (578, N'TONER CARTRIDGE, HP Q5942A, black, for HP Laserjet 4250, 4350 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6567.8392)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (579, N'TONER CARTRIDGE, HP Q5942A, original black, for HP Laserjet 4250/4350 printer series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6567.8392)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (580, N'TONER CARTRIDGE, HP Q5949A, black, for HP Laserjet 1160, 1320 printer series, 3390, 3392 All-in-One', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3724.994)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (581, N'TONER CARTRIDGE, HP Q5950A, black, for HP Color LaserJet 4700 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8064.42)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (582, N'TONER CARTRIDGE, HP Q5951A, cyan, for HP Color LaserJet 4700 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11441.6016)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (583, N'TONER CARTRIDGE, HP Q5952A, yellow, for HP Color LaserJet 4700 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11441.6016)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (584, N'TONER CARTRIDGE, HP Q5953A, magenta, for HP Color LaserJet 4700 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11441.6016)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (585, N'TONER CARTRIDGE, HP Q6000A, black, for HP Color LaserJet 1600, 2600n, 2605, CM1015MFP, CM1017MFP Printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3350.8488)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (586, N'TONER CARTRIDGE, HP Q6001A, cyan, for HP Color LaserJet 1600, 2600n, 2605, CM1015MFP, CM1017MFP Printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3812.77)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (587, N'TONER CARTRIDGE, HP Q6002A, yellow, for HP Color LaserJet 1600, 2600n, 2605, CM1015MFP, CM1017MFP Printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3812.77)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (588, N'TONER CARTRIDGE, HP Q6003A, magenta, for HP Color LaserJet 1600, 2600n, 2605, CM1015MFP, CM1017MFP Printer', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3812.77)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (589, N'TONER CARTRIDGE, HP Q6460A, black, for HP Color LaserJet 4730MFP, CM4730 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6375.8292)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (590, N'TONER CARTRIDGE, HP Q6461A, cyan, for HP Color LaserJet 4730MFP, CM4730 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 13716.0972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (591, N'TONER CARTRIDGE, HP Q6462A, yellow, for HP Color LaserJet 4730MFP, CM4730 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 13716.0972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (592, N'TONER CARTRIDGE, HP Q6463A, magenta, for HP Color LaserJet 4730MFP, CM4730 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 13716.0972)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (593, N'TONER CARTRIDGE, HP Q6470A, black, for HP Color LaserJet 3600 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5830.5208)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (594, N'TONER CARTRIDGE, HP Q6471A, cyan, for HP Color LaserJet 3600 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5797.6048)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (595, N'TONER CARTRIDGE, HP Q6472A, yellow, for HP Color LaserJet 3600 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6166.264)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (596, N'TONER CARTRIDGE, HP Q6473A, magenta, for HP Color LaserJet 3600 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6166.264)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (597, N'TONER CARTRIDGE, HP Q6511A', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5461.8616)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (598, N'TONER CARTRIDGE, HP Q7516AC, black', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8236.6804)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (599, N'TONER CARTRIDGE, HP Q7551A, black, for HP Laserjet P3005, M3027, M3035 MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5857.9508)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (600, N'TONER CARTRIDGE, HP Q7553A, black, for HP Laserjet P3005, M3027, M3035 MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3551.6364)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (601, N'TONER CARTRIDGE, HP Q7570A, black, HP Laserjet M5025, M5035 MFP Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7823.036)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (602, N'TONER CARTRIDGE, HP Q7581A, cyan, for HP Color LaserJet 3800, CP3505 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7505.9452)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (603, N'TONER CARTRIDGE, HP Q7582A, yellow, for HP Color LaserJet 3800, CP3505 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7505.9452)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (604, N'TONER CARTRIDGE, HP Q7583A, magenta, for HP Color LaserJet 3800, CP3505 Printer Series', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7505.9452)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (605, N'TONER CARTRIDGE, Kyocera Mita TK 564C, for Printer FS-C5300DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11117.9276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (606, N'TONER CARTRIDGE, Kyocera Mita TK 564K, for Printer FS-C5300DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8544.2129)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (607, N'TONER CARTRIDGE, Kyocera Mita TK174', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5851.3676)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (608, N'TONER CARTRIDGE, Kyocera Mita TK330, for printer FS4000DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8484.6476)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (609, N'TONER CARTRIDGE, Kyocera Mita TK364 for printer FS4020DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 8777.6)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (610, N'TONER CARTRIDGE, Kyocera TK-120, original, for Kyocera Laser Printer model FS1030D', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5651.6772)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (611, N'TONER CARTRIDGE, Kypcera Mita TK 564Y, for Printer FS-C5300DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 11117.9276)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (612, N'TONER CARTRIDGE, Lexmark 1382920', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 15275.134)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (613, N'TONER CARTRIDGE, Lexmark 34217HR(12A8400)', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4971.4132)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (614, N'TONER CARTRIDGE, Lexmark E360H11P', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 9362.4076)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (615, N'TONER CARTRIDGE, Lexmark E450A11P', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7189.9516)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (616, N'TONER CARTRIDGE, Lexmark T650A11P', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 10160.072)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (617, N'TONER CARTRIDGE, Lexmark X340A11G', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 6418.62)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (618, N'TONER CARTRIDGE, Samsung CLT-508L, Cyan, original, for CLP-620ND, CLP-670N, CLP-670ND', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5924.88)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (619, N'TONER CARTRIDGE, Samsung CLT-K508L, Magenta, original, for CLP-620ND, CLP-620ND, CLP-670N', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5321.42)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (620, N'TONER CARTRIDGE, Samsung CLT-M508L, Black, original, for CLP-620ND, CLP-670N, CLP-670ND', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5924.88)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (621, N'TONER CARTRIDGE, Samsung CLT-Y508L, Yellow, original, for CLP-620ND, CLP-670N, CLP-670ND', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5924.88)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (622, N'TONER CARTRIDGE, Samsung ML-2010D3', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3291.6)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (623, N'TONER CARTRIDGE, Samsung ML-2250D5', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5047.12)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (624, N'TONER CARTRIDGE, Samsung MLTD205L', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5047.12)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (625, N'TONER CARTRIDGE, Samsung Part No. ML D2850B, for printer ML D2851ND', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 5156.84)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (626, N'TONER CARTRIDGE, Samsung Part No. MLD3050B for printer ML-D3050 / ML-D3051', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 7022.08)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (627, N'TONER CARTRIDGE, Samsung Part No. MLTD103-S', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3072.16)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (628, N'TONER CARTRIDGE, Samsung Part No. MLTD104-S, for Samsung ML 1660', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 2633.28)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (629, N'TONER CARTRIDGE, Samsung Part No. MLTD105L, for printer ML 2580n', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3017.3)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (630, N'TONER CARTRIDGE, Samsung Part No. SCX-D655A for printer model MFP 6545', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 4443.66)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (631, N'TONER CARTRIDGE, TN-3250, low yield, for Brother HL5350DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3681.106)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (632, N'TONER CARTRIDGE, TN-3320, for printer 5450DN', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 3519.8176)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (633, N'TONER CARTRIDGE,Brother TN-2260, black, original, for HL2240D, 2270DW, DCP7065, MFC7360, 7470D, 7860DW', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'NO', N'cart', 1788.436)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (634, N'TWINE, plastic, one kilo per roll', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'roll', 47.88645)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (635, N'WIRELESS-N ROUTER, 300 MBPS, 2.4GHz Frequency Band, Standard IEEE 802.11G, IEEE 802.11n, IEEE 802.3u, IEEE 802.3, Microsoft Windows 2000/XP/Vista/XP SP3/7 System Requirements, FCC Class B, Wifi Certification, 4 x 10/100 ports, 1 x 10/100 WAN port, WPA, WPA2, NAT, SPI, VPN pass thru,', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE EQUIPMENT', N'NO', N'unit', 1095.0056)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (636, N'WRAPPING PAPER, kraft, 65gsm, approx. 40m', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'NO', N'roll', 118.4976)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (637, N'* AUTOMATIC VOLTAGE REGULATOR', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'UNIT', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (638, N'* BALIKBAYAN BOXES', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 150)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (639, N'* BALLPEN, Good quality, color black', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 5)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (640, N'* BALLPEN, Good quality, color blue', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 5)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (641, N'* BARCODE LABEL SIZE 1.5" X 1"', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ROLL', 1000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (642, N'* BARCODE LABEL, SIZE 3"X3"', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ROLL', 1500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (643, N'* BATTERY, 9V, ALKALINE, Long lasting', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 160)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (644, N'* BLANK PVC CARDS, 500S/BOX', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 2250)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (645, N'* CALLIGRAPHY PEN, POINT 1.0MM OR 2.0MM, COLOR BLACK (FOR EXECUTIVE USE)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (646, N'* CLIPBOARD, LONG', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (647, N'* CONSTRUCTION PAPER, LONG, ASSORTED COLORS/REAM', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'REAM', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (648, N'* CONTINUOUS FORM, SELF CARBONIZED, SHORT', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 800)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (649, N'* CONTINUOUS FORM, SELF-CARBONIZED, LONG', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 800)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (650, N'* D-LINK UTP CAT 5, 305 METERS/BOX', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'BOX', 4000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (651, N'* DOUBLE SIDED TAPE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ROLL', 30)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (652, N'* EARLY WARNING DEVICE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (653, N'* EASEL SHEET', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PAD', 300)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (654, N'* EXTENSION WIRE, 3-OUTLET, UNIVERSAL TYPE WITH SWITCH', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'UNIT', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (655, N'* FILING TRAY, 3-LAYERS', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (656, N'* FLOURESCENT TUBES, 40WATTS', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON ELECTRICAL SUPPLIES', N'YES', N'piece', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (657, N'* HAND TOWEL, WHITE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 50)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (658, N'* HANDSOAP, LIQUID, ANTI-BACTERIAL, 1000ML', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON CLEANING SUPPLIES', N'YES', N'piece', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (659, N'* HYDRAULIC JACK, 2-TON', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (660, N'* ID JACKET WITH SLING, 50S/BOX', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (661, N'* INK CART, CANON CL-35 BLACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (662, N'* INK CART, CANON CL-36 COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 950)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (663, N'* INK CART, CANON CL-746, COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 1000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (664, N'* INK CART, CANON PG-745, BLACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 800)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (665, N'* INK CART, CANON PIXMA IX6770 CLI-750, BLACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (666, N'* INK CART, CANON PIXMA IX6770 CLI-751, BLACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (667, N'* INK CART, CANON PIXMA IX6770 CLI-751, CYAN', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (668, N'* INK CART, CANON PIXMA IX6770 CLI-751, MAGENTA', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (669, N'* INK CART, CANON PIXMA IX6770 CLI-751, YELLOW', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (670, N'* INK CART, HP-678 BLACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (671, N'* INK CART, HP-678 COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (672, N'* INK CART, HP82, CYAN', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 1600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (673, N'* INK CART, HP82, MAGENTA', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 1600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (674, N'* INK CART, HP82, YELLOW', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'CART', 1600)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (675, N'* INK GESTETNER, DX2430,', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 1200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (676, N'* MASK, DISPOSABLE, 50S/BOX', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (677, N'* MINDA PIN, WITH BOX', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 150)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (678, N'* MOBILE DRAWER, STEEL, 3-DRAWERS, BIEGE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 4000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (679, N'* NEWS PRINT, BLUE, 70GSM, LONG', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (680, N'* NEWS PRINT, PINK, 70GSM, LONG', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (681, N'* NEWS PRINT, YELLOW, 70GSM, SHORT', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'ream', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (682, N'* NOTE PAD, STRIPS, 4-D', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PAD', 40)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (683, N'* PAPER FASTENER, PLASTIC COATED, 6"', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 80)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (684, N'* PAPER FASTENER, PLASTIC COATED, REGULAR SIZE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 30)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (685, N'* PAPER, LAID, CORNFIELD/CREAM COLOR, 500s/REAM, LONG', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'REAM', 700)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (686, N'* PAPER, LAID, CORNFIELD/CREAM COLOR, 500s/REAM, SHORT', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'REAM', 650)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (687, N'* PAPER, SPECIAL CUT, W=14" X L=25", SUB 20, 500S/PACK (FOR PAYROLL PRINTING)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 1500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (688, N'* PHOTO FRAME, WOOD', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 2000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (689, N'* PLOTTER PAPER, 36"X50y X 2" CORE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'ROLL', 1000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (690, N'* PREPAID CARDS-300 (SMART, TNT, SUN, GLOBE)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'CARD', 330)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (691, N'* PRINTER, INKJET, COLORED, MULTI-FUNCTION, SCAN, PRINT COPY', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 5000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (692, N'* PRINTER, MONOCHROME, LASERJET', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 5000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (693, N'* RIBBON, LQ-2180, ORIGINAL', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'CART', 800)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (694, N'* RUBBER BAND, 1.6MM THICK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 150)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (695, N'* RUBBER STAMPS 2-LINES (LARGE, MULTIPLE LINES)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (696, N'* RUBBER STAMPS 2-LINES (NAME AND DESIGNATION ONLY)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (697, N'* SIGN HERE FLAGS, (RE-USABLE UP TO 30 TIMES), 50S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 155)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (698, N'* SIGNPEN, POINT 0.7, NEEDLE POINT/ROLLER BALL, RETRACTABLE, COLOR, BLACK (FOR EXECUTIVE USE)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (699, N'* STICKER PAPER, WHITE, GLOSSY, SIZE A4, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 60)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (700, N'* STICKER PAPER, WHITE, GLOSSY, SIZE LONG, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 70)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (701, N'* STICKER PAPER, WHITE, MATTE, SIZE A4, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 60)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (702, N'* STICKER PAPER, WHITE, MATTE, SIZE LONG, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 70)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (703, N'* TIRE WRENCH CROSS', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'unit', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (704, N'* TISSUE, FACIAL, UNSCENTED, 3-PLY, 140PULLS OR 420 SHEETS/BOX, APPROXIMATE SHEET SIZE: 200MMX200MM', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'BOX', 90)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (705, N'* TOILET BLOCK, PERFUMED/SCENTED, (FOR TOILET BOWL)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (706, N'* TOILET PAPER DISPENSER (FOR JUMBO SIZED TOILET PAPER)', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'piece', 700)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (707, N'* TOILET PAPER, JUMBO ROLL, 2-PLY', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'roll', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (708, N'* TONER, DEVELOP ENEO, TN-414', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 4900)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (709, N'* TONER, KYOCERA TK-1147', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 6180)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (710, N'* TONER, KYOCERA TK-135', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 3700)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (711, N'* UPS, 600-650VA', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'UNIT', 2000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (712, N'* VELLUM BOARD, WHITE, A4, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 45)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (713, N'* VELLUM BOARD, WHITE, LONG, 20S/PACK', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON OFFICE SUPPLIES', N'YES', N'PACK', 50)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (714, N'* YMCO RIBBON FOR SMART ID PRINTER', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'piece', 4800)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (715, N'* POLO SHIRT, WITH COLAR, HONEYCOMB COTTON, WITH PRINT, COLOR WHITE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 400)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (716, N'* POLO SHIRT, WITH COLAR, HONEYCOMB COTTON, WITH PRINT, COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 450)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (717, N'* T-SHIRT, WHITE, ROUND NECK, COTTON WITH PRINT, COLOR WHITE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 180)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (718, N'* T-SHIRT, WHITE, ROUND NECK, COTTON WITH PRINT, COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 170)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (719, N'* T-SHIRT, WHITE, V-NECK, COTTON WITH PRINT, COLOR WHITE', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 180)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (720, N'* T-SHIRT, WHITE, V-NECK, COTTON WITH PRINT, COLORED', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'OTHER SUPPLIES', N'YES', N'piece', 200)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (721, N'dddddddddd', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE DEVICES', N'YES', N'CART', 15555)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (722, N'hgfghfgh', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON JANITORIAL SUPPLIES', N'NO', N'container', 500)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (723, N'1111111111', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON CLEANING SUPPLIES', N'NO', N'bottle', 100)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (724, N'Computer Set i7 , 8gb Ram', N'NOT AVAILABLE AT PROCUREMENT SERVICE STORE', N'COMMON COMPUTER SUPPLIES/CONSUMABLES', N'YES', N'set', 25000)
GO

INSERT INTO dbo.mnda_procurement_items (id, item_specifications, general_category, sub_category, minda_inventory, unit_of_measure, price)
VALUES 
  (725, N'Computer Mouse', N'AVAILABLE AT PROCUREMENT SERVICE STORES', N'COMMON OFFICE SUPPLIES', N'YES', N'bundle', 500)
GO

SET IDENTITY_INSERT dbo.mnda_procurement_items OFF
GO

--
-- Data for table dbo.mnda_program_encoded  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_program_encoded ON
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (1, N'1', NULL, NULL, NULL, NULL, N'5', N'Project 1', N'2016')
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (2, N'2', NULL, NULL, NULL, NULL, N'5', N'Communication', N'2016')
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (3, N'3', NULL, NULL, NULL, NULL, N'5', N'EXTRA EXP', N'2016')
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (4, N'4', NULL, NULL, NULL, NULL, N'5', N'Program MDC', N'2016')
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (5, N'5', NULL, NULL, NULL, NULL, N'6', N'Knowledge Development', N'2016')
GO

INSERT INTO dbo.mnda_program_encoded (id, main_program_id, key_result_code, mindanao_2020_code, scope_code, fund_source_code, acountable_division_code, project_name, project_year)
VALUES 
  (6, N'6', NULL, NULL, NULL, NULL, N'6', N'KD Convention', N'2016')
GO

SET IDENTITY_INSERT dbo.mnda_program_encoded OFF
GO

--
-- Data for table dbo.mnda_programs  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_programs ON
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (1, N'Program PRD')
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (2, N'PRD Program and Expenses')
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (3, N'PRD Other Program')
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (4, N'PRD Program MDC')
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (5, N'Knowledge Development')
GO

INSERT INTO dbo.mnda_programs (id, name)
VALUES 
  (6, N'KD Convention')
GO

SET IDENTITY_INSERT dbo.mnda_programs OFF
GO

--
-- Data for table dbo.mnda_project_output  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_project_output ON
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (1, N'1', N'Output', N'45')
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (2, N'2', N'Communication', N'45')
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (3, N'3', N'EXTRA EXP', N'45')
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (4, N'4', N'Program MDC', N'45')
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (5, N'5', N'Development', N'39')
GO

INSERT INTO dbo.mnda_project_output (id, program_code, name, assigned_user)
VALUES 
  (6, N'6', N'KM Upgrade', N'39')
GO

SET IDENTITY_INSERT dbo.mnda_project_output OFF
GO

--
-- Data for table dbo.mnda_report_data_mao_urs  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_report_data_mao_urs ON
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'Traveling Expenses', N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'0', N'Planning and Research Division (PRD)', N'PRD Regular', 1530, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'          Local Travel', N'5020101000', 50000, 320, 720, 2850, 0, 0, 3780, 0, 0, 0, 0, 0, 0, 7670, N'42330', N'Planning and Research Division (PRD)', N'PRD Regular', 1531, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'Supplies and Materials Expenses', N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'0', N'Planning and Research Division (PRD)', N'PRD Regular', 1532, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'          Gasoline, Oil and Lubricants Expenses', N'5020309000', 0, 0, 3315, 1181, 576, 0, 0, 0, 0, 0, 0, 0, 0, 5072, N'-5071.91', N'Planning and Research Division (PRD)', N'PRD Regular', 1533, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'          Other Supplies Expenses', N'5020399000', 0, 0, 0, 0, 2396, 0, 0, 0, 0, 0, 0, 0, 0, 2396, N'-2396', N'Planning and Research Division (PRD)', N'PRD Regular', 1534, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'Other Maintenance & Operating Expenses', N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'0', N'Planning and Research Division (PRD)', N'PRD Regular', 1535, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'          Representation Expense', N'5029903000', 100000, 0, 0, 395, 0, 0, 0, 0, 0, 0, 0, 0, 0, 395, N'99605', N'Planning and Research Division (PRD)', N'PRD Regular', 1536, N'301010001')
GO

INSERT INTO dbo.mnda_report_data_mao_urs (ReportHeader, UACS, Allocation, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Balance, Divisions, FundSource, id, PAP)
VALUES 
  (N'          Other Maintenance & Operating Expenses', N'5029999099', 0, 0, 8050, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8050, N'-8050', N'Planning and Research Division (PRD)', N'PRD Regular', 1537, N'301010001')
GO

SET IDENTITY_INSERT dbo.mnda_report_data_mao_urs OFF
GO

--
-- Data for table dbo.mnda_report_data_ppmp  (LIMIT 0,500)
--

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'PRD Regular', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'0.00', N'Planning and Research Division (PRD)', N'', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'5020101000', N'Local Travel', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'0.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'AH-Elsewhere-Sorsogon', N'1', N'45,000.00', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'45,000.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'5029903000', N'Representation Expense', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'45,000.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'Hotel', N'1', N'70,395.00', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'115,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'PRD-Fixed Cost', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'115,395.00', N'Planning and Research Division (PRD)', N'', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'5020502001', N'Telephone Expenses - Mobile', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'115,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'Globe', N'1', N'15,000.00', N'', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'130,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'5021003000', N'Extraordinary and Miscellaneous Expenses', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'130,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'Expense Item 1', N'1', N'10,000.00', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'140,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'Expense Item 1', N'1', N'90,000.00', N'', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'220,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

INSERT INTO dbo.mnda_report_data_ppmp (Uacs, Description, Quantity, EstimateBudget, ModeOfProcurement, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Octs, Nov, Dec, Total, Division, Yearssss, PAP, approved)
VALUES 
  (N'', N'Expense Item 2', N'1', N'94,000.00', N'', N'', N'', N'X', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'224,395.00', N'Planning and Research Division (PRD)', N'2016', N'301010001', 0)
GO

--
-- Data for table dbo.mnda_scopes  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_scopes ON
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (1, N's1', N'Mindanaowide')
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (2, N's2', N'Interregional')
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (3, N's3', N'Region Specific')
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (4, N's4', N'Provincial')
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (5, N's5', N'Municipal')
GO

INSERT INTO dbo.mnda_scopes (Scope_Id, Scope_Code, Scope_Desc)
VALUES 
  (6, N's6', N'Baranggay')
GO

SET IDENTITY_INSERT dbo.mnda_scopes OFF
GO

--
-- Data for table dbo.mnda_type_service  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_type_service ON
GO

INSERT INTO dbo.mnda_type_service (id, uacs, name)
VALUES 
  (1, N'50100000', N'Personnel Services')
GO

INSERT INTO dbo.mnda_type_service (id, uacs, name)
VALUES 
  (2, N'50200000', N'MOOE')
GO

INSERT INTO dbo.mnda_type_service (id, uacs, name)
VALUES 
  (3, N'50600000', N'Capital Outlays')
GO

SET IDENTITY_INSERT dbo.mnda_type_service OFF
GO

--
-- Data for table dbo.mnda_user_accounts  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_user_accounts ON
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (25, 6, N'Admin', N'0000000', N'KM Admin', N'km-admin', N'admin', N'registered', 1)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (38, 6, N'Raymund Tejano', N'00000000', N'Division Head', N'km-head', N'head', N'registered', 2)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (39, 6, N'Angelo Aduana', N'03012004535645', N'Division Staff', N'anjo', N'anjo', N'registered', 0)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (40, 3, N'Admin', N'222222', N'Finance Admin', N'fd-admin', N'admin', N'registered', 1)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (41, 8, N'PDD Head', N'655478', N'Division Head', N'pdd-head', N'head', N'registered', 2)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (42, 8, N'PDD Staff', N'99878', N'Division Staff', N'pdd-member', N'member', N'registered', 0)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (43, 4, N'Maris', N'11254554', N'Procurement', N'maris', N'maris', N'registered', 1)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (44, 6, N'Ryan', N'4444', N'Team', N'ryan', N'ryan', N'registered', 0)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (45, 5, N'Member PRD', N'234234', N'Member', N'prd-member', N'member', N'registered', 0)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (46, 5, N'Chied PRD', N'34234', N'Division Chief', N'prd-head', N'head', N'registered', 2)
GO

INSERT INTO dbo.mnda_user_accounts (User_Id, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin)
VALUES 
  (48, 5, N'Admin PRD', N'000', N'Admin', N'prd-admin', N'admin', N'registered', 1)
GO

SET IDENTITY_INSERT dbo.mnda_user_accounts OFF
GO

--
-- Data for table dbo.mnda_users  (LIMIT 0,500)
--

SET IDENTITY_INSERT dbo.mnda_users ON
GO

INSERT INTO dbo.mnda_users (id, username, password)
VALUES 
  (1, N'admin', N'admin')
GO

INSERT INTO dbo.mnda_users (id, username, password)
VALUES 
  (2, N'asd', N'asd')
GO

INSERT INTO dbo.mnda_users (id, username, password)
VALUES 
  (3, N'asdasdsagdg4353', N'234234')
GO

INSERT INTO dbo.mnda_users (id, username, password)
VALUES 
  (4, N'sdfsdfsdf2342', N'234324')
GO

SET IDENTITY_INSERT dbo.mnda_users OFF
GO

